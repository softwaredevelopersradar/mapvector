﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;


//using USR_DLL;
using PC_DLL;
//using Contract;

namespace GrozaMap
{


    public partial class MapForm : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        [DllImport("gisacces.dll")]
        static extern Int32 mapOpenMtrForMap(int hmap, String mtrname, Int32 mode);
        [DllImport("gisacces.dll")]
        static extern void mapCloseMtr(int hmap, Int32 number);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();



        // sob
        public delegate void UpdateTableReconFWSEventHandler(TReconFWS[] tReconFWS);
        public event UpdateTableReconFWSEventHandler UpdateTableReconFWS;

        private bool MapIsOpenned = false;
        private bool HeightMatrixIsOpenned = false;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        // Lena

        Form1 form1;
        Form2 ObjCommPowerAvail;
        FormAirPlane ObjFormAirPlane;
        //FormAz objFormAz;
        //FormLineSightRange ObjFormLineSightRange;
        FormPeleng objFormPeleng;
        FormS objFormS;
        //FormWay objFormWay;
        public FormWay objFormWay;
        public ZonePowerAvail objZonePowerAvail;
        public FormSP objFormSP;
        public FormLF objFormLF;
        public FormLF2 objFormLF2;
        public FormZO objFormZO;
        public FormTRO objFormTRO;
        public FormOB1 objFormOB1;
        public FormOB2 objFormOB2;
        public FormSuppression objFormSuppression;
        public FormSPFB objFormSPFB;
        public FormSPFB1 objFormSPFB1;
        public FormSPFB2 objFormSPFB2;
        public FormAz1 objFormAz1;
        public FormAz objFormAz;
        public FormLineSightRange objFormLineSightRange;
        public FormSupSpuf objFormSupSpuf;
        public FormTab objFormTab;
        // seg1
        public FormSpuf objFormSpuf;
        public FormScreen objFormScreen;


        //public MapForm objMapForm;

        public static int hmapl1;


        public static bool blAirObject = false;
        
        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        
        //public static System.Windows.Forms.CheckBox chbair1;

        //private double dchislo;
        //private long ichislo;


        // Otl
        //TDataADSBReceiver[] tDadaADSBReceiver = new TDataADSBReceiver[10];
//        TDataADSBReceiver[] tDadaADSBReceiver;

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        int iCounter = 0;

        // create client
        public ClientPC clientPC;// = new ClientPC(1);
        

        public MapForm()
        {
            InitializeComponent();

            form1 = new Form1(ref axaxcMapScreen1);
            ObjCommPowerAvail = new Form2(ref axaxcMapScreen1);
            ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
            objFormAz = new FormAz(ref axaxcMapScreen1);
           // ObjFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
            objFormPeleng = new FormPeleng(ref axaxcMapScreen1);
            objFormS = new FormS(ref axaxcMapScreen1);
            objFormWay = new FormWay(ref axaxcMapScreen1);
            objZonePowerAvail = new ZonePowerAvail(ref axaxcMapScreen1);
            objFormSP = new FormSP(ref axaxcMapScreen1);
            objFormLF = new FormLF(ref axaxcMapScreen1);
            objFormLF2 = new FormLF2(ref axaxcMapScreen1);
            objFormZO = new FormZO(ref axaxcMapScreen1);
            objFormTRO = new FormTRO(ref axaxcMapScreen1);
            objFormOB1 = new FormOB1(ref axaxcMapScreen1);
            objFormOB2 = new FormOB2(ref axaxcMapScreen1);
            objFormSuppression = new FormSuppression(ref axaxcMapScreen1);
            objFormSPFB = new FormSPFB(ref axaxcMapScreen1);
            objFormSPFB1 = new FormSPFB1(ref axaxcMapScreen1);
            objFormSPFB2 = new FormSPFB2(ref axaxcMapScreen1, this);
            objFormAz1 = new FormAz1(ref axaxcMapScreen1);
            objFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
            objFormSupSpuf = new FormSupSpuf(ref axaxcMapScreen1);
            objFormTab = new FormTab(this);
            // seg1
            objFormSpuf = new FormSpuf(ref axaxcMapScreen1);
            //objFormScreen = new FormScreen(ref axaxcMapScreen1);
            objFormScreen = new FormScreen(this, ref axaxcMapScreen1);

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                string sIPAddress = iniRW.get_IPaddress();
                tbIP.Text = sIPAddress;

                int iPort = iniRW.get_Port();
                tbPort.Text = iPort.ToString();

                GlobalVarLn.AdressOwn = iniRW.get_AdressOwn();
                GlobalVarLn.AdressLinked = iniRW.get_AdressLinked();

            }
            else
            {
                MessageBox.Show("Can’t open file INI! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            // Otl
//        tDadaADSBReceiver = new TDataADSBReceiver[10];


            // MAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAP
            // otl55

            string pathMap = "";
            string pathMatrix = "";

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                //axaxcMapScreen1.MapFileName = iniRW.get_map_path();
                pathMap = iniRW.get_map_path();
                axaxcMapScreen1.MapFileName = path + iniRW.get_map_path();
                //pathMap = iniRW.get_map_path();
                 
                pathMatrix = path + iniRW.get_matrix_path();


                
            }
            else
            {
                MessageBox.Show("Can’t open file INI! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, axaxcMapScreen1.MapFileName, Application.StartupPath + "\\Setting.ini");

            mapOpenMtrForMap((int)axaxcMapScreen1.MapHandle, pathMatrix, 0);

            HeightMatrixIsOpenned = true; //14/08

            //MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Belarus 200\\Belarus.MAP", Application.StartupPath + "\\Init.ini");
            //mapOpenMtrForMap((int)axaxcMapScreen1.MapHandle, Application.StartupPath + "\\Belarus 200\\Belarus_200.mtw", 0);

            if (MapIsOpenned == true)
            {
                MapCore.uselessLayersOff(ref axaxcMapScreen1);
            }
            // MAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAP

            MouseWheel += MapForm_MouseWheel;

            // Lena
            //GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
            //chbair1 = chbair;

            // .................................................................................................
/*
            /// WCF
            //создаем сервис
            var service = new ServiceImplementation();
            //подписываемся на событие HelloReceived
            service.AirPlaneReceived += Service_AirPlaneReceived;
            //стартуем сервер
            var svh = new ServiceHost(service);
            svh.AddServiceEndpoint(typeof(Contract.IService), new NetTcpBinding(), "net.tcp://localhost:8000");
            svh.Open();
 */ 
            // .................................................................................................


            axaxMapSelDlg.cMapView = axaxcMapScreen1.C_CONTAINER;

        }

// ***************************************************************************************************************
// sob
// GPS
// Обработчик события

        void clientPC_OnConfirmCoord(object sender, byte bAddress, byte bCodeError, double dLatitudeOwn, double dLongitudeOwn, double dLatitudeLinked, double dLongitudeLinked)
        {

            GlobalVarLn.lt1sp = dLatitudeOwn;
            GlobalVarLn.ln1sp = dLongitudeOwn;
            GlobalVarLn.lt2sp = dLatitudeLinked;
            GlobalVarLn.ln2sp = dLongitudeLinked;

            // Otladka

/*
            double x1 = 2382464;
            double y1 = 5498821;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x1, ref y1);
            GlobalVarLn.lt1sp = x1 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln1sp = y1 * 180 / Math.PI;

            double x3 = 2240316;
            double y4 = 5551638;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x3, ref y4);
            GlobalVarLn.lt2sp = x3 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln2sp = y4 * 180 / Math.PI;
 */

            GlobalVarLn.flagGPS_SP = 1;
            GlobalVarLn.objFormSPG.BeginInvoke(
                (MethodInvoker) (() => GlobalVarLn.objFormSPG.AcceptGPS()));
        }
// ***************************************************************************************************************

// ****************************************************************************************************
// Обновление листа СП
// ****************************************************************************************************

        public static void UpdateDropDownList(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }

        public static void UpdateDropDownList1(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            //dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }
// ****************************************************************************************************


        //Обработка колёсика
        void MapForm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (MapIsOpenned == true)
            {

                Rectangle rp = axaxcMapScreen1.Bounds;
                Rectangle rp2 = panel2.Bounds;

                if ((e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom) && (e.X >= rp2.Left && e.X <= rp2.Right && e.Y >= rp2.Top && e.Y <= rp2.Bottom))
                {
                    if (e.Delta != 0)
                    {
                        if (e.Delta > 0)
                        {
                            MapCore.ZoomInFunc(ref axaxcMapScreen1);
                        }
                        if (e.Delta < 0)
                        {
                            MapCore.ZoomOutFunc(ref axaxcMapScreen1);
                        }

                    }
                }
            }
        }

        //Закрытие формы
        //private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    if (MapIsOpenned)
        //    {
        //        iniRW.write_map_inf(axaxcMapScreen1.MapLeft, axaxcMapScreen1.MapTop, axaxcMapScreen1.ViewScale, axaxcMapScreen1.MapFileName);
        //        MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Init.ini");
        //    }
        //}

        
        //открыть карту
        private void openMapToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == false)
            {
                openFileDialog1.FileName = ".map";
                openFileDialog1.Filter = "Map file|*.map|All files|*.*";
                openFileDialog1.InitialDirectory = Application.StartupPath;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\5 000 000\\555.map", Application.StartupPath + "\\Setting.ini");
                    MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, openFileDialog1.FileName, Application.StartupPath + "\\Setting.ini");

                    // Lena
                    GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
                    hmapl1 = (int)axaxcMapScreen1.MapHandle;


                }
                else
                {
                    MapIsOpenned = false;
                }

/*
                MapCore.openMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, ref openFileDialog1, Application.StartupPath + "\\Settings.ini");
                // Lena
                GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
                hmapl1 = (int)axaxcMapScreen1.MapHandle;
*/



                // otl55
                try
                {
                    bmas = iniRW.GetMapLayersSettings();
                    SetViewSelectLayers();
                }
                catch (Exception) { }


            }
            else MessageBox.Show("The map is already open");
        }

        private void GetViewSelectLayers()
        {
            axaxcMapScreen1.Selecting = true;
            for (int i = 0; i < 20; i++)
            {
                bmas[i] = axaxcMapScreen1.ViewSelect.get_Layers(i);
            }
        }

        //закрыть карту
        private void closeMapToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                // otl55
                //MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Init.ini");
                iniRW.write_map_inf(axaxcMapScreen1.MapLeft, axaxcMapScreen1.MapTop, axaxcMapScreen1.ViewScale);

                try
                {
                    GetViewSelectLayers();
                    iniRW.SetMapLayersSettings(bmas);
                }
                catch (Exception) { }

                axaxcMapScreen1.MapClose();
                MapIsOpenned = false;
                HeightMatrixIsOpenned = false;
            }

           
        }

        //открыть матрицу высот
        private void openTheHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (HeightMatrixIsOpenned == false)
                {
                    MapCore.openMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1, ref openFileDialog1);


             // otl55
             bool Value=false;
             axaxcMapScreen1.Selecting = true;
            //axaxcMapScreen1.ViewSelect.Layers_set(0, Value); //ваще всё, оставить
            //axaxcMapScreen1.ViewSelect.Layers_set(1, Value); //зелёная трава оставить
            axaxcMapScreen1.ViewSelect.Layers_set(2, Value); //непонятно
            //axaxcMapScreen1.ViewSelect.Layers_set(3, Value); //высотность рельефа оставить
            axaxcMapScreen1.ViewSelect.Layers_set(4, Value); //гидрография болота, непонятно
            //axaxcMapScreen1.ViewSelect.Layers_set(5, Value); //гидрография оставить
            axaxcMapScreen1.ViewSelect.Layers_set(6, Value); //маленькие палки может вода убрать
            axaxcMapScreen1.ViewSelect.Layers_set(7, Value); //хрень убрать
            //axaxcMapScreen1.ViewSelect.Layers_set(8, Value); //что-то нужное может рельефы оставить
            axaxcMapScreen1.ViewSelect.Layers_set(9, Value); //что-то ересь и точки убрать
            axaxcMapScreen1.ViewSelect.Layers_set(10, Value); //херь убрать
            //axaxcMapScreen1.ViewSelect.Layers_set(11, Value); //города оставить
            //axaxcMapScreen1.ViewSelect.Layers_set(12, Value); //железные дороги и точки оставить
            axaxcMapScreen1.ViewSelect.Layers_set(13, Value); //много всяких точек убрать
            //axaxcMapScreen1.ViewSelect.Layers_set(14, Value); //сетка оставить
            axaxcMapScreen1.ViewSelect.Layers_set(15, Value); //белые дороги непонятно
            axaxcMapScreen1.ViewSelect.Layers_set(16, Value); //хрень убрать
            axaxcMapScreen1.ViewSelect.Layers_set(17, Value); //хрень убрать
            axaxcMapScreen1.ViewSelect.Layers_set(18, Value); //непонятно что точно убрать
            axaxcMapScreen1.ViewSelect.Layers_set(19, Value); //непонятно что точно убрать
            axaxcMapScreen1.ViewSelect.Layers_set(6, Value); //маленькие палки может вода убрать
            axaxcMapScreen1.Repaint();

                }
                else
                {
                    MessageBox.Show("The height matrix is already open");
                }
            }
            else
            {
                MessageBox.Show("The map is not open");
            }
        }


        //закрыть матрицу высот
        private void closeTheHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapCore.closeMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1);
        }



        //увеличить масштаб
        private void ZoomIn_Click(object sender, EventArgs e)
        {
            MapCore.ZoomInFunc(ref axaxcMapScreen1);
        }

        //уменьшить масштаб
        private void ZoomOut_Click(object sender, EventArgs e)
        {
            MapCore.ZoomOutFunc(ref axaxcMapScreen1);
        }

        //исходный масштаб
        private void ZoomInitial_Click(object sender, EventArgs e)
        {
            MapCore.ZoomInitialFunc(ref axaxcMapScreen1);
        }

        //Форма по кнопке 1
        public CheckPointForm checkPointForm;
        //private void button1_Click(object sender, EventArgs e)
        //{
/*
            if (checkPointForm == null || checkPointForm.IsDisposed)
            {
                checkPointForm = new CheckPointForm(this, axaxcMapScreen1);
                checkPointForm.Show();

            }
 */
        //}

        //Клик правой кнопкой мыши
        private double righteX;
        private double righteY;

        //Клик левой кнопкой мыши и премещение
        private bool waspressleft = false;
        private double startlefteX;
        private double startlefteY;
        private double movelefteX;
        private double movelefteY;

// MOUSE_DOWN *************************************************************************************************
        //Обработка мыши на карте
        private void axaxcMapScreen1_OnMapMouseDown(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseDownEvent e)
        {


            // 0510
            GlobalVarLn.MousePress = true;
            

            // Lena *******************************************************************************

            // Сняли координаты -> GlobalVarLn.MapX1,...(m на местности)
            ClassMap.f_XYMap(e.x, e.y);

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута

            // 0310
            //if (GlobalVarLn.blWay_stat == true)
            if (GlobalVarLn.fl_Open_objFormWay == 1)

            {
                objFormWay.f_Way(
                               GlobalVarLn.MapX1,
                               GlobalVarLn.MapY1
                              );
            }
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS


            // 1509&
            //if (GlobalVarLn.blTRO_stat != true)
            //{


                // SPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSP

                // 1509&
                //if (GlobalVarLn.blSP_stat == true)
                // 0310
                //if(GlobalVarLn.fFSP==1)
                if (GlobalVarLn.fl_Open_objFormSP == 1)
                {

                    GlobalVarLn.fclSP = 0;

                    objFormSP.f_SP(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }

                // -----------------------------------------------------------------------------------
                // 1509&               
                // LF1

                //if (GlobalVarLn.blLF1_stat == true)
                // 0310
                //if (GlobalVarLn.fFLF1 == 1)
                if (GlobalVarLn.fl_Open_objFormLF == 1)

                {
                    objFormLF.f_LF1(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
                // -----------------------------------------------------------------------------------
                // LF2
                // 1509&
                //if (GlobalVarLn.blLF2_stat == true)
                // 0310
                //if (GlobalVarLn.fFLF2 == 1)
                if (GlobalVarLn.fl_Open_objFormLF2 == 1)
                {
                    objFormLF2.f_LF2(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
                // -----------------------------------------------------------------------------------
                // ZO
                // 1509&
                //if (GlobalVarLn.blZO_stat == true)
                // 0310
                //if (GlobalVarLn.fFZO == 1)
                if (GlobalVarLn.fl_Open_objFormZO == 1)

                {
                    objFormZO.f_ZO(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
  
                // ---------------------------------------------------------------------------------
                // OB1
                // 1509&
                //if (GlobalVarLn.blOB1_stat == true)
                // 0310
                //if (GlobalVarLn.fFOB1 == 1)
                if (GlobalVarLn.fl_Open_objFormOB1 == 1)

                {
                    objFormOB1.f_OB1(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
                // ---------------------------------------------------------------------------------
                // OB2
                // 1509&
                //if (GlobalVarLn.blOB2_stat == true)
                // 0310
                //if (GlobalVarLn.fFOB2 == 1)
                if (GlobalVarLn.fl_Open_objFormOB2 == 1)

                {
                    objFormOB2.f_OB2(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }

            //}

            // ---------------------------------------------------------------------------------
            // Az1(Object)

            if (GlobalVarLn.blOB_az1 == true)
            {

                /*objFormAz1.f_OB_az1(
                               GlobalVarLn.MapX1,
                               GlobalVarLn.MapY1
                              );
                GlobalVarLn.objFormAz1G.chbXY.Checked = false;

                */
                GlobalVarLn.XXXaz = GlobalVarLn.MapX1;
                GlobalVarLn.YYYaz = GlobalVarLn.MapY1;

            }
            // ---------------------------------------------------------------------------------
            // SPFBRR

            // 0310
            //if (GlobalVarLn.flF_f1 == 1)
            if (GlobalVarLn.fl_Open_objFormSPFB == 1)

            {
                objFormSPFB.f_SPFB_f1();
                //GlobalVarLn.objFormSPFBG.chbXY.Checked = false;
            }
            // ---------------------------------------------------------------------------------
            // SPFBPR

            // 0310
            //if (GlobalVarLn.flF_f2 == 1)
            if (GlobalVarLn.fl_Open_objFormSPFB1 == 1)

            {
                objFormSPFB1.f_SPFB_f2();
                //GlobalVarLn.objFormSPFBG.chbXY.Checked = false;
            }
            // ---------------------------------------------------------------------------------
            // SCREEN
            // SCR 1-я точка

            if ((GlobalVarLn.flScrM1 == 1) && (GlobalVarLn.flScrM2 == 0))
            {
                GlobalVarLn.flScrM2 = 1;

                GlobalVarLn.X1Scr = GlobalVarLn.MapX1;
                GlobalVarLn.Y1Scr = GlobalVarLn.MapY1;

                GlobalVarLn.location1 = new Point(Control.MousePosition.X, Control.MousePosition.Y);

            }
            // ---------------------------------------------------------------------------------


            // LFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLFLF


            // ******************************************************************************* Lena


            if (e.button == 0)
            {
                waspressleft = true;
                startlefteX = e.x;
                startlefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref startlefteX, ref startlefteY);

                //MapCore.layersMapFunc(ref axaxcMapScreen1, false);
            }
            if (e.button == 1)
            {
                righteX = e.x;
                righteY = e.y;
            }

        }
        // ************************************************************************************************* MOUSE_DOWN



       // MOUSE_MOVE *************************************************************************************************
       // MouseMove

        private void axaxcMapScreen1_OnMapMouseMove(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseMoveEvent e)
        {
            if ((waspressleft == true)&&
                (GlobalVarLn.flScrM1 == 0)
                //(GlobalVarLn.blOB_az1==false)
                )
            {
                movelefteX = e.x;
                movelefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref movelefteX, ref movelefteY);

                this.Cursor = Cursors.SizeAll;
                axaxcMapScreen1.MapLeft -= (int)(movelefteX - startlefteX);
                axaxcMapScreen1.MapTop -= (int)(movelefteY - startlefteY);

            }

            // llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll
            // Lena
            if (MapIsOpenned == true)
            {
                double moveX1 = 0;
                double moveY1 = 0;
                double moveHH = 0;

                //Пересчет
                double moveX = e.x;
                double moveY = e.y;

                lX.Text = string.Format("X, m = {0}", Convert.ToString((int)moveX));
                lY.Text = string.Format("Y, m = {0}", Convert.ToString((int)moveY));

                moveX1 = moveX;
                moveY1 = moveY;


                // Сняли координаты -> GlobalVarLn.MapX1,...(m на местности)
                //ClassMap.f_XYMap(e.x, e.y);


                // SCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCR
                // SCR MouseMove

                if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 0))
                {
                    // Сняли координаты -> GlobalVarLn.MapX1,...(m на местности)
                    ClassMap.f_XYMap(e.x, e.y);

                    GlobalVarLn.X2Scr = moveX1;
                    GlobalVarLn.Y2Scr = moveY1;

                    GlobalVarLn.axMapScreenGlobal.Repaint();
                    ClassMap.f_Screen_stat();
                }
                // SCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCR


                // .......................................................................
                // H

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(moveX1, moveY1);
                moveHH = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                //statusStrip1.Items[4].Text = string.Format("H, m = {0}", Convert.ToString((int)moveHH));
                lH.Text = string.Format("H, m = {0}", Convert.ToString((int)moveHH));
                // ......................................................................

                MapCore.mapPlaneToGeo((int)axaxcMapScreen1.MapHandle, ref moveX, ref moveY);
                MapCore.mapGeoToRealGeo(ref moveX, ref moveY);

                lLat.Text = string.Format("Latitude = {0}", moveX.ToString("0.000"));
                lLon.Text = string.Format("Longitude = {0}", moveY.ToString("0.000"));

                String strLine2="";
                String strLine3 = "";

                int iaz = 0;
                double X_Coordl3_1 = 0;
                double Y_Coordl3_1 = 0;
                double X_Coordl3_2 = 0;
                double Y_Coordl3_2 = 0;
                double DXX3 = 0;
                double DYY3 = 0;
                double azz = 0;
                long ichislo = 0;
                double dchislo = 0;


                // SPFB
                ClassMap objClassMap10 = new ClassMap();
                if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == true) &&
                    (GlobalVarLn.flF_f1 == 1)

                )
                {
                    strLine2 = Convert.ToString(
                        GlobalVarLn.objFormSPFBG.cbChooseSC.Items[
                            GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]); // Numb

                    // if2
                    if (!string.IsNullOrEmpty(strLine2))
                    {
                        var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                        for (iaz = 0; iaz < stations.Count; iaz++)
                        {

                            X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                            Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                            X_Coordl3_2 = (int) moveX1;
                            Y_Coordl3_2 = (int) moveY1;

                            strLine3 = stations[iaz].Name;

                            // if1
                            if (String.Compare(strLine2, strLine3) == 0)
                            {
                                // Разность координат для расчета азимута
                                // На карте X - вверх
                                DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                                DYY3 = X_Coordl3_2 - X_Coordl3_1;

                                // grad
                                azz = objClassMap10.f_Def_Azimuth(
                                    DYY3,
                                    DXX3
                                );

                                GlobalVarLn.Az_f1 = azz;

                                ichislo = (long) (azz * 100);
                                dchislo = ((double) ichislo) / 100;
                                //statusStrip1.Items[5].Text = string.Format("B,grad = {0}", azz.ToString("0.0"));
                                //statusStrip1.Items[5].Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz));
                                lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int) azz));


                                // Убрать с карты
                                GlobalVarLn.axMapScreenGlobal.Repaint();
                                GlobalVarLn.objFormAzG.f_Map_Line_XY1(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                    Y_Coordl3_2);

                            } // if1



                        } // FOR

                    }
                }

                // seg
                if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == false) && (GlobalVarLn.flF_f1 == 1))
                {
                    //statusStrip1.Items[5].Text = "";
                    lB.Text = "";
                }

                // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB

                // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1
                if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == true) &&
                    (GlobalVarLn.flF_f2 == 1)

                )
                {
                    strLine2 = Convert.ToString(
                        GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[
                            GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]); // Numb

                    // if2
                    if (!string.IsNullOrEmpty(strLine2))
                    {
                        var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                        for (iaz = 0; iaz < stations.Count; iaz++)
                        {

                            X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                            Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                            X_Coordl3_2 = (int) moveX1;
                            Y_Coordl3_2 = (int) moveY1;

                            strLine3 = stations[iaz].Name;

                            // if1
                            if (String.Compare(strLine2, strLine3) == 0)
                            {
                                // Разность координат для расчета азимута
                                // На карте X - вверх
                                DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                                DYY3 = X_Coordl3_2 - X_Coordl3_1;

                                // grad
                                azz = objClassMap10.f_Def_Azimuth(
                                    DYY3,
                                    DXX3
                                );

                                GlobalVarLn.Az_f2 = azz;

                                ichislo = (long) (azz * 100);
                                dchislo = ((double) ichislo) / 100;
                                //statusStrip1.Items[4].Text = string.Format("B,grad = {0}", azz.ToString("0.0000"));
                                //statusStrip1.Items[5].Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz)); // seg
                                lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int) azz)); // seg

                                // Убрать с карты
                                GlobalVarLn.axMapScreenGlobal.Repaint();
                                GlobalVarLn.objFormAzG.f_Map_Line_XY1(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                    Y_Coordl3_2);

                            } // if1



                        } // FOR

                    }
                }

                // seg
                if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == false) && (GlobalVarLn.flF_f2 == 1))
                {
                    //statusStrip1.Items[5].Text = "";
                    lB.Text = "";
                }

                // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1


            }
            // llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll


            // Перерисовка зон **********************************************************************
            // 0510


            if (
                (GlobalVarLn.MousePress == true)&&
                (GlobalVarLn.flScrM2==1)
               )

            {
                // -----------------------------------------------------------------------------------
                // ZPV
                if (
                    (GlobalVarLn.fl_LineSightRange == 1)
                    )
                {
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_ZPV,  // m на местности
                                  GlobalVarLn.YCenter_ZPV,
                                      ""
                                 );
                    ClassMap.DrawLSR();
                }
                // -----------------------------------------------------------------------------------
                // Зона обнаружения СП
                if (
                    (GlobalVarLn.fl_ZOSP == 1)
                   )
                {
                    ClassMap.DrawPolygon(GlobalVarLn.listDetectionZone, Color.HotPink);
                }
                // -----------------------------------------------------------------------------------
                // Зона подавления
                if (
                    (GlobalVarLn.flCoordSP_sup == 1)
                   )
                {
                    ClassMap.f_Map_Redraw_Zon_Suppression();
                }
                // -------------------------------------------------------------------------------------
                // Зона подавления навигации
                if (
                    (GlobalVarLn.fl_supnav == 1)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                        GlobalVarLn.XCenter_supnav, // m на местности
                        GlobalVarLn.YCenter_supnav,
                        ""
                    );
                    ClassMap.DrawPolygon(GlobalVarLn.listNavigationJammingZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------
                // Зона спуфинга
                if (
                    (GlobalVarLn.fl_spf == 1)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_spf,  // m на местности
                                  GlobalVarLn.YCenter_spf,
                                      ""
                                 );
                    if (GlobalVarLn.flS_spf == 1)
                        ClassMap.DrawPolygon(GlobalVarLn.listSpoofingZone, Color.Green);

                    if (GlobalVarLn.flJ_spf == 1)
                        ClassMap.DrawPolygon(GlobalVarLn.listSpoofingJamZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------
            } // Mouse Press==true
            // ********************************************************************** Перерисовка зон


        }
        // *********************************************************************************** MOUSE_MOVE

       // MOUSE_UP **************************************************************************************
       // MouseUp 

        private void axaxcMapScreen1_OnMapMouseUp(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseUpEvent e)
        {

            // ------------------------------------------------------------------------------------
            if (e.button == 0)
            {
                waspressleft = false;
                //MapCore.layersMapFunc(ref axaxcMapScreen1, true);

                // 0510
                GlobalVarLn.MousePress = false;



                // 0510
                if(
                    (GlobalVarLn.fl_LineSightRange == 1)||
                    (GlobalVarLn.fl_ZOSP == 1)||
                    (GlobalVarLn.flCoordSP_sup == 1)||
                    (GlobalVarLn.fl_supnav == 1)||
                    (GlobalVarLn.fl_spf == 1) 
                   )
                // Убрать с карты
                // 6_9_18
                GlobalVarLn.axMapScreenGlobal.Repaint();

                // Перерисовка зон **********************************************************************
                // 0510

                // -----------------------------------------------------------------------------------
                // ZPV
                if (
                    (GlobalVarLn.fl_LineSightRange == 1) &&
                    (GlobalVarLn.MousePress == false)
                    )
                {
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_ZPV,  // m на местности
                                  GlobalVarLn.YCenter_ZPV,
                                      ""
                                 );
                    ClassMap.DrawLSR();
                }
                // -----------------------------------------------------------------------------------
                // Зона обнаружения СП
                if (
                    (GlobalVarLn.fl_ZOSP == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    ClassMap.DrawPolygon(GlobalVarLn.listDetectionZone, Color.HotPink);
                }
                // -----------------------------------------------------------------------------------
                // Зона подавления
                if (
                    (GlobalVarLn.flCoordSP_sup == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    ClassMap.f_Map_Redraw_Zon_Suppression();
                }
                // -------------------------------------------------------------------------------------
                // Зона подавления навигации
                if (
                    (GlobalVarLn.fl_supnav == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                        GlobalVarLn.XCenter_supnav, // m на местности
                        GlobalVarLn.YCenter_supnav,
                        ""
                    );
                    ClassMap.DrawPolygon(GlobalVarLn.listNavigationJammingZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------
                // Зона спуфинга
                if (
                    (GlobalVarLn.fl_spf == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_spf,  // m на местности
                                  GlobalVarLn.YCenter_spf,
                                      ""
                                 );
                    if (GlobalVarLn.flS_spf == 1)
                        ClassMap.DrawPolygon(GlobalVarLn.listSpoofingZone, Color.Green);

                    if (GlobalVarLn.flJ_spf == 1)
                        ClassMap.DrawPolygon(GlobalVarLn.listSpoofingJamZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------

                // ********************************************************************** Перерисовка зон

            } // e.button=0
            // ------------------------------------------------------------------------------------


            // --------------------------------------------------------------------------------------------------
            // SCR  последняя точка

            if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 0))
            {
                GlobalVarLn.location2 = new Point(Control.MousePosition.X, Control.MousePosition.Y);


                GlobalVarLn.flScrM3 = 1;
                GlobalVarLn.X2Scr = GlobalVarLn.MapX1;
                GlobalVarLn.Y2Scr = GlobalVarLn.MapY1;
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                this.TopMost = true;
                this.Invalidate();
                GlobalVarLn.objFormScreenG.TopMost = false;
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Close();
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Hide();
                GlobalVarLn.objFormScreenG.Invalidate();
                this.Show();
                this.Activate();
                this.Invalidate();

                GlobalVarLn.objFormScreenG.pbScreen.Image = null;
                GlobalVarLn.objFormScreenG.pbScreen.Image = GlobalVarLn.objFormScreenG.ImageFromScreen(axaxcMapScreen1);
                GlobalVarLn.bmpScr = new Bitmap(GlobalVarLn.objFormScreenG.pbScreen.Image);

                this.TopMost = false;
                this.Invalidate();
                GlobalVarLn.objFormScreenG.TopMost = true;
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Activate();
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Show();
                GlobalVarLn.objFormScreenG.Invalidate();


                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            }
            // --------------------------------------------------------------------------------------------------



        } // MouseUp
      // ************************************************************************************** MOUSE_UP

        //Флаги перерисовки
        bool paintRadioSources = true;
        bool paintStations = true;
        bool paintArcPoints = true;
        bool paintPeleng = true;
        bool infopaint = true;


// ПЕРЕРИСОВКА КАРТЫ ***********************************************************************************************

        //Перисовка карты
        private void axaxcMapScreen1_OnMapPaint(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapPaintEvent e)
        {

            if (paintRadioSources == true)
            {
                for (int i = 0; i < radioSources.Count(); i++)
                {
                    MapCore.RadioSource temp = radioSources[i];
                    MapCore.DrawAndSaveTriangle(ref temp, ref axaxcMapScreen1);
                    radioSources[i] = temp;
                }
            }

 
            if (paintArcPoints == true)
            {
                if (ArcPoints.Count >= 2)
                    MapCore.DrawArc(ArcPoints.ToArray(), ref axaxcMapScreen1);
            }


            if (paintStations == true)
            {
                for (int i = 0; i < stations.Count(); i++)
                {
                    MapCore.Station temp = stations[i];
                    MapCore.DrawAndSaveStation(ref temp, ref axaxcMapScreen1, imageList1);
                    stations[i] = temp;
                }
            }

            if (paintPeleng == true)
            {
                for (int i = 0; i < radioSources.Count(); i++)
                {
                    if (radioSources[i].latitude != null && radioSources[i].longitude != null)
                        MapCore.DrawPeleng(radioSources[i].latitude, radioSources[i].longitude, ref axaxcMapScreen1);
                }
            }

            if (infopaint == true)
            {
                for (int i = 0; i < radioSources.Count(); i++)
                {
                    if (radioSources[i].isinfopaint == true)
                    {
                        MapCore.InfoPaint(radioSources[i], ref axaxcMapScreen1);
                    }
                }

            }

            // LENA **********************************************************************************
            // Lena (перерисовка)


            if (GlobalVarLn.fllTab == 1)
            {
                for (int iitab = 0; iitab < GlobalVarLn.list_air_tab.Count; iitab++)
                {
                    //ClassMap.f_DrawAirPlane(GlobalVarLn.list_air_tab[iitab].Lat, GlobalVarLn.list_air_tab[iitab].Long, 0);
                    ClassMap.f_DrawAirPlane_Tab(GlobalVarLn.list_air_tab[iitab].Lat, GlobalVarLn.list_air_tab[iitab].Long, 0, GlobalVarLn.list_air_tab[iitab].Num);

                }
            }
            // ------------------------------------------------------------------------------------
            // Az

            if (GlobalVarLn.flDrawAzz == 1)
            {

                GlobalVarLn.objFormAzG.f_ReDrawAzz();
            }

            // ------------------------------------------------------------------------------------
            // Координаты (Form1)

            if (GlobalVarLn.fl_DrawCoord == 1)
            {

                ClassMap.f_Map_Rect_LatLong_stat(
                                     GlobalVarLn.Lat_CRD,
                                     GlobalVarLn.Long_CRD
                                     );
            }
            // ------------------------------------------------------------------------------------
            // Пеленг

            if (GlobalVarLn.fl_Peleng_stat == 1)
            {
                ClassMap.f_ReDrawPeleng();
            }
            // -----------------------------------------------------------------------------------
            // S

            if (GlobalVarLn.fl_S1_stat == 1)
            {
                ClassMap.f_ReDrawS_stat();
            }
            // -----------------------------------------------------------------------------------
            // Маршрут

            if (GlobalVarLn.flEndWay_stat == 1)
            {
                objFormWay.f_WayReDraw();

            }
            // -----------------------------------------------------------------------------------
            // Самолеты

            if (GlobalVarLn.fl_AirPlaneVisible == 1)
            {

                // Перерисовка самолетов
                ClassMap.f_ReDrawAirPlane1();
            }
            // -------------------------------------------------------------------------------------
            // Зона энергодоступности

            if (GlobalVarLn.fl_ZonePowerAvail == 1)
            {
                // SP
                //ClassMap.f_Map_Pol_XY_stat(
                //              GlobalVarLn.XCenter_ed,  // m
                //              GlobalVarLn.YCenter_ed,
                //              1,
                //              ""
                 //            );

                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ed,  // m
                              GlobalVarLn.YCenter_ed,
                                  ""
                             );

                // Перерисовка зоны
                ClassMap.f_Map_El_XY_ed(
                                        GlobalVarLn.tpOwnCoordRect,
                                        GlobalVarLn.liRadiusZone_ed
                                        );
            }
            // -------------------------------------------------------------------------------------
            // Энергодоступность по УС

            if (GlobalVarLn.fl_CommPowerAvail == 1)
            {
                ClassMap.f_Map_Redraw_CommPowerAvail();
            }
            // -------------------------------------------------------------------------------------
            // ЗПВ

            // 0510
            if (
                (GlobalVarLn.fl_LineSightRange == 1)&&
                (GlobalVarLn.MousePress==false)
                )
            {
                // Центр ЗПВ
                //ClassMap.f_Map_Pol_XY_stat(
                //              GlobalVarLn.XCenter_ZPV,  // m на местности
                //              GlobalVarLn.YCenter_ZPV,
                //              1,
                //              ""
                //             );
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ZPV,  // m на местности
                              GlobalVarLn.YCenter_ZPV,
                                  ""
                             );


                ClassMap.DrawLSR();
            }
            // -------------------------------------------------------------------------------------
            // SP

            // otl33
            if (GlobalVarLn.flEndSP_stat == 1)
            //if ((GlobalVarLn.flEndSP_stat == 1) && (GlobalVarLn.fclSP == 0))

            {
                objFormSP.f_SPReDraw();

            }
            // -----------------------------------------------------------------------------------
            // LF1

            if (
                (GlobalVarLn.flEndLF1_stat == 1)
                )
            {
                objFormLF.f_LF1ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // LF2

            if (
                (GlobalVarLn.flEndLF2_stat == 1)
                )
            {
                objFormLF2.f_LF2ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // ZO

            if (
                (GlobalVarLn.flEndZO_stat == 1)
                )
            {
                objFormZO.f_ZOReDraw();
            }
            // -----------------------------------------------------------------------------------
            // TRO

            if (
                (GlobalVarLn.flEndTRO_stat == 1)
                )
            {
               // objFormTRO.f_TROReDraw();
                objFormSP.f_SPReDraw();
                objFormLF.f_LF1ReDraw();
                objFormLF2.f_LF2ReDraw();
                objFormZO.f_ZOReDraw();
                objFormOB1.f_OB1ReDraw();
                objFormOB2.f_OB2ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // OB1

            if (
                (GlobalVarLn.flEndOB1_stat == 1)
                )
            {
                objFormOB1.f_OB1ReDraw();
            }
            // -----------------------------------------------------------------------------------
            // OB2

            if (
                (GlobalVarLn.flEndOB2_stat == 1)
                )
            {
                objFormOB2.f_OB2ReDraw();
            }
            // -----------------------------------------------------------------------------------
            // Зона подавления

            //if (GlobalVarLn.fl_Suppression == 1)
            // 0510
            if (
                (GlobalVarLn.flCoordSP_sup == 1)&&
                (GlobalVarLn.MousePress==false)
               )

            {
                ClassMap.f_Map_Redraw_Zon_Suppression();
            }
            // -------------------------------------------------------------------------------------
            // Az1

            if (GlobalVarLn.flEndOB_az1 == 1)
            {
                objFormAz1.f_OBReDraw_Az1();

            }
            // -----------------------------------------------------------------------------------
            // Зона обнаружения СП

            // 0510
            if (
                (GlobalVarLn.fl_ZOSP == 1)&&
                (GlobalVarLn.MousePress==false)
               )

            {
                /*ClassMap.f_ZOSP(
                                GlobalVarLn.XCenter_ZOSP,
                                GlobalVarLn.YCenter_ZOSP,
                                GlobalVarLn.iR_ZOSP
                                );*/
                ClassMap.DrawPolygon(GlobalVarLn.listDetectionZone, Color.HotPink);
            }
            // -----------------------------------------------------------------------------------
            // Зона подавления навигации

            // 0510
            if (
                (GlobalVarLn.fl_supnav == 1)&&
                (GlobalVarLn.MousePress==false)
               )

            {
                // SP
                ClassMap.f_DrawSPXY(
                    GlobalVarLn.XCenter_supnav, // m на местности
                    GlobalVarLn.YCenter_supnav,
                    ""
                );

                /*ClassMap.f_Z_supnav(
                                GlobalVarLn.XCenter_supnav,
                                GlobalVarLn.YCenter_supnav,
                                GlobalVarLn.iR_supnav
                                );*/
                ClassMap.DrawPolygon(GlobalVarLn.listNavigationJammingZone, Color.Blue);
            }
            // -----------------------------------------------------------------------------------
            // Зона спуфинга
            // seg1

            // 0510
            if (
                (GlobalVarLn.fl_spf == 1)&&
                (GlobalVarLn.MousePress==false)
               )

            {
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_spf,  // m на местности
                              GlobalVarLn.YCenter_spf,
                                  ""
                             );
                /*
                ClassMap.f_Z_spf(
                                GlobalVarLn.XCenter_spf,
                                GlobalVarLn.YCenter_spf,
                                GlobalVarLn.iR1_spf,
                                GlobalVarLn.iR2_spf
                                );
                */

                if(GlobalVarLn.flS_spf==1)
                      ClassMap.DrawPolygon(GlobalVarLn.listSpoofingZone, Color.Green);

                if (GlobalVarLn.flJ_spf == 1)
                  ClassMap.DrawPolygon(GlobalVarLn.listSpoofingJamZone, Color.Blue);
                 

            }
            // -----------------------------------------------------------------------------------
            // SCR перерисовка (пока ничего)


            if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 0))
            //if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 1))

            {
                //ClassMap.f_Screen_stat();
            }

            // -----------------------------------------------------------------------------------


            // ********************************************************************************** LENA



        } // Перерисовка карты
        // *********************************************************************************************** ПЕРЕРИСОВКА КАРТЫ


        //Радиоисточники
        private List<MapCore.RadioSource> radioSources = new List<MapCore.RadioSource>();

        //Станции
        private List<MapCore.Station> stations = new List<MapCore.Station>();

        //Точки кривой
        private List<PointF> ArcPoints = new List<PointF>();

        // Снять координаты
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (checkPointForm != null && !checkPointForm.IsDisposed)
                {
                    checkPointForm.FillTextBoxes((int)axaxcMapScreen1.MapHandle, righteX, righteY);

                }
            }
        }

        // добавить ИРИ
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.RadioSource temp = new MapCore.RadioSource(righteX, righteY);
                MapCore.DrawAndSaveTriangle(ref temp, ref axaxcMapScreen1);
                radioSources.Add(temp);
            }
        }

        // удалить ИРИ
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.DeleteTriangle(righteX, righteY, ref radioSources, ref axaxcMapScreen1);
            }
        }

        // добавить точку для дуги
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                ArcPoints.Add(new PointF((float)righteX, (float)righteY));
            }
        }

        //нарисовать дугу
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                if (ArcPoints.Count >= 2)
                    MapCore.DrawArc(ArcPoints.ToArray(), ref axaxcMapScreen1);
            }
        }

        //очистить точки для дуги
        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                ArcPoints.Clear();
                axaxcMapScreen1.Repaint();
            }
        }

        //Добавить станцию
        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.Station temp = new MapCore.Station(righteX, righteY);
                MapCore.DrawAndSaveStation(ref temp, ref axaxcMapScreen1, imageList1);
                stations.Add(temp);
            }
        }

        //Удалить станцию
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            if (MapIsOpenned == true)
            {
                MapCore.DeleteStation(righteX, righteY, ref stations, ref axaxcMapScreen1);
            }
        }

        //Отрисовать пеленг
        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            double a = 0;
            double b = 0;
            double c = 0;
            double d = 0;
            double f = 0;
            double g = 0;

            var currRSIndex = MapCore.GetRadioSourceIndex(righteX, righteY, ref radioSources, ref axaxcMapScreen1);

            if (currRSIndex != -1)
            {

                double peleng = 255;
                double distance = 200;
                uint numberofdots = 1000;

                double[] arr_Pel = new double[numberofdots * 3];
                double[] arr_Pel_XYZ = new double[numberofdots * 3];

                ClassMap classMap = new ClassMap();
                classMap.f_Peleng(peleng, distance, numberofdots, radioSources[currRSIndex].geoX, radioSources[currRSIndex].geoY, ref a, ref b, ref c, ref d, ref f, ref g, ref arr_Pel, ref arr_Pel_XYZ);

                double[] latitude = new double[numberofdots];
                double[] longitude = new double[numberofdots];

                MapCore.One3NumArrtoTwo1NumArrs(arr_Pel, ref latitude, ref longitude);

                var temp = radioSources[currRSIndex];
                temp.RadioSourceSetLatLon(latitude, longitude);
                radioSources[currRSIndex] = temp;

                MapCore.DrawPeleng(radioSources[currRSIndex].latitude, radioSources[currRSIndex].longitude, ref axaxcMapScreen1);
            }
        }

        //Отобразить/скрыть текст
        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            MapCore.SetInfoPaint(righteX, righteY, ref radioSources, ref axaxcMapScreen1);
        }


        // LENA ************************************************************************************
        // Lena

        // *****************************************************************************************
        // Загрузка головной формы
        // *****************************************************************************************


        bool[] bmas;
        private void SetViewSelectLayers()
        {
            axaxcMapScreen1.Selecting = true;
            for (int i = 0; i < 20; i++)
            {
              axaxcMapScreen1.ViewSelect.Layers_set(i, bmas[i]);
            }
            axaxcMapScreen1.Repaint();
        }

        private void MapForm_Load(object sender, EventArgs e)
        {

            //GlobalVar.axMapPointGlobal = axaxMapPoint1;
            GlobalVarLn.axMapScreenGlobal = axaxcMapScreen1;

            // Привязка Point к Screen
            GlobalVarLn.axMapPointGlobalAdd = new axGisToolKit.axMapPoint();
            GlobalVarLn.axMapPointGlobalAdd.cMapView = axaxcMapScreen1.C_CONTAINER;
            GlobalVarLn.axMapPointGlobalAdd.PlaceInp = axGisToolKit.TxPPLACE.PP_PLANE;
            GlobalVarLn.axMapPointGlobalAdd.PlaceOut = axGisToolKit.TxPPLACE.PP_PLANE;

            // ??????????????
            GlobalVarLn.hmapl = (int)axaxcMapScreen1.MapHandle;
            hmapl1 = (int)axaxcMapScreen1.MapHandle;

           //GlobalVar.list_air=new List<AirPlane>();
           // .................................................................
           // Forms

            GlobalVarLn.objFormSPG = objFormSP;
            GlobalVarLn.objFormLFG = objFormLF;
            GlobalVarLn.objFormLF2G = objFormLF2;
            GlobalVarLn.objFormZOG = objFormZO;
            GlobalVarLn.objFormOB1G = objFormOB1;
            GlobalVarLn.objFormOB2G = objFormOB2;
            GlobalVarLn.objFormSuppressionG = objFormSuppression;
            GlobalVarLn.objFormSPFBG = objFormSPFB;
            GlobalVarLn.objFormSPFB1G = objFormSPFB1;
            GlobalVarLn.objFormSPFB2G = objFormSPFB2;
            GlobalVarLn.objFormAz1G = objFormAz1;
            GlobalVarLn.objFormAzG = objFormAz;
            GlobalVarLn.objFormLineSightRangeG = objFormLineSightRange;
            GlobalVarLn.objZonePowerAvailG = objZonePowerAvail;
            GlobalVarLn.objFormWayG = objFormWay;
            GlobalVarLn.objFormSupSpufG = objFormSupSpuf;
            // seg1
            GlobalVarLn.objFormSpufG = objFormSpuf;
            GlobalVarLn.objFormScreenG = objFormScreen;
            GlobalVarLn.objFormTROG = objFormTRO;
            GlobalVarLn.objFormTabG = objFormTab;

           // .................................................................
            if (HeightMatrixIsOpenned != true) // 14/08
            {
                openFileDialog1.FileName = ".mtw";
                openFileDialog1.Filter = "Matrix file|*.mtw|All files|*.*";
                openFileDialog1.InitialDirectory = Application.StartupPath;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    mapOpenMtrForMap((int)axaxcMapScreen1.MapHandle, openFileDialog1.FileName, 0);
                    HeightMatrixIsOpenned = true;
                    axaxcMapScreen1.Repaint();
                }
                else
                {
                    HeightMatrixIsOpenned = false;
                }
            }

            GlobalVarLn.LoadListJS();
            //double Value = 0;

            try
            {
                bmas = iniRW.GetMapLayersSettings();
                SetViewSelectLayers();
            }
            catch (Exception) { }
            

            
// *********************************************************************************************
// Значки
            String nn;
            String nn1;
            String s;
            long iz;
// ..............................................................................................
 // OB1
            GlobalVarLn.objFormOB1G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\OwnObject\\";
                DirectoryInfo di = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi in di.GetFiles("*.png"))
                {

                    nn = fi.Name;
                    nn1 = fi.DirectoryName + "\\" + fi.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.bmp"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.jpeg"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }


                if (iz == 0) MessageBox.Show("No OwnObject signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
// ..............................................................................................
 // OB2
            GlobalVarLn.objFormOB2G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\EnemyObject\\";
                DirectoryInfo di1 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi1 in di1.GetFiles("*.png"))
                {

                    nn = fi1.Name;
                    nn1 = fi1.DirectoryName + "\\" + fi1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.bmp"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.jpeg"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                if (iz == 0) MessageBox.Show("No EnemyObject signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
//...............................................................................................
// SP
            GlobalVarLn.objFormSPG.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\Jammer\\";
                DirectoryInfo di2 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi2 in di2.GetFiles("*.png"))
                {

                    nn = fi2.Name;
                    nn1 = fi2.DirectoryName + "\\" + fi2.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.bmp"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.jpeg"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                if (iz == 0) MessageBox.Show("No Jammer signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
//...............................................................................................
// SPV
            GlobalVarLn.objFormSPG.imageList1V.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\JammerPlanned\\";
                DirectoryInfo di3 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi3 in di3.GetFiles("*.png"))
                {

                    nn = fi3.Name;
                    nn1 = fi3.DirectoryName + "\\" + fi3.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi3_1 in di3.GetFiles("*.bmp"))
                {

                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi3_1 in di3.GetFiles("*.jpeg"))
                {

                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                if (iz == 0) MessageBox.Show("No Jammer(planned) signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
//...............................................................................................


            // *********************************************************************************************

            // .....................................................................................
            // Очистка dataGridView1+ Установка 100 строк
            // 05_10_2018

            // Очистка dataGridView1
            while (objFormOB2.dataGridView1.Rows.Count != 0)
                objFormOB2.dataGridView1.Rows.Remove(objFormOB2.dataGridView1.Rows[objFormOB2.dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                objFormOB2.dataGridView1.Rows.Add("", "", "", "", "");
            }
            // -------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк
            // 09_10_2018

            // Очистка dataGridView1
            while (objFormOB1.dataGridView1.Rows.Count != 0)
                objFormOB1.dataGridView1.Rows.Remove(objFormOB1.dataGridView1.Rows[objFormOB1.dataGridView1.Rows.Count - 1]);

            for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
            {
                objFormOB1.dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------


        } // Загрузка головной формы
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button2: открыть окно отображения координат
        // *****************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {

            //ToolTip.("Координаты");

            if (form1 == null || form1.IsDisposed)
            {
                form1 = new Form1(ref axaxcMapScreen1);
                form1.Show();
            }
            else
            {
                form1.Show();
            }

            //// Окно отображения координат
            //Form1 form1 = new Form1(ref axaxcMapScreen1);
            //form1.Show();

        } // Button2

        // *****************************************************************************************
        // Обработчик кнопки Button3: открыть окно отображения пеленга
        // *****************************************************************************************
      

        // *****************************************************************************************
        //// Обработчик кнопки Button4: расстояние между двумя пунктами
        // Screen
        // *****************************************************************************************
        private void button4_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormScreen = 1;
            ClassMap.F_Close_Form(18);
            // -------------------------------------------------------------------------------------

/*
            if (objFormS == null || objFormS.IsDisposed)
            {
                objFormS = new FormS(ref axaxcMapScreen1);
                objFormS.Show();
            }
            else
            {
                objFormS.Show();
            }
*/

            if (objFormScreen == null || objFormScreen.IsDisposed)
            {
                GlobalVarLn.flScr = 0;
                GlobalVarLn.flScrM1 = 0;
                GlobalVarLn.flScrM2 = 0;
                GlobalVarLn.flScrM3 = 0;

                //objFormScreen = new FormScreen(ref axaxcMapScreen1);
                objFormScreen = new FormScreen(this, ref axaxcMapScreen1);

                objFormScreen.Show();
            }
            else
            {
                objFormScreen.Show();
            }

        } // Button4

        // ************************************************************************************ LENA
        // Обработчик кнопки Button5: Azimuth
        // *****************************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormTab = 1;
            ClassMap.F_Close_Form(17);
            // -------------------------------------------------------------------------------------

            if (objFormTab == null || objFormTab.IsDisposed)
            {
                // .....................................................................................
                // Очистка dataGridView

                objFormTab.dgvTab.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeTab; i++)
                {
                    objFormTab.dgvTab.Rows.Add("", "", "", "", "", "");
                }
                // .....................................................................................
                GlobalVarLn.fllTab = 0;
                GlobalVarLn.list_air_tab.Clear();

                objFormTab = new FormTab(this);
                objFormTab.Show();
            }
            else
            {
                objFormTab.Show();
            }


        } // Button5
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button6: Маршрут
        // *****************************************************************************************

        private void button6_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormWay = 1;
            ClassMap.F_Close_Form(8);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormWayG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFWay_stat = 1;
            //ClassMap.f_RemoveFrm(8);

            // -------------------------------------------------------------------------------------

            if (objFormWay == null || objFormWay.IsDisposed)
            {
                objFormWay = new FormWay(ref axaxcMapScreen1);
                objFormWay.Show();

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута
            // 1-я загрузка формы

            // .....................................................................................
            // Очистка dataGridView

                objFormWay.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
                {
                    objFormWay.dataGridView1.Rows.Add("", "", "");
                }
           // .....................................................................................
                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }

                // way
                //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
                GlobalVarLn.list_way.Clear();

                GlobalVarLn.iW_stat = 0;
                GlobalVarLn.X_StartW_stat = 0;
                GlobalVarLn.Y_StartW_stat = 0;
                GlobalVarLn.LW_stat = 0;
            // .....................................................................................

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
           // -------------------------------------------------------------------------------------
            else
            {
                objFormWay.Show();

                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                // Для маршрута
                // НЕ 1-я загрузка формы

                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }
                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
            // -------------------------------------------------------------------------------------


        } // Button6
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button7: Расчет энергодоступности по зонам 
        // *****************************************************************************************

        private void button7_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSupSpuf = 1;
            ClassMap.F_Close_Form(12);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSupSpufG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSupSpuf = 1;
            //ClassMap.f_RemoveFrm(12);

            // Зона подавления навигации
            if (objFormSupSpuf == null || objFormSupSpuf.IsDisposed)
            {
                // ----------------------------------------------------------------------
                objFormSupSpuf = new FormSupSpuf(ref axaxcMapScreen1);
                objFormSupSpuf.Show();

                GlobalVarLn.objFormSupSpufG.cbCenterLSR.SelectedIndex = 0;
                GlobalVarLn.objFormSupSpufG.comboBox2.SelectedIndex = 1;

                GlobalVarLn.objFormSupSpufG.tbOwnHeight.Text = "";
                GlobalVarLn.objFormSupSpufG.tbDistSightRange.Text = "";

                GlobalVarLn.NumbSP_supnav = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSupSpufG.gbRect.Visible = true;
                GlobalVarLn.objFormSupSpufG.gbRect.Location = new Point(7, 30);
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_supnav = 0;
                GlobalVarLn.flCoord_supnav = 0; // =1-> Выбрали центр ЗПВ
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                    GlobalVarLn.listNavigationJammingZone.Clear();
                // ----------------------------------------------------------------------


            }
            else
            {
                objFormSupSpuf.Show();
            }


        } // Button7
        // *****************************************************************************************

        // *****************************************************************************************
        //// Обработчик кнопки Button8: Расчет энергодоступности по зонам для УС
        // Spoofing Zone
        // *****************************************************************************************

        private void button8_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSpuf = 1;
            ClassMap.F_Close_Form(13);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSpufG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSpuf = 1;
            //ClassMap.f_RemoveFrm(13);


            if (objFormSpuf == null || objFormSpuf.IsDisposed)
            {

                // ----------------------------------------------------------------------
                objFormSpuf = new FormSpuf(ref axaxcMapScreen1);
                objFormSpuf.Show();
                GlobalVarLn.objFormSpufG.cbCenterLSR.SelectedIndex = 0;
                GlobalVarLn.objFormSpufG.comboBox2.SelectedIndex = 1;

                GlobalVarLn.objFormSpufG.tbOwnHeight.Text = "";
                GlobalVarLn.objFormSpufG.tbR1.Text = "";
                GlobalVarLn.objFormSpufG.tbR2.Text = "";

                GlobalVarLn.NumbSP_spf = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSpufG.gbRect.Visible = true;
                GlobalVarLn.objFormSpufG.gbRect.Location = new Point(7, 30);
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_spf = 0;
                GlobalVarLn.flCoord_spf = 0; // =1-> Выбрали центр ЗПВ
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listSpoofingZone.Count != 0)
                    GlobalVarLn.listSpoofingZone.Clear();
                if (GlobalVarLn.listSpoofingJamZone.Count != 0)
                    GlobalVarLn.listSpoofingJamZone.Clear();
                // ----------------------------------------------------------------------
                GlobalVarLn.flS_spf = 0;
                GlobalVarLn.flJ_spf = 0;
                GlobalVarLn.objFormSpufG.chbS.Checked = false;
                GlobalVarLn.objFormSpufG.chbJ.Checked = false;
                // ----------------------------------------------------------------------

            }
            else
            {
                objFormSpuf.Show();
            }



        } // Button8
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button9: Самолеты
        // *****************************************************************************************

        //private void button9_Click(object sender, EventArgs e)
        //{

/*
            if (ObjFormAirPlane == null || ObjFormAirPlane.IsDisposed)
            {
                ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
                ObjFormAirPlane.Show();
            }
            else
            {
                ObjFormAirPlane.Show();
            }
*/

        //} // Button9
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button10: ЗПВ
        // *****************************************************************************************

        private void button10_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLineSightRange = 1;
            ClassMap.F_Close_Form(10);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLineSightRange = 1;
            //ClassMap.f_RemoveFrm(10);

            if (objFormLineSightRange == null || objFormLineSightRange.IsDisposed)
            {
                GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;

                // ----------------------------------------------------------------------
                objFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
                objFormLineSightRange.Show();
                // ---------------------------------------------------------------------
                GlobalVarLn.objFormLineSightRangeG.cbCenterLSR.SelectedIndex = 0;
                GlobalVarLn.NumbSP_lsr = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormLineSightRangeG.gbRect.Visible = true;
                GlobalVarLn.objFormLineSightRangeG.gbRect.Location = new Point(7, 30);
                GlobalVarLn.objFormLineSightRangeG.gbRect42.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.gbRad.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.gbDegMin.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.gbDegMinSec.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.objFormLineSightRangeG.cbHeightOwnObject.SelectedIndex = 0;
                GlobalVarLn.objFormLineSightRangeG.cbHeightOpponObject.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormLineSightRangeG.chbXY.Checked = false;
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_LineSightRange = 0;
                GlobalVarLn.flCoordZPV = 0; // =1-> Выбрали центр ЗПВ
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listPointDSR.Count != 0)
                    GlobalVarLn.listPointDSR.Clear();
                // ----------------------------------------------------------------------
            }
            else
            {
                objFormLineSightRange.Show();
            }

            //FormLineSightRange ObjFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
            //ObjFormLineSightRange.Show();

        } // Button10

        // *****************************************************************************************
        // Обработчик кнопки Button11:SP
        // *****************************************************************************************

        private void button11_Click(object sender, EventArgs e)
        {

            //GlobalVarLn.objFormSPG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSP = 1;
            //ClassMap.f_RemoveFrm(1);

            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSP = 1;
            ClassMap.F_Close_Form(1);

            // 0510_1
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            // -------------------------------------------------------------------------------------


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Ручная отладка удаления самолетов

            // -------------------------------------------------------------------------------------

            if (objFormSP == null || objFormSP.IsDisposed)
            {
                objFormSP = new FormSP(ref axaxcMapScreen1);
                objFormSP.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView
                    // .....................................................................................
                    GlobalVarLn.blSP_stat = true;
                    GlobalVarLn.flEndSP_stat = 1;
                    GlobalVarLn.XCenter_SP = 0;
                    GlobalVarLn.YCenter_SP = 0;
                    GlobalVarLn.HCenter_SP = 0;
                    GlobalVarLn.flCoord_SP2 = 0;

                    // seg2
                    GlobalVarLn.ClearListJS();

                    GlobalVarLn.iZSP = 0;
                    GlobalVarLn.objFormSPG.pbSP.Image = imageList1.Images[0];

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSP.Show();

            }
            // -------------------------------------------------------------------------------------

            // SPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSP

        } // Button11 : SP
        // *****************************************************************************************

        // *****************************************************************************************
        // LF
        // *****************************************************************************************

        private void button13_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF = 1;
            ClassMap.F_Close_Form(4);

            // 0510_1
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormLFG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLF1 = 1;
            //ClassMap.f_RemoveFrm(4);

            if (objFormLF == null || objFormLF.IsDisposed)
            {

                objFormLF = new FormLF(ref axaxcMapScreen1);
                objFormLF.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView

                    objFormLF.dataGridView1.ClearSelection();
                    for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
                    {
                        objFormLF.dataGridView1.Rows.Add("", "", "");
                    }
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blLF1_stat = true;
                    GlobalVarLn.flEndLF1_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iLF1_stat = 0;
                    GlobalVarLn.X_LF1 = 0;
                    GlobalVarLn.Y_LF1 = 0;
                    GlobalVarLn.H_LF1 = 0;
                    GlobalVarLn.list_LF1.Clear();
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }

            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF.Show();

            }

        } // LF
        // *****************************************************************************************

        // *****************************************************************************************
        // LF2
        // *****************************************************************************************
        private void button14_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF2 = 1;
            ClassMap.F_Close_Form(5);

            // 0510_1
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormLF2G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLF2 = 1;
            //ClassMap.f_RemoveFrm(5);

            if (objFormLF2 == null || objFormLF2.IsDisposed)
            {

                objFormLF2 = new FormLF2(ref axaxcMapScreen1);
                objFormLF2.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView

                    objFormLF2.dataGridView1.ClearSelection();
                    for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
                    {
                        objFormLF2.dataGridView1.Rows.Add("", "", "");
                    }
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blLF2_stat = true;
                    GlobalVarLn.flEndLF2_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iLF2_stat = 0;
                    GlobalVarLn.X_LF2 = 0;
                    GlobalVarLn.Y_LF2 = 0;
                    GlobalVarLn.H_LF2 = 0;
                    GlobalVarLn.list_LF2.Clear();
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF2.Show();
            }

        } // LF2
        // *****************************************************************************************

        // *****************************************************************************************
        // ZO
        // *****************************************************************************************
        private void button12_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormZO = 1;
            ClassMap.F_Close_Form(6);

            // 0510_1
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormZOG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFZO = 1;
            //ClassMap.f_RemoveFrm(6);

            if (objFormZO == null || objFormZO.IsDisposed)
            {

                objFormZO = new FormZO(ref axaxcMapScreen1);
                objFormZO.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView

                    objFormZO.dataGridView1.ClearSelection();
                    for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
                    {
                        objFormZO.dataGridView1.Rows.Add("", "", "");
                    }
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blZO_stat = true;
                    GlobalVarLn.flEndZO_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iZO_stat = 0;
                    GlobalVarLn.X_ZO = 0;
                    GlobalVarLn.Y_ZO = 0;
                    GlobalVarLn.H_ZO = 0;
                    GlobalVarLn.list_ZO.Clear();
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }

            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormZO.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // TRO
        // *****************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormTRO = 1;
            ClassMap.F_Close_Form(7);

            // 0510_1
            GlobalVarLn.blTRO_stat = true;
            GlobalVarLn.flEndTRO_stat = 1;
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormTROG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFTRO = 1;
            //ClassMap.f_RemoveFrm(7);

            if (objFormTRO == null || objFormTRO.IsDisposed)
            {

                objFormTRO = new FormTRO(ref axaxcMapScreen1);
                objFormTRO.Show();
                // .....................................................................................
                // Очистка TRO

                GlobalVarLn.blTRO_stat = true;
                GlobalVarLn.flEndTRO_stat = 1;
                // .....................................................................................
                // Очистка SP

                GlobalVarLn.blSP_stat = true;
                GlobalVarLn.flEndSP_stat = 1;
                GlobalVarLn.XCenter_SP = 0;
                GlobalVarLn.YCenter_SP = 0;
                GlobalVarLn.HCenter_SP = 0;
                GlobalVarLn.ClearListJS();
                // .....................................................................................
                // Очистка LF1

                objFormLF.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
                {
                    objFormLF.dataGridView1.Rows.Add("", "", "");
                }
                GlobalVarLn.blLF1_stat = true;
                GlobalVarLn.flEndLF1_stat = 1;
                GlobalVarLn.iLF1_stat = 0;
                GlobalVarLn.X_LF1 = 0;
                GlobalVarLn.Y_LF1 = 0;
                GlobalVarLn.H_LF1 = 0;
                GlobalVarLn.list_LF1.Clear();
                // .....................................................................................
                // Очистка LF2

                objFormLF2.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
                {
                    objFormLF2.dataGridView1.Rows.Add("", "", "");
                }
                GlobalVarLn.blLF2_stat = true;
                GlobalVarLn.flEndLF2_stat = 1;
                GlobalVarLn.iLF2_stat = 0;
                GlobalVarLn.X_LF2 = 0;
                GlobalVarLn.Y_LF2 = 0;
                GlobalVarLn.H_LF2 = 0;
                GlobalVarLn.list_LF2.Clear();
                // .....................................................................................
                // Очистка ZO

                objFormZO.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
                {
                    objFormZO.dataGridView1.Rows.Add("", "", "");
                }
                GlobalVarLn.blZO_stat = true;
                GlobalVarLn.flEndZO_stat = 1;
                GlobalVarLn.iZO_stat = 0;
                GlobalVarLn.X_ZO = 0;
                GlobalVarLn.Y_ZO = 0;
                GlobalVarLn.H_ZO = 0;
                GlobalVarLn.list_ZO.Clear();
                // .....................................................................................
                // OB1

                GlobalVarLn.blOB1_stat = true;
                GlobalVarLn.flEndOB1_stat = 1;
                GlobalVarLn.iOB1_stat = 0;
                GlobalVarLn.X_OB1 = 0;
                GlobalVarLn.Y_OB1 = 0;
                GlobalVarLn.H_OB1 = 0;
                GlobalVarLn.list_OB1.Clear();
                GlobalVarLn.list1_OB1.Clear();
                GlobalVarLn.objFormOB1G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
                {
                    GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "", "");
                }
                // -------------------------------------------------------------------
                // OB2

                GlobalVarLn.blOB2_stat = true;
                GlobalVarLn.flEndOB2_stat = 1;
                GlobalVarLn.X_OB2 = 0;
                GlobalVarLn.Y_OB2 = 0;
                GlobalVarLn.H_OB2 = 0;
                GlobalVarLn.list_OB2.Clear();
                GlobalVarLn.list1_OB2.Clear();
                GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
                {
                    GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "", "");
                }
                // -------------------------------------------------------------------


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormTRO.Show();
            }

        } // TRO
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button9: OB1
        // *****************************************************************************************

        private void button9_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB1 = 1;
            ClassMap.F_Close_Form(2);

            // 0510_1
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormOB1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB1 = 1;
            //ClassMap.f_RemoveFrm(2);

            /*
                        if (ObjFormAirPlane == null || ObjFormAirPlane.IsDisposed)
                        {
                            ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
                            ObjFormAirPlane.Show();
                        }
                        else
                        {
                            ObjFormAirPlane.Show();
                        }
            */

            if (objFormOB1 == null || objFormOB1.IsDisposed)
            {

                objFormOB1 = new FormOB1(ref axaxcMapScreen1);
                objFormOB1.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // -------------------------------------------------------------------
                    // Очистка dataGridView1+ Установка 100 строк
                    // 09_10_2018

                    // Очистка dataGridView1
                    while (objFormOB1.dataGridView1.Rows.Count != 0)
                        objFormOB1.dataGridView1.Rows.Remove(objFormOB1.dataGridView1.Rows[objFormOB1.dataGridView1.Rows.Count - 1]);

                    for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
                    {
                        objFormOB1.dataGridView1.Rows.Add("", "", "", "");
                    }
                    // -------------------------------------------------------------------
                    // Флаги

                    GlobalVarLn.blOB1_stat = true;
                    GlobalVarLn.flEndOB1_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iOB1_stat = 0;
                    GlobalVarLn.X_OB1 = 0;
                    GlobalVarLn.Y_OB1 = 0;
                    GlobalVarLn.H_OB1 = 0;
                    GlobalVarLn.list_OB1.Clear();
                    GlobalVarLn.list1_OB1.Clear();
                    GlobalVarLn.iZOB1 = 0;
                    GlobalVarLn.objFormOB1G.pbOB1.Image = imageList1.Images[0];
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB1.Show();
            }

        } // Button9:OB1
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button15: OB2
        // *****************************************************************************************
        private void button15_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB2 = 1;
            ClassMap.F_Close_Form(3);

            // 0510_1
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormOB2G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB2 = 1;
            //ClassMap.f_RemoveFrm(3);

            if (objFormOB2 == null || objFormOB2.IsDisposed)
            {

                objFormOB2 = new FormOB2(ref axaxcMapScreen1);
                objFormOB2.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView

                    // .....................................................................................
                    // Очистка dataGridView1+ Установка 100 строк
                    // 05_10_2018

                    // Очистка dataGridView1
                    while (objFormOB2.dataGridView1.Rows.Count != 0)
                        objFormOB2.dataGridView1.Rows.Remove(objFormOB2.dataGridView1.Rows[objFormOB2.dataGridView1.Rows.Count - 1]);

                    for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
                    {
                        objFormOB2.dataGridView1.Rows.Add("", "", "", "", "");
                    }
                    // -------------------------------------------------------------------

                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blOB2_stat = true;
                    GlobalVarLn.flEndOB2_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.X_OB2 = 0;
                    GlobalVarLn.Y_OB2 = 0;
                    GlobalVarLn.H_OB2 = 0;
                    GlobalVarLn.list_OB2.Clear();
                    GlobalVarLn.list1_OB2.Clear();
                    GlobalVarLn.iZOB2 = 0;
                    GlobalVarLn.objFormOB2G.pbOB2.Image = imageList1.Images[0];
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB2.Show();
            }
        } // OB2
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки : Зона подавления
        // *****************************************************************************************
        private void bZoneSuppressAvia_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSuppression = 1;
            ClassMap.F_Close_Form(11);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSuppressionG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSuppr = 1;
            //ClassMap.f_RemoveFrm(11);


            if (objFormSuppression == null || objFormSuppression.IsDisposed)
            {
                objFormSuppression = new FormSuppression(ref axaxcMapScreen1);
                objFormSuppression.Show();

                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.cbOwnObject.SelectedIndex = 0;
                GlobalVarLn.NumbSP_sup = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.gbOwnRect.Visible = true;
                GlobalVarLn.objFormSuppressionG.gbOwnRect.Location = new Point(8, 26);
                GlobalVarLn.objFormSuppressionG.gbOwnRect42.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbOwnRad.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbOwnDegMin.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbOwnDegMinSec.Visible = false;
                GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.gbPt1Rect.Visible = true;
                GlobalVarLn.objFormSuppressionG.gbPt1Rect.Location = new Point(6, 11);
                GlobalVarLn.objFormSuppressionG.gbPt1Rect42.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbPt1Rad.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbPt1DegMin.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbPt1DegMinSec.Visible = false;
                GlobalVarLn.objFormSuppressionG.cbCommChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Средство РП

                GlobalVarLn.objFormSuppressionG.cbHeightOwnObject.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Object РП

                GlobalVarLn.objFormSuppressionG.cbPt1HeightOwnObject.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Поляризация сигнала

                GlobalVarLn.objFormSuppressionG.cbPolarOpponent.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.chbXY.Checked = false;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.chbXY1.Checked = false;
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_Suppression = 0; // Отрисовка зоны
                GlobalVarLn.flCoordSP_sup = 0; // =1-> Выбрали СП
                GlobalVarLn.flCoordOP = 0;
                GlobalVarLn.flA_sup = 0;
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listControlJammingZone.Count != 0)
                    GlobalVarLn.listControlJammingZone.Clear();
                // ----------------------------------------------------------------------

            }
            else
            {
                objFormSuppression.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки :Диапазоны частот и секторов радиоразведки
        // *****************************************************************************************
        private void button16_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB = 1;
            ClassMap.F_Close_Form(14);
            // 0510_1
            GlobalVarLn.flF_f1 = 1;

            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSPFBG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSPFB = 1;
            //ClassMap.f_RemoveFrm(14);


            if (objFormSPFB == null || objFormSPFB.IsDisposed)
            {

                objFormSPFB = new FormSPFB(ref axaxcMapScreen1);
                objFormSPFB.Show();

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.NumbSP_f1 = Convert.ToString(GlobalVarLn.objFormSPFBG.cbChooseSC.Items[GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]);
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormSPFBG.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f1; i++)
                {
                    GlobalVarLn.objFormSPFBG.dataGridView1.Rows.Add("", "", "", "");
                }
                // .......................................................................
                GlobalVarLn.flF_f1 = 1;
                GlobalVarLn.Az1_f1 = 0;
                GlobalVarLn.Az2_f1 = 0;
                GlobalVarLn.Az_f1 = 0;
                GlobalVarLn.flAz1_f1 = 0;
                GlobalVarLn.flAz2_f1 = 0;
                // ......................................................................
                GlobalVarLn.list1_f1.Clear();
                // ----------------------------------------------------------------------

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки :Диапазоны частот и секторов РП
        // *****************************************************************************************
        private void button17_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB1 = 1;
            ClassMap.F_Close_Form(15);
            // 0510_1
            GlobalVarLn.flF_f2 = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSPFB1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSPFB1 = 1;
            //ClassMap.f_RemoveFrm(15);

            if (objFormSPFB1 == null || objFormSPFB1.IsDisposed)
            {

                objFormSPFB1 = new FormSPFB1(ref axaxcMapScreen1);
                objFormSPFB1.Show();

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                // 1-я загрузка формы

                GlobalVarLn.iSP_f2 = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.NumbSP_f2 = Convert.ToString(GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]);
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormSPFB1G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f2; i++)
                {
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Rows.Add("", "", "", "");
                }
                // .......................................................................
                GlobalVarLn.flF_f2 = 1;
                GlobalVarLn.Az1_f2 = 0;
                GlobalVarLn.Az2_f2 = 0;
                GlobalVarLn.Az_f2 = 0;
                GlobalVarLn.flAz1_f2 = 0;
                GlobalVarLn.flAz2_f2 = 0;
                // ......................................................................
                GlobalVarLn.list1_f2.Clear();
                // ----------------------------------------------------------------------

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB1.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки : запрещенные частоты
        // *****************************************************************************************
        private void button18_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB2 = 1;
            ClassMap.F_Close_Form(16);

            // 0510_1
            GlobalVarLn.flF_f3 = 1;
            GlobalVarLn.flF1_f3 = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSPFB2G.WindowState = FormWindowState.Normal;

            if (objFormSPFB2 == null || objFormSPFB2.IsDisposed)
            {

                objFormSPFB2 = new FormSPFB2(ref axaxcMapScreen1, this);
                objFormSPFB2.Show();

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                // 1-я загрузка формы

                GlobalVarLn.iSP_f3 = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPFB2G.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.NumbSP_f3 = Convert.ToString(GlobalVarLn.objFormSPFB2G.cbChooseSC.Items[GlobalVarLn.objFormSPFB2G.cbChooseSC.SelectedIndex]);
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormSPFB2G.dgvFreqForbid.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
                {
                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows.Add("", "");
                }
                GlobalVarLn.objFormSPFB2G.dgvFreqSpec.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
                {
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows.Add("", "");
                }

                // .......................................................................
                GlobalVarLn.flF_f3 = 1;
                GlobalVarLn.flF1_f3 = 1;
                // ......................................................................
                GlobalVarLn.list1_f3.Clear();
                GlobalVarLn.list2_f3.Clear();
                // ----------------------------------------------------------------------

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB2.Show();
            }
        }
        // *****************************************************************************************


        // *****************************************************************************************
        // Обработчик кнопки : Azimut1
        // *****************************************************************************************
        private void button19_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormAz1 = 1;
            ClassMap.F_Close_Form(9);

            // 0510_1
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormAz1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFAz1 = 1;
            //ClassMap.f_RemoveFrm(9);

            if (objFormAz1 == null || objFormAz1.IsDisposed)
            {

                objFormAz1 = new FormAz1(ref axaxcMapScreen1);
                objFormAz1.Show();

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                // 1-я загрузка формы

                // ----------------------------------------------------------------------
                //GlobalVarLn.objFormAz1G.cbChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormAz1G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB_az1; i++)
                {
                    GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("","","","");
                }
                // .......................................................................
                GlobalVarLn.blOB_az1 = true;
                GlobalVarLn.flEndOB_az1 = 1;
                GlobalVarLn.flCoordOB_az1 = 0;
                GlobalVarLn.XCenter_az1 = 0;
                GlobalVarLn.YCenter_az1 = 0;
                GlobalVarLn.HCenter_az1 = 0;
                // ----------------------------------------------------------------------
                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl

            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormAz1.Show();
            }

        }
        // *****************************************************************************************





        // *****************************************************************************************
        // Слои
        // *****************************************************************************************

        private void составКартыToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (axaxMapSelDlg.Execute(axaxcMapScreen1.ViewSelect, false) == true)
                axaxcMapScreen1.Invalidate();
        } // Слои
        // *****************************************************************************************


        private void cmStripMap_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

// ********************************************************************************************
// CONNECTION
// sob
       
        private void bConnectPC_Click(object sender, EventArgs e)
        {
            bool blConnect = false;

            if (bConnectPC.BackColor == Color.Red)
            {
                blConnect = true;
                GlobalVarLn.flllsp = 1;
            }

            InitConnectionPC(blConnect);
        }

        private void InitConnectionPC(bool blConnect)
        {
            String s1;
            int nn;

            if (blConnect)
            {
                if (clientPC != null)
                    clientPC = null;

                // sob
                //clientPC = new ClientPC(3);
                clientPC = new ClientPC((byte)GlobalVarLn.AdressOwn);

                // GPS
                // sob
                clientPC.OnConfirmCoord += clientPC_OnConfirmCoord;
                GlobalVarLn.clientPCG = clientPC;

                // connect
                clientPC.OnConnect += new ClientPC.ConnectEventHandler(ShowConnectPC);

                // disconnect 
                clientPC.OnDisconnect += new ClientPC.ConnectEventHandler(ShowDisconnectPC);

                // sob
                clientPC.OnConfirmReconFWS += new ClientPC.ConfirmReconFWSEventHandler(clientPC_OnConfirmReconFWS);

                s1 = tbIP.Text;
                nn = Convert.ToInt32(tbPort.Text);
                //clientPC.Connect("127.0.0.1", 9101);
                clientPC.Connect(s1, nn);

            }
            else
            {
                clientPC.Disconnect();

                clientPC = null;
                GlobalVarLn.flllsp = 0;

            }
        }

        private void clientPC_OnConfirmReconFWS(object sender, byte bAddress, byte bCodeError, TReconFWS[] tReconFWS)
        {
            if (UpdateTableReconFWS != null)
                UpdateTableReconFWS(tReconFWS);
        }

        /// <summary>
        /// если подключено -- зеленая лампочка
        /// </summary>
        /// <param name="sender"></param>
        private void ShowConnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = Color.Green;

                }));
            }
            else
            {
                bConnectPC.BackColor = Color.Green;
            }
        }

        /// <summary>
        /// если не подключено -- краскная лампочка
        /// </summary>
        /// <param name="sender"></param>
        private void ShowDisconnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = Color.Red;
                }));
            }
            else
            {
                bConnectPC.BackColor = Color.Red;
            }
        }

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        private void button2_Click_1(object sender, EventArgs e)
        {

            ;
        }


// ********************************************************************************************

        private void MapForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MapIsOpenned)
            {
                //iniRW.write_map_inf(axaxcMapScreen1.MapLeft, axaxcMapScreen1.MapTop, axaxcMapScreen1.ViewScale, axaxcMapScreen1.MapFileName);
                iniRW.write_map_inf(axaxcMapScreen1.MapLeft, axaxcMapScreen1.MapTop, axaxcMapScreen1.ViewScale);

                try
                {
                    GetViewSelectLayers();
                    iniRW.SetMapLayersSettings(bmas);
                }
                catch (Exception) { }

                axaxcMapScreen1.MapClose();
                MapIsOpenned = false;

                //MapCore.closeMapFunc(ref MapIsOpenned, ref axaxcMapScreen1, Application.StartupPath + "\\Setting.ini");
            }
        }

        private void MapForm_MouseDown(object sender, MouseEventArgs e)
        {
            ;
        }

        private void button2_Click_2(object sender, EventArgs e)
        {


            double lt = GlobalVarLn.listJS[0].CurrentPosition.x;
            double ln = GlobalVarLn.listJS[0].CurrentPosition.y;

/*
            double lt = (double)iniRW.get_MLT();
            double ln = (double)iniRW.get_MLN();
           // grad->rad
            lt = (lt * Math.PI) / 180;
            ln = (ln * Math.PI) / 180;
            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(hmapl1, ref lt, ref ln);
*/
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);

            //axaxcMapScreen1.ViewScale = iniRW.get_MS();
            axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width/2);
            axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);

            axaxcMapScreen1.Repaint();

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            double lt = 0;
            double ln = 0;
            lt = GlobalVarLn.listJS[0].CurrentPosition.x;
            ln = GlobalVarLn.listJS[0].CurrentPosition.y;


            if (GlobalVarLn.listJS[0].HasCurrentPosition == false)
            {
                MessageBox.Show("No coordinates");
                return;
            }

            /*
                        double lt = (double)iniRW.get_MLT();
                        double ln = (double)iniRW.get_MLN();
                       // grad->rad
                        lt = (lt * Math.PI) / 180;
                        ln = (ln * Math.PI) / 180;
                        // Подаем rad, получаем там же расстояние на карте в м
                        mapGeoToPlane(hmapl1, ref lt, ref ln);
            */
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);

            //axaxcMapScreen1.ViewScale = iniRW.get_MS();
            axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width / 2);
            axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);

            axaxcMapScreen1.Repaint();

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            double lt = 0;
            double ln = 0;
            lt = GlobalVarLn.listJS[1].CurrentPosition.x;
            ln = GlobalVarLn.listJS[1].CurrentPosition.y;


            if (GlobalVarLn.listJS[1].HasCurrentPosition == false)
            {
                MessageBox.Show("No coordinates");
                return;
            }

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);

            //axaxcMapScreen1.ViewScale = iniRW.get_MS();
            axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width / 2);
            axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);

            axaxcMapScreen1.Repaint();

        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            ;
        }

        FormLayersSelection FLS;
        private void mapCompositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FLS == null || FLS.IsDisposed)
            {
                FLS = new FormLayersSelection(ref axaxcMapScreen1);
                FLS.Show();
            }
            else
                FLS.Show();

        }

       

       


        // *****************************************************************************************


        // ************************************************************************************ LENA


    } // Class
} // namespace
