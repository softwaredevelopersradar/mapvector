﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrozaMap
{
    class iniRW
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileSection(string section, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public static string get_IPaddress()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Connect", "IPaddress", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }

        public static int get_Port()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Connect", "Port", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }



        // otl33
        public static int get_AdressOwn()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Common", "AdressOwn", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_MLT()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Position", "Lat", "0", temp, 255, Application.StartupPath + "\\InitPoz.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_MLN()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Position", "Long", "0", temp, 255, Application.StartupPath + "\\InitPoz.ini");
            return int.Parse(temp.ToString());
        }



        public static int get_AdressLinked()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Common", "AdressLinked", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }


        public static void write_map_inf(int mapLeft, int mapTop, int scale, string path)
        {
            WritePrivateProfileString("Position", "MapLeft", mapLeft.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "MapTop", mapTop.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "Scale", scale.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Map", "Path", path, Application.StartupPath + "\\Setting.ini");
        }

        public static void write_map_inf(int mapLeft, int mapTop, int scale)
        {
            WritePrivateProfileString("Position", "MapLeft", mapLeft.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "MapTop", mapTop.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "Scale", scale.ToString(), Application.StartupPath + "\\Setting.ini");
        }
        public static string get_map_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "Path", "", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }

        public static string get_matrix_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "PathMatrix", "", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }

        public static bool[] GetMapLayersSettings()
        {
            bool[] bmas = new bool[20];
            for (int i = 0; i < 20; i++)
            {
                StringBuilder temp = new StringBuilder(255);
                GetPrivateProfileString("MapLayers", i.ToString(), "", temp, 255, Application.StartupPath + "\\MapLayersSettings.ini");
                bmas[i] = bool.Parse(temp.ToString());
            }
            return bmas;
        }

        public static void SetMapLayersSettings(bool [] bmas)
        {
            for (int i = 0; i < 20; i++)
            {
                WritePrivateProfileString("MapLayers", i.ToString(), bmas[i].ToString(), Application.StartupPath + "\\MapLayersSettings.ini");
            }
        }
/*
        public static string get_map_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "Path", "", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }
*/

    }
}
