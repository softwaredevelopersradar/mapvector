﻿using System;
using System.Drawing;
using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Runtime.CompilerServices;
using System.Collections.Generic;



namespace GrozaMap
{
    public partial class FormSupSpuf : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);


        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        private double OwnHeight_comm;
        private int i_HeightOwnObject_comm;
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // объект подавления
        private int i_HeightOpponent_comm;
        private double HeightOpponent_comm;
        // Высота антенны противника
        private int iOpponAnten_comm;
        private int iMiddleHeight_comm;

        // ДПВ
        private int iDSR;
        // ......................................................................
        // ZPodnav
        private double F_supnav;
        private double P_supnav;
        private double K_supnav;
        private double iKP_supnav;
        private double dKP_supnav;
        private double iP_supnav;
        private double dP_supnav;
        private double VC_supnav;
        private double ipw_supnav;
        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormSupSpuf(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;
            OwnHeight_comm = 0;
            i_HeightOwnObject_comm = 0;

            HeightAntennOwn_comm = 0; // антенна
            HeightTotalOwn_comm = 0;

            // объект подавления
            i_HeightOpponent_comm = 0;
            HeightOpponent_comm = 0;
            // Высота антенны противника
            iOpponAnten_comm = 0;
            iMiddleHeight_comm = 0;

            // ДПВ
            iDSR = 0;

            // ......................................................................
            // ZPodnav
            F_supnav = 1575000000;
            P_supnav = 0;
            K_supnav = 0;
            iKP_supnav = 0;
            dKP_supnav = 0;
            iP_supnav = -155;
            dP_supnav = 0;
            VC_supnav = 300000000;
            ipw_supnav = 0;

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            MapForm.UpdateDropDownList(cbCenterLSR);
            // ......................................................................


        } // Конструктор
          // ***********************************************************  Конструктор

        private void OnListJSChanged(object sender, EventArgs e)
        {
            MapForm.UpdateDropDownList(cbCenterLSR);
        }

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormSupSpuf_Load(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSupSpufG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSupSpuf = 1;

            gbRect.Visible = true;
            gbRect.Location = new Point(7, 30);
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_supnav = 0;
            GlobalVarLn.flCoord_supnav = 0; // =1-> Выбрали центр ЗПВ
            // ----------------------------------------------------------------------
            GlobalVarLn.objFormSupSpufG.cbCenterLSR.SelectedIndex = 0;
            P_supnav = 15;
            GlobalVarLn.objFormSupSpufG.comboBox2.SelectedIndex = 1;

            GlobalVarLn.objFormSupSpufG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSupSpufG.tbDistSightRange.Text = "";

            GlobalVarLn.NumbSP_supnav = "";
            // ----------------------------------------------------------------------
            if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                GlobalVarLn.listNavigationJammingZone.Clear();
            // ----------------------------------------------------------------------

            //ClassMap.f_RemoveFrm(12);


        } // Load
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            tbXRect.Text = "";
            tbYRect.Text = "";
            GlobalVarLn.objFormSupSpufG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSupSpufG.tbDistSightRange.Text = "";
            GlobalVarLn.NumbSP_supnav = "";
            // ...................................................................
            // переменные

            GlobalVarLn.fl_supnav = 0; // =1-> Выбрали центр ЗПВ
            GlobalVarLn.flCoord_supnav = 0; // Отрисовка 
            GlobalVarLn.XCenter_supnav = 0;
            GlobalVarLn.YCenter_supnav = 0;
            // ----------------------------------------------------------------------
            if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                GlobalVarLn.listNavigationJammingZone.Clear();
            // ----------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------

        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ;
        } // Closing
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbCenterLSR": Выбор SP
        // ************************************************************************
        private void cbCenterLSR_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCenterLSR.SelectedIndex == 0)
                GlobalVarLn.NumbSP_supnav = "";
            else
                GlobalVarLn.NumbSP_supnav = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);

        } // SP
        // ************************************************************************


        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            K_supnav = Convert.ToDouble(comboBox2.Items[comboBox2.SelectedIndex]);
        } 

        // ************************************************************************
        // Обработчик Button1 "Центр ЗПВ": Выбор центра ЗПВ
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            int it = 0;
            String strLine3 = "";
            String strLine2 = "";


            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();

            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbCenterLSR.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_supnav = "";
                // !!! реальные координаты на местности карты в м (Plane)
                GlobalVarLn.XCenter_supnav = GlobalVarLn.MapX1;
                GlobalVarLn.YCenter_supnav = GlobalVarLn.MapY1;

            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_supnav = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);
                var stations = GlobalVarLn.listJS;
                var station = stations[cbCenterLSR.SelectedIndex - 1];

                GlobalVarLn.XCenter_supnav = station.CurrentPosition.x;
                GlobalVarLn.YCenter_supnav = station.CurrentPosition.y;
            }

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_supnav, GlobalVarLn.YCenter_supnav);
            OwnHeight_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            // COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR Выбор координат
            GlobalVarLn.tpOwnCoordRect_supnav.X = (int)GlobalVarLn.XCenter_supnav;
            GlobalVarLn.tpOwnCoordRect_supnav.Y = (int)GlobalVarLn.YCenter_supnav;
            // ......................................................................


            // ......................................................................
            // SP на карте

            // SP
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter_supnav,  // m на местности
                          GlobalVarLn.YCenter_supnav,
                              ""
                         );

            // ......................................................................
            GlobalVarLn.fl_supnav = 1;
            GlobalVarLn.flCoord_supnav = 1; // Центр ЗПВ выбран
            // ......................................................................
            // Отображение СП в выбранной СК

            OtobrSP_supnav();
            // .......................................................................

        } // Центр зоны
        // ************************************************************************


        // ************************************************************************
        // Кнопка Принять
        // ************************************************************************
        private void bAccept_Click(object sender, EventArgs e)
        {
            String s1 = "";

            // ........................................................................
            if (GlobalVarLn.XCenter_supnav == 0 || GlobalVarLn.YCenter_supnav == 0)
            {
                MessageBox.Show("The center of the zone is not selected");
                return;
            }
            // ........................................................................
            if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                GlobalVarLn.listNavigationJammingZone.Clear();
            // ----------------------------------------------------------------------

            // H *********************************************************************
            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_supnav, GlobalVarLn.axMapPointGlobalAdd, GlobalVarLn.axMapScreenGlobal);

            // Высота антенны
            HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);

            /*
                        if (HeightAntennOwn_comm < 0 || HeightAntennOwn_comm > 50)
                        {
                            MessageBox.Show("Parameter 'antenna height' out of range 0m - 50m");
                            return;
                        }
            */

            HeightTotalOwn_comm = OwnHeight_comm + HeightAntennOwn_comm;

            // OP
            iOpponAnten_comm = (int)Convert.ToDouble(tbOpponentAntenna.Text);
            HeightOpponent_comm = iMiddleHeight_comm + iOpponAnten_comm;
            // ********************************************************************* H


            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // ........................................................................

            VC_supnav = 300000000; // Скорость света
            F_supnav = 1575000000;
            iP_supnav = -155;

            // ........................................................................
            // Мощность 

            s1 = Convert.ToString(powerTextbox.Text);
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                P_supnav = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                P_supnav = Convert.ToDouble(s1);
            }

            if (P_supnav < 10 || P_supnav > 100)
            {
                MessageBox.Show("Parameter 'Power' out of range 10W - 100W");
                return;
            }

            // Коэффициент усиления
            s1 = Convert.ToString(comboBox2.Items[comboBox2.SelectedIndex]);
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                K_supnav = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                K_supnav = Convert.ToDouble(s1);
            }

            // ........................................................................
            // Коэффициент подавления

            s1 = textBox1.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                iKP_supnav = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                iKP_supnav = Convert.ToDouble(s1);
            }

            if (iKP_supnav < 30 || iKP_supnav > 50)
            {
                MessageBox.Show("Parameter 'Jamming-to-Signal Ratio (J/Sratio)' out of range 30dB - 50dB");
                return;
            }

            ipw_supnav = iKP_supnav / 10;
            dKP_supnav = Math.Pow(10, ipw_supnav);
            // ........................................................................

            ipw_supnav = iP_supnav / 10;
            dP_supnav = Math.Pow(10, ipw_supnav);
            // ........................................................................

            // ******************************************************** Ввод параметров

            // Расчет зоны ************************************************************

            GlobalVarLn.fl_supnav = 1;
            GlobalVarLn.iR_supnav = (int) GetNavigationZoneRadius(P_supnav, K_supnav, iKP_supnav);

            //var polygon = FormLineSightRange.Instance.CreateLineSightPolygon(
            //    new Point((int)GlobalVarLn.XCenter_supnav, (int)GlobalVarLn.YCenter_supnav),
            //    GlobalVarLn.iR_supnav);
            var polygon = CreateLineSightPolygon(new Point((int)GlobalVarLn.XCenter_supnav, (int)GlobalVarLn.YCenter_supnav),GlobalVarLn.iR_supnav);

            GlobalVarLn.listNavigationJammingZone = polygon;

            tbDistSightRange.Text = Convert.ToString(GlobalVarLn.iR_supnav);
            // ************************************************************ Расчет зоны
            ClassMap.DrawPolygon(GlobalVarLn.listNavigationJammingZone, Color.Blue);


        } // Принять
        // ************************************************************************

        // ************************************************************************
        // R

        public static double GetNavigationZoneRadius(double power, double gain, double jsRatio)
        {
            var VC_supnav = 300000000; // Скорость света
            var F_supnav = 1575000000;
            var iP_supnav = -155.0;

            var ipwSupnav = jsRatio / 10;
            var dKpSupnav = Math.Pow(10, ipwSupnav);
            ipwSupnav = iP_supnav / 10;
            var dPSupnav = Math.Pow(10, ipwSupnav);

            return 0.7 * VC_supnav / (4 * Math.PI * F_supnav) *
                   Math.Sqrt((power * gain) / (dKpSupnav * dPSupnav));
        }
        // ************************************************************************

        // ************************************************************************
        // функция отображения координат центра зоны
        // ************************************************************************
        private void OtobrSP_supnav()
        {
            var pos = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.XCenter_supnav, GlobalVarLn.YCenter_supnav);

            tbXRect.Text = pos.X.ToString("F3");
            tbYRect.Text = pos.Y.ToString("F3");

        } // OtobrSP_supnav

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN

        // ************************************************************************
        // Функция определения средней высота местности

        private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)

        {
            //int iRadius = 2000;
            int iRadius = 30000;

            int iStep = 100;
            int iCount = 0;
            double dMiddleHeightStep = 0;
            double dMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                double dMinX = 0;
                double dMinY = 0;

                double dMaxX = 0;
                double dMaxY = 0;

                dMinX = tpReferencePoint.X - iRadius;
                dMinY = tpReferencePoint.Y - iRadius;

                dMaxX = tpReferencePoint.X + iRadius;
                dMaxY = tpReferencePoint.Y + iRadius;

                // пройти по координатам карты с шагом Shag
                for (int i = (int)dMinX; i < dMaxX; i = i + iStep)
                {
                    for (int j = (int)dMinY; j < dMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;

                        dSetX = i;
                        dSetY = j;

                        //GlobalVar::axMapPointGlobal.SetPoint(dSetX,dSetY);
                        //dMiddleHeightStep = 0;
                        //dMiddleHeightStep = GlobalVar::axMapScreenGlobal->PointHeight_get(GlobalVar::axMapPointGlobal);

                        axMapPointTemp.SetPoint(dSetX, dSetY);
                        dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);

                        // 0809
/*
                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeight = 0;
                            return (int)dMiddleHeight;
                        }
*/
                        // 6_9_18
                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeightStep = 0;
                        }

                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        dMiddleHeight = dMiddleHeight + dMiddleHeightStep;
                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                dMiddleHeight = dMiddleHeight / (double)iCount;

                if (dMiddleHeight < 0)
                    dMiddleHeight = 0;
            }

            return (int)dMiddleHeight;
        }
        // ************************************************************************

        // ************************************************************************
        // функция расчета ДПВ

        public int CountDSR(Point p)
        {
            var iOpponAntenComm = iOpponAnten_comm;
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iMiddleHeightComm = iMiddleHeight_comm;
            var OwnHeightComm = OwnHeight_comm;
            var HeightOpponentComm = HeightOpponent_comm;

            return CountDSR((int)heightTotalOwnComm, (int)OwnHeightComm, (int)iMiddleHeightComm, (int)HeightOpponentComm);

        }

        private int CountDSR(int iHeightTotalOwn, int iHeightOwnObj, int iHeightMiddle, int iHeightOpponentObj)
        {
            int iDSR = 0;
            int iHeightMin = 0;
            int h1 = 0;
            int h2 = 0;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hant

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hant

            } // iHeightMiddle > iHeightOwnObj

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));

            // рассчитать ДПВ по формуле
            iDSR = iDSR * 1000;

            return iDSR;
        }

        // ************************************************************************

        // ************************************************************************
        // функция расчета точек ЗПВ

        void CountPointLSR(Point tpCenterLSR,
                           int iHeightCenterLSR,     // HeightTotalOwn_comm
                           int iHeightAnten,         //HeightAntennOwn_comm
                           double dDSR,
                           int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var points = CreateLineSightPolygon(tpCenterLSR);
            GlobalVarLn.listNavigationJammingZone.AddRange(points);
        }
        // --------------------------------------------------------------------------
        public List<Point> CreateLineSightPolygon(Point tpCenterLSR, int maxRadius = int.MaxValue)
        {
            var dsr = CountDSR(tpCenterLSR);

            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iOpponAntenComm = iOpponAnten_comm;


            var points = CreateLineSightPolygon(tpCenterLSR, (int)heightTotalOwnComm, dsr, (int)iOpponAntenComm);

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = p.X - tpCenterLSR.X;
                double dy = p.Y - tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;

                points[i] = new Point((int)x, (int)y);
            }

            return points;
        }
        // --------------------------------------------------------------------------

        // ************************************************************************************
        // 27_09_2018
        // Расчет Зоны прямой видимости
        // ************************************************************************************

        public List<Point> CreateLineSightPolygon(Point tpCenterLSR,
                                                 int iHeightCenterLSR, // HeightTotalOwn_comm
                                                 double dDSR,
                                                 int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var listKoordReal = new List<Point>();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;

            // otl***
            //String strFileName;
            //strFileName = "RLF.txt";
            //StreamWriter srFile;
            //srFile = new StreamWriter(strFileName);

            // otl***
            //srFile.WriteLine("DSR =" + Convert.ToString(dDSR));

            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO
            // пройти в цикле по всем углам   (против часовой стрелки, 1 град)

            for (angleFi = 0; angleFi < 361; angleFi = angleFi + GlobalVarLn.iStepAngleInput_ZPV)
            {

                // otl***
                //srFile.WriteLine("Fi =" + Convert.ToString(angleFi));

                // .................................................................................
                // найти координаты точки реальной ДПВ (от центра на расстоянии ДПВ)

                KoordThree koord2;

                koord2.y = tpCenterLSR.Y + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.x = tpCenterLSR.X + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                //GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetY, dSetX);
                // koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                if (koord2.h < 0)
                    koord2.h = 0;

                // антенна ОП
                koord2.h += iHeightAntenOpponent;
                // .................................................................................
                // otl***
                //srFile.WriteLine("(1)" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("HC =" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("H2 =" + Convert.ToString(koord2.h));
                // .................................................................................
                // изначально координаты Koord4 равны координатам точки реальной ДПВ

                KoordThree koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;
                // .................................................................................

                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ
                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }
                // .................................................................................

                // Hsp>Hdpv IF1********************************************************************     
                // если высота СП больше высоты точки реальной ДПВ

                // IF1
                KoordThree koord3;
                KoordThree koordPrev;

                // 27_09_2018
                KoordThree koordPrev1;

                if (iHeightCenterLSR > koord2.h)
                {
                    // -----------------------------------------------------------------------------
                    // otl***
                    //srFile.WriteLine("(2) FIRST");

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(3)");
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    // -----------------------------------------------------------------------------

                    // WHILE1 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE1
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(4)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("A =" + Convert.ToString(alfa));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF2 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF2
                        if (koord3.h > H_Line)
                        {
                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            // Словили выход рельефа над линией видимости
                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(5)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));

                            // WHILE2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE2
                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("6");
                                //srFile.WriteLine("dL=" + Convert.ToString(dL));
                                //srFile.WriteLine("H3=" + Convert.ToString(koord3.h));
                                //srFile.WriteLine("HDob=" + Convert.ToString(h_Dob));
                                //srFile.WriteLine("HPrev=" + Convert.ToString(koordPrev.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // otl***
                                    //srFile.WriteLine("Dalshe1");

                                    // 27_09_2018
                                    koordPrev1 = koord3;
                                }
                                else // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while2
                                        dL = dDSR + 1;
                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;
                                        //koordPrev = koord3; // &&&&&&&&&&&

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                        // otl***
                                        // srFile.WriteLine("Dalshe1_1");

                                        // 27_09_2018
                                        koordPrev1 = koord3;
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // ELSE

                            } // WHILE2
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE2

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while1
                            dL = dDSR + 1;

                        } // IF2 (высота рельефа больше воображаемой линии)
                        //  ..................................................................... IF2

                        // ELSE по IF2 ..............................................................
                        // если значение высоты воображаемой линии больше или равно
                        // поверхности земли в этой точке

                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            //rFile.WriteLine("(5_1)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // ELSE po IF2 (высота воображаемой линии больше или равна рельефу в этой точке)
                        // .............................................................. ELSE по IF2

                    } // конец WHILE1 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE1

                    // otl***
                    //srFile.WriteLine("END WHILE: dL<DSR");

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                    // otl***
                    //srFile.WriteLine("(6)");
                    //srFile.WriteLine("X3 =" + Convert.ToString(koord3.x));
                    //rFile.WriteLine("Y3 =" + Convert.ToString(koord3.y));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("X4 =" + Convert.ToString(koord4.x));
                    //srFile.WriteLine("Y4 =" + Convert.ToString(koord4.y));
                    //srFile.WriteLine("H4 =" + Convert.ToString(koord4.h));

                } // IF1 
                // ******************************************************************* Hsp>Hdpv IF1

                // Hsp<Hdpv ELSE po IF1 ***********************************************************
                // если высота СП меньше высоты точки реальной ДПВ   

                else
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(7)");
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));

                    // WHILE3 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE3
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        //H_Line = VarL * Math.Tan(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(8)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF3 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF3
                        // Словили выход рельефа над линией видимости
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(9)");
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));

                            // WHILE4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE4
                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;  // ???????????????????

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;
                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("(10)");
                                //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                                // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // 27_09_2018
                                    koordPrev1 = koord3;

                                    // otl***
                                    //srFile.WriteLine("dalse2");
                                }
                                else  // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit2");
                                        // выйти из цикла while4
                                        dL = dDSR + 1;

                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                        // 27_09_2018
                                        koordPrev1 = koord3;

                                        // otl***
                                        //srFile.WriteLine("Dalshe2");
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // else

                            } // WHILE4 
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE4

                            // выйти из цикла while3
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // otl***
                            //srFile.WriteLine("(11)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // IF3 если значение высоты воображаемой линии меньше поверхности земли в этой точке
                        // ..................................................................... IF3

                        // ELSE po IF3 .............................................................
                        // если значение высоты воображаемой линии блольше или равно
                        // поверхности земли в этой точке   
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                            dSetX = koord3.x;
                            dSetY = koord3.y;
                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            // srFile.WriteLine("(12)");
                            // srFile.WriteLine("dL =" + Convert.ToString(dL));
                            // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // Else po IF3
                        // ............................................................. ELSE po IF3

                    } // конец while3 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE3

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                } // ELSE po IF1 (Hsp<Hdpv)
                // *********************************************************** Hsp<Hdpv ELSE po IF1

            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   
            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO

            //srFile.Close();

            return listKoordReal;

        } // Расчет Зоны прямой видимости
        // ************************************************************************************



        // *************************************************************** FUNCTIONS







        // ************************************************************************
        // FormClosing
        // ************************************************************************
        private void FormSupSpuf_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            //GlobalVarLn.fFSupSpuf = 0;

            GlobalVarLn.fl_Open_objFormSupSpuf = 0;

        }

        private void tbHAnt_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormSupSpuf_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSupSpufG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSupSpuf = 1;
            //ClassMap.f_RemoveFrm(12);

            GlobalVarLn.fl_Open_objFormSupSpuf = 1;

        } // Closing



        // ************************************************************************


    } // Class
} // Namespace
