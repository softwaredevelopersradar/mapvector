﻿using System;
using System.Collections.Generic;
using System.Drawing;
using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;


namespace GrozaMap
{
    public partial class FormLineSightRange : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;



        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;
        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        private double OwnHeight_comm;

        public static FormLineSightRange Instance { get; private set; }

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // объект подавления
        private int i_HeightOpponent_comm;
        private int i_HeightOwnObject_comm;
        private double HeightOpponent_comm;
        // Высота антенны противника
        private int iOpponAnten_comm;


        private int iMiddleHeight_comm;

        // ДПВ
        private int iDSR;

        //private Point[] tpPointDSR;
        //private Point[] tpPointPictDSR;

        // ......................................................................
        // ZOSP
        private double F_ZOSP;
        private double P_ZOSP;
        private double K_ZOSP;
        private int iKP_ZOSP;
        private double dKP_ZOSP;
        private double VC_ZOSP;
        private double ipw_ZOSP;
        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        public FormLineSightRange(ref AxaxcMapScreen axaxcMapScreen1)
        {
            Instance = this;

            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            OwnHeight_comm = 0;

            i_HeightOwnObject_comm = 0;
            HeightAntennOwn_comm = 0; // антенна
            HeightTotalOwn_comm = 0;

            // объект подавления
            i_HeightOpponent_comm = 0;
            HeightOpponent_comm = 0;
            // Высота антенны противника
            iOpponAnten_comm = 0;

            iMiddleHeight_comm = 0;

            // ДПВ
            iDSR = 0;

            // ......................................................................
            // ZOSP
            F_ZOSP = 0;
            P_ZOSP = 0;
            K_ZOSP = 0;
            iKP_ZOSP = 0;
            dKP_ZOSP = 0;
            VC_ZOSP = 300000000;
            ipw_ZOSP = 0;
            // ......................................................................


        } // Конструктор
        // ***********************************************************  Конструктор


        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormLineSightRange_Load(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLineSightRange = 1;

            gbRect.Visible = true;
            gbRect.Location = new Point(7, 30);

            gbRect42.Visible = false;
            gbRad.Visible = false;
            gbDegMin.Visible = false;
            gbDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Средство РП
            UpdateDropDownList();
            cbHeightOwnObject.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Объект РП

            cbHeightOpponObject.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            chbXY.Checked = false;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_LineSightRange = 0;
            GlobalVarLn.flCoordZPV = 0; // =1-> Выбрали центр ЗПВ
            // ----------------------------------------------------------------------
            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            // ---------------------------------------------------------------------
            if (GlobalVarLn.listDetectionZone.Count != 0)
                GlobalVarLn.listDetectionZone.Clear();
            // ---------------------------------------------------------------------
            cbCenterLSR.SelectedIndex = 0;
            GlobalVarLn.NumbSP_lsr = "";
            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // ----------------------------------------------------------------------

            //ClassMap.f_RemoveFrm(10);

        } // Загрузка формы

        private void OnListJSChanged(object sender, EventArgs e)
        {
            UpdateDropDownList();
        }

        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click_1(object sender, EventArgs e)
        {
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbXRect42.Text = "";
            tbYRect42.Text = "";
            tbBRad.Text = "";
            tbLRad.Text = "";
            tbBMin1.Text = "";
            tbLMin1.Text = "";
            tbBDeg2.Text = "";
            tbBMin2.Text = "";
            tbBSec.Text = "";
            tbLDeg2.Text = "";
            tbLMin2.Text = "";
            tbLSec.Text = "";

            tbHeightOwnObject.Text = "";
            tbHeightOpponObject.Text = "";

            tbMiddleHeight.Text = "";
            tbDepth.Text = "";
            tbOwnHeight.Text = "";

            //tbDistSightRange.Text = ""; // ???????????????
            // ...................................................................
            // переменные

            GlobalVarLn.fl_LineSightRange = 0; // =1-> Выбрали центр ЗПВ
            GlobalVarLn.flCoordZPV = 0; // Отрисовка 
            GlobalVarLn.XCenter_ZPV = 0;
            GlobalVarLn.YCenter_ZPV = 0;
            // ----------------------------------------------------------------------
            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            // ----------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------


        } // Clear
        // ************************************************************************

        // ************************************************************************
        // clear1
        private void button3_Click(object sender, EventArgs e)
        {
            // ...................................................................
            textBox2.Text = "";

            // ...................................................................
            // переменные

            GlobalVarLn.fl_ZOSP = 0;


            if (GlobalVarLn.fl_LineSightRange == 0)
            {
                GlobalVarLn.XCenter_ZOSP = 0;
                GlobalVarLn.YCenter_ZOSP = 0;
            }
            // ...................................................................
            if (GlobalVarLn.listDetectionZone.Count != 0)
                GlobalVarLn.listDetectionZone.Clear();
            // ---------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------

        } // clear1
        // ************************************************************************

        // ************************************************************************
        // Выбор ЦЗ
 
        private void button1_Click_1(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            int it = 0;
            String strLine3 = "";
            String strLine2 = "";

            ClassMap objClassMap3_ed = new ClassMap();

            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbCenterLSR.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_lsr = "";
                // !!! реальные координаты на местности карты в м (Plane)
                GlobalVarLn.XCenter_ZPV = GlobalVarLn.MapX1;
                GlobalVarLn.YCenter_ZPV = GlobalVarLn.MapY1;

            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_lsr = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);
                var stations = GlobalVarLn.listJS;
                var station = stations[cbCenterLSR.SelectedIndex - 1];

                GlobalVarLn.XCenter_ZPV = station.CurrentPosition.x;
                GlobalVarLn.YCenter_ZPV = station.CurrentPosition.y;

            }
            // ----------------------------------------------------------------------
            // 09_10_2018
            // Ручной ввод

            if (chbXY.Checked == true)
            {
                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Invalid coordinates of the LOS center");
                    return;
                }
                //var x = Convert.ToDouble(tbXRect.Text);
                //var y = Convert.ToDouble(tbYRect.Text);
                //axaxcMapScreen.MapRealToPlaneGeo(ref x, ref y);
                //GlobalVarLn.XCenter_ZPV = x;
                //GlobalVarLn.YCenter_ZPV = y;

                double h = 0;
                double Lt = 0;
                double Ln = 0;
                String s = "";

                //Lat
                s = tbXRect.Text;
                try
                {
                    Lt = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Lt = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                //Long
                s = tbYRect.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Ln = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Ln = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                // ......................................................................
                // grad->rad
                Lt = (Lt * Math.PI) / 180;
                Ln = (Ln * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref Lt, ref Ln);

                GlobalVarLn.XCenter_ZPV = Lt;
                GlobalVarLn.YCenter_ZPV = Ln;

            } // chbXY.Checked == true
            // ---------------------------------------------------------------------
            // Для зоны обнаружения СП
            GlobalVarLn.XCenter_ZOSP = GlobalVarLn.XCenter_ZPV;
            GlobalVarLn.YCenter_ZOSP = GlobalVarLn.YCenter_ZPV;
            // ---------------------------------------------------------------------
            // Center ZPV (H)

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_ZPV, GlobalVarLn.YCenter_ZPV);
            OwnHeight_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            // COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR Выбор координат

            // ......................................................................
            // SP на карте


            // Центр ЗПВ
            //ClassMap.f_Map_Pol_XY_stat(
            //              GlobalVarLn.XCenter_ZPV,  // m на местности
            //              GlobalVarLn.YCenter_ZPV,
            //              1,
            //              ""
            //             );

            // SP
            ClassMap.f_DrawSPXY(
                          GlobalVarLn.XCenter_ZPV,  // m на местности
                          GlobalVarLn.YCenter_ZPV,
                              ""
                         );

            // ......................................................................
            GlobalVarLn.fl_LineSightRange = 1;
            GlobalVarLn.flCoordZPV = 1; // Центр ЗПВ выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            //xtmp_ed = XSP_comm;
            //ytmp_ed = YSP_comm;
            xtmp_ed = GlobalVarLn.XCenter_ZPV;
            ytmp_ed = GlobalVarLn.YCenter_ZPV;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;
            // .......................................................................

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS
            // CDel
            // !!! Здесь это WGS84

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );
            OtobrSP_Comm();
            // .......................................................................


        } // Button1: СК СП
        // ************************************************************************

        // ************************************************************************

        //  Зона обнаружения СП ZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOS
        // Кнопка Принять Зона обнаружения СП

        private void button2_Click(object sender, EventArgs e)
        {
            String s1 = "";

            // ........................................................................
            if ((GlobalVarLn.XCenter_ZOSP == 0) || (GlobalVarLn.YCenter_ZOSP == 0))
            {
                MessageBox.Show("The center of the zone is not selected");
                return;
            }
            // ........................................................................
            if (GlobalVarLn.listDetectionZone.Count != 0)
                GlobalVarLn.listDetectionZone.Clear();
            // ---------------------------------------------------------------------

            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // ........................................................................
            VC_ZOSP = 300000000; // Скорость света
            // ........................................................................
            // Мощность передатчика связи 

            s1 = tbPowerOwn.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                P_ZOSP = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                P_ZOSP = Convert.ToDouble(s1);
            }

            if ((P_ZOSP < 0.1) || (P_ZOSP > 100))
            {
                MessageBox.Show("Parameter 'Power' out of range 0.1W - 100W");
                return;
            }
            // ........................................................................
            // Частота передатчика связи 

            s1 = tbFreq.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                F_ZOSP = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                F_ZOSP = Convert.ToDouble(s1);
            }

            if ((F_ZOSP < 100) || (F_ZOSP > 6000))
            {
                MessageBox.Show("Parameter 'frequency' out of range 100MHz - 6000MHz");
                return;
            }

            F_ZOSP = F_ZOSP * 1000000;
            // ........................................................................
            // Коэффициент усиления пеленгатора 

            s1 = tbCoeffTransmitOpponent.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                K_ZOSP = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                K_ZOSP = Convert.ToDouble(s1);
            }

            if ((K_ZOSP < 1) || (K_ZOSP > 6))
            {
                MessageBox.Show("Parameter 'gain' out of range  1 - 6");
                return;
            }
            // ........................................................................
            // Порог чувствительности пеленгатора

            s1 = textBox1.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                iKP_ZOSP = Convert.ToInt32(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                iKP_ZOSP = Convert.ToInt32(s1);
            }

            if ((iKP_ZOSP < -140) || (iKP_ZOSP > -100))
            {
                MessageBox.Show("Parameter 'Direction finder threshold sensitivity' out of range -140dBW - -100dBW");
                return;
            }

            ipw_ZOSP = iKP_ZOSP / 10.0;
            dKP_ZOSP = Math.Pow(10, (double)ipw_ZOSP);
            // ........................................................................


            // ******************************************************** Ввод параметров

            // !!! Высоты *************************************************************
            // ***
            // !!! OwnHeight_comm введена по кнопке ЦентрЗоны

            // Средняя высота местности
            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_ZPV, GlobalVarLn.axMapPointGlobalAdd,
                                                         GlobalVarLn.axMapScreenGlobal);
            tbMiddleHeight.Text = iMiddleHeight_comm.ToString();

            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            // Антенна ОП
            iOpponAnten_comm = Convert.ToInt32(tbOpponentAntenna.Text);

            // Общая высота ОП
            HeightOpponent_comm = iMiddleHeight_comm + iOpponAnten_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightOpponent_comm);
            tbHeightOpponObject.Text = Convert.ToString(ichislo);
            // ************************************************************* !!! Высоты

            // Расчет зоны ************************************************************

            GlobalVarLn.fl_ZOSP = 1;

            var k = 1 + F_ZOSP / 2.4e9;
            if (F_ZOSP > 2.4e9)
            {
                k = 2;
            }

            GlobalVarLn.iR_ZOSP = (int)(k / 5 * (VC_ZOSP / (4 * Math.PI * F_ZOSP)) *
                                        Math.Sqrt((P_ZOSP * K_ZOSP) / dKP_ZOSP));

            GlobalVarLn.listDetectionZone = CreateLineSightPolygon(
                new Point((int)GlobalVarLn.XCenter_ZOSP, (int)GlobalVarLn.YCenter_ZOSP), GlobalVarLn.iR_ZOSP);

            textBox2.Text = Convert.ToString(GlobalVarLn.iR_ZOSP);
            // ************************************************************ Расчет зоны
            /*
            ClassMap.f_ZOSP(
                            GlobalVarLn.XCenter_ZOSP,
                            GlobalVarLn.YCenter_ZOSP,
                            GlobalVarLn.iR_ZOSP
                            );*/
            ClassMap.DrawPolygon(GlobalVarLn.listDetectionZone, Color.HotPink);
        } // Зона обнаружения СП (Кнопка Принять)
        //  ZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOSPZOS Зона обнаружения СП


        // Расчет зоны MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
        // Button "Принять" ЗПВ

        private void bAccept_Click(object sender, EventArgs e)
        {
            
            if (GlobalVarLn.flCoordZPV == 0)
            {
                MessageBox.Show("The center of the zone is not selected");
                return;
            }
            // ----------------------------------------------------------------------

            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            // ----------------------------------------------------------------------

            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // Высота антенны
            // ??????????????????????
            HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);

            // ComboBox (Индексы)
            i_HeightOwnObject_comm = cbHeightOwnObject.SelectedIndex; // Средство РП
            i_HeightOpponent_comm = cbHeightOpponObject.SelectedIndex; // Объект РП

            // ******************************************************** Ввод параметров

            // Центр ЗПВ **************************************************************
            // Координаты на местности в м

            GlobalVarLn.tpOwnCoordRect_ZPV.X = (int)GlobalVarLn.XCenter_ZPV;
            GlobalVarLn.tpOwnCoordRect_ZPV.Y = (int)GlobalVarLn.YCenter_ZPV;

            if ((tbXRect.Text == "") || (tbYRect.Text == ""))
            {
                MessageBox.Show("Incorrect data");
                return;
            }

            // ************************************************************** Центр ЗПВ

            // !!! Высоты *************************************************************
            // ***
            // !!! OwnHeight_comm введена по кнопке ЦентрЗоны

            // Средняя высота местности
            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_ZPV, GlobalVarLn.axMapPointGlobalAdd,
                                                         GlobalVarLn.axMapScreenGlobal);
            tbMiddleHeight.Text = iMiddleHeight_comm.ToString();

            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            // Антенна ОП
            iOpponAnten_comm = Convert.ToInt32(tbOpponentAntenna.Text);

            // Общая высота ОП
            HeightOpponent_comm = iMiddleHeight_comm + iOpponAnten_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightOpponent_comm);
            tbHeightOpponObject.Text = Convert.ToString(ichislo);
            // ************************************************************* !!! Высоты


            // *************************************************** H объекта подавления

            // Расчет зоны ************************************************************

            // Шаг
            //GlobalVarLn.iStepAngleInput_ZPV = Convert.ToInt32(tbStepAngle.Text);
            //GlobalVarLn.iStepLengthInput_ZPV = Convert.ToInt32(tbStepDist.Text);

            // Средняя высота местности
            //iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_ZPV, GlobalVarLn.axMapPointGlobalAdd, 
            //                                             GlobalVarLn.axMapScreenGlobal);

            //tbMiddleHeight.Text = iMiddleHeight_comm.ToString();

            // Высота антенны противника
            //iOpponAnten_comm = Convert.ToInt32(tbOpponentAntenna.Text);

            // определить ДПВ
            //iDSR = CountDSR(iHeightTotalOwn, iHeightOwn, iHeightMiddle, iHeightMiddle + iOpponAnten);
            iDSR = CountDSR((int)HeightTotalOwn_comm, (int)OwnHeight_comm, iMiddleHeight_comm, iMiddleHeight_comm + iOpponAnten_comm);

            tbDistSightRange.Text = iDSR.ToString();

            // отобразить глубину подавления
            // ?????????????????
            tbDepth.Text = GlobalVarLn.iSupDepth.ToString();

            // рассчитать точки полигона
            //CountPointLSR(tpCoordRect, iHeightTotalOwn, iHeightAnten, (double)iDSR, iOpponAnten);
            CountPointLSR(GlobalVarLn.tpOwnCoordRect_ZPV, (int)HeightTotalOwn_comm,
                         (int)HeightAntennOwn_comm, (double)iDSR, iOpponAnten_comm);

            // отрисовать ЗПВ
            ClassMap.DrawLSR();

            // ************************************************************ Расчет зоны


        } // Button "Принять"
        // MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN  Расчет зоны

        public void UpdateDropDownList()
        {
            MapForm.UpdateDropDownList(cbCenterLSR);
        }

        // *************************************************************************************
        private void OtobrSP_Comm()
        {
            var pos = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.XCenter_ZPV, GlobalVarLn.YCenter_ZPV);

            // 09_10_2018
            //tbXRect.Text = pos.X.ToString("F3");
            //tbYRect.Text = pos.Y.ToString("F3");
            tbXRect.Text = pos.X.ToString("F6");
            tbYRect.Text = pos.Y.ToString("F6");

            ichislo = (long)(LatKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBRad.Text = Convert.ToString(dchislo);   // X, карта

            ichislo = (long)(LongKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLRad.Text = Convert.ToString(dchislo);   // X, карта

            //} // IF


            // Градусы (Красовский)
            // CDel
            // !!!Здесь это WGS84
            //else if (gbOwnDegMin.Visible == true)
            //{

            ichislo = (long)(LatKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBMin1.Text = Convert.ToString(dchislo);

            ichislo = (long)(LongKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLMin1.Text = Convert.ToString(dchislo);

            //} // IF


            // Градусы,мин,сек (Красовский)
            // CDel
            // !!!Здесь это WGS84
            //else if (gbOwnDegMinSec.Visible == true)
            // {

            tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
            tbBMin2.Text = Convert.ToString(Lat_Min_comm);
            ichislo = (long)(Lat_Sec_comm);
            tbBSec.Text = Convert.ToString(ichislo);

            tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
            tbLMin2.Text = Convert.ToString(Long_Min_comm);
            ichislo = (long)(Long_Sec_comm);
            tbLSec.Text = Convert.ToString(ichislo);

            //} // IF


        } // OtobrSP_ed
        // *************************************************************************************

        // ************************************************************************
        // Функция определения средней высота местности

        private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
        //private int DefineMiddleHeight_Comm(Point tpReferencePoint)
        {
            //int iRadius = 2000;
            int iRadius = 30000;

            int iStep = 100;
            int iCount = 0;
            double dMiddleHeightStep = 0;
            //double dMiddleHeight = 0;
            int iMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                int iMinX = 0;
                int iMinY = 0;
                int iMaxX = 0;
                int iMaxY = 0;

                iMinX = tpReferencePoint.X - iRadius;
                iMinY = tpReferencePoint.Y - iRadius;
                iMaxX = tpReferencePoint.X + iRadius;
                iMaxY = tpReferencePoint.Y + iRadius;

                // пройти по координатам карты с шагом Shag
                //for (int i = (int)dMinX; i < dMaxX; i = i + iStep)
                for (int i = iMinX; i < iMaxX; i = i + iStep)
                {
                    //for (int j = (int)dMinY; j < dMaxY; j = j + iStep)
                    for (int j = iMinY; j < iMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;
                        dSetX = i;
                        dSetY = j;

                        axMapPointTemp.SetPoint(dSetX, dSetY);
                        dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);

                        // 0809
                        // 6_9_18
                        if (dMiddleHeightStep < 0)
                        {
                           // iMiddleHeight = 0;
                            //return iMiddleHeight;
                            dMiddleHeightStep = 0;
                        }
//
                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        //dMiddleHeight = dMiddleHeight + dMiddleHeightStep;
                        iMiddleHeight = (int)((double)iMiddleHeight + dMiddleHeightStep);

                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                //dMiddleHeight = dMiddleHeight / (double)iCount;
                iMiddleHeight = (int)((double)iMiddleHeight / (double)iCount);

                //if (dMiddleHeight < 0)
                //    dMiddleHeight = 0;
                if (iMiddleHeight < 0)
                    iMiddleHeight = 0;

            } // IF

            //return (int)dMiddleHeight;
            return iMiddleHeight;

        }
        // ************************************************************************

        // ************************************************************************
        // функция расчета ДПВ
        // ***

        public int CountDSR(Point p)
        {
/*
            var iOpponAntenComm = Convert.ToInt32(tbOpponentAntenna.Text);
            var heightAntennOwnComm = Convert.ToDouble(tbHAnt.Text);
            var heightTotalOwnComm = heightAntennOwnComm + OwnHeight_comm;
            var iMiddleHeightComm = DefineMiddleHeight_Comm(p, GlobalVarLn.axMapPointGlobalAdd,
                GlobalVarLn.axMapScreenGlobal);
*/

            var iOpponAntenComm = iOpponAnten_comm;
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iMiddleHeightComm = iMiddleHeight_comm;
            var OwnHeightComm = OwnHeight_comm;
            var HeightOpponentComm = HeightOpponent_comm;

            //return CountDSR((int)heightTotalOwnComm, 0, iMiddleHeightComm, iMiddleHeightComm + iOpponAntenComm);
            return CountDSR((int)heightTotalOwnComm, (int)OwnHeightComm, (int)iMiddleHeightComm, (int)HeightOpponentComm);

        }

        private int CountDSR(int iHeightTotalOwn, int iHeightOwnObj, int iHeightMiddle, int iHeightOpponentObj)
        {
            int iDSR = 0;
            int iHeightMin = 0;
            int h1 = 0;
            int h2 = 0;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/*
            // рассчитать ДПВ по формуле
            h1 = iHeightTotalOwn - iHeightMiddle;
            if (h1 < 0) h1 = (int)HeightAntennOwn_comm;
            h2 = iHeightOpponentObj - iHeightMiddle;
            if (h2 < 0) h2 = iOpponAnten_comm;
*/
            // ***
            // iHeightMiddle средняя высота рельефа (радиус 30 км)
            // iHeightOwnObj - высота рельефа СП
            // iHeightTotalOwn - общая высота СП (рельеф+антенна)
            // iHeightOpponentObj - средняя высота+высота антенны объекта
/*
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hant

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hant

            } // iHeightMiddle > iHeightOwnObj
*/
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // ***
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hantop

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hantop

            } // iHeightMiddle > iHeightOwnObj

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



            // ДЛЯ КАРТИНОК *****************************************************
            // 15_10_2018
            // FOR sea_eatrh
/*
            // sea
            h1 = iHeightTotalOwn;
            h2 = iHeightOpponentObj - iHeightMiddle; // Hantop
            //eatrh
            h1 = 23;

           //h1 = iHeightTotalOwn - iHeightMiddle;
           h1 = 23;

            h2 = iHeightOpponentObj - iHeightMiddle; // Hantop
 */
            // ***************************************************** ДЛЯ КАРТИНОК



            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));




            // рассчитать ДПВ по формуле
            //iDSR = 0;
            // iDSR=(int)(4.12*(Math.Pow(iHeightTotalOwn - iHeightMin,0.5)+Math.Pow(iHeightOpponentObj - iHeightMin,0.5)));

            iDSR = iDSR * 1000;

            return iDSR;
        }
        // ************************************************************************
        // функция расчета точек ЗПВ

        void CountPointLSR(Point tpCenterLSR, 
                           int iHeightCenterLSR, // HeightTotalOwn_comm
                           int iHeightAnten,     //HeightAntennOwn_comm
                           double dDSR, 
                           int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var points = CreateLineSightPolygon(tpCenterLSR);
            GlobalVarLn.listPointDSR.AddRange(points);
        }

       // --------------------------------------------------------------------------
        public List<Point> CreateLineSightPolygon(Point tpCenterLSR, int maxRadius = int.MaxValue)
        {
            var dsr = CountDSR(tpCenterLSR);

            // ***
/*
            var heightAntennOwnComm = Convert.ToDouble(tbHAnt.Text);
            var heightTotalOwnComm = heightAntennOwnComm + OwnHeight_comm;
            var iOpponAntenComm = Convert.ToInt32(tbOpponentAntenna.Text);
*/
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iOpponAntenComm = iOpponAnten_comm;


            var points = CreateLineSightPolygon(tpCenterLSR, (int)heightTotalOwnComm, dsr, iOpponAntenComm);

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = p.X - tpCenterLSR.X;
                double dy = p.Y - tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;

                points[i] = new Point((int)x, (int)y);
            }

            return points;
        }
        // --------------------------------------------------------------------------

        // ************************************************************************************
        // 27_09_2018
        // Расчет Зоны прямой видимости
        // ************************************************************************************
        public List<Point> CreateLineSightPolygon(Point tpCenterLSR, 
                                                 int iHeightCenterLSR, // HeightTotalOwn_comm
                                                 double dDSR,
                                                 int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var listKoordReal = new List<Point>();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;

            // otl***
            //String strFileName;
            //strFileName = "RLF.txt";
            //StreamWriter srFile;
            //srFile = new StreamWriter(strFileName);

            // otl***
            //srFile.WriteLine("DSR =" + Convert.ToString(dDSR));

            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO
            // пройти в цикле по всем углам   (против часовой стрелки, 1 град)

            for (angleFi = 0; angleFi < 361; angleFi = angleFi + GlobalVarLn.iStepAngleInput_ZPV)
            {

                // otl***
                //srFile.WriteLine("Fi =" + Convert.ToString(angleFi));

                // .................................................................................
                // найти координаты точки реальной ДПВ (от центра на расстоянии ДПВ)

                KoordThree koord2;

                koord2.y = tpCenterLSR.Y + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.x = tpCenterLSR.X + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                //GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetY, dSetX);
                // koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                if (koord2.h < 0)
                    koord2.h = 0;

                // антенна ОП
                koord2.h += iHeightAntenOpponent;
                // .................................................................................
                // otl***
                //srFile.WriteLine("(1)" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("HC =" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("H2 =" + Convert.ToString(koord2.h));
                // .................................................................................
                // изначально координаты Koord4 равны координатам точки реальной ДПВ

                KoordThree koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;
                // .................................................................................

                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ
                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }
                // .................................................................................

                // Hsp>Hdpv IF1********************************************************************     
                // если высота СП больше высоты точки реальной ДПВ

                // IF1
                KoordThree koord3;
                KoordThree koordPrev;

                // 27_09_2018
                KoordThree koordPrev1;

                if (iHeightCenterLSR > koord2.h)
                {
                    // -----------------------------------------------------------------------------
                    // otl***
                    //srFile.WriteLine("(2) FIRST");

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(3)");
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    // -----------------------------------------------------------------------------

                    // WHILE1 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE1
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(4)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("A =" + Convert.ToString(alfa));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF2 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF2
                        if (koord3.h > H_Line)
                        {
                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            // Словили выход рельефа над линией видимости
                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(5)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));

                            // WHILE2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE2
                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("6");
                                //srFile.WriteLine("dL=" + Convert.ToString(dL));
                                //srFile.WriteLine("H3=" + Convert.ToString(koord3.h));
                                //srFile.WriteLine("HDob=" + Convert.ToString(h_Dob));
                                //srFile.WriteLine("HPrev=" + Convert.ToString(koordPrev.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // otl***
                                    //srFile.WriteLine("Dalshe1");

                                    // 27_09_2018
                                    koordPrev1 = koord3;
                                }
                                else // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while2
                                        dL = dDSR + 1;
                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;
                                        //koordPrev = koord3; // &&&&&&&&&&&

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                        // otl***
                                        // srFile.WriteLine("Dalshe1_1");

                                        // 27_09_2018
                                        koordPrev1 = koord3;
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // ELSE

                            } // WHILE2
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE2

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while1
                            dL = dDSR + 1;

                        } // IF2 (высота рельефа больше воображаемой линии)
                        //  ..................................................................... IF2

                        // ELSE по IF2 ..............................................................
                        // если значение высоты воображаемой линии больше или равно
                        // поверхности земли в этой точке

                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            //rFile.WriteLine("(5_1)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // ELSE po IF2 (высота воображаемой линии больше или равна рельефу в этой точке)
                        // .............................................................. ELSE по IF2

                    } // конец WHILE1 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE1

                    // otl***
                    //srFile.WriteLine("END WHILE: dL<DSR");

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                    // otl***
                    //srFile.WriteLine("(6)");
                    //srFile.WriteLine("X3 =" + Convert.ToString(koord3.x));
                    //rFile.WriteLine("Y3 =" + Convert.ToString(koord3.y));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("X4 =" + Convert.ToString(koord4.x));
                    //srFile.WriteLine("Y4 =" + Convert.ToString(koord4.y));
                    //srFile.WriteLine("H4 =" + Convert.ToString(koord4.h));

                } // IF1 
                // ******************************************************************* Hsp>Hdpv IF1

                // Hsp<Hdpv ELSE po IF1 ***********************************************************
                // если высота СП меньше высоты точки реальной ДПВ   

                else
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(7)");
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));

                    // WHILE3 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE3
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        //H_Line = VarL * Math.Tan(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(8)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF3 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF3
                        // Словили выход рельефа над линией видимости
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(9)");
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));

                            // WHILE4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE4
                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;  // ???????????????????

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;
                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("(10)");
                                //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                                // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // 27_09_2018
                                    koordPrev1 = koord3;

                                    // otl***
                                    //srFile.WriteLine("dalse2");
                                }
                                else  // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit2");
                                        // выйти из цикла while4
                                        dL = dDSR + 1;

                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                        // 27_09_2018
                                        koordPrev1 = koord3;

                                        // otl***
                                        //srFile.WriteLine("Dalshe2");
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // else

                            } // WHILE4 
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE4

                            // выйти из цикла while3
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // otl***
                            //srFile.WriteLine("(11)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // IF3 если значение высоты воображаемой линии меньше поверхности земли в этой точке
                        // ..................................................................... IF3

                        // ELSE po IF3 .............................................................
                        // если значение высоты воображаемой линии блольше или равно
                        // поверхности земли в этой точке   
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                            dSetX = koord3.x;
                            dSetY = koord3.y;
                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            // srFile.WriteLine("(12)");
                            // srFile.WriteLine("dL =" + Convert.ToString(dL));
                            // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // Else po IF3
                        // ............................................................. ELSE po IF3

                    } // конец while3 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE3

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                } // ELSE po IF1 (Hsp<Hdpv)
                // *********************************************************** Hsp<Hdpv ELSE po IF1

            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   
            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO

            //srFile.Close();

            return listKoordReal;
        
        } // Расчет Зоны прямой видимости
        // ************************************************************************************

        private void FormLineSightRange_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

           // GlobalVarLn.fFLineSightRange = 0;

            GlobalVarLn.fl_Open_objFormLineSightRange = 0;

        }

        private void FormLineSightRange_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLineSightRange = 1;
            //ClassMap.f_RemoveFrm(10);

            GlobalVarLn.fl_Open_objFormLineSightRange = 1;

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }


    } // Class
} // Namespace
