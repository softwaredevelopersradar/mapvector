﻿using System;
using System.Drawing;
using AxaxGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GrozaMap
{
    public partial class FormSP : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        private RadioButton[] radioButtons;

        private SPView[] spViews;

        private int _checkedRadioButtonIndex;

        private int CheckedRadioButtonIndex 
        {
            get { return _checkedRadioButtonIndex; }
            set
            {
                if (value == _checkedRadioButtonIndex)
                {
                    return;
                }
                if (value < 0 || value >= radioButtons.Length)
                {
                    return;
                }
                _checkedRadioButtonIndex = value;
                radioButtons[value].Checked = true;
            }
        }

        private int CheckedStationIndex { get { return CheckedRadioButtonIndex / 2; } }

        private bool IsCurrentRadioButtonChecked 
        { 
            get
            {
                return CurrentRadioButton1.Checked || CurrentRadioButton2.Checked;
            }
        }

        public FormSP(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            radioButtons = new[] { CurrentRadioButton1, PlannedRadioButton1, CurrentRadioButton2, PlannedRadioButton2 };
            spViews = new[] {spView1, spView2};

            // sob
            //GlobalVarLn.clientPCG.OnConfirmCoord +=clientPCG_OnConfirmCoord;



        } // Конструктор
        // ***********************************************************  Конструктор

// sob
/*
private void clientPCG_OnConfirmCoord(object sender, byte bAddress, byte bCodeError, double dLatitudeOwn, double dLongitudeOwn, double dLatitudeLinked, double dLongitudeLinked)
{
 	throw new NotImplementedException();
} 
*/

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ACCEPT

        private void bGetFreq_Click(object sender, EventArgs e)
        {
            if (GlobalVarLn.flllsp == 0)
            {
                MessageBox.Show("No connection");
                return;
            }

            GlobalVarLn.clientPCG.SendCoord((byte)GlobalVarLn.AdressOwn);
        }

        public void AcceptGPS()
        {
            GetSPCoordinates(GlobalVarLn.lt1sp, GlobalVarLn.ln1sp);

            var index = 0;
            var position = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.hhsp);

            // Функция, определяет нажатие CurrentRadioButton1||CurrentRadioButton2
            GlobalVarLn.listJS[0].CurrentPosition = position;

            // GlobalVarLn.listJS[index].indzn = GlobalVarLn.iZSP;

            spViews[index].UpdateView(GlobalVarLn.listJS[index]);
            GlobalVarLn.axMapScreenGlobal.Repaint();

            // SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1

            // SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP

            GetSPCoordinates(GlobalVarLn.lt2sp, GlobalVarLn.ln2sp);

            var index1 = 1;
            var position1 = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.hhsp);

            // Функция, определяет нажатие CurrentRadioButton1||CurrentRadioButton2
            GlobalVarLn.listJS[1].CurrentPosition = position1;


            spViews[index1].UpdateView(GlobalVarLn.listJS[index1]);
            GlobalVarLn.axMapScreenGlobal.Repaint();

            // SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP

            GlobalVarLn.flagGPS_SP = 0;
        }
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormSP_Load(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSPG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSP = 1;

            if (GlobalVarLn.flEndTRO_stat != 1)
            {

                // ----------------------------------------------------------------------
                gbRect.Visible = true;
                gbRect.Location = new Point(7, 30);

                gbRect42.Visible = false;
                gbRad.Visible = false;
                gbDegMin.Visible = false;
                gbDegMinSec.Visible = false;

                cbChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPG.chbXY.Checked = false;
                // ----------------------------------------------------------------------
                // Очистка dataGridView
                // .......................................................................
                GlobalVarLn.blSP_stat = true;
                GlobalVarLn.flEndSP_stat = 1;

                GlobalVarLn.XCenter_SP = 0;
                GlobalVarLn.YCenter_SP = 0;
                GlobalVarLn.HCenter_SP = 0;
                GlobalVarLn.flCoord_SP2 = 0;

                GlobalVarLn.iZSP = 0;
                pbSP.Image = imageList1.Images[0];

            }


            //if (GlobalVarLn.flEndTRO_stat != 1)
            //{
                // 0809_3
                //ClassMap.f_RemoveFrm(1);
            //}

            spView1.CurrentLabelClickEvent += spView1_CurrentLabelClickEvent;
            spView1.PlannedlabelClickEvent += spView1_PlannedlabelClickEvent;
            spView1.Map = axaxcMapScreen;
            spView1.UpdateView(GlobalVarLn.listJS[0]);

            spView2.CurrentLabelClickEvent += spView2_CurrentLabelClickEvent;
            spView2.PlannedlabelClickEvent += spView2_PlannedlabelClickEvent;
            spView2.Map = axaxcMapScreen;
            spView2.UpdateView(GlobalVarLn.listJS[1]);

            foreach (var spView in spViews)
            {
                spView.NameChangedEvent += SpViewNameChangedEvent;
                spView.TypeChangedEvent += SpViewTypeChangedEvent;
                spView.IpChangedEvent += SpViewIpChangedEvent;   
            }

            UpdateSPViews();
            axaxcMapScreen.Repaint();
        }

        void SpViewNameChangedEvent(object sender, string name)
        {
            var index = sender == spView1 ? 0 : 1;
            GlobalVarLn.listJS[index].Name = name;
            axaxcMapScreen.Repaint();
        }

        void SpViewTypeChangedEvent(object sender, string type)
        {
            var index = sender == spView1 ? 0 : 1;
            GlobalVarLn.listJS[index].Type = type;
        }

        void SpViewIpChangedEvent(object sender, int ip)
        {
            var index = sender == spView1 ? 0 : 1;
            GlobalVarLn.listJS[index].IP = ip;
        }

        void spView1_CurrentLabelClickEvent(object sender, EventArgs e)
        {
            CurrentRadioButton1.Checked = true;
        } 

        void spView1_PlannedlabelClickEvent(object sender, EventArgs e)
        {
            PlannedRadioButton1.Checked = true;
        }

        void spView2_CurrentLabelClickEvent(object sender, EventArgs e)
        {
            CurrentRadioButton2.Checked = true;
        }

        void spView2_PlannedlabelClickEvent(object sender, EventArgs e)
        {
            PlannedRadioButton2.Checked = true;
        }

        // Загрузка формы
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbXRect42.Text = "";
            tbYRect42.Text = "";
            tbBRad.Text = "";
            tbLRad.Text = "";
            tbBMin1.Text = "";
            tbLMin1.Text = "";
            tbBDeg2.Text = "";
            tbBMin2.Text = "";
            tbBSec.Text = "";
            tbLDeg2.Text = "";
            tbLMin2.Text = "";
            tbLSec.Text = "";

            tbOwnHeight.Text = "";
            tbNumSP.Text = "";
            // -------------------------------------------------------------------
            GlobalVarLn.objFormSPG.chbXY.Checked = false;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.XCenter_SP = 0;
            GlobalVarLn.YCenter_SP = 0;
            GlobalVarLn.HCenter_SP = 0;
            GlobalVarLn.flCoord_SP2 = 0;
            // seg2

            GlobalVarLn.ClearListJS();
            UpdateSPViews();
            // -------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();

            // otl33
            //GlobalVarLn.flEndSP_stat = 0;
            //GlobalVarLn.fclSP = 1;

        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoord_Comm(cbChooseSC.SelectedIndex);

        } // Выбор СК
 
        private void ChooseSystemCoord_Comm(int iSystemCoord)
        {
            gbRect.Visible = false;
            gbRect42.Visible = false;
            gbRad.Visible = false;
            gbDegMin.Visible = false;
            gbDegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    gbRect.Visible = true;
                    gbRect.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {

                        ichislo = (long)(GlobalVarLn.XCenter_SP);
                        tbXRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.YCenter_SP);
                        tbYRect.Text = Convert.ToString(ichislo);

                    } // IF

                    break;
                case 1: // Радианы (Красовский)

                    gbRad.Visible = true;
                    gbRad.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {

                        ichislo = (long)(LatKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 2: // Градусы (Красовский)

                    gbDegMin.Visible = true;
                    gbDegMin.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {

                        ichislo = (long)(LatKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 3: // Градусы,мин,сек (Красовский)

                    gbDegMinSec.Visible = true;
                    gbDegMinSec.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
                        tbBMin2.Text = Convert.ToString(Lat_Min_comm);
                        ichislo = (long)(Lat_Sec_comm);
                        tbBSec.Text = Convert.ToString(ichislo);

                        tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
                        tbLMin2.Text = Convert.ToString(Long_Min_comm);
                        ichislo = (long)(Long_Sec_comm);
                        tbLSec.Text = Convert.ToString(ichislo);

                    } // IF
                    break;

            } // SWITCH

        } 

        private void OtobrSP_Comm()
        {

            // -----------------------------------------------------------------------
            // Метры на местности

            ichislo = (long)(GlobalVarLn.XCenter_SP);
            tbXRect.Text = Convert.ToString(ichislo);

            ichislo = (long)(GlobalVarLn.YCenter_SP);
            tbYRect.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------
            // CDel
            // Метры 1942 года

            //ichislo = (long)(XSP42_comm);
            //tbXRect42.Text = Convert.ToString(ichislo);

            //ichislo = (long)(YSP42_comm);
            //tbYRect42.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------
            // Радианы (Красовский)
            // CDel
            // !!!Здесь это WGS84

            ichislo = (long)(LatKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBRad.Text = Convert.ToString(dchislo);   // X, карта

            ichislo = (long)(LongKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLRad.Text = Convert.ToString(dchislo);   // X, карта
            // -----------------------------------------------------------------------
            // Градусы (Красовский)
            // CDel
            // !!!Здесь это WGS84

            ichislo = (long)(LatKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBMin1.Text = Convert.ToString(dchislo);

            ichislo = (long)(LongKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLMin1.Text = Convert.ToString(dchislo);
            // -----------------------------------------------------------------------
            // Градусы,мин,сек (Красовский)
            // CDel
            // !!!Здесь это WGS84

            tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
            tbBMin2.Text = Convert.ToString(Lat_Min_comm);
            ichislo = (long)(Lat_Sec_comm);
            tbBSec.Text = Convert.ToString(ichislo);

            tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
            tbLMin2.Text = Convert.ToString(Long_Min_comm);
            ichislo = (long)(Long_Sec_comm);
            tbLSec.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------

        } // OtobrSP_comm


// ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
        private void GetSPCoordinates(double lt,double lng)
        {
            // ......................................................................
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_SP = GlobalVarLn.MapX1;
            GlobalVarLn.YCenter_SP = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY.Checked == true)
            {

                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Invalid coordinates of the jammer station");
                    return;
                }

                GlobalVarLn.XCenter_SP = Convert.ToDouble(tbXRect.Text);
                GlobalVarLn.YCenter_SP = Convert.ToDouble(tbYRect.Text);
            }

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.flCoord_SP2 = 1;

            // ......................................................................
            // sob

            if (GlobalVarLn.flagGPS_SP == 1)
            {
                // grad->rad
                lt = (lt * Math.PI) / 180;
                lng = (lng * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

              GlobalVarLn.XCenter_SP = lt;
              GlobalVarLn.YCenter_SP = lng;

            }
            // ......................................................................

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
            GlobalVarLn.hhsp = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);



            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = GlobalVarLn.XCenter_SP;
            ytmp_ed = GlobalVarLn.YCenter_SP;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS
            // CDel
            // !!! Здесь это WGS84

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );

            OtobrSP_Comm();
            // .......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
            GlobalVarLn.HCenter_SP = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
        }
        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
        // 04_10_2018

        private void GetSPCoordinates1(double lt, double lng)
        {
            // ......................................................................
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.flCoord_SP2 = 1;
            // ......................................................................
                // grad->rad
                lt = (lt * Math.PI) / 180;
                lng = (lng * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

                GlobalVarLn.XCenter_SP = lt;
                GlobalVarLn.YCenter_SP = lng;
            // ......................................................................

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
            GlobalVarLn.hhsp = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            // .......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
            GlobalVarLn.HCenter_SP = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
        }

        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        public void f_SP(double X, double Y)
        {
            GetSPCoordinates(0,0);

            tbOwnHeight.Text = Convert.ToString(GlobalVarLn.HCenter_SP);

            var index = CheckedStationIndex;
            var position = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.HCenter_SP);
            if (IsCurrentRadioButtonChecked)
            {
                GlobalVarLn.listJS[index].CurrentPosition = position;
            }
            else
            {
                GlobalVarLn.listJS[index].PlannedPosition = position;                
            }
            GlobalVarLn.listJS[index].indzn = GlobalVarLn.iZSP;

            spViews[index].UpdateView(GlobalVarLn.listJS[index]);
            GlobalVarLn.axMapScreenGlobal.Repaint();
        }
        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        // *****************************************************************************************
        // Обработчик кнопки : сохранить
        // *****************************************************************************************
        private void bAccept_Click(object sender, EventArgs e)
        {
            if (GlobalVarLn.listJS.Count == 0)
            {
                return;
            }

            var formatter = new BinaryFormatter();
            try
            {
                using (var fs = new FileStream("SP.bin", FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, GlobalVarLn.listJS);
                }


                // otl33
                //GlobalVarLn.flEndSP_stat = 1;
                //GlobalVarLn.fclSP = 0;



            }
            catch (Exception exception)
            {
                MessageBox.Show("Can’t save file");
            }
        } // Save in file
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки : read from file
        // *****************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            GlobalVarLn.LoadListJS();
            UpdateSPViews();
            axaxcMapScreen.Repaint();


            // otl33
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.objFormSPG.f_SPReDraw();
            GlobalVarLn.fclSP = 0;


        } // read from file
        // *****************************************************************************************

        public void UpdateSPViews()
        {
            spView1.UpdateView(GlobalVarLn.listJS[0]);
            spView2.UpdateView(GlobalVarLn.listJS[1]);
        }

        // *****************************************************************************************
        // Удалить СП
        // *****************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
            var station = GlobalVarLn.listJS[CheckedStationIndex];

            station.HasCurrentPosition = false;
            station.HasPlannedPosition = false;
            
            UpdateSPViews();

            GlobalVarLn.axMapScreenGlobal.Repaint();
            f_SPReDraw();
        }

        // *****************************************************************************************
        // Button4 EnterSP (Ручной ввод)
        // 04_10_2018
        // *****************************************************************************************

        private void button4_Click(object sender, EventArgs e)
        {

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            double h = 0;
            double Lt = 0;
            double Ln = 0;
            String s = "";
            // ----------------------------------------------------------------------------
            //0/1 station1/2

            var index = CheckedStationIndex;
            // ----------------------------------------------------------------------------
            //Current
            if (IsCurrentRadioButtonChecked)
            {
                //Lat
                s = spViews[index].CurrentLatTextBox.Text;
                try
                {
                    Lt = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Lt = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                //Long
                s = spViews[index].CurrentLongTextBox.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Ln = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Ln = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

            }//Current
            // ...........................................................................
            // Planned

            else
            {

                //Lat
                s = spViews[index].PlannedLatTextBox.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Lt = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Lt = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                //Long
                s = spViews[index].PlannedLongTextBox.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Ln = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Ln = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

            } // Planned
            // ----------------------------------------------------------------------------
            GetSPCoordinates1(Lt, Ln);

            var position = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.HCenter_SP);
            if (IsCurrentRadioButtonChecked)
            {
                GlobalVarLn.listJS[index].CurrentPosition = position;
            }
            else
            {
                GlobalVarLn.listJS[index].PlannedPosition = position;
            }
            GlobalVarLn.listJS[index].indzn = GlobalVarLn.iZSP;

            spViews[index].UpdateView(GlobalVarLn.listJS[index]);
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ----------------------------------------------------------------------------

        } // Button4 (Ручной ввод)
        // *****************************************************************************************


        // *************************************************************************************
        // Перерисовка SP
        // *************************************************************************************
// 1509


        public void f_SPReDraw()
        {


            foreach (var station in GlobalVarLn.listJS)
            {
                if (station.HasCurrentPosition)
                {



                    ClassMap.f_DrawSPXY1(
                        station.CurrentPosition.x,
                        station.CurrentPosition.y,
                        station.Name,
                        (Bitmap) imageList1.Images[station.indzn]);


                    //f_DrawSPXY111(
                    //    station.CurrentPosition.x,
                    //    station.CurrentPosition.y,
                    //    station.Name,
                    //    (Bitmap)imageList1.Images[station.indzn]);

                }
                if (station.HasPlannedPosition)
                {


                    ClassMap.f_DrawSPXYV1(
                        station.PlannedPosition.x,
                        station.PlannedPosition.y,
                        station.Name,
                        (Bitmap) imageList1V.Images[station.indzn]);
 

                    //f_DrawSPXYV111(
                    //    station.PlannedPosition.x,
                    //    station.PlannedPosition.y,
                    //    station.Name,
                    //    (Bitmap)imageList1V.Images[station.indzn]);


                }
            }





/*
            int ii = 0;
            double X = 0;
            double Y = 0;

            for (ii = 0; ii < GlobalVarLn.listJS.Count; ii++)
            {

                if (GlobalVarLn.listJS[ii].HasCurrentPosition == true)
                {
                    //f_DrawSPXY111(
                    //    GlobalVarLn.listJS[ii].CurrentPosition.x,
                    //    GlobalVarLn.listJS[ii].CurrentPosition.y,
                    //    GlobalVarLn.listJS[ii].Name,
                    //    (Bitmap)imageList1.Images[GlobalVarLn.listJS[ii].indzn]);

 

                    // -------------------------------------------------------------------------------------

                    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
                    X = GlobalVarLn.listJS[ii].CurrentPosition.x;
                    Y = GlobalVarLn.listJS[ii].CurrentPosition.y;
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
                    // -------------------------------------------------------------------------------------

                    if (graph != null)
                    {
                        Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                                       (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                                       GlobalVarLn.width_SP,
                                                       GlobalVarLn.height_SP
                                                      );
                        graph.DrawImage((Bitmap)GlobalVarLn.objFormSPG.imageList1.Images[GlobalVarLn.listJS[ii].indzn], rec);

                        // Подпись
                            Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                            Brush brushi = new SolidBrush(Color.White);
                            Brush brushj = new SolidBrush(Color.Blue);
                            Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                              (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                                //40,
                                                              30,
                                                              13
                                                             );


                            graph.DrawString(
                                             GlobalVarLn.listJS[ii].Name,
                                             font2,
                                             brushj,
                                             (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                             (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                             );
                        // -------------------------------------------------------------------------------------

                    } // graph!=0

                }

                if (GlobalVarLn.listJS[ii].HasPlannedPosition == true)
                {
                    f_DrawSPXYV111(
                        GlobalVarLn.listJS[ii].PlannedPosition.x,
                        GlobalVarLn.listJS[ii].PlannedPosition.y,
                        GlobalVarLn.listJS[ii].Name,
                        (Bitmap)imageList1.Images[GlobalVarLn.listJS[ii].indzn]);
                }


            } // FOR
*/


        } // P/P



        private void f_DrawSPXY111(
                                         double X,
                                         double Y,
                                         String s,
                                         Bitmap pbmp
                                         )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );



                Bitmap bmp = new Bitmap("1.bmp");


                //graph.DrawImage(bmp, rec);
                graph.DrawImage(pbmp, rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                if (s != "")
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);
                    Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                      (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                        //40,
                                                      30,
                                                      13
                                                     );

                    //graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                     );
                }
                // -------------------------------------------------------------------------------------

            } // graph!=0


        } // Функция f_DrawSPXY1

        public static void f_DrawSPXYV111(
                                         double X,
                                         double Y,
                                         String s,
                                         Bitmap pbmp
                                         )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );

                Bitmap bmp = new Bitmap("5_1.bmp");
                //graph.DrawImage(bmp, rec);
                graph.DrawImage(pbmp, rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                if (s != "")
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);
                    Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                      (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                        //40,
                                                      30,
                                                      13
                                                     );


                    //graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                     );
                }
                // -------------------------------------------------------------------------------------

            } // graph!=0


        } // Функция f_DrawSPXYV1


        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormSP_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blSP_stat = false;

            //GlobalVarLn.fFSP = 0;

            GlobalVarLn.fl_Open_objFormSP = 0;


        } // Закрыть форму
        // ****************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormSP_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSPG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSP = 1;

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;

           // 0809_3
           //ClassMap.f_RemoveFrm(1);

            GlobalVarLn.fl_Open_objFormSP = 1;

        } // Активизировать форму
        // ****************************************************************************************

        private void lMiddleHeight_Click(object sender, EventArgs e)
        {

        }

        // ****************************************************************************************
        // Значок
        // ****************************************************************************************
        private void buttonZSP_Click(object sender, EventArgs e)
        {
            ///GlobalVarLn.iZSP = (GlobalVarLn.iZSP + 1) % GlobalVarLn.NumbZSP;
            GlobalVarLn.iZSP = (GlobalVarLn.iZSP + 1) % imageList1.Images.Count;

            pbSP.Image = imageList1.Images[GlobalVarLn.iZSP];

            GlobalVarLn.listJS[CheckedStationIndex].indzn = GlobalVarLn.iZSP;

            axaxcMapScreen.Repaint();
        }

        private void label15_Click(object sender, EventArgs e)
        {
        }

        private void CurrentRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = sender as RadioButton;
            if (radioButton.Checked == false)
            {
                return;
            }

            for (var i = 0; i < radioButtons.Length; ++i)
            {
                if (radioButtons[i] == radioButton)
                {
                    CheckedRadioButtonIndex = i;
                    return;
                }
            }
        }

        // 1509Otl
        private void FormSP_FormClosed(object sender, FormClosedEventArgs e)
        {
            //e.Cancel = true;
            //Hide();

        }


    }
}
