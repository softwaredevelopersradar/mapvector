﻿using System;
using System.Drawing;
using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections.Generic;

using System.IO;
using System.Collections.Generic;



namespace GrozaMap
{
    public partial class FormSuppression : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        private double LAMBDA;

        // .....................................................................
        // Координаты СП,ys

        //private uint flCoordSP_comm; // =1-> Выбрали СП
        //private uint flCoordYS1_comm; // =1-> Выбрали YS1
        //private uint flCoordYS2_comm; // =1-> Выбрали YS2

        // Координаты СП на местности в м
        private double XSP_comm;
        private double YSP_comm;
        private double XYS1_comm;
        private double YYS1_comm;
        private double XYS2_comm;
        private double YYS2_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS1_comm;
        private double LongKrG_YS1_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS1_comm;
        private double LongKrR_YS1_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS1_comm;
        private int Lat_Min_YS1_comm;
        private double Lat_Sec_YS1_comm;
        private int Long_Grad_YS1_comm;
        private int Long_Min_YS1_comm;
        private double Long_Sec_YS1_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS142_comm;
        private double YYS142_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS2_comm;
        private double LongKrG_YS2_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS2_comm;
        private double LongKrR_YS2_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS2_comm;
        private int Lat_Min_YS2_comm;
        private double Lat_Sec_YS2_comm;
        private int Long_Grad_YS2_comm;
        private int Long_Min_YS2_comm;
        private double Long_Sec_YS2_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS242_comm;
        private double YYS242_comm;

        // ......................................................................
        // Основные параметры

        private double OwnHeight_comm;
        private double Point1Height_comm;
        private double Point2Height_comm;
        //private double HeightOwnObject_comm;
        private double PowerOwn_comm;
        private double CoeffOwn_comm;
        private double CoeffOwnPod_comm;
        //private double RadiusZone_comm;
        //private double MaxDist_comm;

        private int i_HeightOwnObject_comm;
        private int i_Pt1HeightOwnObject_comm;
        private int i_Cap1_comm;
        private int i_WidthHindrance_comm;
        private int i_Surface_comm;
        private double Cap1_comm;
        private double WidthHindrance_comm;
        //private double Surface_comm;

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;
        private double Pt1HeightTotalOwn_comm;
        private double Pt1HeightTotalOwn1_comm;


        // Для подавляемой линии
        private double Freq_comm;
        private double PowerOpponent_comm;
        private double CoeffTransmitOpponent_comm;
        private double CoeffReceiverOpponent_comm;
        private double RangeComm_comm;
        private double WidthSignal_comm;
        private double HeightTransmitOpponent_comm;
        private double HeightTransmitOpponent1_comm;
        private double HeightReceiverOpponent_comm;
        private double CoeffSupOpponent_comm;
        private int i_PolarOpponent_comm;
        private int i_CoeffSupOpponent_comm;
        private int i_TypeCommOpponent_comm;

        // ......................................................................
        // Зона

        private double dCoeffQ_comm;
        private double dCoeffHE_comm;
        private int iCorrectHeightOwn_comm;
        private int iResultHeightOwn_comm;
        private int iMiddleHeight_comm;
        private int iMinHeight_comm;
        private int iCorrectHeightOpponent_comm;
        private int iResultHeightOpponent_comm;
        private long iMaxDistance_comm;
        private double dGamma_comm;
        //private long liRadiusZone_comm;
        private double dDistanceObject;
        private double dRadiusZoneSup;
        private double dDelta1;
        private double dDelta2;

        private int iGamma;
        private double dKp1;
        private double dKp2;
        private double dCoeffA1;
        private double dCoeffA2;
        private double dRadiusZone1;
        private double dRadiusZone2;
        private double iXmin;
        private double iXmax;

        // ......................................................................
        private int iDistJammerComm1; // расстояние от УС1 до средства подаления
        private int iDistJammerComm2; // расстояние от УС2 до средства подаления
        private int iDistBetweenComm;
        private bool blResultSupress;


        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormSuppression(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;

            LAMBDA = 300000;

            //flCoordSP_comm = 0; // =1-> Выбрали СП
            //flCoordYS1_comm = 0; // =1-> Выбрали YS1
            //flCoordYS2_comm = 0; // =1-> Выбрали YS2

            // .....................................................................
            // Координаты СП

            // Координаты СП на местности в м
            XSP_comm = 0;
            YSP_comm = 0;
            XYS1_comm = 0;
            YYS1_comm = 0;
            XYS2_comm = 0;
            YYS2_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS1_comm = 0;
            LongKrG_YS1_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS1_comm = 0;
            LongKrR_YS1_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS1_comm = 0;
            Lat_Min_YS1_comm = 0;
            Lat_Sec_YS1_comm = 0;
            Long_Grad_YS1_comm = 0;
            Long_Min_YS1_comm = 0;
            Long_Sec_YS1_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS142_comm = 0;
            YYS142_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS2_comm = 0;
            LongKrG_YS2_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS2_comm = 0;
            LongKrR_YS2_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS2_comm = 0;
            Lat_Min_YS2_comm = 0;
            Lat_Sec_YS2_comm = 0;
            Long_Grad_YS2_comm = 0;
            Long_Min_YS2_comm = 0;
            Long_Sec_YS2_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS242_comm = 0;
            YYS242_comm = 0;

            // ......................................................................
            // Основные параметры

            OwnHeight_comm = 0;
            Point1Height_comm = 0;
            Point2Height_comm = 0;
            //HeightOwnObject_comm = 0;
            PowerOwn_comm = 0;
            CoeffOwn_comm = 0;
            CoeffOwnPod_comm = 0;
            //RadiusZone_comm = 0;
            //MaxDist_comm = 0;

            i_HeightOwnObject_comm = 0;
            i_Pt1HeightOwnObject_comm = 0;
            i_Cap1_comm = 0;
            i_WidthHindrance_comm = 0;
            i_Surface_comm = 0;
            Cap1_comm = 0;
            WidthHindrance_comm = 0;
            //Surface_comm = 0;

            // Высота средства подавления
            // ??????????????????????
            HeightAntennOwn_comm = 0;
            HeightTotalOwn_comm = 0;
            Pt1HeightTotalOwn_comm = 0;
            Pt1HeightTotalOwn1_comm = 0;

            // Для подавляемой линии
            Freq_comm = 0;
            PowerOpponent_comm = 0;
            CoeffTransmitOpponent_comm = 0;
            CoeffReceiverOpponent_comm = 0;
            RangeComm_comm = 0;
            WidthSignal_comm = 0;
            HeightTransmitOpponent_comm = 0;
            HeightTransmitOpponent1_comm = 0;
            HeightReceiverOpponent_comm = 0;
            CoeffSupOpponent_comm = 0;
            i_PolarOpponent_comm = 0;
            i_CoeffSupOpponent_comm = 0;
            i_TypeCommOpponent_comm = 0;

            // ......................................................................
            // Зона

            dCoeffQ_comm = 0;
            dCoeffHE_comm = 0;
            iCorrectHeightOwn_comm = 0;
            iResultHeightOwn_comm = 0;
            iMiddleHeight_comm = 0;
            iMinHeight_comm = 0;
            iCorrectHeightOpponent_comm = 0;
            iResultHeightOpponent_comm = 0;
            iMaxDistance_comm = 0;
            dGamma_comm = 0;
            //liRadiusZone_comm = 0;
            dDistanceObject = 0;
            dRadiusZoneSup = 0;

            iGamma = 0;
            dKp1 = 0;
            dKp2 = 0;
            dCoeffA1 = 0;
            dCoeffA2 = 0;
            dRadiusZone1 = 0;
            dRadiusZone2 = 0;
            dDelta1 = 0;
            dDelta2 = 0;

            iXmin = 0;
            iXmax = 0;

            // ......................................................................
            iDistJammerComm1 = 0; // расстояние от УС1 до средства подаления
            iDistJammerComm2 = 0; // расстояние от УС2 до средства подаления
            iDistBetweenComm = 0;
            blResultSupress = false;

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormSuppression_Load(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSuppressionG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSuppr = 1;

            // ----------------------------------------------------------------------
            cbOwnObject.SelectedIndex = 0;
            GlobalVarLn.NumbSP_sup = "";
            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(8, 26);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            gbPt1Rect.Visible = true;
            gbPt1Rect.Location = new Point(6, 11);

            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;

            cbCommChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Средство РП

            cbHeightOwnObject.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Object РП

            cbPt1HeightOwnObject.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Поляризация сигнала

            cbPolarOpponent.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            chbXY.Checked = false;
            // ----------------------------------------------------------------------
            chbXY1.Checked = false;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_Suppression = 0; // Отрисовка зоны
            GlobalVarLn.flCoordSP_sup = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordOP = 0;

            GlobalVarLn.flA_sup = 0;
            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            MapForm.UpdateDropDownList(cbOwnObject);
            // ----------------------------------------------------------------------
            if (GlobalVarLn.listControlJammingZone.Count != 0)
                GlobalVarLn.listControlJammingZone.Clear();
            // ----------------------------------------------------------------------
            //ClassMap.f_RemoveFrm(11);

        }

        private void OnListJSChanged(object sender, EventArgs e)
        {
            MapForm.UpdateDropDownList(cbOwnObject);
        }

        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            //--------------------------------------------------------------------
            cbOwnObject.SelectedIndex = 0;
            GlobalVarLn.NumbSP_sup = "";
            //--------------------------------------------------------------------
            // SP
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbXRect42.Text = "";
            tbYRect42.Text = "";
            tbBRad.Text = "";
            tbLRad.Text = "";
            tbBMin1.Text = "";
            tbLMin1.Text = "";
            tbBDeg2.Text = "";
            tbBMin2.Text = "";
            tbBSec.Text = "";
            tbLDeg2.Text = "";
            tbLMin2.Text = "";
            tbLSec.Text = "";

            // OP
            tbPt1XRect.Text = "";
            tbPt1YRect.Text = "";
            tbPt1XRect42.Text = "";
            tbPt1YRect42.Text = "";
            tbPt1BRad.Text = "";
            tbPt1LRad.Text = "";
            tbPt1BMin1.Text = "";
            tbPt1LMin1.Text = "";
            tbPt1BDeg2.Text = "";
            tbPt1BMin2.Text = "";
            tbPt1BSec.Text = "";
            tbPt1LDeg2.Text = "";
            tbPt1LMin2.Text = "";
            tbPt1LSec.Text = "";

            tbOwnHeight.Text = "";
            tbHeightOwnObject.Text = "";

            tbPt1Height.Text = "";

            tbRadiusZone.Text = "";
            tbRadiusZone1.Text = "";
            // ----------------------------------------------------------------------
            chbXY.Checked = false;
            // ----------------------------------------------------------------------
            chbXY1.Checked = false;
            // ----------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_Suppression = 0; // Отрисовка зоны
            GlobalVarLn.flCoordSP_sup = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordOP = 0;
            GlobalVarLn.flA_sup = 0;

            // ----------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------
            if (GlobalVarLn.listControlJammingZone.Count != 0)
                GlobalVarLn.listControlJammingZone.Clear();
            // ----------------------------------------------------------------------

        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox : Выбор СК SP
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoordSP_sup(cbChooseSC.SelectedIndex);

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox : Выбор СК OP
        // ************************************************************************
        private void cbCommChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoordOP_sup(cbCommChooseSC.SelectedIndex);

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbOwnObject": Выбор SP
        // ************************************************************************
        private void cbOwnObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOwnObject.SelectedIndex == 0)
                GlobalVarLn.NumbSP_sup = "";
            else
                GlobalVarLn.NumbSP_sup = Convert.ToString(cbOwnObject.Items[cbOwnObject.SelectedIndex]);

        }
        // ************************************************************************

        // ************************************************************************
        // Выбор СП
        private void button1_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;

            var objClassMap3_ed = new ClassMap();
            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            int it = 0;
            String strLine3 = "";
            String strLine2 = "";
            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbOwnObject.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_sup = "";
                // !!! реальные координаты на местности карты в м (Plane)

                XSP_comm = GlobalVarLn.MapX1;
                YSP_comm = GlobalVarLn.MapY1;

            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_sup = Convert.ToString(cbOwnObject.Items[cbOwnObject.SelectedIndex]);
                var stations = GlobalVarLn.listJS;
                var station = stations[cbOwnObject.SelectedIndex - 1];

                XSP_comm = station.CurrentPosition.x;
                YSP_comm = station.CurrentPosition.y;
            }
            // ----------------------------------------------------------------------
            // Ручной ввод

            if (chbXY.Checked == true)
            {
                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
                var x = Convert.ToDouble(tbXRect.Text);
                var y = Convert.ToDouble(tbYRect.Text);
                axaxcMapScreen.MapRealToPlaneGeo(ref x, ref y);
                XSP_comm = x;
                YSP_comm = y;
            }
            // ----------------------------------------------------------------------
            // Высота из карты

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(XSP_comm, YSP_comm);
            OwnHeight_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);
            // ----------------------------------------------------------------------

            GlobalVarLn.XCenter_sup = XSP_comm;
            GlobalVarLn.YCenter_sup = YSP_comm;

            ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_sup,  // m
                              GlobalVarLn.YCenter_sup,
                              "");

            // ......................................................................
            //GlobalVarLn.fl_Suppression = 1;
            GlobalVarLn.flCoordSP_sup = 1; // СП выбрана
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XSP_comm;
            ytmp_ed = YSP_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;
            // .......................................................................
            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS
            // CDel
            // !!! Здесь это WGS84

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );

            OtobrSP_sup();
            // .......................................................................


        } // SP
        // ************************************************************************

        // ************************************************************************
        // OP

        private void button2_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            // ......................................................................
            var objClassMap5_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            XYS1_comm = GlobalVarLn.MapX1;
            YYS1_comm = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY1.Checked == true)
            {

                if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
                var x = Convert.ToDouble(tbPt1XRect.Text);
                var y = Convert.ToDouble(tbPt1YRect.Text);
                axaxcMapScreen.MapRealToPlaneGeo(ref x, ref y);
                XYS1_comm = x;
                YYS1_comm = y;

            }

            // ........................................................................
            // Высота из карты

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(XYS1_comm, YYS1_comm);
            Point1Height_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbPt1Height.Text = Convert.ToString(Point1Height_comm);

            // ......................................................................
            // Треугольник(сиий) на карте

            GlobalVarLn.XPoint1_sup = XYS1_comm;
            GlobalVarLn.YPoint1_sup = YYS1_comm;

            // OP
            ClassMap.f_Map_Pol_XY_stat(
                          GlobalVarLn.XPoint1_sup,  // m
                         GlobalVarLn.YPoint1_sup,
                          //2,
                          1,
                          ""
                         );

            GlobalVarLn.flCoordOP = 1; // OP выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XYS1_comm;
            ytmp_ed = YYS1_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_YS1_comm = xtmp1_ed;
            LongKrG_YS1_comm = ytmp1_ed;
            // Широта
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_YS1_comm,

                // Выходные параметры 
                ref Lat_Grad_YS1_comm,
                ref Lat_Min_YS1_comm,
                ref Lat_Sec_YS1_comm

              );

            // Долгота
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_YS1_comm,

                // Выходные параметры 
                ref Long_Grad_YS1_comm,
                ref Long_Min_YS1_comm,
                ref Long_Sec_YS1_comm

              );

            OtobrOP_sup();
            axaxcMapScreen.Repaint();
            // .......................................................................


        } // OP
        // ************************************************************************


        // Расчет зоны MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
        // Button "Принять"

        private void bAccept_Click(object sender, EventArgs e)
        {

            String s1 = "";

            // ------------------------------------------------------------------
            if (GlobalVarLn.flCoordSP_sup == 0)
            {
                MessageBox.Show("Jammer station is not selected");
                return;
            }

            if (GlobalVarLn.flCoordOP == 0)
            {
                MessageBox.Show("Object of jam is not selected");
                return;
            }
            // ------------------------------------------------------------------
            if (GlobalVarLn.listControlJammingZone.Count != 0)
                GlobalVarLn.listControlJammingZone.Clear();
            // ----------------------------------------------------------------------


            GlobalVarLn.fl_Suppression = 1;



            // Ввод параметров ********************************************************
            // !!! Координаты SP,OP уже расчитаны и введены по кнопке 'SP','OP'

            // ------------------------------------------------------------------
            // SP

            // Мощность 

            s1 = tbPowerOwn.Text;
            //PowerOwn_comm = Convert.ToDouble(tbPowerOwn.Text);

            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                PowerOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                PowerOwn_comm = Convert.ToDouble(s1);
            }

            if ((PowerOwn_comm < 10) || (PowerOwn_comm > 300))
            {
                MessageBox.Show("Parameter 'power' out of range 10W - 300W ");
                return;
            }


            // Коэффициент усиления
            s1 = tbCoeffOwn.Text;
            //CoeffOwn_comm = Convert.ToDouble(tbCoeffOwn.Text);

            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                CoeffOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                CoeffOwn_comm = Convert.ToDouble(s1);
            }

            if ((CoeffOwn_comm < 4) || (CoeffOwn_comm > 8))
            {
                MessageBox.Show("Parameter 'gain' out of range 4-8");
                return;
            }

            // Коэффициент подавления
            s1 = tbCoeffSupOwn.Text;
            //CoeffOwnPod_comm = Convert.ToDouble(tbCoeffSupOwn.Text);

            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                CoeffOwnPod_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                CoeffOwnPod_comm = Convert.ToDouble(s1);
            }

            if ((CoeffOwnPod_comm < 1) || (CoeffOwnPod_comm > 100))
            {
                MessageBox.Show("Parameter 'J/S ratio' out of range 1-100");
                return;
            }

            // Высота антенны
            //HeightAntennOwn_comm = Convert.ToDouble(FormLineSightRange.Instance.tbHAnt.Text);
            HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);

/*
            if (HeightAntennOwn_comm < 0 || HeightAntennOwn_comm > 50)
            {
                MessageBox.Show("Parameter 'antenna height' out of range 0m - 50m");
                return;
            }
*/
            // ComboBox (Индексы)
            i_HeightOwnObject_comm = cbHeightOwnObject.SelectedIndex; // Средство РП

            // ------------------------------------------------------------------
            // OP

            // Мощность 
            s1 = textBox2.Text;
            //PowerOpponent_comm = Convert.ToDouble(textBox2.Text);

            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                PowerOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                PowerOpponent_comm = Convert.ToDouble(s1);
            }

            if ((PowerOpponent_comm < 1) || (PowerOpponent_comm > 100))
            {
                MessageBox.Show("Parameter 'power' out of range 1W - 100W");
                return;
            }


            // коэффициент усиления 
            s1 = textBox3.Text;
            //CoeffTransmitOpponent_comm = Convert.ToDouble(textBox3.Text);

            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
            }

            if ((CoeffTransmitOpponent_comm < 1) || (CoeffTransmitOpponent_comm > 10))
            {
                MessageBox.Show("Parameter 'gain' out of range 1-10");
                return;
            }

            // коэффициент подавления
            s1 = textBox4.Text;
            //CoeffReceiverOpponent_comm = Convert.ToDouble(textBox4.Text);

            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                //else s1 = s1.Replace('.', ',');

                CoeffReceiverOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                else s1 = s1.Replace('.', ',');

                CoeffReceiverOpponent_comm = Convert.ToDouble(s1);
            }

            // антенна 
            //HeightTransmitOpponent_comm = Convert.ToDouble(FormLineSightRange.Instance.tbOpponentAntenna.Text);
            HeightTransmitOpponent_comm = Convert.ToDouble(tbOpponentAntenna.Text); // transmitter
            HeightTransmitOpponent1_comm = Convert.ToDouble(tbOpponentAntenna1.Text); // receiver

/*
            if ((HeightTransmitOpponent_comm < 1) || (HeightTransmitOpponent_comm > 500))
            {
                MessageBox.Show("Parameter 'antenna height' out of rangeЗ 1m - 500m");
                return;
            }
            if ((HeightTransmitOpponent1_comm < 1) || (HeightTransmitOpponent_comm > 500))
            {
                MessageBox.Show("Parameter 'antenna height' out of rangeЗ 1m - 500m");
                return;
            }
*/
            // ComboBox (Индексы)
            i_Pt1HeightOwnObject_comm = cbPt1HeightOwnObject.SelectedIndex; // OP
            i_PolarOpponent_comm = cbPolarOpponent.SelectedIndex;       // Поляризация

            // ------------------------------------------------------------------

            // ******************************************************** Ввод параметров

            // SP *********************************************************************
            // Координаты на местности в 

            GlobalVarLn.tpOwnCoordRect_sup.X = (int) GlobalVarLn.XCenter_sup;
            GlobalVarLn.tpOwnCoordRect_sup.Y = (int) GlobalVarLn.YCenter_sup;

            if ((tbXRect.Text == "") || (tbYRect.Text == ""))
            {
                MessageBox.Show("Incorrect data");
                return;
            }
            // ........................................................................
            // Высота из карты

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.tpOwnCoordRect_sup.X, GlobalVarLn.tpOwnCoordRect_sup.Y);
            OwnHeight_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            //tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            GlobalVarLn.H_sup = OwnHeight_comm;
            // ********************************************************************* SP

            // OP *********************************************************************
            // Координаты на местности в м

            var x = Convert.ToDouble(tbPt1XRect.Text);
            var y = Convert.ToDouble(tbPt1YRect.Text);
            axaxcMapScreen.MapRealToPlaneGeo(ref x, ref y);

            GlobalVarLn.tpPoint1Rect_sup.X = (int) x;
            GlobalVarLn.tpPoint1Rect_sup.Y = (int) y;

            if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
            {
                MessageBox.Show("Incorrect data");
                return;
            }
            // ........................................................................
            // Высота из карты

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.tpPoint1Rect_sup.X, GlobalVarLn.tpPoint1Rect_sup.Y);
            Point1Height_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            //tbPt1Height.Text = Convert.ToString(Point1Height_comm);

            // ********************************************************************* OP

            // H средства подавления **************************************************
            // определить значение высоты средства подавления

            i_HeightOwnObject_comm = 0;
            switch (i_HeightOwnObject_comm)
            {
                // рельеф местности+высота антенны
                case 0:
                    HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
                    break;

                // высота антенны
                case 1:
                    HeightTotalOwn_comm = HeightAntennOwn_comm;
                    break;

                // задать самостоятельно
                case 2:
                    if (tbHeightOwnObject.Text == "")
                        HeightTotalOwn_comm = 0;
                    else
                        HeightTotalOwn_comm = Convert.ToDouble(tbHeightOwnObject.Text);

                    break;

            } // Switch

            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            // ************************************************** H средства подавления

            // H объекта подавления ***************************************************
            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.axMapPointGlobalAdd, GlobalVarLn.axMapScreenGlobal);

            i_Pt1HeightOwnObject_comm = 0;
            switch (i_Pt1HeightOwnObject_comm)
            {
                // рельеф местности+высота антенны
                case 0:
                    Pt1HeightTotalOwn_comm = HeightTransmitOpponent_comm + Point1Height_comm;
                    Pt1HeightTotalOwn1_comm = iMiddleHeight_comm + HeightTransmitOpponent1_comm;
                    break;

                // высота антенны
                case 1:
                    Pt1HeightTotalOwn_comm = HeightTransmitOpponent_comm;
                    break;

                // задать самостоятельно
                case 2:
                    Pt1HeightTotalOwn_comm = 0;
                    break;

            } // Switch

            // отобразить значение высоты
            ichislo = (long)(Pt1HeightTotalOwn_comm);

            // *************************************************** H объекта подавления

            // Расчет зоны ************************************************************

            dDistanceObject = DefineDistanceObject(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.tpPoint1Rect_sup);
            //tbCoeffQ.Text = dDistanceObject.ToString();

            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.axMapPointGlobalAdd, GlobalVarLn.axMapScreenGlobal);
            tbCoeffHE.Text = iMiddleHeight_comm.ToString();

            //dRadiusZoneSup = DefineDistanceSup((int)OwnHeight_comm, (int)HeightAntennOwn_comm, (int)Pt1HeightTotalOwn_comm, iMiddleHeight_comm);
            // !!! Receiver
            dRadiusZoneSup = DefineDistanceSup((int)OwnHeight_comm, (int)HeightAntennOwn_comm, (int)Pt1HeightTotalOwn1_comm, iMiddleHeight_comm);

            tbCorrectHeightOwn.Text = dRadiusZoneSup.ToString();
            GlobalVarLn.RZ_sup = dRadiusZoneSup;
            tbRadiusZone.Text = Convert.ToString((int)dRadiusZoneSup);

            iGamma = 1;
            dKp1 = CoeffOwnPod_comm; // К подавления СП
            //dKp2 = CoeffReceiverOpponent_comm; // К подавления ОП

            dCoeffA1 = DefineCoeffA(
                                   (int)PowerOwn_comm,
                                   CoeffOwn_comm, // К усиления СП 
                                   iGamma,
                                   (int)PowerOpponent_comm,
                                   CoeffTransmitOpponent_comm, // К усиления ОП
                                   dKp1,
                                   HeightAntennOwn_comm,
                                   HeightTransmitOpponent_comm,
                                   HeightTransmitOpponent1_comm
                                   );



            //dCoeffA1 = 0.1;


            tbResultHeightOwn.Text = dCoeffA1.ToString();

            dRadiusZone1 = DefineZoneNotSup(dDistanceObject, ref dCoeffA1);
            tbMiddleHeight.Text = dRadiusZone1.ToString();
            GlobalVarLn.RZ1_sup = dRadiusZone1;
            tbRadiusZone1.Text = Convert.ToString((int)dRadiusZone1);

            dDelta1 = DefineDelta(dDistanceObject, dCoeffA1);
            tbCorrectHeightOpponent.Text = dDelta1.ToString();

            GlobalVarLn.tpPointCentre1_sup.X = 0;
            GlobalVarLn.tpPointCentre1_sup.Y = 0;
            if (GlobalVarLn.flA_sup == 2)
              GlobalVarLn.tpPointCentre1_sup = DefineCentreRNS(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.tpPoint1Rect_sup, dDelta1);
            else if (GlobalVarLn.flA_sup == 3)
                GlobalVarLn.tpPointCentre1_sup = DefineCentreRNS(GlobalVarLn.tpPoint1Rect_sup,GlobalVarLn.tpOwnCoordRect_sup, dDelta1);

            tbMaxDistance.Text = GlobalVarLn.tpPointCentre1_sup.X.ToString();
            textBox5.Text = GlobalVarLn.tpPointCentre1_sup.Y.ToString();

            iXmin = 0;
            iXmin = DefineXmin(GlobalVarLn.tpOwnCoordRect_sup.X, dRadiusZoneSup);
            iXmax = 0;
            iXmax = DefineXmax(GlobalVarLn.tpOwnCoordRect_sup.X, dRadiusZoneSup);
            textBox8.Text = iXmin.ToString();
            textBox9.Text = iXmax.ToString();

            GlobalVarLn.tpPointMiddle_sup = DefineCoordMiddleDistance(GlobalVarLn.tpOwnCoordRect_sup, GlobalVarLn.tpPoint1Rect_sup);


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            //GlobalVarLn.listControlJammingZone = FormLineSightRange.Instance.CreateLineSightPolygon(GlobalVarLn.tpOwnCoordRect_sup, 
            //                                                                  (int) dRadiusZoneSup
            //                                                                     );

            GlobalVarLn.listControlJammingZone = CreateLineSightPolygon(GlobalVarLn.tpOwnCoordRect_sup,(int)dRadiusZoneSup);

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            // --------------------------------------------------------------------------- Очистка
            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();
            // --------------------------------------------------------------------------- Очистка

            /*ClassMap.f_Map_Zon_Suppression(
                                           GlobalVarLn.tpOwnCoordRect_sup,
                                           1,
                                           (long)dRadiusZoneSup
                                          ); */
            ClassMap.DrawPolygon(GlobalVarLn.listControlJammingZone, Color.Red);

            if (GlobalVarLn.flA_sup == 2)
            {
                ClassMap.f_Map_Zon_Suppression(
                                               GlobalVarLn.tpPointCentre1_sup,
                                               2,
                                               (long)dRadiusZone1
                                              );

            }

            else if (GlobalVarLn.flA_sup == 3) // SP
            {
                ClassMap.f_Map_Zon_Suppression(
                                               //GlobalVarLn.tpOwnCoordRect_sup,
                                               GlobalVarLn.tpPointCentre1_sup,
                                               2,
                                               (long)dRadiusZone1
                                              );

            }

            else
            {
                MessageBox.Show("A=1");
            }


        } // Zona







 // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        // ************************************************************************
        // функция расчета ДПВ

        public int CountDSR(Point p)
        {
            var iOpponAntenComm = HeightTransmitOpponent1_comm;
            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iMiddleHeightComm = iMiddleHeight_comm;
            var OwnHeightComm = OwnHeight_comm;
            var HeightOpponentComm = Pt1HeightTotalOwn1_comm;

            return CountDSR((int)heightTotalOwnComm, (int)OwnHeightComm, (int)iMiddleHeightComm, (int)HeightOpponentComm);

        }

        private int CountDSR(int iHeightTotalOwn, int iHeightOwnObj, int iHeightMiddle, int iHeightOpponentObj)
        {
            int iDSR = 0;
            int iHeightMin = 0;
            int h1 = 0;
            int h2 = 0;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (iHeightMiddle < iHeightOwnObj)
            {
                h1 = iHeightTotalOwn - iHeightMiddle;
                h2 = iHeightOpponentObj - iHeightMiddle; // Hop=Hsredn+Hant

            } // iHeightMiddle < iHeightOwnObj

            else // iHeightMiddle > iHeightOwnObj
            {
                h1 = iHeightTotalOwn - iHeightOwnObj;
                h2 = iHeightOpponentObj - iHeightOwnObj; // Hop=Hsredn+Hant

            } // iHeightMiddle > iHeightOwnObj

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            iDSR = (int)(4.12 * (Math.Pow(h1, 0.5) + Math.Pow(h2, 0.5)));

            // рассчитать ДПВ по формуле
            iDSR = iDSR * 1000;

            return iDSR;
        }

        // ************************************************************************

        // ************************************************************************
        // функция расчета точек ЗПВ

        void CountPointLSR(Point tpCenterLSR,
                           int iHeightCenterLSR,     // HeightTotalOwn_comm
                           int iHeightAnten,         //HeightAntennOwn_comm
                           double dDSR,
                           int iHeightAntenOpponent) // HeightTransmitOpponent1_comm
        {
            var points = CreateLineSightPolygon(tpCenterLSR);
            GlobalVarLn.listControlJammingZone.AddRange(points);
        }
        // --------------------------------------------------------------------------

        public List<Point> CreateLineSightPolygon(Point tpCenterLSR, int maxRadius = int.MaxValue)
        {
            var dsr = CountDSR(tpCenterLSR);

            var heightAntennOwnComm = HeightAntennOwn_comm;
            var heightTotalOwnComm = HeightTotalOwn_comm;
            var iOpponAntenComm = HeightTransmitOpponent1_comm;


            var points = CreateLineSightPolygon(tpCenterLSR, (int)heightTotalOwnComm, dsr, (int)iOpponAntenComm);

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];

                double dx = p.X - tpCenterLSR.X;
                double dy = p.Y - tpCenterLSR.Y;

                var distance = Math.Sqrt(dx * dx + dy * dy);
                distance = Math.Min(distance, maxRadius);

                var x = tpCenterLSR.X + Math.Sin(i * Math.PI / 180) * distance;
                var y = tpCenterLSR.Y + Math.Cos(i * Math.PI / 180) * distance;

                points[i] = new Point((int)x, (int)y);
            }

            return points;
        }
        // ************************************************************************************
        // 27_09_2018
        // Расчет Зоны прямой видимости
        // ************************************************************************************
        public List<Point> CreateLineSightPolygon(Point tpCenterLSR,
                                                 int iHeightCenterLSR, // HeightTotalOwn_comm
                                                 double dDSR,
                                                 int iHeightAntenOpponent) // iOpponAnten_comm
        {
            var listKoordReal = new List<Point>();

            double h_Dob;
            double dH;
            double alfa;
            double dL;
            double VarL;
            double H_Line;
            int angleFi;

            // otl***
            //String strFileName;
            //strFileName = "RLF.txt";
            //StreamWriter srFile;
            //srFile = new StreamWriter(strFileName);

            // otl***
            //srFile.WriteLine("DSR =" + Convert.ToString(dDSR));

            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO
            // пройти в цикле по всем углам   (против часовой стрелки, 1 град)

            for (angleFi = 0; angleFi < 361; angleFi = angleFi + GlobalVarLn.iStepAngleInput_ZPV)
            {

                // otl***
                //srFile.WriteLine("Fi =" + Convert.ToString(angleFi));

                // .................................................................................
                // найти координаты точки реальной ДПВ (от центра на расстоянии ДПВ)

                KoordThree koord2;

                koord2.y = tpCenterLSR.Y + dDSR * Math.Cos((angleFi * Math.PI) / 180);
                koord2.x = tpCenterLSR.X + dDSR * Math.Sin((angleFi * Math.PI) / 180);

                var dSetX = koord2.x;
                var dSetY = koord2.y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                //GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetY, dSetX);
                // koord2.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                if (koord2.h < 0)
                    koord2.h = 0;

                // антенна ОП
                koord2.h += iHeightAntenOpponent;
                // .................................................................................
                // otl***
                //srFile.WriteLine("(1)" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("HC =" + Convert.ToString(iHeightCenterLSR));
                //srFile.WriteLine("H2 =" + Convert.ToString(koord2.h));
                // .................................................................................
                // изначально координаты Koord4 равны координатам точки реальной ДПВ

                KoordThree koord4;
                koord4.x = koord2.x;
                koord4.y = koord2.y;
                koord4.h = koord2.h;
                // .................................................................................

                // если значение высоты СП совпадает со значением высоты
                // точки реальной ДПВ
                if (Math.Abs(iHeightCenterLSR - koord2.h) < 1e-3)
                {
                    // увеличить первую на 2 м.
                    iHeightCenterLSR = iHeightCenterLSR + 2;
                }
                // .................................................................................

                // Hsp>Hdpv IF1********************************************************************     
                // если высота СП больше высоты точки реальной ДПВ

                // IF1
                KoordThree koord3;
                KoordThree koordPrev;

                // 27_09_2018
                KoordThree koordPrev1;

                if (iHeightCenterLSR > koord2.h)
                {
                    // -----------------------------------------------------------------------------
                    // otl***
                    //srFile.WriteLine("(2) FIRST");

                    // разница высот (см. рис)
                    dH = iHeightCenterLSR - koord2.h;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на STEP_LENGTH метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(3)");
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    // -----------------------------------------------------------------------------

                    // WHILE1 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE1
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dDSR - dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + koord2.h;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(4)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("A =" + Convert.ToString(alfa));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF2 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF2
                        if (koord3.h > H_Line)
                        {
                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            // Словили выход рельефа над линией видимости
                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(5)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));

                            // WHILE2 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE2
                            while (dL < dDSR)
                            {
                                // (см. рис)
                                VarL = dDSR - dL;

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;

                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("6");
                                //srFile.WriteLine("dL=" + Convert.ToString(dL));
                                //srFile.WriteLine("H3=" + Convert.ToString(koord3.h));
                                //srFile.WriteLine("HDob=" + Convert.ToString(h_Dob));
                                //srFile.WriteLine("HPrev=" + Convert.ToString(koordPrev.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // otl***
                                    //srFile.WriteLine("Dalshe1");

                                    // 27_09_2018
                                    koordPrev1 = koord3;
                                }
                                else // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while2
                                        dL = dDSR + 1;
                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;
                                        //koordPrev = koord3; // &&&&&&&&&&&

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;
                                        // otl***
                                        // srFile.WriteLine("Dalshe1_1");

                                        // 27_09_2018
                                        koordPrev1 = koord3;
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // ELSE

                            } // WHILE2
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE2

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // выйти из цикла while1
                            dL = dDSR + 1;

                        } // IF2 (высота рельефа больше воображаемой линии)
                        //  ..................................................................... IF2

                        // ELSE по IF2 ..............................................................
                        // если значение высоты воображаемой линии больше или равно
                        // поверхности земли в этой точке

                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);

                            dSetX = koord3.x;
                            dSetY = koord3.y;

                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            //rFile.WriteLine("(5_1)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // ELSE po IF2 (высота воображаемой линии больше или равна рельефу в этой точке)
                        // .............................................................. ELSE по IF2

                    } // конец WHILE1 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE1

                    // otl***
                    //srFile.WriteLine("END WHILE: dL<DSR");

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                    // otl***
                    //srFile.WriteLine("(6)");
                    //srFile.WriteLine("X3 =" + Convert.ToString(koord3.x));
                    //rFile.WriteLine("Y3 =" + Convert.ToString(koord3.y));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("X4 =" + Convert.ToString(koord4.x));
                    //srFile.WriteLine("Y4 =" + Convert.ToString(koord4.y));
                    //srFile.WriteLine("H4 =" + Convert.ToString(koord4.h));

                } // IF1 
                // ******************************************************************* Hsp>Hdpv IF1

                // Hsp<Hdpv ELSE po IF1 ***********************************************************
                // если высота СП меньше высоты точки реальной ДПВ   

                else
                {
                    // разница высот (см. рис)
                    dH = koord2.h - iHeightCenterLSR;

                    // угол альфа (см. рис)
                    //alfa = Math.Asin(dH / dDSR);
                    alfa = Math.Atan(dH / dDSR);

                    // координаты текущей точки, удаленной от СП на len метров
                    // и отклоненной на angle_fi угол от 0
                    koord3.y = tpCenterLSR.Y + GlobalVarLn.iStepLengthInput_ZPV * Math.Cos((angleFi * Math.PI) / 180);
                    koord3.x = tpCenterLSR.X + GlobalVarLn.iStepLengthInput_ZPV * Math.Sin((angleFi * Math.PI) / 180);
                    dSetX = koord3.x;
                    dSetY = koord3.y;
                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                    koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    if (koord3.h < 0)
                        koord3.h = 0;

                    // расстояние от СП до текущей точки (см. рис)
                    dL = Math.Sqrt((tpCenterLSR.X - koord3.x) * (tpCenterLSR.X - koord3.x) +
                                   (tpCenterLSR.Y - koord3.y) * (tpCenterLSR.Y - koord3.y));

                    // otl***
                    //srFile.WriteLine("(7)");
                    //srFile.WriteLine("dH =" + Convert.ToString(dH));
                    //srFile.WriteLine("A =" + Convert.ToString(alfa));
                    //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));
                    //srFile.WriteLine("dL =" + Convert.ToString(dL));

                    // WHILE3 ----------------------------------------------------------------------
                    // пока не достигнута реальная ДПВ     

                    // WHILE3
                    while (dL < dDSR)
                    {
                        // (см. рис)
                        VarL = dL;

                        // высота воображаемой линии в точки с координатами Koord3
                        //H_Line = VarL * Math.Sin(alfa) + koord2.h;
                        //H_Line = VarL * Math.Tan(alfa) + koord2.h;
                        H_Line = VarL * Math.Tan(alfa) + iHeightCenterLSR;

                        h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                        koord3.h = koord3.h + h_Dob;

                        // 27_09_2018
                        koordPrev1 = koord3;

                        // otl***
                        //srFile.WriteLine("(8)");
                        //srFile.WriteLine("HLine =" + Convert.ToString(H_Line));
                        //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                        //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        // IF3 .....................................................................
                        // если значение высоты воображаемой линии меньше поверхности земли в этой точке

                        // IF3
                        // Словили выход рельефа над линией видимости
                        if (koord3.h > H_Line)
                        {

                            koordPrev.x = 0;
                            koordPrev.y = 0;
                            koordPrev.h = 0;

                            koordPrev = koord3;

                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // otl***
                            //srFile.WriteLine("(9)");
                            //srFile.WriteLine("HPrev =" + Convert.ToString(koordPrev.h));
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));

                            // WHILE4 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            // WHILE4
                            while (dL < dDSR)
                            {
                                VarL = dDSR - dL;  // ???????????????????

                                // расссчитать координаты в этой точке
                                koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                                koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                                dSetX = koord3.x;
                                dSetY = koord3.y;
                                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                                koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                                if (koord3.h < 0)
                                    koord3.h = 0;
                                h_Dob = dL * (dDSR - dL) / (2 * GlobalVarLn.RADIUS_EARTH);
                                koord3.h = koord3.h + h_Dob;

                                // 27_09_2018
                                //koordPrev1 = koord3;

                                // otl***
                                //srFile.WriteLine("(10)");
                                //srFile.WriteLine("HDob =" + Convert.ToString(h_Dob));
                                // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                                // Еще идем вверх
                                if (koord3.h > koordPrev.h)
                                {
                                    koordPrev = koord3;
                                    dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                    // 27_09_2018
                                    koordPrev1 = koord3;

                                    // otl***
                                    //srFile.WriteLine("dalse2");
                                }
                                else  // Пошли вниз
                                {
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                    // 1109

                                    // Уже не видим ОП
                                    if ((koordPrev.h - koord3.h) > iHeightAntenOpponent)
                                    {
                                        // 27_09_2018
                                        //koord3 = koordPrev;
                                        koord3 = koordPrev1;

                                        // otl***
                                        //srFile.WriteLine("Exit2");
                                        // выйти из цикла while4
                                        dL = dDSR + 1;

                                    }
                                    else // Еще видим ОП
                                    {
                                        // otl***
                                        //srFile.WriteLine("Exit1");
                                        // выйти из цикла while
                                        //dL = dDSR + 1;

                                        dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                                        // 27_09_2018
                                        koordPrev1 = koord3;

                                        // otl***
                                        //srFile.WriteLine("Dalshe2");
                                    }
                                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                                } // else

                            } // WHILE4 
                            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> WHILE4

                            // выйти из цикла while3
                            dL = dDSR + 1;

                            // присвоить Koord4 значения текущих координат
                            koord4.x = koord3.x;
                            koord4.y = koord3.y;
                            koord4.h = koord3.h;

                            // otl***
                            //srFile.WriteLine("(11)");
                            //srFile.WriteLine("dL =" + Convert.ToString(dL));
                            //srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // IF3 если значение высоты воображаемой линии меньше поверхности земли в этой точке
                        // ..................................................................... IF3

                        // ELSE po IF3 .............................................................
                        // если значение высоты воображаемой линии блольше или равно
                        // поверхности земли в этой точке   
                        else
                        {
                            // увеличить рсстояние от СП на STEP_LENGTH метров
                            dL = dL + GlobalVarLn.iStepLengthInput_ZPV;

                            // расссчитать координаты в этой точке
                            koord3.y = tpCenterLSR.Y + dL * Math.Cos((angleFi * Math.PI) / 180);
                            koord3.x = tpCenterLSR.X + dL * Math.Sin((angleFi * Math.PI) / 180);
                            dSetX = koord3.x;
                            dSetY = koord3.y;
                            GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);
                            koord3.h = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                            if (koord3.h < 0)
                                koord3.h = 0;

                            // otl***
                            // srFile.WriteLine("(12)");
                            // srFile.WriteLine("dL =" + Convert.ToString(dL));
                            // srFile.WriteLine("H3 =" + Convert.ToString(koord3.h));

                        } // Else po IF3
                        // ............................................................. ELSE po IF3

                    } // конец while3 (dL<dDSR)
                    // ---------------------------------------------------------------------- WHILE3

                    // записать конечный результат расчета координат
                    // с углом angle_fi
                    listKoordReal.Add(new Point((int)koord4.x, (int)koord4.y));

                } // ELSE po IF1 (Hsp<Hdpv)
                // *********************************************************** Hsp<Hdpv ELSE po IF1

            } // конец  for (angle_fi=0; angle_fi<361; angle_fi = angle_fi+STEP_ANGLE,j++ )   
            // FORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFORFO

            //srFile.Close();

            return listKoordReal;
        } // Расчет Зоны прямой видимости
        // ************************************************************************************



// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!







        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
        private void ChooseSystemCoordSP_sup(int iSystemCoord)
        {
            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(8, 26);

                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {
                        var pos = axaxcMapScreen.MapPlaneToRealGeo(XSP_comm, YSP_comm);
                        tbXRect.Text = pos.X.ToString("F3");
                        tbYRect.Text = pos.Y.ToString("F3");

                    } // IF

                    break;
                case 1: // Радианы (Красовский)

                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(8, 27);

                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {

                        ichislo = (long)(LatKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 2: // Градусы (Красовский)

                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(8, 27);

                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {

                        ichislo = (long)(LatKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 3: // Градусы,мин,сек (Красовский)

                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(8, 27);

                    if (GlobalVarLn.flCoordSP_sup == 1)
                    {

                        tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
                        tbBMin2.Text = Convert.ToString(Lat_Min_comm);
                        ichislo = (long)(Lat_Sec_comm);
                        tbBSec.Text = Convert.ToString(ichislo);

                        tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
                        tbLMin2.Text = Convert.ToString(Long_Min_comm);
                        ichislo = (long)(Long_Sec_comm);
                        tbLSec.Text = Convert.ToString(ichislo);

                    } // IF


                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoordSP_sup

        private void ChooseSystemCoordOP_sup(int iSystemCoord)
        {
            gbPt1Rect.Visible = false;
            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    gbPt1Rect.Visible = true;
                    gbPt1Rect.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        var pos = axaxcMapScreen.MapPlaneToRealGeo(XYS1_comm, YYS1_comm);

                        tbPt1XRect.Text = pos.X.ToString("F3");
                        tbPt1YRect.Text = pos.Y.ToString("F3");
                    }

                    break;
                case 1: // Радианы (Красовский)

                    gbPt1Rad.Visible = true;
                    gbPt1Rad.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        ichislo = (long)(LatKrR_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(LongKrR_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LRad.Text = Convert.ToString(dchislo);
                    }

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 2: // Градусы (Красовский)

                    gbPt1DegMin.Visible = true;
                    gbPt1DegMin.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        ichislo = (long)(LatKrG_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(LongKrG_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LMin1.Text = Convert.ToString(dchislo);
                    }

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 3: // Градусы,мин,сек (Красовский)

                    gbPt1DegMinSec.Visible = true;
                    gbPt1DegMinSec.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordOP == 1)
                    {
                        tbPt1BDeg2.Text = Convert.ToString(Lat_Grad_YS1_comm);
                        tbPt1BMin2.Text = Convert.ToString(Lat_Min_YS1_comm);
                        ichislo = (long)(Lat_Sec_YS1_comm);
                        tbPt1BSec.Text = Convert.ToString(ichislo);
                        tbPt1LDeg2.Text = Convert.ToString(Long_Grad_YS1_comm);
                        tbPt1LMin2.Text = Convert.ToString(Long_Min_YS1_comm);
                        ichislo = (long)(Long_Sec_YS1_comm);
                        tbPt1LSec.Text = Convert.ToString(ichislo);
                    }

                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoordOP_sup


        private void OtobrSP_sup()
        {
            // Метры на местности
            //if (gbOwnRect.Visible == true)
            //{

            var pos = axaxcMapScreen.MapPlaneToRealGeo(XSP_comm, YSP_comm);

            tbXRect.Text = pos.X.ToString("F3");
            tbYRect.Text = pos.Y.ToString("F3");

            ichislo = (long) (LatKrR_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbBRad.Text = Convert.ToString(dchislo); // X, карта

            ichislo = (long) (LongKrR_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbLRad.Text = Convert.ToString(dchislo); // X, карта

            ichislo = (long) (LatKrG_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbBMin1.Text = Convert.ToString(dchislo);

            ichislo = (long) (LongKrG_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbLMin1.Text = Convert.ToString(dchislo);

            //} // IF

            // Градусы,мин,сек (Красовский)
            // CDel
            // !!!Здесь это WGS84
            //else if (gbOwnDegMinSec.Visible == true)
            //{
            tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
            tbBMin2.Text = Convert.ToString(Lat_Min_comm);
            ichislo = (long) (Lat_Sec_comm);
            tbBSec.Text = Convert.ToString(ichislo);

            tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
            tbLMin2.Text = Convert.ToString(Long_Min_comm);
            ichislo = (long) (Long_Sec_comm);
            tbLSec.Text = Convert.ToString(ichislo);

            //} // IF


        } // OtobrSP_sup

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
        private void OtobrOP_sup()
        {
            var pos = axaxcMapScreen.MapPlaneToRealGeo(XYS1_comm, YYS1_comm);

            tbPt1XRect.Text = pos.X.ToString("F3");
            tbPt1YRect.Text = pos.Y.ToString("F3");

            ichislo = (long) (LatKrR_YS1_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbPt1BRad.Text = Convert.ToString(dchislo); // X, карта

            ichislo = (long) (LongKrR_YS1_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbPt1LRad.Text = Convert.ToString(dchislo); // X, карта

            ichislo = (long) (LatKrG_YS1_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbPt1BMin1.Text = Convert.ToString(dchislo);

            ichislo = (long) (LongKrG_YS1_comm * 1000000);
            dchislo = ((double) ichislo) / 1000000;
            tbPt1LMin1.Text = Convert.ToString(dchislo);

            tbPt1BDeg2.Text = Convert.ToString(Lat_Grad_YS1_comm);
            tbPt1BMin2.Text = Convert.ToString(Lat_Min_YS1_comm);
            ichislo = (long) (Lat_Sec_YS1_comm);
            tbPt1BSec.Text = Convert.ToString(ichislo);

            tbPt1LDeg2.Text = Convert.ToString(Long_Grad_YS1_comm);
            tbPt1LMin2.Text = Convert.ToString(Long_Min_YS1_comm);
            ichislo = (long) (Long_Sec_YS1_comm);
            tbPt1LSec.Text = Convert.ToString(ichislo);

            //} // IF

        } // OtobrOP_sup


        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
          
        // ************************************************************************



        // *************************************************************** FUNCTIONS

        // **********************************************************************************
        // функция определения расстояния между средством и объектом подавления
        private double DefineDistanceObject(Point tpPointOwn, Point tpPointOpponent)
        {
            double dDistObject = 0;
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = (double)tpPointOwn.X;
            y1 = (double)tpPointOwn.Y;
            x2 = (double)tpPointOpponent.X;
            y2 = (double)tpPointOpponent.Y;

            // seg2
            //dDistObject = Math.Sqrt((tpPointOwn.X - tpPointOpponent.X) * (tpPointOwn.X - tpPointOpponent.X) +
            //    (tpPointOwn.Y - tpPointOpponent.Y) * (tpPointOwn.Y - tpPointOpponent.Y));
            //dDistObject = Math.Sqrt((tpPointOwn.X/1000 - tpPointOpponent.X/1000) * (tpPointOwn.X/1000 - tpPointOpponent.X/1000) +
            //    (tpPointOwn.Y/1000 - tpPointOpponent.Y/1000) * (tpPointOwn.Y/1000 - tpPointOpponent.Y/1000));
            dDistObject = Math.Sqrt((x1 - x2) * (x1 - x2) +
                (y1 - y2) * (y1 - y2));

            //dDistObject = dDistObject * 1000;

            return dDistObject;

        }
        // **********************************************************************************
        // Радиус зоны подавления

        private double DefineDistanceSup(int iHeightCoordOwn, int iHeightAntenOwn, int iHeightPlaneOpponent, int mdl)
        {
            double dDistSup = 0;
            double l_p = 0;
            double l_pr = 0;

/*
            l_p = iHeightCoordOwn + iHeightAntenOwn - mdl;
            if (l_p < 0)
            {
                l_p = 0;
            }

            l_pr = iHeightPlaneOpponent - mdl;

            if (l_pr < 0)
            {
                l_pr = 0;
            }
 */


            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            double mdl1 = Math.Min(iHeightCoordOwn, mdl);

            l_p = iHeightCoordOwn + iHeightAntenOwn - mdl1;
            l_pr = iHeightPlaneOpponent - mdl1;
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            dDistSup = 4120 * (Math.Sqrt(l_p) + Math.Sqrt(l_pr));

            return dDistSup;
        }
        // **********************************************************************************
        // Функция определения средней высота местности

        private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
        {
            //int iRadius = 2000;
            int iRadius = 30000;

            int iStep = 100;
            int iCount = 0;
            double dMiddleHeightStep = 0;
            double dMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                double dMinX = 0;
                double dMinY = 0;

                double dMaxX = 0;
                double dMaxY = 0;

                dMinX = tpReferencePoint.X - iRadius;
                dMinY = tpReferencePoint.Y - iRadius;

                dMaxX = tpReferencePoint.X + iRadius;
                dMaxY = tpReferencePoint.Y + iRadius;

                // пройти по координатам карты с шагом Shag
                for (int i = (int)dMinX; i < dMaxX; i = i + iStep)
                {
                    for (int j = (int)dMinY; j < dMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;

                        dSetX = i;
                        dSetY = j;

                        //GlobalVar::axMapPointGlobal.SetPoint(dSetX,dSetY);
                        //dMiddleHeightStep = 0;
                        //dMiddleHeightStep = GlobalVar::axMapScreenGlobal->PointHeight_get(GlobalVar::axMapPointGlobal);

                        axMapPointTemp.SetPoint(dSetX, dSetY);
                        dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);

                        // 0809
/*
                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeight = 0;
                            return (int)dMiddleHeight;
                        }
*/
                        // 6_9_18
                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeightStep = 0;
                        }

                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        dMiddleHeight = dMiddleHeight + dMiddleHeightStep;
                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                dMiddleHeight = dMiddleHeight / (double)iCount;

                if (dMiddleHeight < 0)
                    dMiddleHeight = 0;
            }

            return (int)dMiddleHeight;
        }
        // ************************************************************************
        // функция определения коэффициента A
        private double DefineCoeffA(int iPowerOwn, double dCoeffOwn, int iGamma, int iPowerOpponent, double dCoeffOpponent, double dKp,double HASP,double HAOP,double HAOP1)
        {
            double dCoeffA = 0;
            double A1 = 0;

            if ((HAOP > 50)||(HAOP1>50))
                dCoeffA = Math.Sqrt((iPowerOwn * dCoeffOwn * iGamma) / (iPowerOpponent * dCoeffOpponent * dKp));
            else
            {
                A1 = ((iPowerOwn * dCoeffOwn * iGamma) / (iPowerOpponent * dCoeffOpponent * dKp)) *
                    ((HASP * HASP) / (HAOP * HAOP));

                dCoeffA = Math.Pow(A1, 0.25);

            }
            return dCoeffA;
        }
        // ************************************************************************
        // функция определения зоны неподавления
        private double DefineZoneNotSup(double dDistObj, ref double dCoeffA)
        {
            double dRadZone = 0;
            double A2=0;
            
            //if (Math.Abs(dCoeffA - 1) < 0.02)
            if (Math.Abs(dCoeffA - 1) < 0.001)

            {
                //dCoeffA = 1.02;
                dCoeffA = 1.001;

            }

            //if (dCoeffA > 1 - 0.02)
            if (dCoeffA > 1 - 0.001)

            {
                GlobalVarLn.flA_sup = 2;
                A2 = dCoeffA;
            }
            else
            {
                GlobalVarLn.flA_sup = 3; // SP
                A2 = 1 / dCoeffA;
            }

            dRadZone = (dDistObj * A2) / (A2 * A2 - 1);

            return dRadZone;
        }
        // ************************************************************************
        // функция определения дельта
        private double DefineDelta(double dDistObj, double dCoeffA)
        {
            double dDelta = 0;
            double A1 = 0;


/*
            if (dCoeffA > 1)
            {
                dDelta = dDistObj / (dCoeffA * dCoeffA - 1);

            }
            else if (dCoeffA < 1)
            {
                dDelta = dDistObj / (1 - dCoeffA * dCoeffA);

            }

            else
                return 0;
*/

            if (dCoeffA > 1)
            {
                A1 = dCoeffA;
            }
            else 
            {
                A1 = 1 / dCoeffA;
            }

            dDelta = dDistObj / (A1 * A1 - 1);

            return dDelta;
        }
        // ************************************************************************
        private Point DefineCentreRNS(Point tpPointOwn, Point tpPointOpponent, double dDelta)
        {
            //TODO: Add your source code here

            Point tpPointCentre = new Point();
            //tpPointCentre.X = 0;
            //tpPointCentre.Y = 0;

            double dHypotenuse = 0;
            double dLegTriangle1 = 0;
            double dLegTriangle2 = 0;
            double dLegTriangle3 = 0;

            double dAlfaRad = 0;

            // seg2
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = (double)tpPointOwn.X;
            y1 = (double)tpPointOwn.Y;
            x2 = (double)tpPointOpponent.X;
            y2 = (double)tpPointOpponent.Y;

            if (dDelta == 0)
            {
                tpPointCentre.X = 0;
                tpPointCentre.Y = 0;
                return tpPointCentre;
            }

            tpPointCentre.X = 0;
            tpPointCentre.Y = 0;



            if (tpPointOwn.X == tpPointOpponent.X)
                tpPointOpponent.X = tpPointOpponent.X + 1;

            if (tpPointOwn.Y == tpPointOpponent.Y)
                tpPointOpponent.Y = tpPointOpponent.Y + 1;


            // seg2
            //dHypotenuse = Math.Sqrt((tpPointOwn.X/1000 - tpPointOpponent.X/1000) * (tpPointOwn.X/1000 - tpPointOpponent.X/1000)
            //    + (tpPointOwn.Y/1000 - tpPointOpponent.Y/1000) * (tpPointOwn.Y/1000 - tpPointOpponent.Y/1000));
            dHypotenuse = Math.Sqrt((x1 - x2) * (x1 - x2) +
                (y1 - y2) * (y1 - y2));

            // если попала точка в первую четверть
            if ((tpPointOpponent.X > tpPointOwn.X) & (tpPointOpponent.Y > tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOpponent.X - tpPointOwn.X;

                dAlfaRad = Math.Asin(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X + dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Cos(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y + dLegTriangle3);

            }


            // если попала точка во вторую четверть
            if ((tpPointOpponent.X > tpPointOwn.X) & (tpPointOpponent.Y < tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOpponent.X - tpPointOwn.X;

                dAlfaRad = Math.Acos(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X + dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Sin(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y - dLegTriangle3);
            }



            // если попала точка в третюю четверть
            if ((tpPointOpponent.X < tpPointOwn.X) & (tpPointOpponent.Y < tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOwn.X - tpPointOpponent.X;

                dAlfaRad = Math.Asin(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Sin(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X - dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Cos(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y - dLegTriangle3);
            }



            // если попала точка в третюю четверть
            if ((tpPointOpponent.X < tpPointOwn.X) & (tpPointOpponent.Y > tpPointOwn.Y))
            {
                dLegTriangle1 = 0;
                dLegTriangle1 = tpPointOwn.X - tpPointOpponent.X;

                dAlfaRad = Math.Acos(dLegTriangle1 / dHypotenuse);


                dLegTriangle2 = 0;
                dLegTriangle2 = dDelta * Math.Cos(dAlfaRad);

                tpPointCentre.X = (int)(tpPointOpponent.X - dLegTriangle2);

                dLegTriangle3 = 0;
                dLegTriangle3 = dDelta * Math.Sin(dAlfaRad);


                tpPointCentre.Y = (int)(tpPointOpponent.Y + dLegTriangle3);
            }

            return tpPointCentre;
        }
        // ************************************************************************
        private int DefineXmin(int iPointOwnX, double dRadZoneSup)
        {
            //TODO: Add your source code here
            int iXmin = 0;

            if (iPointOwnX != 0)
            {
                iXmin = (int)(iPointOwnX - dRadZoneSup);
            }

            return iXmin;
        }

        private int DefineXmax(int iPointOwnX, double dRadZoneSup)
        {
            int iXmax = 0;

            if (iPointOwnX != 0)
            {
                iXmax = (int)(iPointOwnX + dRadZoneSup);
            }

            return iXmax;
        }
        // ************************************************************************
        private Point DefineCoordMiddleDistance(Point tpPointOwn, Point tpPointOpponent)
        {

            Point tpPointMiddle = new Point();

            tpPointMiddle.X = 0;
            tpPointMiddle.Y = 0;

            if ((tpPointOwn.X != 0) & (tpPointOwn.Y != 0) & (tpPointOpponent.X != 0) & (tpPointOpponent.Y != 0))
            {
                tpPointMiddle.X = (tpPointOwn.X + tpPointOpponent.X) / 2;
                tpPointMiddle.Y = (tpPointOwn.Y + tpPointOpponent.Y) / 2;
            }

            return tpPointMiddle;

        }
        // ************************************************************************


        private void DefLuch(Point tpPointOwn, Point tpPointOp)
        {


            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double x3 = 0;
            double y3 = 0;
            double x4 = 0;
            double y4 = 0;
            double dx = 0;
            double dy = 0;
            double alf = 0;
            double bet = 0;
            double x0 = 0;
            double y0 = 0;
            double D = 0;
            double a = 0;

            GlobalVarLn.Luch_sup3.X = 0;
            GlobalVarLn.Luch_sup3.Y = 0;
            GlobalVarLn.Luch_sup4.X = 0;
            GlobalVarLn.Luch_sup4.Y = 0;

            // IF1
            if ((tpPointOwn.X != 0) & (tpPointOwn.Y != 0) & (tpPointOp.X != 0) & (tpPointOp.Y != 0))
            {
                x1 = (double)(tpPointOwn.X);
                y1 = (double)(tpPointOwn.Y);
                x2 = (double)(tpPointOp.X);
                y2 = (double)(tpPointOp.Y);
                // ----------------------------------------------------------------------------------
                if ((y1 == y2) && (x2 > x1))
                {
                    x3 = (x2 - x1) / 2;
                    x4 = (x2 - x1) / 2;
                    y3 = y1 - GlobalVarLn.s_sup;
                    y4 = y1 + GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((y1 == y2) && (x2 < x1))
                {
                    x3 = (x1 - x2) / 2;
                    x4 = (x1 - x2) / 2;
                    y3 = y1 - GlobalVarLn.s_sup;
                    y4 = y1 + GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((x1 == x2) && (y2 > y1))
                {
                    y3 = (y2 - y1) / 2;
                    y4 = (y2 - y1) / 2;
                    x3 = x1 + GlobalVarLn.s_sup;
                    x4 = x1 - GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((x1 == x2) && (y1 > y2))
                {
                    y3 = (y1 - y2) / 2;
                    y4 = (y1 - y2) / 2;
                    x3 = x1 + GlobalVarLn.s_sup;
                    x4 = x1 - GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((x2 > x1) && (y1 < y2))
                {
                    dx = x2 - x1;
                    dy = y2 - y1;
                    alf = Math.Atan(dx/dy);
                    x0=(x2-x1)/2;
                    y0=(y2-y1)/2;
                    D = Math.Sqrt(dx*dx+dy*dy);
                    a = Math.Sqrt((D / 2) * (D / 2) + GlobalVarLn.s_sup * GlobalVarLn.s_sup);
                    bet = Math.Atan(GlobalVarLn.s_sup/(D/2));
                    y3 = y1 + a * Math.Cos(alf+bet);
                    x3 = x1 + a * Math.Sin(alf+bet);
                    y4 = y1 + a * Math.Cos(alf - bet);
                    x4 = x1 - a * Math.Sin(alf-  bet);

                }
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------




            } // IF1

            GlobalVarLn.Luch_sup3.X = (int)x3;
            GlobalVarLn.Luch_sup3.Y = (int)y3;
            GlobalVarLn.Luch_sup4.X = (int)x4;
            GlobalVarLn.Luch_sup4.Y = (int)y4;


        }
        // ************************************************************************





        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormSuppression_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            //GlobalVarLn.fFSuppr = 0;

            GlobalVarLn.fl_Open_objFormSuppression = 0;

        } // Closing
        // ****************************************************************************************


        private void label20_Click(object sender, EventArgs e)
        {
            ;
        }

        private void tbRadiusZone1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FormSuppression_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSuppressionG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSuppr = 1;
            //ClassMap.f_RemoveFrm(11);

            GlobalVarLn.fl_Open_objFormSuppression = 1;

        }


    } // Class
} // Namespace
