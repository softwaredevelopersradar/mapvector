﻿using System;
using AxaxGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Linq;

namespace GrozaMap
{
    public partial class FormAz1 : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        // Конструктор *********************************************************** 

        public FormAz1(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormAz1_Load(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormAz1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFAz1 = 1;

            // ----------------------------------------------------------------------
            gbRect.Visible = true;
            //gbRect.Location = new Point(7, 30);

            //gbRect42.Visible = false;
            //gbRad.Visible = false;
            //gbDegMin.Visible = false;
            //gbDegMinSec.Visible = false;

            //cbChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            chbXY.Checked = false;
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            GlobalVarLn.objFormAz1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB_az1; i++)
            {
                GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("", "", "", "");
            }
            // ----------------------------------------------------------------------
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 0;
            GlobalVarLn.XCenter_az1 = 0;
            GlobalVarLn.YCenter_az1 = 0;
            GlobalVarLn.HCenter_az1 = 0;

            // otl33
            //GlobalVarLn.flEndSP_stat = GlobalVarLn.flEndSP_stat;
            //GlobalVarLn.LoadListJS();

            UpdateDataGridView();

            //ClassMap.f_RemoveFrm(9);

        } // Load_form

        private void OnListJSChanged(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void UpdateDataGridView()
        {
            var stations = GlobalVarLn.listJS.ToList();

            for (var i = 0; i < stations.Count && i < dataGridView1.RowCount; ++i)
            {
                var row = dataGridView1.Rows[i];
                var pos = axaxcMapScreen.MapPlaneToRealGeo(
                    stations[i].CurrentPosition.x,
                    stations[i].CurrentPosition.y);

                row.Cells[0].Value = stations[i].Name;
                row.Cells[1].Value = pos.X.ToString("F3");
                row.Cells[2].Value = pos.Y.ToString("F3");
            }
        }

        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbOwnHeight.Text = "";
            // -------------------------------------------------------------------
            chbXY.Checked = false;
            
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 0;
            GlobalVarLn.XCenter_az1 = 0;
            GlobalVarLn.YCenter_az1 = 0;
            GlobalVarLn.HCenter_az1 = 0;
            // ----------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // -------------------------------------------------------------------


        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChooseSystemCoord_az1(cbChooseSC.SelectedIndex);

        } // Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Принять (посчитать азимут)
        // ************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            f_OB_az1(0, 0);

            var objClassMap6 = new ClassMap();
            var stations = GlobalVarLn.listJS
                .Where(s => s.HasCurrentPosition)
                .ToList();


            for (var i = 0; i < stations.Count; i++)
            {
                var X_Coordl3_1 = stations[i].CurrentPosition.x;
                var Y_Coordl3_1 = stations[i].CurrentPosition.y;
                var X_Coordl3_2 = GlobalVarLn.XCenter_az1;
                var Y_Coordl3_2 = GlobalVarLn.YCenter_az1;

                // Разность координат для расчета азимута
                // На карте X - вверх
                var DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                var DYY3 = X_Coordl3_2 - X_Coordl3_1;

                // grad
                var azz = objClassMap6.f_Def_Azimuth(DYY3, DXX3);

                ichislo = (long) (azz * 100);
                dchislo = ((double) ichislo) / 100;
                dataGridView1.Rows[i].Cells[3].Value = (int) azz;
            }





            axaxcMapScreen.Repaint();

        }

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
        private void OtobrOB_az1()
        {

            // -----------------------------------------------------------------------
            // Метры на местности

            var p = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);
            tbXRect.Text = p.X.ToString("F3");
            tbYRect.Text = p.Y.ToString("F3");
            // ----------------------------------------------------------------------
            // CDel
            // Метры 1942 года

            //ichislo = (long)(XSP42_comm);
            //tbXRect42.Text = Convert.ToString(ichislo);

            //ichislo = (long)(YSP42_comm);
            //tbYRect42.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------
            // Радианы (Красовский)
            // CDel
            // !!!Здесь это WGS84

            ichislo = (long)(LatKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            //tbBRad.Text = Convert.ToString(dchislo);   // X, карта

            ichislo = (long)(LongKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            //tbLRad.Text = Convert.ToString(dchislo);   // X, карта
            // -----------------------------------------------------------------------
            // Градусы (Красовский)
            // CDel
            // !!!Здесь это WGS84

            ichislo = (long)(LatKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            //tbBMin1.Text = Convert.ToString(dchislo);

            ichislo = (long)(LongKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            //tbLMin1.Text = Convert.ToString(dchislo);
            // -----------------------------------------------------------------------
            // Градусы,мин,сек (Красовский)
            // CDel
            // !!!Здесь это WGS84

            //tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
            //tbBMin2.Text = Convert.ToString(Lat_Min_comm);
            ichislo = (long)(Lat_Sec_comm);
            //tbBSec.Text = Convert.ToString(ichislo);

            //tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
            //tbLMin2.Text = Convert.ToString(Long_Min_comm);
            ichislo = (long)(Long_Sec_comm);
            //tbLSec.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------

        } // OtobrOB_az1


        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
  
        // ************************************************************************

        // ************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке Object
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ************************************************************************

        // OLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLD
        // CDel
        // !!! СТАРЫЙ ВАРИАНТ -> ОСТАВИТЬ

/*
        public void f_OB_az1(
                          double X,
                          double Y
                         )
        {

            // ......................................................................
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_az1 = GlobalVarLn.MapX1;
            GlobalVarLn.YCenter_az1 = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY.Checked == true)
            {

                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Недопустимые координаты объекта");
                    return;
                }

                GlobalVarLn.XCenter_az1 = Convert.ToDouble(tbXRect.Text);
                GlobalVarLn.YCenter_az1 = Convert.ToDouble(tbYRect.Text);
            }
            // ......................................................................

            // Object
            ClassMap.f_DrawObjXY(
                          GlobalVarLn.XCenter_az1,  // m на местности
                          GlobalVarLn.YCenter_az1,  // m на местности
                          "",
                          2
                                );

            // ......................................................................

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 1;
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = GlobalVarLn.XCenter_az1;
            ytmp_ed = GlobalVarLn.YCenter_az1;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................

            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек

                );
            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_comm,   // широта
                       ref LongKrG_comm   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );
            // .......................................................................

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_comm,   // широта
                       LongKrG_comm,  // долгота

                       // Выходные параметры (km)
                       ref XSP42_comm,
                       ref YSP42_comm

                   );

            // km->m
            XSP42_comm = XSP42_comm * 1000;
            YSP42_comm = YSP42_comm * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отображение СП в выбранной СК

            OtobrOB_az1();
            // .......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.MapX1, GlobalVarLn.MapY1);
            GlobalVarLn.HCenter_az1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

            tbOwnHeight.Text = Convert.ToString(GlobalVarLn.HCenter_az1);
            // ......................................................................


        } // P/P f_OB_az1
 */
        // OLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLD



        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
        public void f_OB_az1(double X, double Y)
        {

            // ......................................................................
            var objClassMap3_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_az1 = GlobalVarLn.XXXaz;
            GlobalVarLn.YCenter_az1 = GlobalVarLn.YYYaz;
            GlobalVarLn.objFormWayG.f_Map_Rect_XY2(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 1;
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            var xtmp_ed = GlobalVarLn.XCenter_az1;
            var ytmp_ed = GlobalVarLn.YCenter_az1;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            var xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            var ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;
            // .......................................................................

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;

            objClassMap3_ed.f_Grad_GMS
            (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

            );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
            (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

            );

            OtobrOB_az1();
            // .......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XXXaz, GlobalVarLn.YYYaz);
            GlobalVarLn.HCenter_az1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

            tbOwnHeight.Text = Convert.ToString((int) GlobalVarLn.HCenter_az1);
            axaxcMapScreen.Repaint();
        }

        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW

        // *************************************************************************************

        // *************************************************************************************
        // Перерисовка Object
        // *************************************************************************************
        public void f_OBReDraw_Az1()
        {
            GlobalVarLn.objFormWayG.f_Map_Rect_XY2(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);
        }

        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormAz1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blOB_az1 = false;

            GlobalVarLn.fl_Open_objFormAz1 = 0;

            //GlobalVarLn.fFAz1 = 0;


        } // Closing
        // ****************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormAz1_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormAz1G.WindowState = FormWindowState.Normal;

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;

            GlobalVarLn.fl_Open_objFormAz1 = 1;

            // otl33
            //GlobalVarLn.flEndSP_stat = GlobalVarLn.flEndSP_stat;
            //GlobalVarLn.LoadListJS();
            //GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            //UpdateDataGridView();

            //GlobalVarLn.fFAz1 = 1;
            //ClassMap.f_RemoveFrm(9);

        }

        private void label15_Click(object sender, EventArgs e)
        {
            //ClassMap.f_RemoveFrm(9);

        } // Activated


        // ****************************************************************************************





    } // Class
} // namespace
