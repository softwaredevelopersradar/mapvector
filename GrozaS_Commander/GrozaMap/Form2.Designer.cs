﻿namespace GrozaMap
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tcParam = new System.Windows.Forms.TabControl();
            this.tpOwnObject = new System.Windows.Forms.TabPage();
            this.lCapacity = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.cbCap1 = new System.Windows.Forms.ComboBox();
            this.lPowerOwn = new System.Windows.Forms.Label();
            this.tbCoeffOwn = new System.Windows.Forms.TextBox();
            this.lCoeffOwn = new System.Windows.Forms.Label();
            this.tbPowerOwn = new System.Windows.Forms.TextBox();
            this.cbSurface = new System.Windows.Forms.ComboBox();
            this.lSurface = new System.Windows.Forms.Label();
            this.cbWidthHindrance = new System.Windows.Forms.ComboBox();
            this.lWidthHindrance = new System.Windows.Forms.Label();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.cbOwnObject = new System.Windows.Forms.ComboBox();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.grbOwnObject = new System.Windows.Forms.GroupBox();
            this.cbHeightOwnObject = new System.Windows.Forms.ComboBox();
            this.tbHeightOwnObject = new System.Windows.Forms.TextBox();
            this.lHeightOwnObject = new System.Windows.Forms.Label();
            this.tpOpponentObject = new System.Windows.Forms.TabPage();
            this.grbCoeffSupOpponent = new System.Windows.Forms.GroupBox();
            this.tbCoeffSupOpponent = new System.Windows.Forms.TextBox();
            this.cbCoeffSupOpponent = new System.Windows.Forms.ComboBox();
            this.cbTypeCommOpponent = new System.Windows.Forms.ComboBox();
            this.lTypeCommOpponent = new System.Windows.Forms.Label();
            this.cbPolarOpponent = new System.Windows.Forms.ComboBox();
            this.lPolarOpponent = new System.Windows.Forms.Label();
            this.tbRangeComm = new System.Windows.Forms.TextBox();
            this.lRangeComm = new System.Windows.Forms.Label();
            this.tbHeightReceiverOpponent = new System.Windows.Forms.TextBox();
            this.lHeightReceiverOpponent = new System.Windows.Forms.Label();
            this.tbHeightTransmitOpponent = new System.Windows.Forms.TextBox();
            this.lHeightTransmitOpponent = new System.Windows.Forms.Label();
            this.tbWidthSignal = new System.Windows.Forms.TextBox();
            this.lWidthSignal = new System.Windows.Forms.Label();
            this.tbFreq = new System.Windows.Forms.TextBox();
            this.lFreq = new System.Windows.Forms.Label();
            this.tbCoeffReceiverOpponent = new System.Windows.Forms.TextBox();
            this.lCoeffReceivertOpponent = new System.Windows.Forms.Label();
            this.tbCoeffTransmitOpponent = new System.Windows.Forms.TextBox();
            this.lCoeffTransmitOpponent = new System.Windows.Forms.Label();
            this.tbPowerOpponent = new System.Windows.Forms.TextBox();
            this.lPowerOpponent = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.rbPoint2 = new System.Windows.Forms.RadioButton();
            this.rbPoint1 = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.chbXY1 = new System.Windows.Forms.CheckBox();
            this.cbCommChooseSC = new System.Windows.Forms.ComboBox();
            this.lCommChooseSC = new System.Windows.Forms.Label();
            this.nudCountOpponent = new System.Windows.Forms.NumericUpDown();
            this.gbPoint2 = new System.Windows.Forms.GroupBox();
            this.tbPt2Height = new System.Windows.Forms.TextBox();
            this.lPt2Height = new System.Windows.Forms.Label();
            this.gbPt2DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt2LSec = new System.Windows.Forms.TextBox();
            this.tbPt2BSec = new System.Windows.Forms.TextBox();
            this.lPt2Min4 = new System.Windows.Forms.Label();
            this.lPt2Min3 = new System.Windows.Forms.Label();
            this.tbPt2LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt2BMin2 = new System.Windows.Forms.TextBox();
            this.lPt2Sec2 = new System.Windows.Forms.Label();
            this.lPt2Sec1 = new System.Windows.Forms.Label();
            this.lPt2Deg4 = new System.Windows.Forms.Label();
            this.lPt2Deg3 = new System.Windows.Forms.Label();
            this.tbPt2LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt2LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt2BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt2BDegMinSec = new System.Windows.Forms.Label();
            this.gbPt2Rad = new System.Windows.Forms.GroupBox();
            this.tbPt2LRad = new System.Windows.Forms.TextBox();
            this.lPt2LRad = new System.Windows.Forms.Label();
            this.tbPt2BRad = new System.Windows.Forms.TextBox();
            this.lPt2BRad = new System.Windows.Forms.Label();
            this.gbPt2Rect = new System.Windows.Forms.GroupBox();
            this.tbPt2YRect = new System.Windows.Forms.TextBox();
            this.lPt2YRect = new System.Windows.Forms.Label();
            this.tbPt2XRect = new System.Windows.Forms.TextBox();
            this.lPt2XRect = new System.Windows.Forms.Label();
            this.gbPt2DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt2LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt2BMin1 = new System.Windows.Forms.TextBox();
            this.lPt2LDegMin = new System.Windows.Forms.Label();
            this.lPt2BDegMin = new System.Windows.Forms.Label();
            this.gbPt2Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt2YRect42 = new System.Windows.Forms.TextBox();
            this.lPt2YRect42 = new System.Windows.Forms.Label();
            this.tbPt2XRect42 = new System.Windows.Forms.TextBox();
            this.lPt2XRect42 = new System.Windows.Forms.Label();
            this.gbPoint1 = new System.Windows.Forms.GroupBox();
            this.tbPt1Height = new System.Windows.Forms.TextBox();
            this.lPt1Height = new System.Windows.Forms.Label();
            this.gbPt1DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt1LSec = new System.Windows.Forms.TextBox();
            this.tbPt1BSec = new System.Windows.Forms.TextBox();
            this.lPt1Min4 = new System.Windows.Forms.Label();
            this.lPt1Min3 = new System.Windows.Forms.Label();
            this.tbPt1LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin2 = new System.Windows.Forms.TextBox();
            this.lPt1Sec2 = new System.Windows.Forms.Label();
            this.lPt1Sec1 = new System.Windows.Forms.Label();
            this.lPt1Deg4 = new System.Windows.Forms.Label();
            this.lPt1Deg3 = new System.Windows.Forms.Label();
            this.tbPt1LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt1BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1BDegMinSec = new System.Windows.Forms.Label();
            this.gbPt1Rad = new System.Windows.Forms.GroupBox();
            this.tbPt1LRad = new System.Windows.Forms.TextBox();
            this.lPt1LRad = new System.Windows.Forms.Label();
            this.tbPt1BRad = new System.Windows.Forms.TextBox();
            this.lPt1BRad = new System.Windows.Forms.Label();
            this.gbPt1Rect = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect = new System.Windows.Forms.TextBox();
            this.lPt1YRect = new System.Windows.Forms.Label();
            this.tbPt1XRect = new System.Windows.Forms.TextBox();
            this.lPt1XRect = new System.Windows.Forms.Label();
            this.gbPt1DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt1LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin1 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMin = new System.Windows.Forms.Label();
            this.lPt1BDegMin = new System.Windows.Forms.Label();
            this.gbPt1Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect42 = new System.Windows.Forms.TextBox();
            this.lPt1YRect42 = new System.Windows.Forms.Label();
            this.tbPt1XRect42 = new System.Windows.Forms.TextBox();
            this.lPt1XRect42 = new System.Windows.Forms.Label();
            this.lCountOpponent = new System.Windows.Forms.Label();
            this.tbRadiusZone = new System.Windows.Forms.TextBox();
            this.lRadiusZone = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.tbMaxDist = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbHindSignal = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bAccept = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.grbDetail = new System.Windows.Forms.GroupBox();
            this.tbCorrectHeightOpponent = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbGamma = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbResultHeightOpponent = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMinHeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbMiddleHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbMaxDistance = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCorrectHeightOwn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCoeffHE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCoeffQ = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbResultHeightOwn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tcParam.SuspendLayout();
            this.tpOwnObject.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbOwnRect.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.grbOwnObject.SuspendLayout();
            this.tpOpponentObject.SuspendLayout();
            this.grbCoeffSupOpponent.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountOpponent)).BeginInit();
            this.gbPoint2.SuspendLayout();
            this.gbPt2DegMinSec.SuspendLayout();
            this.gbPt2Rad.SuspendLayout();
            this.gbPt2Rect.SuspendLayout();
            this.gbPt2DegMin.SuspendLayout();
            this.gbPt2Rect42.SuspendLayout();
            this.gbPoint1.SuspendLayout();
            this.gbPt1DegMinSec.SuspendLayout();
            this.gbPt1Rad.SuspendLayout();
            this.gbPt1Rect.SuspendLayout();
            this.gbPt1DegMin.SuspendLayout();
            this.gbPt1Rect42.SuspendLayout();
            this.grbDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcParam
            // 
            this.tcParam.Controls.Add(this.tpOwnObject);
            this.tcParam.Controls.Add(this.tpOpponentObject);
            this.tcParam.Controls.Add(this.tabPage1);
            this.tcParam.Location = new System.Drawing.Point(12, 12);
            this.tcParam.Name = "tcParam";
            this.tcParam.SelectedIndex = 0;
            this.tcParam.Size = new System.Drawing.Size(505, 223);
            this.tcParam.TabIndex = 81;
            // 
            // tpOwnObject
            // 
            this.tpOwnObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOwnObject.Controls.Add(this.lCapacity);
            this.tpOwnObject.Controls.Add(this.label17);
            this.tpOwnObject.Controls.Add(this.tbHAnt);
            this.tpOwnObject.Controls.Add(this.label15);
            this.tpOwnObject.Controls.Add(this.chbXY);
            this.tpOwnObject.Controls.Add(this.cbCap1);
            this.tpOwnObject.Controls.Add(this.lPowerOwn);
            this.tpOwnObject.Controls.Add(this.tbCoeffOwn);
            this.tpOwnObject.Controls.Add(this.lCoeffOwn);
            this.tpOwnObject.Controls.Add(this.tbPowerOwn);
            this.tpOwnObject.Controls.Add(this.cbSurface);
            this.tpOwnObject.Controls.Add(this.lSurface);
            this.tpOwnObject.Controls.Add(this.cbWidthHindrance);
            this.tpOwnObject.Controls.Add(this.lWidthHindrance);
            this.tpOwnObject.Controls.Add(this.lChooseSC);
            this.tpOwnObject.Controls.Add(this.pCoordPoint);
            this.tpOwnObject.Controls.Add(this.cbChooseSC);
            this.tpOwnObject.Controls.Add(this.grbOwnObject);
            this.tpOwnObject.Location = new System.Drawing.Point(4, 22);
            this.tpOwnObject.Name = "tpOwnObject";
            this.tpOwnObject.Padding = new System.Windows.Forms.Padding(3);
            this.tpOwnObject.Size = new System.Drawing.Size(497, 197);
            this.tpOwnObject.TabIndex = 0;
            this.tpOwnObject.Text = "Средство подавления";
            // 
            // lCapacity
            // 
            this.lCapacity.AutoSize = true;
            this.lCapacity.Location = new System.Drawing.Point(179, 100);
            this.lCapacity.Name = "lCapacity";
            this.lCapacity.Size = new System.Drawing.Size(217, 13);
            this.lCapacity.TabIndex = 109;
            this.lCapacity.Text = "Пропуск. способность................................";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 174);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(114, 13);
            this.label17.TabIndex = 108;
            this.label17.Text = "Высота антенны,м....";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(123, 171);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(39, 20);
            this.tbHAnt.TabIndex = 107;
            this.tbHAnt.Text = "2";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 155);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 13);
            this.label15.TabIndex = 106;
            this.label15.Text = "Ручной выбор координат";
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(156, 155);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(15, 14);
            this.chbXY.TabIndex = 105;
            this.chbXY.UseVisualStyleBackColor = true;
            // 
            // cbCap1
            // 
            this.cbCap1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCap1.FormattingEnabled = true;
            this.cbCap1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cbCap1.Location = new System.Drawing.Point(407, 95);
            this.cbCap1.Name = "cbCap1";
            this.cbCap1.Size = new System.Drawing.Size(62, 21);
            this.cbCap1.TabIndex = 102;
            // 
            // lPowerOwn
            // 
            this.lPowerOwn.AutoSize = true;
            this.lPowerOwn.Location = new System.Drawing.Point(180, 53);
            this.lPowerOwn.Name = "lPowerOwn";
            this.lPowerOwn.Size = new System.Drawing.Size(220, 13);
            this.lPowerOwn.TabIndex = 89;
            this.lPowerOwn.Text = "Мощность передачитка, Вт.........................";
            // 
            // tbCoeffOwn
            // 
            this.tbCoeffOwn.Location = new System.Drawing.Point(408, 71);
            this.tbCoeffOwn.Name = "tbCoeffOwn";
            this.tbCoeffOwn.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffOwn.TabIndex = 88;
            this.tbCoeffOwn.Text = "4";
            // 
            // lCoeffOwn
            // 
            this.lCoeffOwn.AutoSize = true;
            this.lCoeffOwn.Location = new System.Drawing.Point(180, 79);
            this.lCoeffOwn.Name = "lCoeffOwn";
            this.lCoeffOwn.Size = new System.Drawing.Size(217, 13);
            this.lCoeffOwn.TabIndex = 87;
            this.lCoeffOwn.Text = "Коэффициент усиления..............................";
            // 
            // tbPowerOwn
            // 
            this.tbPowerOwn.Location = new System.Drawing.Point(407, 48);
            this.tbPowerOwn.Name = "tbPowerOwn";
            this.tbPowerOwn.Size = new System.Drawing.Size(60, 20);
            this.tbPowerOwn.TabIndex = 86;
            this.tbPowerOwn.Text = "1000";
            // 
            // cbSurface
            // 
            this.cbSurface.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSurface.FormattingEnabled = true;
            this.cbSurface.Items.AddRange(new object[] {
            "Сухая почва",
            "Влажная почва",
            "Пресная вода",
            "Морская вода",
            "Лес"});
            this.cbSurface.Location = new System.Drawing.Point(319, 169);
            this.cbSurface.Name = "cbSurface";
            this.cbSurface.Size = new System.Drawing.Size(178, 21);
            this.cbSurface.TabIndex = 85;
            // 
            // lSurface
            // 
            this.lSurface.AutoSize = true;
            this.lSurface.Location = new System.Drawing.Point(168, 172);
            this.lSurface.Name = "lSurface";
            this.lSurface.Size = new System.Drawing.Size(152, 13);
            this.lSurface.TabIndex = 84;
            this.lSurface.Text = "Подстилающая поверхность";
            // 
            // cbWidthHindrance
            // 
            this.cbWidthHindrance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWidthHindrance.FormattingEnabled = true;
            this.cbWidthHindrance.Items.AddRange(new object[] {
            "3,5",
            "4,94",
            "7",
            "8,4",
            "10",
            "14,14",
            "20",
            "31,6",
            "50",
            "70,7",
            "100",
            "140",
            "200",
            "300",
            "500",
            "700",
            "1000",
            "1400",
            "2000"});
            this.cbWidthHindrance.Location = new System.Drawing.Point(407, 120);
            this.cbWidthHindrance.Name = "cbWidthHindrance";
            this.cbWidthHindrance.Size = new System.Drawing.Size(62, 21);
            this.cbWidthHindrance.TabIndex = 83;
            // 
            // lWidthHindrance
            // 
            this.lWidthHindrance.AutoSize = true;
            this.lWidthHindrance.Location = new System.Drawing.Point(180, 122);
            this.lWidthHindrance.Name = "lWidthHindrance";
            this.lWidthHindrance.Size = new System.Drawing.Size(217, 13);
            this.lWidthHindrance.TabIndex = 82;
            this.lWidthHindrance.Text = "Ширина спектра помехи, кГц.....................";
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(2, 13);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(45, 13);
            this.lChooseSC.TabIndex = 78;
            this.lChooseSC.Text = "СК........";
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbOwnRect);
            this.pCoordPoint.Controls.Add(this.cbOwnObject);
            this.pCoordPoint.Controls.Add(this.lCenterLSR);
            this.pCoordPoint.Controls.Add(this.tbOwnHeight);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMin);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMinSec);
            this.pCoordPoint.Controls.Add(this.gbOwnRect42);
            this.pCoordPoint.Controls.Add(this.lOwnHeight);
            this.pCoordPoint.Controls.Add(this.gbOwnRad);
            this.pCoordPoint.Location = new System.Drawing.Point(2, 33);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(172, 113);
            this.pCoordPoint.TabIndex = 79;
            // 
            // gbOwnRect
            // 
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Location = new System.Drawing.Point(8, 26);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.Size = new System.Drawing.Size(158, 63);
            this.gbOwnRect.TabIndex = 67;
            this.gbOwnRect.TabStop = false;
            this.gbOwnRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(6, 39);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(85, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Y, м...................";
            // 
            // tbXRect
            // 
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(85, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "X, м...................";
            // 
            // cbOwnObject
            // 
            this.cbOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOwnObject.FormattingEnabled = true;
            this.cbOwnObject.Location = new System.Drawing.Point(99, 5);
            this.cbOwnObject.Name = "cbOwnObject";
            this.cbOwnObject.Size = new System.Drawing.Size(65, 21);
            this.cbOwnObject.TabIndex = 65;
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(5, 10);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(97, 13);
            this.lCenterLSR.TabIndex = 66;
            this.lCenterLSR.Text = "СП ........................";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.Menu;
            this.tbOwnHeight.Location = new System.Drawing.Point(113, 89);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.Size = new System.Drawing.Size(48, 20);
            this.tbOwnHeight.TabIndex = 64;
            // 
            // gbOwnDegMin
            // 
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Location = new System.Drawing.Point(6, 30);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.Size = new System.Drawing.Size(142, 63);
            this.gbOwnDegMin.TabIndex = 29;
            this.gbOwnDegMin.TabStop = false;
            this.gbOwnDegMin.Visible = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(75, 36);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(50, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(75, 13);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(50, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(20, 39);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(64, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L.................";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(20, 16);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(59, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B...............";
            // 
            // gbOwnDegMinSec
            // 
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Location = new System.Drawing.Point(7, 32);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.Size = new System.Drawing.Size(143, 60);
            this.gbOwnDegMinSec.TabIndex = 30;
            this.gbOwnDegMinSec.TabStop = false;
            this.gbOwnDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(102, 36);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(25, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(102, 12);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(25, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(91, 35);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(92, 12);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(67, 35);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(67, 12);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(132, 36);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(132, 12);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(51, 38);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(51, 13);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(23, 36);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(6, 36);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L.................";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(23, 13);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(5, 14);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B...............";
            // 
            // gbOwnRect42
            // 
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Location = new System.Drawing.Point(7, 32);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.Size = new System.Drawing.Size(160, 63);
            this.gbOwnRect42.TabIndex = 27;
            this.gbOwnRect42.TabStop = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(78, 36);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 39);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(85, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, м...................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(78, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(85, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, м...................";
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(9, 96);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(116, 13);
            this.lOwnHeight.TabIndex = 63;
            this.lOwnHeight.Text = "H, м.............................";
            // 
            // gbOwnRad
            // 
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Location = new System.Drawing.Point(180, 69);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.Size = new System.Drawing.Size(160, 63);
            this.gbOwnRad.TabIndex = 28;
            this.gbOwnRad.TabStop = false;
            this.gbOwnRad.Visible = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(94, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(95, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Метры 1942 г.",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(54, 6);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(123, 21);
            this.cbChooseSC.TabIndex = 77;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // grbOwnObject
            // 
            this.grbOwnObject.Controls.Add(this.cbHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.tbHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.lHeightOwnObject);
            this.grbOwnObject.Location = new System.Drawing.Point(180, 5);
            this.grbOwnObject.Name = "grbOwnObject";
            this.grbOwnObject.Size = new System.Drawing.Size(310, 41);
            this.grbOwnObject.TabIndex = 76;
            this.grbOwnObject.TabStop = false;
            this.grbOwnObject.Text = "Средство РП";
            // 
            // cbHeightOwnObject
            // 
            this.cbHeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeightOwnObject.FormattingEnabled = true;
            this.cbHeightOwnObject.Items.AddRange(new object[] {
            "Высота антенны+рельеф",
            "Высота антенны",
            "Задать самостоятельно"});
            this.cbHeightOwnObject.Location = new System.Drawing.Point(3, 16);
            this.cbHeightOwnObject.Name = "cbHeightOwnObject";
            this.cbHeightOwnObject.Size = new System.Drawing.Size(165, 21);
            this.cbHeightOwnObject.TabIndex = 27;
            // 
            // tbHeightOwnObject
            // 
            this.tbHeightOwnObject.Location = new System.Drawing.Point(235, 15);
            this.tbHeightOwnObject.MaxLength = 3;
            this.tbHeightOwnObject.Name = "tbHeightOwnObject";
            this.tbHeightOwnObject.Size = new System.Drawing.Size(52, 20);
            this.tbHeightOwnObject.TabIndex = 26;
            // 
            // lHeightOwnObject
            // 
            this.lHeightOwnObject.AutoSize = true;
            this.lHeightOwnObject.Location = new System.Drawing.Point(187, 22);
            this.lHeightOwnObject.Name = "lHeightOwnObject";
            this.lHeightOwnObject.Size = new System.Drawing.Size(98, 13);
            this.lHeightOwnObject.TabIndex = 21;
            this.lHeightOwnObject.Text = "H , м......................";
            // 
            // tpOpponentObject
            // 
            this.tpOpponentObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOpponentObject.Controls.Add(this.grbCoeffSupOpponent);
            this.tpOpponentObject.Controls.Add(this.cbTypeCommOpponent);
            this.tpOpponentObject.Controls.Add(this.lTypeCommOpponent);
            this.tpOpponentObject.Controls.Add(this.cbPolarOpponent);
            this.tpOpponentObject.Controls.Add(this.lPolarOpponent);
            this.tpOpponentObject.Controls.Add(this.tbRangeComm);
            this.tpOpponentObject.Controls.Add(this.lRangeComm);
            this.tpOpponentObject.Controls.Add(this.tbHeightReceiverOpponent);
            this.tpOpponentObject.Controls.Add(this.lHeightReceiverOpponent);
            this.tpOpponentObject.Controls.Add(this.tbHeightTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.lHeightTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.tbWidthSignal);
            this.tpOpponentObject.Controls.Add(this.lWidthSignal);
            this.tpOpponentObject.Controls.Add(this.tbFreq);
            this.tpOpponentObject.Controls.Add(this.lFreq);
            this.tpOpponentObject.Controls.Add(this.tbCoeffReceiverOpponent);
            this.tpOpponentObject.Controls.Add(this.lCoeffReceivertOpponent);
            this.tpOpponentObject.Controls.Add(this.tbCoeffTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.lCoeffTransmitOpponent);
            this.tpOpponentObject.Controls.Add(this.tbPowerOpponent);
            this.tpOpponentObject.Controls.Add(this.lPowerOpponent);
            this.tpOpponentObject.Location = new System.Drawing.Point(4, 22);
            this.tpOpponentObject.Name = "tpOpponentObject";
            this.tpOpponentObject.Padding = new System.Windows.Forms.Padding(3);
            this.tpOpponentObject.Size = new System.Drawing.Size(497, 197);
            this.tpOpponentObject.TabIndex = 1;
            this.tpOpponentObject.Text = "Параметры УС";
            // 
            // grbCoeffSupOpponent
            // 
            this.grbCoeffSupOpponent.Controls.Add(this.tbCoeffSupOpponent);
            this.grbCoeffSupOpponent.Controls.Add(this.cbCoeffSupOpponent);
            this.grbCoeffSupOpponent.Location = new System.Drawing.Point(5, 123);
            this.grbCoeffSupOpponent.Name = "grbCoeffSupOpponent";
            this.grbCoeffSupOpponent.Size = new System.Drawing.Size(237, 46);
            this.grbCoeffSupOpponent.TabIndex = 122;
            this.grbCoeffSupOpponent.TabStop = false;
            this.grbCoeffSupOpponent.Text = "Коэфффициент подавления";
            // 
            // tbCoeffSupOpponent
            // 
            this.tbCoeffSupOpponent.Location = new System.Drawing.Point(176, 18);
            this.tbCoeffSupOpponent.MaxLength = 3;
            this.tbCoeffSupOpponent.Name = "tbCoeffSupOpponent";
            this.tbCoeffSupOpponent.Size = new System.Drawing.Size(51, 20);
            this.tbCoeffSupOpponent.TabIndex = 96;
            // 
            // cbCoeffSupOpponent
            // 
            this.cbCoeffSupOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCoeffSupOpponent.FormattingEnabled = true;
            this.cbCoeffSupOpponent.Items.AddRange(new object[] {
            "Автоматический расчет",
            "Задать самостоятельно"});
            this.cbCoeffSupOpponent.Location = new System.Drawing.Point(6, 18);
            this.cbCoeffSupOpponent.Name = "cbCoeffSupOpponent";
            this.cbCoeffSupOpponent.Size = new System.Drawing.Size(148, 21);
            this.cbCoeffSupOpponent.TabIndex = 94;
            // 
            // cbTypeCommOpponent
            // 
            this.cbTypeCommOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeCommOpponent.FormattingEnabled = true;
            this.cbTypeCommOpponent.Items.AddRange(new object[] {
            "АМ",
            "ЧМ",
            "ОМ",
            "ЧМн",
            "ФМн",
            "АМн",
            "ФМн ШПС"});
            this.cbTypeCommOpponent.Location = new System.Drawing.Point(375, 135);
            this.cbTypeCommOpponent.Name = "cbTypeCommOpponent";
            this.cbTypeCommOpponent.Size = new System.Drawing.Size(91, 21);
            this.cbTypeCommOpponent.TabIndex = 121;
            // 
            // lTypeCommOpponent
            // 
            this.lTypeCommOpponent.AutoSize = true;
            this.lTypeCommOpponent.Location = new System.Drawing.Point(261, 143);
            this.lTypeCommOpponent.Name = "lTypeCommOpponent";
            this.lTypeCommOpponent.Size = new System.Drawing.Size(149, 13);
            this.lTypeCommOpponent.TabIndex = 120;
            this.lTypeCommOpponent.Text = "Вид связи..............................";
            // 
            // cbPolarOpponent
            // 
            this.cbPolarOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPolarOpponent.FormattingEnabled = true;
            this.cbPolarOpponent.Items.AddRange(new object[] {
            "Линейная вертикальная",
            "Линейная горизонтальная",
            "Круговая правая",
            "Круговая левая"});
            this.cbPolarOpponent.Location = new System.Drawing.Point(273, 101);
            this.cbPolarOpponent.Name = "cbPolarOpponent";
            this.cbPolarOpponent.Size = new System.Drawing.Size(193, 21);
            this.cbPolarOpponent.TabIndex = 118;
            // 
            // lPolarOpponent
            // 
            this.lPolarOpponent.AutoSize = true;
            this.lPolarOpponent.Location = new System.Drawing.Point(2, 104);
            this.lPolarOpponent.Name = "lPolarOpponent";
            this.lPolarOpponent.Size = new System.Drawing.Size(323, 13);
            this.lPolarOpponent.TabIndex = 117;
            this.lPolarOpponent.Text = "Поляризация сигнала.............................................................." +
    "......";
            // 
            // tbRangeComm
            // 
            this.tbRangeComm.Location = new System.Drawing.Point(407, 4);
            this.tbRangeComm.Name = "tbRangeComm";
            this.tbRangeComm.Size = new System.Drawing.Size(60, 20);
            this.tbRangeComm.TabIndex = 116;
            this.tbRangeComm.Text = "3000";
            // 
            // lRangeComm
            // 
            this.lRangeComm.AutoSize = true;
            this.lRangeComm.Location = new System.Drawing.Point(239, 11);
            this.lRangeComm.Name = "lRangeComm";
            this.lRangeComm.Size = new System.Drawing.Size(185, 13);
            this.lRangeComm.TabIndex = 115;
            this.lRangeComm.Text = "Дальность связи, м.........................";
            // 
            // tbHeightReceiverOpponent
            // 
            this.tbHeightReceiverOpponent.Location = new System.Drawing.Point(407, 71);
            this.tbHeightReceiverOpponent.Name = "tbHeightReceiverOpponent";
            this.tbHeightReceiverOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbHeightReceiverOpponent.TabIndex = 114;
            this.tbHeightReceiverOpponent.Text = "1";
            // 
            // lHeightReceiverOpponent
            // 
            this.lHeightReceiverOpponent.AutoSize = true;
            this.lHeightReceiverOpponent.Location = new System.Drawing.Point(239, 77);
            this.lHeightReceiverOpponent.Name = "lHeightReceiverOpponent";
            this.lHeightReceiverOpponent.Size = new System.Drawing.Size(221, 13);
            this.lHeightReceiverOpponent.TabIndex = 113;
            this.lHeightReceiverOpponent.Text = "Антенна приемника, м.................................";
            // 
            // tbHeightTransmitOpponent
            // 
            this.tbHeightTransmitOpponent.Location = new System.Drawing.Point(407, 48);
            this.tbHeightTransmitOpponent.Name = "tbHeightTransmitOpponent";
            this.tbHeightTransmitOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbHeightTransmitOpponent.TabIndex = 112;
            this.tbHeightTransmitOpponent.Text = "1";
            // 
            // lHeightTransmitOpponent
            // 
            this.lHeightTransmitOpponent.AutoSize = true;
            this.lHeightTransmitOpponent.Location = new System.Drawing.Point(239, 55);
            this.lHeightTransmitOpponent.Name = "lHeightTransmitOpponent";
            this.lHeightTransmitOpponent.Size = new System.Drawing.Size(187, 13);
            this.lHeightTransmitOpponent.TabIndex = 111;
            this.lHeightTransmitOpponent.Text = "Антенна передачитка, м...................";
            // 
            // tbWidthSignal
            // 
            this.tbWidthSignal.Location = new System.Drawing.Point(407, 26);
            this.tbWidthSignal.Name = "tbWidthSignal";
            this.tbWidthSignal.Size = new System.Drawing.Size(60, 20);
            this.tbWidthSignal.TabIndex = 110;
            this.tbWidthSignal.Text = "25";
            // 
            // lWidthSignal
            // 
            this.lWidthSignal.AutoSize = true;
            this.lWidthSignal.Location = new System.Drawing.Point(239, 33);
            this.lWidthSignal.Name = "lWidthSignal";
            this.lWidthSignal.Size = new System.Drawing.Size(189, 13);
            this.lWidthSignal.TabIndex = 109;
            this.lWidthSignal.Text = "Ширина спектра, кГц.........................";
            // 
            // tbFreq
            // 
            this.tbFreq.Location = new System.Drawing.Point(170, 3);
            this.tbFreq.Name = "tbFreq";
            this.tbFreq.Size = new System.Drawing.Size(60, 20);
            this.tbFreq.TabIndex = 108;
            this.tbFreq.Text = "40000";
            // 
            // lFreq
            // 
            this.lFreq.AutoSize = true;
            this.lFreq.Location = new System.Drawing.Point(2, 12);
            this.lFreq.Name = "lFreq";
            this.lFreq.Size = new System.Drawing.Size(194, 13);
            this.lFreq.TabIndex = 107;
            this.lFreq.Text = "Несущая частота, кГц.........................";
            // 
            // tbCoeffReceiverOpponent
            // 
            this.tbCoeffReceiverOpponent.Location = new System.Drawing.Point(170, 70);
            this.tbCoeffReceiverOpponent.Name = "tbCoeffReceiverOpponent";
            this.tbCoeffReceiverOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffReceiverOpponent.TabIndex = 106;
            this.tbCoeffReceiverOpponent.Text = "2";
            this.tbCoeffReceiverOpponent.Visible = false;
            // 
            // lCoeffReceivertOpponent
            // 
            this.lCoeffReceivertOpponent.AutoSize = true;
            this.lCoeffReceivertOpponent.Location = new System.Drawing.Point(2, 78);
            this.lCoeffReceivertOpponent.Name = "lCoeffReceivertOpponent";
            this.lCoeffReceivertOpponent.Size = new System.Drawing.Size(193, 13);
            this.lCoeffReceivertOpponent.TabIndex = 105;
            this.lCoeffReceivertOpponent.Text = "КУ антенны приемника......................";
            this.lCoeffReceivertOpponent.Visible = false;
            // 
            // tbCoeffTransmitOpponent
            // 
            this.tbCoeffTransmitOpponent.Location = new System.Drawing.Point(170, 47);
            this.tbCoeffTransmitOpponent.Name = "tbCoeffTransmitOpponent";
            this.tbCoeffTransmitOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffTransmitOpponent.TabIndex = 104;
            this.tbCoeffTransmitOpponent.Text = "1";
            // 
            // lCoeffTransmitOpponent
            // 
            this.lCoeffTransmitOpponent.AutoSize = true;
            this.lCoeffTransmitOpponent.Location = new System.Drawing.Point(2, 56);
            this.lCoeffTransmitOpponent.Name = "lCoeffTransmitOpponent";
            this.lCoeffTransmitOpponent.Size = new System.Drawing.Size(174, 13);
            this.lCoeffTransmitOpponent.TabIndex = 103;
            this.lCoeffTransmitOpponent.Text = "КУ антенны передатчика ............";
            // 
            // tbPowerOpponent
            // 
            this.tbPowerOpponent.Location = new System.Drawing.Point(170, 25);
            this.tbPowerOpponent.Name = "tbPowerOpponent";
            this.tbPowerOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbPowerOpponent.TabIndex = 102;
            this.tbPowerOpponent.Text = "25";
            // 
            // lPowerOpponent
            // 
            this.lPowerOpponent.AutoSize = true;
            this.lPowerOpponent.Location = new System.Drawing.Point(2, 34);
            this.lPowerOpponent.Name = "lPowerOpponent";
            this.lPowerOpponent.Size = new System.Drawing.Size(220, 13);
            this.lPowerOpponent.TabIndex = 101;
            this.lPowerOpponent.Text = "Мощность передачитка, Вт.........................";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.rbPoint2);
            this.tabPage1.Controls.Add(this.rbPoint1);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.chbXY1);
            this.tabPage1.Controls.Add(this.cbCommChooseSC);
            this.tabPage1.Controls.Add(this.lCommChooseSC);
            this.tabPage1.Controls.Add(this.nudCountOpponent);
            this.tabPage1.Controls.Add(this.gbPoint2);
            this.tabPage1.Controls.Add(this.gbPoint1);
            this.tabPage1.Controls.Add(this.lCountOpponent);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(497, 197);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Координаты УС";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(392, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 111;
            this.label12.Text = "передающий";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(392, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 110;
            this.label11.Text = "приемный";
            // 
            // rbPoint2
            // 
            this.rbPoint2.AutoSize = true;
            this.rbPoint2.Location = new System.Drawing.Point(405, 86);
            this.rbPoint2.Name = "rbPoint2";
            this.rbPoint2.Size = new System.Drawing.Size(52, 17);
            this.rbPoint2.TabIndex = 109;
            this.rbPoint2.Text = "УС 2 ";
            this.rbPoint2.UseVisualStyleBackColor = true;
            // 
            // rbPoint1
            // 
            this.rbPoint1.AutoSize = true;
            this.rbPoint1.Checked = true;
            this.rbPoint1.Location = new System.Drawing.Point(405, 42);
            this.rbPoint1.Name = "rbPoint1";
            this.rbPoint1.Size = new System.Drawing.Size(52, 17);
            this.rbPoint1.TabIndex = 108;
            this.rbPoint1.TabStop = true;
            this.rbPoint1.Text = "УС 1 ";
            this.rbPoint1.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 175);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 13);
            this.label16.TabIndex = 107;
            this.label16.Text = "Ручной выбор координат";
            // 
            // chbXY1
            // 
            this.chbXY1.AutoSize = true;
            this.chbXY1.Location = new System.Drawing.Point(149, 175);
            this.chbXY1.Name = "chbXY1";
            this.chbXY1.Size = new System.Drawing.Size(15, 14);
            this.chbXY1.TabIndex = 106;
            this.chbXY1.UseVisualStyleBackColor = true;
            // 
            // cbCommChooseSC
            // 
            this.cbCommChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCommChooseSC.DropDownWidth = 150;
            this.cbCommChooseSC.FormattingEnabled = true;
            this.cbCommChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Метры 1942 г.",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbCommChooseSC.Location = new System.Drawing.Point(131, 4);
            this.cbCommChooseSC.Name = "cbCommChooseSC";
            this.cbCommChooseSC.Size = new System.Drawing.Size(234, 21);
            this.cbCommChooseSC.TabIndex = 91;
            this.cbCommChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbCommChooseSC_SelectedIndexChanged);
            // 
            // lCommChooseSC
            // 
            this.lCommChooseSC.AutoSize = true;
            this.lCommChooseSC.Location = new System.Drawing.Point(7, 10);
            this.lCommChooseSC.Name = "lCommChooseSC";
            this.lCommChooseSC.Size = new System.Drawing.Size(129, 13);
            this.lCommChooseSC.TabIndex = 92;
            this.lCommChooseSC.Text = "СК....................................";
            // 
            // nudCountOpponent
            // 
            this.nudCountOpponent.Location = new System.Drawing.Point(306, 139);
            this.nudCountOpponent.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCountOpponent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCountOpponent.Name = "nudCountOpponent";
            this.nudCountOpponent.Size = new System.Drawing.Size(60, 20);
            this.nudCountOpponent.TabIndex = 90;
            this.nudCountOpponent.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCountOpponent.ValueChanged += new System.EventHandler(this.nudCountOpponent_ValueChanged);
            // 
            // gbPoint2
            // 
            this.gbPoint2.Controls.Add(this.tbPt2Height);
            this.gbPoint2.Controls.Add(this.lPt2Height);
            this.gbPoint2.Controls.Add(this.gbPt2DegMinSec);
            this.gbPoint2.Controls.Add(this.gbPt2Rad);
            this.gbPoint2.Controls.Add(this.gbPt2Rect);
            this.gbPoint2.Controls.Add(this.gbPt2DegMin);
            this.gbPoint2.Controls.Add(this.gbPt2Rect42);
            this.gbPoint2.Location = new System.Drawing.Point(192, 31);
            this.gbPoint2.Name = "gbPoint2";
            this.gbPoint2.Size = new System.Drawing.Size(173, 103);
            this.gbPoint2.TabIndex = 86;
            this.gbPoint2.TabStop = false;
            this.gbPoint2.Text = "УС 2";
            // 
            // tbPt2Height
            // 
            this.tbPt2Height.BackColor = System.Drawing.SystemColors.Menu;
            this.tbPt2Height.Location = new System.Drawing.Point(116, 77);
            this.tbPt2Height.Name = "tbPt2Height";
            this.tbPt2Height.Size = new System.Drawing.Size(48, 20);
            this.tbPt2Height.TabIndex = 37;
            // 
            // lPt2Height
            // 
            this.lPt2Height.AutoSize = true;
            this.lPt2Height.Location = new System.Drawing.Point(12, 84);
            this.lPt2Height.Name = "lPt2Height";
            this.lPt2Height.Size = new System.Drawing.Size(116, 13);
            this.lPt2Height.TabIndex = 36;
            this.lPt2Height.Text = "H, м.............................";
            // 
            // gbPt2DegMinSec
            // 
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LSec);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BSec);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Min4);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Min3);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LMin2);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BMin2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Sec2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Sec1);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Deg4);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2Deg3);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2LDeg2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2LDegMinSec);
            this.gbPt2DegMinSec.Controls.Add(this.tbPt2BDeg2);
            this.gbPt2DegMinSec.Controls.Add(this.lPt2BDegMinSec);
            this.gbPt2DegMinSec.Location = new System.Drawing.Point(3, 19);
            this.gbPt2DegMinSec.Name = "gbPt2DegMinSec";
            this.gbPt2DegMinSec.Size = new System.Drawing.Size(151, 63);
            this.gbPt2DegMinSec.TabIndex = 35;
            this.gbPt2DegMinSec.TabStop = false;
            this.gbPt2DegMinSec.Visible = false;
            // 
            // tbPt2LSec
            // 
            this.tbPt2LSec.Location = new System.Drawing.Point(113, 31);
            this.tbPt2LSec.MaxLength = 3;
            this.tbPt2LSec.Name = "tbPt2LSec";
            this.tbPt2LSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt2LSec.TabIndex = 23;
            // 
            // tbPt2BSec
            // 
            this.tbPt2BSec.Location = new System.Drawing.Point(113, 13);
            this.tbPt2BSec.MaxLength = 3;
            this.tbPt2BSec.Name = "tbPt2BSec";
            this.tbPt2BSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt2BSec.TabIndex = 18;
            // 
            // lPt2Min4
            // 
            this.lPt2Min4.AutoSize = true;
            this.lPt2Min4.Location = new System.Drawing.Point(102, 37);
            this.lPt2Min4.Name = "lPt2Min4";
            this.lPt2Min4.Size = new System.Drawing.Size(9, 13);
            this.lPt2Min4.TabIndex = 27;
            this.lPt2Min4.Text = "\'";
            // 
            // lPt2Min3
            // 
            this.lPt2Min3.AutoSize = true;
            this.lPt2Min3.Location = new System.Drawing.Point(102, 13);
            this.lPt2Min3.Name = "lPt2Min3";
            this.lPt2Min3.Size = new System.Drawing.Size(9, 13);
            this.lPt2Min3.TabIndex = 26;
            this.lPt2Min3.Text = "\'";
            // 
            // tbPt2LMin2
            // 
            this.tbPt2LMin2.Location = new System.Drawing.Point(71, 37);
            this.tbPt2LMin2.MaxLength = 2;
            this.tbPt2LMin2.Name = "tbPt2LMin2";
            this.tbPt2LMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt2LMin2.TabIndex = 22;
            // 
            // tbPt2BMin2
            // 
            this.tbPt2BMin2.Location = new System.Drawing.Point(71, 13);
            this.tbPt2BMin2.MaxLength = 2;
            this.tbPt2BMin2.Name = "tbPt2BMin2";
            this.tbPt2BMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt2BMin2.TabIndex = 16;
            // 
            // lPt2Sec2
            // 
            this.lPt2Sec2.AutoSize = true;
            this.lPt2Sec2.Location = new System.Drawing.Point(144, 34);
            this.lPt2Sec2.Name = "lPt2Sec2";
            this.lPt2Sec2.Size = new System.Drawing.Size(9, 13);
            this.lPt2Sec2.TabIndex = 25;
            this.lPt2Sec2.Text = "\'";
            // 
            // lPt2Sec1
            // 
            this.lPt2Sec1.AutoSize = true;
            this.lPt2Sec1.Location = new System.Drawing.Point(144, 13);
            this.lPt2Sec1.Name = "lPt2Sec1";
            this.lPt2Sec1.Size = new System.Drawing.Size(9, 13);
            this.lPt2Sec1.TabIndex = 24;
            this.lPt2Sec1.Text = "\'";
            // 
            // lPt2Deg4
            // 
            this.lPt2Deg4.AutoSize = true;
            this.lPt2Deg4.Location = new System.Drawing.Point(58, 36);
            this.lPt2Deg4.Name = "lPt2Deg4";
            this.lPt2Deg4.Size = new System.Drawing.Size(13, 13);
            this.lPt2Deg4.TabIndex = 21;
            this.lPt2Deg4.Text = "^";
            // 
            // lPt2Deg3
            // 
            this.lPt2Deg3.AutoSize = true;
            this.lPt2Deg3.Location = new System.Drawing.Point(52, 12);
            this.lPt2Deg3.Name = "lPt2Deg3";
            this.lPt2Deg3.Size = new System.Drawing.Size(13, 13);
            this.lPt2Deg3.TabIndex = 20;
            this.lPt2Deg3.Text = "^";
            // 
            // tbPt2LDeg2
            // 
            this.tbPt2LDeg2.Location = new System.Drawing.Point(30, 34);
            this.tbPt2LDeg2.MaxLength = 2;
            this.tbPt2LDeg2.Name = "tbPt2LDeg2";
            this.tbPt2LDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt2LDeg2.TabIndex = 19;
            // 
            // lPt2LDegMinSec
            // 
            this.lPt2LDegMinSec.AutoSize = true;
            this.lPt2LDegMinSec.Location = new System.Drawing.Point(4, 41);
            this.lPt2LDegMinSec.Name = "lPt2LDegMinSec";
            this.lPt2LDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lPt2LDegMinSec.TabIndex = 17;
            this.lPt2LDegMinSec.Text = "L.................";
            // 
            // tbPt2BDeg2
            // 
            this.tbPt2BDeg2.Location = new System.Drawing.Point(30, 12);
            this.tbPt2BDeg2.MaxLength = 2;
            this.tbPt2BDeg2.Name = "tbPt2BDeg2";
            this.tbPt2BDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt2BDeg2.TabIndex = 15;
            // 
            // lPt2BDegMinSec
            // 
            this.lPt2BDegMinSec.AutoSize = true;
            this.lPt2BDegMinSec.Location = new System.Drawing.Point(4, 18);
            this.lPt2BDegMinSec.Name = "lPt2BDegMinSec";
            this.lPt2BDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lPt2BDegMinSec.TabIndex = 14;
            this.lPt2BDegMinSec.Text = "B...............";
            // 
            // gbPt2Rad
            // 
            this.gbPt2Rad.Controls.Add(this.tbPt2LRad);
            this.gbPt2Rad.Controls.Add(this.lPt2LRad);
            this.gbPt2Rad.Controls.Add(this.tbPt2BRad);
            this.gbPt2Rad.Controls.Add(this.lPt2BRad);
            this.gbPt2Rad.Location = new System.Drawing.Point(172, 75);
            this.gbPt2Rad.Name = "gbPt2Rad";
            this.gbPt2Rad.Size = new System.Drawing.Size(160, 63);
            this.gbPt2Rad.TabIndex = 31;
            this.gbPt2Rad.TabStop = false;
            this.gbPt2Rad.Visible = false;
            // 
            // tbPt2LRad
            // 
            this.tbPt2LRad.Location = new System.Drawing.Point(78, 36);
            this.tbPt2LRad.MaxLength = 10;
            this.tbPt2LRad.Name = "tbPt2LRad";
            this.tbPt2LRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt2LRad.TabIndex = 5;
            // 
            // lPt2LRad
            // 
            this.lPt2LRad.AutoSize = true;
            this.lPt2LRad.Location = new System.Drawing.Point(4, 41);
            this.lPt2LRad.Name = "lPt2LRad";
            this.lPt2LRad.Size = new System.Drawing.Size(94, 13);
            this.lPt2LRad.TabIndex = 6;
            this.lPt2LRad.Text = "L, рад...................";
            // 
            // tbPt2BRad
            // 
            this.tbPt2BRad.Location = new System.Drawing.Point(78, 13);
            this.tbPt2BRad.MaxLength = 10;
            this.tbPt2BRad.Name = "tbPt2BRad";
            this.tbPt2BRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt2BRad.TabIndex = 4;
            // 
            // lPt2BRad
            // 
            this.lPt2BRad.AutoSize = true;
            this.lPt2BRad.Location = new System.Drawing.Point(4, 18);
            this.lPt2BRad.Name = "lPt2BRad";
            this.lPt2BRad.Size = new System.Drawing.Size(95, 13);
            this.lPt2BRad.TabIndex = 3;
            this.lPt2BRad.Text = "B, рад...................";
            // 
            // gbPt2Rect
            // 
            this.gbPt2Rect.Controls.Add(this.tbPt2YRect);
            this.gbPt2Rect.Controls.Add(this.lPt2YRect);
            this.gbPt2Rect.Controls.Add(this.tbPt2XRect);
            this.gbPt2Rect.Controls.Add(this.lPt2XRect);
            this.gbPt2Rect.Location = new System.Drawing.Point(6, 11);
            this.gbPt2Rect.Name = "gbPt2Rect";
            this.gbPt2Rect.Size = new System.Drawing.Size(160, 63);
            this.gbPt2Rect.TabIndex = 29;
            this.gbPt2Rect.TabStop = false;
            this.gbPt2Rect.Visible = false;
            // 
            // tbPt2YRect
            // 
            this.tbPt2YRect.Location = new System.Drawing.Point(78, 36);
            this.tbPt2YRect.MaxLength = 7;
            this.tbPt2YRect.Name = "tbPt2YRect";
            this.tbPt2YRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt2YRect.TabIndex = 5;
            // 
            // lPt2YRect
            // 
            this.lPt2YRect.AutoSize = true;
            this.lPt2YRect.Location = new System.Drawing.Point(4, 42);
            this.lPt2YRect.Name = "lPt2YRect";
            this.lPt2YRect.Size = new System.Drawing.Size(85, 13);
            this.lPt2YRect.TabIndex = 6;
            this.lPt2YRect.Text = "Y, м...................";
            // 
            // tbPt2XRect
            // 
            this.tbPt2XRect.Location = new System.Drawing.Point(78, 13);
            this.tbPt2XRect.MaxLength = 7;
            this.tbPt2XRect.Name = "tbPt2XRect";
            this.tbPt2XRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt2XRect.TabIndex = 4;
            // 
            // lPt2XRect
            // 
            this.lPt2XRect.AutoSize = true;
            this.lPt2XRect.Location = new System.Drawing.Point(4, 20);
            this.lPt2XRect.Name = "lPt2XRect";
            this.lPt2XRect.Size = new System.Drawing.Size(85, 13);
            this.lPt2XRect.TabIndex = 3;
            this.lPt2XRect.Text = "X, м...................";
            // 
            // gbPt2DegMin
            // 
            this.gbPt2DegMin.Controls.Add(this.tbPt2LMin1);
            this.gbPt2DegMin.Controls.Add(this.tbPt2BMin1);
            this.gbPt2DegMin.Controls.Add(this.lPt2LDegMin);
            this.gbPt2DegMin.Controls.Add(this.lPt2BDegMin);
            this.gbPt2DegMin.Location = new System.Drawing.Point(3, 19);
            this.gbPt2DegMin.Name = "gbPt2DegMin";
            this.gbPt2DegMin.Size = new System.Drawing.Size(145, 63);
            this.gbPt2DegMin.TabIndex = 34;
            this.gbPt2DegMin.TabStop = false;
            this.gbPt2DegMin.Visible = false;
            this.gbPt2DegMin.Enter += new System.EventHandler(this.gbPt2DegMin_Enter);
            // 
            // tbPt2LMin1
            // 
            this.tbPt2LMin1.Location = new System.Drawing.Point(60, 38);
            this.tbPt2LMin1.MaxLength = 8;
            this.tbPt2LMin1.Name = "tbPt2LMin1";
            this.tbPt2LMin1.Size = new System.Drawing.Size(50, 20);
            this.tbPt2LMin1.TabIndex = 15;
            // 
            // tbPt2BMin1
            // 
            this.tbPt2BMin1.Location = new System.Drawing.Point(63, 15);
            this.tbPt2BMin1.MaxLength = 8;
            this.tbPt2BMin1.Name = "tbPt2BMin1";
            this.tbPt2BMin1.Size = new System.Drawing.Size(50, 20);
            this.tbPt2BMin1.TabIndex = 13;
            // 
            // lPt2LDegMin
            // 
            this.lPt2LDegMin.AutoSize = true;
            this.lPt2LDegMin.Location = new System.Drawing.Point(4, 41);
            this.lPt2LDegMin.Name = "lPt2LDegMin";
            this.lPt2LDegMin.Size = new System.Drawing.Size(64, 13);
            this.lPt2LDegMin.TabIndex = 12;
            this.lPt2LDegMin.Text = "L.................";
            // 
            // lPt2BDegMin
            // 
            this.lPt2BDegMin.AutoSize = true;
            this.lPt2BDegMin.Location = new System.Drawing.Point(4, 18);
            this.lPt2BDegMin.Name = "lPt2BDegMin";
            this.lPt2BDegMin.Size = new System.Drawing.Size(59, 13);
            this.lPt2BDegMin.TabIndex = 10;
            this.lPt2BDegMin.Text = "B...............";
            // 
            // gbPt2Rect42
            // 
            this.gbPt2Rect42.Controls.Add(this.tbPt2YRect42);
            this.gbPt2Rect42.Controls.Add(this.lPt2YRect42);
            this.gbPt2Rect42.Controls.Add(this.tbPt2XRect42);
            this.gbPt2Rect42.Controls.Add(this.lPt2XRect42);
            this.gbPt2Rect42.Location = new System.Drawing.Point(6, 18);
            this.gbPt2Rect42.Name = "gbPt2Rect42";
            this.gbPt2Rect42.Size = new System.Drawing.Size(148, 63);
            this.gbPt2Rect42.TabIndex = 30;
            this.gbPt2Rect42.TabStop = false;
            // 
            // tbPt2YRect42
            // 
            this.tbPt2YRect42.Location = new System.Drawing.Point(62, 39);
            this.tbPt2YRect42.MaxLength = 7;
            this.tbPt2YRect42.Name = "tbPt2YRect42";
            this.tbPt2YRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt2YRect42.TabIndex = 5;
            // 
            // lPt2YRect42
            // 
            this.lPt2YRect42.AutoSize = true;
            this.lPt2YRect42.Location = new System.Drawing.Point(4, 42);
            this.lPt2YRect42.Name = "lPt2YRect42";
            this.lPt2YRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt2YRect42.TabIndex = 6;
            this.lPt2YRect42.Text = "Y, м...................";
            // 
            // tbPt2XRect42
            // 
            this.tbPt2XRect42.Location = new System.Drawing.Point(62, 13);
            this.tbPt2XRect42.MaxLength = 7;
            this.tbPt2XRect42.Name = "tbPt2XRect42";
            this.tbPt2XRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt2XRect42.TabIndex = 4;
            // 
            // lPt2XRect42
            // 
            this.lPt2XRect42.AutoSize = true;
            this.lPt2XRect42.Location = new System.Drawing.Point(4, 20);
            this.lPt2XRect42.Name = "lPt2XRect42";
            this.lPt2XRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt2XRect42.TabIndex = 3;
            this.lPt2XRect42.Text = "X, м...................";
            // 
            // gbPoint1
            // 
            this.gbPoint1.Controls.Add(this.tbPt1Height);
            this.gbPoint1.Controls.Add(this.lPt1Height);
            this.gbPoint1.Controls.Add(this.gbPt1DegMinSec);
            this.gbPoint1.Controls.Add(this.gbPt1Rad);
            this.gbPoint1.Controls.Add(this.gbPt1Rect);
            this.gbPoint1.Controls.Add(this.gbPt1DegMin);
            this.gbPoint1.Controls.Add(this.gbPt1Rect42);
            this.gbPoint1.Location = new System.Drawing.Point(5, 31);
            this.gbPoint1.Name = "gbPoint1";
            this.gbPoint1.Size = new System.Drawing.Size(172, 103);
            this.gbPoint1.TabIndex = 85;
            this.gbPoint1.TabStop = false;
            this.gbPoint1.Text = "УС 1";
            // 
            // tbPt1Height
            // 
            this.tbPt1Height.BackColor = System.Drawing.SystemColors.Menu;
            this.tbPt1Height.Location = new System.Drawing.Point(114, 77);
            this.tbPt1Height.Name = "tbPt1Height";
            this.tbPt1Height.Size = new System.Drawing.Size(48, 20);
            this.tbPt1Height.TabIndex = 37;
            // 
            // lPt1Height
            // 
            this.lPt1Height.AutoSize = true;
            this.lPt1Height.Location = new System.Drawing.Point(10, 84);
            this.lPt1Height.Name = "lPt1Height";
            this.lPt1Height.Size = new System.Drawing.Size(116, 13);
            this.lPt1Height.TabIndex = 36;
            this.lPt1Height.Text = "H, м.............................";
            // 
            // gbPt1DegMinSec
            // 
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BSec);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LMin2);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BMin2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec1);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1LDegMinSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1BDegMinSec);
            this.gbPt1DegMinSec.Location = new System.Drawing.Point(3, 11);
            this.gbPt1DegMinSec.Name = "gbPt1DegMinSec";
            this.gbPt1DegMinSec.Size = new System.Drawing.Size(142, 63);
            this.gbPt1DegMinSec.TabIndex = 35;
            this.gbPt1DegMinSec.TabStop = false;
            this.gbPt1DegMinSec.Visible = false;
            // 
            // tbPt1LSec
            // 
            this.tbPt1LSec.Location = new System.Drawing.Point(93, 38);
            this.tbPt1LSec.MaxLength = 3;
            this.tbPt1LSec.Name = "tbPt1LSec";
            this.tbPt1LSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LSec.TabIndex = 23;
            // 
            // tbPt1BSec
            // 
            this.tbPt1BSec.Location = new System.Drawing.Point(93, 12);
            this.tbPt1BSec.MaxLength = 3;
            this.tbPt1BSec.Name = "tbPt1BSec";
            this.tbPt1BSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BSec.TabIndex = 18;
            // 
            // lPt1Min4
            // 
            this.lPt1Min4.AutoSize = true;
            this.lPt1Min4.Location = new System.Drawing.Point(84, 41);
            this.lPt1Min4.Name = "lPt1Min4";
            this.lPt1Min4.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min4.TabIndex = 27;
            this.lPt1Min4.Text = "\'";
            // 
            // lPt1Min3
            // 
            this.lPt1Min3.AutoSize = true;
            this.lPt1Min3.Location = new System.Drawing.Point(84, 15);
            this.lPt1Min3.Name = "lPt1Min3";
            this.lPt1Min3.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min3.TabIndex = 26;
            this.lPt1Min3.Text = "\'";
            // 
            // tbPt1LMin2
            // 
            this.tbPt1LMin2.Location = new System.Drawing.Point(59, 38);
            this.tbPt1LMin2.MaxLength = 2;
            this.tbPt1LMin2.Name = "tbPt1LMin2";
            this.tbPt1LMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LMin2.TabIndex = 22;
            // 
            // tbPt1BMin2
            // 
            this.tbPt1BMin2.Location = new System.Drawing.Point(59, 14);
            this.tbPt1BMin2.MaxLength = 2;
            this.tbPt1BMin2.Name = "tbPt1BMin2";
            this.tbPt1BMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BMin2.TabIndex = 16;
            this.tbPt1BMin2.TextChanged += new System.EventHandler(this.tbPt1BMin2_TextChanged);
            // 
            // lPt1Sec2
            // 
            this.lPt1Sec2.AutoSize = true;
            this.lPt1Sec2.Location = new System.Drawing.Point(120, 37);
            this.lPt1Sec2.Name = "lPt1Sec2";
            this.lPt1Sec2.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec2.TabIndex = 25;
            this.lPt1Sec2.Text = "\'";
            // 
            // lPt1Sec1
            // 
            this.lPt1Sec1.AutoSize = true;
            this.lPt1Sec1.Location = new System.Drawing.Point(117, 13);
            this.lPt1Sec1.Name = "lPt1Sec1";
            this.lPt1Sec1.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec1.TabIndex = 24;
            this.lPt1Sec1.Text = "\'";
            // 
            // lPt1Deg4
            // 
            this.lPt1Deg4.AutoSize = true;
            this.lPt1Deg4.Location = new System.Drawing.Point(46, 36);
            this.lPt1Deg4.Name = "lPt1Deg4";
            this.lPt1Deg4.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg4.TabIndex = 21;
            this.lPt1Deg4.Text = "^";
            // 
            // lPt1Deg3
            // 
            this.lPt1Deg3.AutoSize = true;
            this.lPt1Deg3.Location = new System.Drawing.Point(46, 13);
            this.lPt1Deg3.Name = "lPt1Deg3";
            this.lPt1Deg3.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg3.TabIndex = 20;
            this.lPt1Deg3.Text = "^";
            // 
            // tbPt1LDeg2
            // 
            this.tbPt1LDeg2.Location = new System.Drawing.Point(19, 37);
            this.tbPt1LDeg2.MaxLength = 2;
            this.tbPt1LDeg2.Name = "tbPt1LDeg2";
            this.tbPt1LDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LDeg2.TabIndex = 19;
            // 
            // lPt1LDegMinSec
            // 
            this.lPt1LDegMinSec.AutoSize = true;
            this.lPt1LDegMinSec.Location = new System.Drawing.Point(4, 41);
            this.lPt1LDegMinSec.Name = "lPt1LDegMinSec";
            this.lPt1LDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lPt1LDegMinSec.TabIndex = 17;
            this.lPt1LDegMinSec.Text = "L.................";
            // 
            // tbPt1BDeg2
            // 
            this.tbPt1BDeg2.Location = new System.Drawing.Point(19, 13);
            this.tbPt1BDeg2.MaxLength = 2;
            this.tbPt1BDeg2.Name = "tbPt1BDeg2";
            this.tbPt1BDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BDeg2.TabIndex = 15;
            // 
            // lPt1BDegMinSec
            // 
            this.lPt1BDegMinSec.AutoSize = true;
            this.lPt1BDegMinSec.Location = new System.Drawing.Point(4, 18);
            this.lPt1BDegMinSec.Name = "lPt1BDegMinSec";
            this.lPt1BDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lPt1BDegMinSec.TabIndex = 14;
            this.lPt1BDegMinSec.Text = "B...............";
            // 
            // gbPt1Rad
            // 
            this.gbPt1Rad.Controls.Add(this.tbPt1LRad);
            this.gbPt1Rad.Controls.Add(this.lPt1LRad);
            this.gbPt1Rad.Controls.Add(this.tbPt1BRad);
            this.gbPt1Rad.Controls.Add(this.lPt1BRad);
            this.gbPt1Rad.Location = new System.Drawing.Point(172, 75);
            this.gbPt1Rad.Name = "gbPt1Rad";
            this.gbPt1Rad.Size = new System.Drawing.Size(160, 63);
            this.gbPt1Rad.TabIndex = 31;
            this.gbPt1Rad.TabStop = false;
            this.gbPt1Rad.Visible = false;
            // 
            // tbPt1LRad
            // 
            this.tbPt1LRad.Location = new System.Drawing.Point(78, 36);
            this.tbPt1LRad.MaxLength = 10;
            this.tbPt1LRad.Name = "tbPt1LRad";
            this.tbPt1LRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1LRad.TabIndex = 5;
            // 
            // lPt1LRad
            // 
            this.lPt1LRad.AutoSize = true;
            this.lPt1LRad.Location = new System.Drawing.Point(4, 41);
            this.lPt1LRad.Name = "lPt1LRad";
            this.lPt1LRad.Size = new System.Drawing.Size(94, 13);
            this.lPt1LRad.TabIndex = 6;
            this.lPt1LRad.Text = "L, рад...................";
            // 
            // tbPt1BRad
            // 
            this.tbPt1BRad.Location = new System.Drawing.Point(78, 13);
            this.tbPt1BRad.MaxLength = 10;
            this.tbPt1BRad.Name = "tbPt1BRad";
            this.tbPt1BRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1BRad.TabIndex = 4;
            // 
            // lPt1BRad
            // 
            this.lPt1BRad.AutoSize = true;
            this.lPt1BRad.Location = new System.Drawing.Point(4, 18);
            this.lPt1BRad.Name = "lPt1BRad";
            this.lPt1BRad.Size = new System.Drawing.Size(95, 13);
            this.lPt1BRad.TabIndex = 3;
            this.lPt1BRad.Text = "B, рад...................";
            // 
            // gbPt1Rect
            // 
            this.gbPt1Rect.Controls.Add(this.tbPt1YRect);
            this.gbPt1Rect.Controls.Add(this.lPt1YRect);
            this.gbPt1Rect.Controls.Add(this.tbPt1XRect);
            this.gbPt1Rect.Controls.Add(this.lPt1XRect);
            this.gbPt1Rect.Location = new System.Drawing.Point(6, 11);
            this.gbPt1Rect.Name = "gbPt1Rect";
            this.gbPt1Rect.Size = new System.Drawing.Size(160, 63);
            this.gbPt1Rect.TabIndex = 29;
            this.gbPt1Rect.TabStop = false;
            this.gbPt1Rect.Visible = false;
            // 
            // tbPt1YRect
            // 
            this.tbPt1YRect.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect.MaxLength = 7;
            this.tbPt1YRect.Name = "tbPt1YRect";
            this.tbPt1YRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect.TabIndex = 5;
            // 
            // lPt1YRect
            // 
            this.lPt1YRect.AutoSize = true;
            this.lPt1YRect.Location = new System.Drawing.Point(4, 42);
            this.lPt1YRect.Name = "lPt1YRect";
            this.lPt1YRect.Size = new System.Drawing.Size(85, 13);
            this.lPt1YRect.TabIndex = 6;
            this.lPt1YRect.Text = "Y, м...................";
            // 
            // tbPt1XRect
            // 
            this.tbPt1XRect.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect.MaxLength = 7;
            this.tbPt1XRect.Name = "tbPt1XRect";
            this.tbPt1XRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect.TabIndex = 4;
            // 
            // lPt1XRect
            // 
            this.lPt1XRect.AutoSize = true;
            this.lPt1XRect.Location = new System.Drawing.Point(4, 20);
            this.lPt1XRect.Name = "lPt1XRect";
            this.lPt1XRect.Size = new System.Drawing.Size(85, 13);
            this.lPt1XRect.TabIndex = 3;
            this.lPt1XRect.Text = "X, м...................";
            // 
            // gbPt1DegMin
            // 
            this.gbPt1DegMin.Controls.Add(this.tbPt1LMin1);
            this.gbPt1DegMin.Controls.Add(this.tbPt1BMin1);
            this.gbPt1DegMin.Controls.Add(this.lPt1LDegMin);
            this.gbPt1DegMin.Controls.Add(this.lPt1BDegMin);
            this.gbPt1DegMin.Location = new System.Drawing.Point(5, 11);
            this.gbPt1DegMin.Name = "gbPt1DegMin";
            this.gbPt1DegMin.Size = new System.Drawing.Size(146, 63);
            this.gbPt1DegMin.TabIndex = 34;
            this.gbPt1DegMin.TabStop = false;
            this.gbPt1DegMin.Visible = false;
            // 
            // tbPt1LMin1
            // 
            this.tbPt1LMin1.Location = new System.Drawing.Point(62, 38);
            this.tbPt1LMin1.MaxLength = 8;
            this.tbPt1LMin1.Name = "tbPt1LMin1";
            this.tbPt1LMin1.Size = new System.Drawing.Size(50, 20);
            this.tbPt1LMin1.TabIndex = 15;
            // 
            // tbPt1BMin1
            // 
            this.tbPt1BMin1.Location = new System.Drawing.Point(62, 15);
            this.tbPt1BMin1.MaxLength = 8;
            this.tbPt1BMin1.Name = "tbPt1BMin1";
            this.tbPt1BMin1.Size = new System.Drawing.Size(50, 20);
            this.tbPt1BMin1.TabIndex = 13;
            // 
            // lPt1LDegMin
            // 
            this.lPt1LDegMin.AutoSize = true;
            this.lPt1LDegMin.Location = new System.Drawing.Point(4, 41);
            this.lPt1LDegMin.Name = "lPt1LDegMin";
            this.lPt1LDegMin.Size = new System.Drawing.Size(64, 13);
            this.lPt1LDegMin.TabIndex = 12;
            this.lPt1LDegMin.Text = "L.................";
            // 
            // lPt1BDegMin
            // 
            this.lPt1BDegMin.AutoSize = true;
            this.lPt1BDegMin.Location = new System.Drawing.Point(4, 18);
            this.lPt1BDegMin.Name = "lPt1BDegMin";
            this.lPt1BDegMin.Size = new System.Drawing.Size(59, 13);
            this.lPt1BDegMin.TabIndex = 10;
            this.lPt1BDegMin.Text = "B...............";
            // 
            // gbPt1Rect42
            // 
            this.gbPt1Rect42.Controls.Add(this.tbPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.tbPt1XRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1XRect42);
            this.gbPt1Rect42.Location = new System.Drawing.Point(5, 11);
            this.gbPt1Rect42.Name = "gbPt1Rect42";
            this.gbPt1Rect42.Size = new System.Drawing.Size(153, 63);
            this.gbPt1Rect42.TabIndex = 30;
            this.gbPt1Rect42.TabStop = false;
            this.gbPt1Rect42.Enter += new System.EventHandler(this.gbPt1Rect42_Enter);
            // 
            // tbPt1YRect42
            // 
            this.tbPt1YRect42.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect42.MaxLength = 7;
            this.tbPt1YRect42.Name = "tbPt1YRect42";
            this.tbPt1YRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect42.TabIndex = 5;
            // 
            // lPt1YRect42
            // 
            this.lPt1YRect42.AutoSize = true;
            this.lPt1YRect42.Location = new System.Drawing.Point(13, 43);
            this.lPt1YRect42.Name = "lPt1YRect42";
            this.lPt1YRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt1YRect42.TabIndex = 6;
            this.lPt1YRect42.Text = "Y, м...................";
            // 
            // tbPt1XRect42
            // 
            this.tbPt1XRect42.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect42.MaxLength = 7;
            this.tbPt1XRect42.Name = "tbPt1XRect42";
            this.tbPt1XRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect42.TabIndex = 4;
            // 
            // lPt1XRect42
            // 
            this.lPt1XRect42.AutoSize = true;
            this.lPt1XRect42.Location = new System.Drawing.Point(13, 16);
            this.lPt1XRect42.Name = "lPt1XRect42";
            this.lPt1XRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt1XRect42.TabIndex = 3;
            this.lPt1XRect42.Text = "X, м...................";
            // 
            // lCountOpponent
            // 
            this.lCountOpponent.AutoSize = true;
            this.lCountOpponent.Location = new System.Drawing.Point(5, 147);
            this.lCountOpponent.Name = "lCountOpponent";
            this.lCountOpponent.Size = new System.Drawing.Size(303, 13);
            this.lCountOpponent.TabIndex = 79;
            this.lCountOpponent.Text = "Количество объектов.............................................................." +
    "";
            // 
            // tbRadiusZone
            // 
            this.tbRadiusZone.BackColor = System.Drawing.SystemColors.Info;
            this.tbRadiusZone.Location = new System.Drawing.Point(117, 241);
            this.tbRadiusZone.Name = "tbRadiusZone";
            this.tbRadiusZone.Size = new System.Drawing.Size(51, 20);
            this.tbRadiusZone.TabIndex = 118;
            this.tbRadiusZone.TextChanged += new System.EventHandler(this.tbRadiusZone_TextChanged);
            // 
            // lRadiusZone
            // 
            this.lRadiusZone.AutoSize = true;
            this.lRadiusZone.Location = new System.Drawing.Point(9, 248);
            this.lRadiusZone.Name = "lRadiusZone";
            this.lRadiusZone.Size = new System.Drawing.Size(104, 13);
            this.lRadiusZone.TabIndex = 119;
            this.lRadiusZone.Text = "Дальность РП, м...";
            // 
            // tbResult
            // 
            this.tbResult.BackColor = System.Drawing.SystemColors.Info;
            this.tbResult.Location = new System.Drawing.Point(5, 275);
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(190, 20);
            this.tbResult.TabIndex = 120;
            // 
            // tbMaxDist
            // 
            this.tbMaxDist.BackColor = System.Drawing.SystemColors.Info;
            this.tbMaxDist.Location = new System.Drawing.Point(226, 241);
            this.tbMaxDist.Name = "tbMaxDist";
            this.tbMaxDist.ReadOnly = true;
            this.tbMaxDist.Size = new System.Drawing.Size(52, 20);
            this.tbMaxDist.TabIndex = 121;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(174, 244);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 122;
            this.label13.Text = "ДПВ, м..";
            // 
            // tbHindSignal
            // 
            this.tbHindSignal.BackColor = System.Drawing.SystemColors.Info;
            this.tbHindSignal.Location = new System.Drawing.Point(414, 241);
            this.tbHindSignal.Name = "tbHindSignal";
            this.tbHindSignal.Size = new System.Drawing.Size(59, 20);
            this.tbHindSignal.TabIndex = 123;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(304, 241);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(112, 13);
            this.label14.TabIndex = 124;
            this.label14.Text = "K (помеха/сигнал)....";
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(204, 272);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(59, 23);
            this.bAccept.TabIndex = 125;
            this.bAccept.Text = "Принять";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click_1);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(269, 272);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(67, 23);
            this.bClear.TabIndex = 126;
            this.bClear.Text = "Очистить";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(342, 272);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 23);
            this.button1.TabIndex = 127;
            this.button1.Text = "СП";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(386, 272);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(54, 23);
            this.button2.TabIndex = 128;
            this.button2.Text = "УС1";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(446, 272);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(54, 23);
            this.button3.TabIndex = 129;
            this.button3.Text = "УС2";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // grbDetail
            // 
            this.grbDetail.Controls.Add(this.tbCorrectHeightOpponent);
            this.grbDetail.Controls.Add(this.label10);
            this.grbDetail.Controls.Add(this.tbGamma);
            this.grbDetail.Controls.Add(this.label9);
            this.grbDetail.Controls.Add(this.tbResultHeightOpponent);
            this.grbDetail.Controls.Add(this.label5);
            this.grbDetail.Controls.Add(this.tbMinHeight);
            this.grbDetail.Controls.Add(this.label6);
            this.grbDetail.Controls.Add(this.tbMiddleHeight);
            this.grbDetail.Controls.Add(this.label7);
            this.grbDetail.Controls.Add(this.tbMaxDistance);
            this.grbDetail.Controls.Add(this.label8);
            this.grbDetail.Controls.Add(this.tbCorrectHeightOwn);
            this.grbDetail.Controls.Add(this.label4);
            this.grbDetail.Controls.Add(this.tbCoeffHE);
            this.grbDetail.Controls.Add(this.label3);
            this.grbDetail.Controls.Add(this.tbCoeffQ);
            this.grbDetail.Controls.Add(this.label2);
            this.grbDetail.Controls.Add(this.tbResultHeightOwn);
            this.grbDetail.Controls.Add(this.label1);
            this.grbDetail.Location = new System.Drawing.Point(21, 323);
            this.grbDetail.Name = "grbDetail";
            this.grbDetail.Size = new System.Drawing.Size(477, 128);
            this.grbDetail.TabIndex = 130;
            this.grbDetail.TabStop = false;
            // 
            // tbCorrectHeightOpponent
            // 
            this.tbCorrectHeightOpponent.Location = new System.Drawing.Point(411, 55);
            this.tbCorrectHeightOpponent.Name = "tbCorrectHeightOpponent";
            this.tbCorrectHeightOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCorrectHeightOpponent.TabIndex = 116;
            this.tbCorrectHeightOpponent.Text = "4";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(254, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(200, 13);
            this.label10.TabIndex = 115;
            this.label10.Text = "CorrectHeightOpponent...........................";
            // 
            // tbGamma
            // 
            this.tbGamma.Location = new System.Drawing.Point(159, 100);
            this.tbGamma.Name = "tbGamma";
            this.tbGamma.Size = new System.Drawing.Size(60, 20);
            this.tbGamma.TabIndex = 114;
            this.tbGamma.Text = "4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 13);
            this.label9.TabIndex = 113;
            this.label9.Text = "Gamma............................................";
            // 
            // tbResultHeightOpponent
            // 
            this.tbResultHeightOpponent.Location = new System.Drawing.Point(411, 77);
            this.tbResultHeightOpponent.Name = "tbResultHeightOpponent";
            this.tbResultHeightOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbResultHeightOpponent.TabIndex = 112;
            this.tbResultHeightOpponent.Text = "4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(254, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(193, 13);
            this.label5.TabIndex = 111;
            this.label5.Text = "ResultHeightOpponent..........................";
            // 
            // tbMinHeight
            // 
            this.tbMinHeight.Location = new System.Drawing.Point(411, 33);
            this.tbMinHeight.Name = "tbMinHeight";
            this.tbMinHeight.Size = new System.Drawing.Size(60, 20);
            this.tbMinHeight.TabIndex = 110;
            this.tbMinHeight.Text = "4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(254, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 13);
            this.label6.TabIndex = 109;
            this.label6.Text = "MinHeight.......................................";
            // 
            // tbMiddleHeight
            // 
            this.tbMiddleHeight.Location = new System.Drawing.Point(411, 12);
            this.tbMiddleHeight.Name = "tbMiddleHeight";
            this.tbMiddleHeight.Size = new System.Drawing.Size(60, 20);
            this.tbMiddleHeight.TabIndex = 108;
            this.tbMiddleHeight.Text = "4";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(254, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(186, 13);
            this.label7.TabIndex = 107;
            this.label7.Text = "MiddleHeight.......................................";
            // 
            // tbMaxDistance
            // 
            this.tbMaxDistance.Location = new System.Drawing.Point(411, 100);
            this.tbMaxDistance.Name = "tbMaxDistance";
            this.tbMaxDistance.Size = new System.Drawing.Size(60, 20);
            this.tbMaxDistance.TabIndex = 106;
            this.tbMaxDistance.Text = "4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 13);
            this.label8.TabIndex = 105;
            this.label8.Text = "MaxDistance................................";
            // 
            // tbCorrectHeightOwn
            // 
            this.tbCorrectHeightOwn.Location = new System.Drawing.Point(159, 55);
            this.tbCorrectHeightOwn.Name = "tbCorrectHeightOwn";
            this.tbCorrectHeightOwn.Size = new System.Drawing.Size(60, 20);
            this.tbCorrectHeightOwn.TabIndex = 104;
            this.tbCorrectHeightOwn.Text = "4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 13);
            this.label4.TabIndex = 103;
            this.label4.Text = "CorrectHeightOwn...........................";
            // 
            // tbCoeffHE
            // 
            this.tbCoeffHE.Location = new System.Drawing.Point(159, 33);
            this.tbCoeffHE.Name = "tbCoeffHE";
            this.tbCoeffHE.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffHE.TabIndex = 102;
            this.tbCoeffHE.Text = "4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 101;
            this.label3.Text = "CoeffHE.......................................";
            // 
            // tbCoeffQ
            // 
            this.tbCoeffQ.Location = new System.Drawing.Point(159, 12);
            this.tbCoeffQ.Name = "tbCoeffQ";
            this.tbCoeffQ.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffQ.TabIndex = 100;
            this.tbCoeffQ.Text = "4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 13);
            this.label2.TabIndex = 99;
            this.label2.Text = "CoeffQ............................................";
            // 
            // tbResultHeightOwn
            // 
            this.tbResultHeightOwn.Location = new System.Drawing.Point(159, 77);
            this.tbResultHeightOwn.Name = "tbResultHeightOwn";
            this.tbResultHeightOwn.Size = new System.Drawing.Size(60, 20);
            this.tbResultHeightOwn.TabIndex = 98;
            this.tbResultHeightOwn.Text = "4";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 13);
            this.label1.TabIndex = 97;
            this.label1.Text = "ResultHeightOwn.............................";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 311);
            this.Controls.Add(this.grbDetail);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tbHindSignal);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbMaxDist);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.lRadiusZone);
            this.Controls.Add(this.tbRadiusZone);
            this.Controls.Add(this.tcParam);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Расчет энергодоступности по узлам связи";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tcParam.ResumeLayout(false);
            this.tpOwnObject.ResumeLayout(false);
            this.tpOwnObject.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.grbOwnObject.ResumeLayout(false);
            this.grbOwnObject.PerformLayout();
            this.tpOpponentObject.ResumeLayout(false);
            this.tpOpponentObject.PerformLayout();
            this.grbCoeffSupOpponent.ResumeLayout(false);
            this.grbCoeffSupOpponent.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCountOpponent)).EndInit();
            this.gbPoint2.ResumeLayout(false);
            this.gbPoint2.PerformLayout();
            this.gbPt2DegMinSec.ResumeLayout(false);
            this.gbPt2DegMinSec.PerformLayout();
            this.gbPt2Rad.ResumeLayout(false);
            this.gbPt2Rad.PerformLayout();
            this.gbPt2Rect.ResumeLayout(false);
            this.gbPt2Rect.PerformLayout();
            this.gbPt2DegMin.ResumeLayout(false);
            this.gbPt2DegMin.PerformLayout();
            this.gbPt2Rect42.ResumeLayout(false);
            this.gbPt2Rect42.PerformLayout();
            this.gbPoint1.ResumeLayout(false);
            this.gbPoint1.PerformLayout();
            this.gbPt1DegMinSec.ResumeLayout(false);
            this.gbPt1DegMinSec.PerformLayout();
            this.gbPt1Rad.ResumeLayout(false);
            this.gbPt1Rad.PerformLayout();
            this.gbPt1Rect.ResumeLayout(false);
            this.gbPt1Rect.PerformLayout();
            this.gbPt1DegMin.ResumeLayout(false);
            this.gbPt1DegMin.PerformLayout();
            this.gbPt1Rect42.ResumeLayout(false);
            this.gbPt1Rect42.PerformLayout();
            this.grbDetail.ResumeLayout(false);
            this.grbDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TabControl tcParam;
        private System.Windows.Forms.TabPage tpOwnObject;
        private System.Windows.Forms.Label lPowerOwn;
        private System.Windows.Forms.TextBox tbCoeffOwn;
        private System.Windows.Forms.Label lCoeffOwn;
        private System.Windows.Forms.TextBox tbPowerOwn;
        private System.Windows.Forms.ComboBox cbSurface;
        private System.Windows.Forms.Label lSurface;
        private System.Windows.Forms.ComboBox cbWidthHindrance;
        private System.Windows.Forms.Label lWidthHindrance;
        private System.Windows.Forms.Label lChooseSC;
        private System.Windows.Forms.Panel pCoordPoint;
        private System.Windows.Forms.ComboBox cbOwnObject;
        private System.Windows.Forms.Label lCenterLSR;
        private System.Windows.Forms.TextBox tbOwnHeight;
        private System.Windows.Forms.Label lOwnHeight;
        private System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        private System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.GroupBox gbOwnDegMin;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        private System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        private System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.GroupBox grbOwnObject;
        private System.Windows.Forms.ComboBox cbHeightOwnObject;
        private System.Windows.Forms.TextBox tbHeightOwnObject;
        private System.Windows.Forms.Label lHeightOwnObject;
        private System.Windows.Forms.TabPage tpOpponentObject;
        private System.Windows.Forms.GroupBox grbCoeffSupOpponent;
        private System.Windows.Forms.TextBox tbCoeffSupOpponent;
        private System.Windows.Forms.ComboBox cbCoeffSupOpponent;
        private System.Windows.Forms.ComboBox cbTypeCommOpponent;
        private System.Windows.Forms.Label lTypeCommOpponent;
        private System.Windows.Forms.ComboBox cbPolarOpponent;
        private System.Windows.Forms.Label lPolarOpponent;
        private System.Windows.Forms.TextBox tbRangeComm;
        private System.Windows.Forms.Label lRangeComm;
        private System.Windows.Forms.TextBox tbHeightReceiverOpponent;
        private System.Windows.Forms.Label lHeightReceiverOpponent;
        private System.Windows.Forms.TextBox tbHeightTransmitOpponent;
        private System.Windows.Forms.Label lHeightTransmitOpponent;
        private System.Windows.Forms.TextBox tbWidthSignal;
        private System.Windows.Forms.Label lWidthSignal;
        private System.Windows.Forms.TextBox tbFreq;
        private System.Windows.Forms.Label lFreq;
        private System.Windows.Forms.TextBox tbCoeffReceiverOpponent;
        private System.Windows.Forms.Label lCoeffReceivertOpponent;
        private System.Windows.Forms.TextBox tbCoeffTransmitOpponent;
        private System.Windows.Forms.Label lCoeffTransmitOpponent;
        private System.Windows.Forms.TextBox tbPowerOpponent;
        private System.Windows.Forms.Label lPowerOpponent;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cbCommChooseSC;
        private System.Windows.Forms.Label lCommChooseSC;
        private System.Windows.Forms.NumericUpDown nudCountOpponent;
        private System.Windows.Forms.GroupBox gbPoint2;
        private System.Windows.Forms.TextBox tbPt2Height;
        private System.Windows.Forms.Label lPt2Height;
        private System.Windows.Forms.GroupBox gbPt2DegMinSec;
        public System.Windows.Forms.TextBox tbPt2LSec;
        public System.Windows.Forms.TextBox tbPt2BSec;
        private System.Windows.Forms.Label lPt2Min4;
        private System.Windows.Forms.Label lPt2Min3;
        public System.Windows.Forms.TextBox tbPt2LMin2;
        public System.Windows.Forms.TextBox tbPt2BMin2;
        private System.Windows.Forms.Label lPt2Sec2;
        private System.Windows.Forms.Label lPt2Sec1;
        private System.Windows.Forms.Label lPt2Deg4;
        private System.Windows.Forms.Label lPt2Deg3;
        public System.Windows.Forms.TextBox tbPt2LDeg2;
        private System.Windows.Forms.Label lPt2LDegMinSec;
        public System.Windows.Forms.TextBox tbPt2BDeg2;
        private System.Windows.Forms.Label lPt2BDegMinSec;
        private System.Windows.Forms.GroupBox gbPt2DegMin;
        public System.Windows.Forms.TextBox tbPt2LMin1;
        public System.Windows.Forms.TextBox tbPt2BMin1;
        private System.Windows.Forms.Label lPt2LDegMin;
        private System.Windows.Forms.Label lPt2BDegMin;
        private System.Windows.Forms.GroupBox gbPt2Rect42;
        public System.Windows.Forms.TextBox tbPt2YRect42;
        private System.Windows.Forms.Label lPt2YRect42;
        public System.Windows.Forms.TextBox tbPt2XRect42;
        private System.Windows.Forms.Label lPt2XRect42;
        private System.Windows.Forms.GroupBox gbPt2Rad;
        public System.Windows.Forms.TextBox tbPt2LRad;
        private System.Windows.Forms.Label lPt2LRad;
        public System.Windows.Forms.TextBox tbPt2BRad;
        private System.Windows.Forms.Label lPt2BRad;
        private System.Windows.Forms.GroupBox gbPt2Rect;
        public System.Windows.Forms.TextBox tbPt2YRect;
        private System.Windows.Forms.Label lPt2YRect;
        public System.Windows.Forms.TextBox tbPt2XRect;
        private System.Windows.Forms.Label lPt2XRect;
        private System.Windows.Forms.GroupBox gbPoint1;
        private System.Windows.Forms.TextBox tbPt1Height;
        private System.Windows.Forms.Label lPt1Height;
        private System.Windows.Forms.GroupBox gbPt1DegMinSec;
        public System.Windows.Forms.TextBox tbPt1LSec;
        public System.Windows.Forms.TextBox tbPt1BSec;
        private System.Windows.Forms.Label lPt1Min4;
        private System.Windows.Forms.Label lPt1Min3;
        public System.Windows.Forms.TextBox tbPt1LMin2;
        public System.Windows.Forms.TextBox tbPt1BMin2;
        private System.Windows.Forms.Label lPt1Sec2;
        private System.Windows.Forms.Label lPt1Sec1;
        private System.Windows.Forms.Label lPt1Deg4;
        private System.Windows.Forms.Label lPt1Deg3;
        public System.Windows.Forms.TextBox tbPt1LDeg2;
        private System.Windows.Forms.Label lPt1LDegMinSec;
        public System.Windows.Forms.TextBox tbPt1BDeg2;
        private System.Windows.Forms.Label lPt1BDegMinSec;
        private System.Windows.Forms.GroupBox gbPt1DegMin;
        public System.Windows.Forms.TextBox tbPt1LMin1;
        public System.Windows.Forms.TextBox tbPt1BMin1;
        private System.Windows.Forms.Label lPt1LDegMin;
        private System.Windows.Forms.Label lPt1BDegMin;
        private System.Windows.Forms.GroupBox gbPt1Rect42;
        public System.Windows.Forms.TextBox tbPt1YRect42;
        private System.Windows.Forms.Label lPt1YRect42;
        public System.Windows.Forms.TextBox tbPt1XRect42;
        private System.Windows.Forms.Label lPt1XRect42;
        private System.Windows.Forms.GroupBox gbPt1Rad;
        public System.Windows.Forms.TextBox tbPt1LRad;
        private System.Windows.Forms.Label lPt1LRad;
        public System.Windows.Forms.TextBox tbPt1BRad;
        private System.Windows.Forms.Label lPt1BRad;
        private System.Windows.Forms.GroupBox gbPt1Rect;
        public System.Windows.Forms.TextBox tbPt1YRect;
        private System.Windows.Forms.Label lPt1YRect;
        public System.Windows.Forms.TextBox tbPt1XRect;
        private System.Windows.Forms.Label lPt1XRect;
        private System.Windows.Forms.Label lCountOpponent;
        private System.Windows.Forms.TextBox tbRadiusZone;
        private System.Windows.Forms.Label lRadiusZone;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.TextBox tbMaxDist;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbHindSignal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox gbOwnRect;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        private System.Windows.Forms.ComboBox cbCap1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox grbDetail;
        private System.Windows.Forms.TextBox tbCorrectHeightOpponent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbGamma;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbResultHeightOpponent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMinHeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbMiddleHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbMaxDistance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCorrectHeightOwn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCoeffHE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCoeffQ;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbResultHeightOwn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbXY;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chbXY1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.RadioButton rbPoint2;
        public System.Windows.Forms.RadioButton rbPoint1;
        private System.Windows.Forms.TextBox tbHAnt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lCapacity;

    }
}