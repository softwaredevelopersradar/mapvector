﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

namespace GrozaMap
{
    public partial class ZonePowerAvail : Form
    {

        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        //private Point tpOwnCoordRect;


        public double LAMBDA;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public double dchislo;
        public long ichislo;

        // .....................................................................
        // Координаты СП

        public uint flCoordSP_ed; // =1-> Выбрали СП

        // Координаты СП на местности в м
        public double XSP_ed;
        public double YSP_ed;

        // DATUM
        public double dXdat_ed;
        public double dYdat_ed;
        public double dZdat_ed;

        public double dLat_ed;
        public double dLong_ed;

        // Эллипсоид Красовского, град
        public double LatKrG_ed;
        public double LongKrG_ed;
        // Эллипсоид Красовского, rad
        public double LatKrR_ed;
        public double LongKrR_ed;
        // Эллипсоид Красовского, град,мин,сек
        public int Lat_Grad_ed;
        public int Lat_Min_ed;
        public double Lat_Sec_ed;
        public int Long_Grad_ed;
        public int Long_Min_ed;
        public double Long_Sec_ed;
        // Гаусс-крюгер(СК42) м
        public double XSP42_ed;
        public double YSP42_ed;


        // ......................................................................
        // Основные параметры

        public double OwnHeight_ed;
        public double HeightOwnObject_ed;
        public double PowerOwn_ed;
        public double CoeffOwn_ed;
        public double RadiusZone_ed;
        public double MaxDist_ed;

        public int i_HeightOwnObject_ed;
        public int i_Cap1_ed;
        public int i_WidthHindrance_ed;
        public int i_Surface_ed;
        public double Cap1_ed;
        public double WidthHindrance_ed;
        public double Surface_ed;


        // Высота средства подавления
	    public double HeightAntennOwn_ed;
        public double HeightTotalOwn_ed;

        // Для подавляемой линии
        public double Freq_ed;
        public double PowerOpponent_ed;
        public double CoeffTransmitOpponent_ed;
        public double CoeffReceiverOpponent_ed;
        public double RangeComm_ed;
        public double WidthSignal_ed;
        public double HeightTransmitOpponent_ed;
        public double HeightReceiverOpponent_ed;
        public double CoeffSupOpponent_ed;
        public int i_PolarOpponent_ed;
        public int i_CoeffSupOpponent_ed;
        public int i_TypeCommOpponent_ed;

        // ......................................................................
        // Зона

        public double dCoeffQ_ed;
        public double dCoeffHE_ed;
        public int iCorrectHeightOwn_ed;
        public int iResultHeightOwn_ed;
        public int iMiddleHeight_ed;
        public int iMinHeight_ed;
        public int iCorrectHeightOpponent_ed;
        public int iResultHeightOpponent_ed;
        public long iMaxDistance_ed;
        public double dGamma_ed;
        //public long liRadiusZone_ed;
        
        // ......................................................................


        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        public ZonePowerAvail(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;


           LAMBDA = 300000;

            dchislo=0;
            ichislo=0;

            flCoordSP_ed=0; // =1-> Выбрали СП

            // .....................................................................
            // Координаты СП

            // Координаты СП на местности в м
            XSP_ed=0;
            YSP_ed=0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_ed = 25;
            dYdat_ed = -141;
            dZdat_ed = -80;

            dLat_ed=0;
            dLong_ed=0;

            // Эллипсоид Красовского, град
            LatKrG_ed=0;
            LongKrG_ed=0;
            // Эллипсоид Красовского, rad
            LatKrR_ed=0;
            LongKrR_ed=0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_ed=0;
            Lat_Min_ed=0;
            Lat_Sec_ed=0;
            Long_Grad_ed=0;
            Long_Min_ed=0;
            Long_Sec_ed=0;
            // Гаусс-крюгер(СК42) м
            XSP42_ed=0;
            YSP42_ed=0;
            // ......................................................................
            // Основные параметры

            OwnHeight_ed=0;
            HeightOwnObject_ed=0;
            PowerOwn_ed = 0;
            CoeffOwn_ed = 0;
            RadiusZone_ed = 0;
            MaxDist_ed = 0;

            i_HeightOwnObject_ed = 0;
            i_Cap1_ed = 0;
            i_WidthHindrance_ed = 0;
            i_Surface_ed = 0;
            Cap1_ed=0;
            WidthHindrance_ed=0;
            Surface_ed=0;

            // Высота средства подавления
            // ??????????????????????
	        HeightAntennOwn_ed=0;
            HeightTotalOwn_ed=0;

            // Для подавляемой линии
            Freq_ed = 0;
            PowerOpponent_ed = 0;
            CoeffTransmitOpponent_ed = 0;
            CoeffReceiverOpponent_ed = 0;
            RangeComm_ed = 0;
            WidthSignal_ed = 0;
            HeightTransmitOpponent_ed = 0;
            HeightReceiverOpponent_ed = 0;
            CoeffSupOpponent_ed = 0;
            i_PolarOpponent_ed = 0;
            i_CoeffSupOpponent_ed = 0;
            i_TypeCommOpponent_ed = 0;

            // ......................................................................
            // Зона

            dCoeffQ_ed=0;
            dCoeffHE_ed=0;
            iCorrectHeightOwn_ed=0;
            iResultHeightOwn_ed=0;
            iMiddleHeight_ed=0;
            iMinHeight_ed=0;
            iCorrectHeightOpponent_ed=0;
            iResultHeightOpponent_ed=0;
            iMaxDistance_ed=0;
            dGamma_ed=0;
            //liRadiusZone_ed=0;

            // ......................................................................


        } // Конструктор
        // ***********************************************************  Конструктор

     
        private void button1_Click(object sender, EventArgs e)
        {
            ;
        }

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************

        private void ZonePowerAvail_Load(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            GlobalVarLn.listSP_ed.Clear();
            GlobalVarLn.iSP_ed = 0;
            ReadFileSP_ed();
            cbOwnObject.SelectedIndex = 0;
            GlobalVarLn.NumbSP_ed = "";
         // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(8, 26);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
         // ----------------------------------------------------------------------
         // TextBox

         tbCoeffSupOpponent.Text ="2,3" ;

         // ----------------------------------------------------------------------
         // Средство РП

            cbHeightOwnObject.SelectedIndex = 0;  
         // ----------------------------------------------------------------------
         // Средство РП

         // cbCapacity.SelectedIndex = 0;
         // ----------------------------------------------------------------------
         // Подстилающая поверхность

          cbSurface.SelectedIndex = 0;
         // ---------------------------------------------------------------------
         // Ширина спектра помехи

          cbWidthHindrance.SelectedIndex = 0;
         // ----------------------------------------------------------------------
         // Пропускная способность

          cbCap1.SelectedIndex = 0;
         // ----------------------------------------------------------------------
         // Поляризация сигнала

          cbPolarOpponent.SelectedIndex = 0;
         // ----------------------------------------------------------------------
         // Коэффициент подавления

          cbCoeffSupOpponent.SelectedIndex = 0;
         // ----------------------------------------------------------------------
          // Вид связи

          cbTypeCommOpponent.SelectedIndex = 0;
          // ----------------------------------------------------------------------
          chbXY.Checked = false;
          // ----------------------------------------------------------------------

        } // Load_Form
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            //--------------------------------------------------------------------
            cbOwnObject.SelectedIndex = 0;
            GlobalVarLn.NumbSP_ed = "";
            //--------------------------------------------------------------------
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbXRect42.Text = "";
            tbYRect42.Text = "";
            tbBRad.Text = "";
            tbLRad.Text = "";
            tbBMin1.Text = "";
            tbLMin1.Text = "";
            tbBDeg2.Text = "";
            tbBMin2.Text = "";
            tbBSec.Text = "";
            tbLDeg2.Text = "";
            tbLMin2.Text = "";
            tbLSec.Text = "";
            tbOwnHeight.Text = "";
            tbHeightOwnObject.Text = "";
            tbRadiusZone.Text = "";
            tbMaxDist.Text = "";
            // ...................................................................
            chbXY.Checked = false;
            // ----------------------------------------------------------------------
            // переменные

            flCoordSP_ed=0; // =1-> Выбрали СП
            GlobalVarLn.fl_ZonePowerAvail = 0; // Отрисовка зоны
            // ---------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------

        } // Очистка
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************

        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoord(cbChooseSC.SelectedIndex);


        } // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbOwnObject": Выбор SP
        // ************************************************************************
        private void cbOwnObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOwnObject.SelectedIndex == 0)
                GlobalVarLn.NumbSP_ed = "";
            else
                GlobalVarLn.NumbSP_ed = Convert.ToString(cbOwnObject.Items[cbOwnObject.SelectedIndex]);

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button1 "СП": Выбор СП
        // ************************************************************************

        // OLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLD
        // !!! СТАРЫЙ ВАРИАНТ -> ОСТАВИТЬ

        /*

                private void button1_Click_1(object sender, EventArgs e)
                {
                    double xtmp_ed, ytmp_ed;
                    double xtmp1_ed, ytmp1_ed;

                    xtmp_ed = 0;
                    ytmp_ed = 0;
                    xtmp1_ed = 0;
                    ytmp1_ed = 0;

                    // ......................................................................
                    flCoordSP_ed = 1; // СП выбрана
                    // ......................................................................
                    ClassMap objClassMap1_ed = new ClassMap();
                    ClassMap objClassMap2_ed = new ClassMap();
                    ClassMap objClassMap3_ed = new ClassMap();

                    // ......................................................................
                    // !!! XSP_ed,YSP_ed -> реальные координаты на местности карты в м (Plane)

                    XSP_ed = GlobalVarLn.MapX1;
                    YSP_ed = GlobalVarLn.MapY1;
                    // ......................................................................
                    // Ручной ввод

                    if (chbXY.Checked == true)
                    {

                        if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                        {
                            MessageBox.Show("Недопустимые координаты СП");
                            return;
                        }

                      XSP_ed = Convert.ToDouble(tbXRect.Text);
                      YSP_ed = Convert.ToDouble(tbYRect.Text);
                    }
                    // ......................................................................
                    // Треугольник на карте

                    xtmp_ed = XSP_ed;
                    ytmp_ed = YSP_ed;
                    GlobalVarLn.XCenter_ed = XSP_ed;
                    GlobalVarLn.YCenter_ed = YSP_ed;

                    // SP
                    //ClassMap.f_Map_Pol_XY_stat(
                    //              GlobalVarLn.XCenter_ed,  // m
                    //              GlobalVarLn.YCenter_ed,
                    //              1,
                    //              ""
                    //             );

                    // SP
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_ed,  // m
                                  GlobalVarLn.YCenter_ed,
                                      ""
                                 );

                    // ......................................................................
                    // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
                    // !!! Выход функции(rad) идет на место входных переменных

                    xtmp_ed = XSP_ed;
                    ytmp_ed = YSP_ed;

                    mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                    // rad(WGS84)->grad(WGS84)
                    xtmp1_ed = (xtmp_ed * 180) / Math.PI;
                    ytmp1_ed = (ytmp_ed * 180) / Math.PI;
                    // .......................................................................


                    // WGS84(эллипсоид)->элл.Красовского *************************************
                    // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
                    // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
                    // Входные параметры -> град,km
                    // Перевод в рад - внутри функции

                    // dLong ..................................................................
                    // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap1_ed.f_dLong
                        (

                            // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            dXdat_ed,
                            dYdat_ed,
                            dZdat_ed,

                            ref dLong_ed   // приращение по долготе, угл.сек

                        );

                    // ................................................................ dLong

                    // dLat .................................................................
                    // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
                    // (преобразования Молоденского), угл.сек

                    objClassMap1_ed.f_dLat
                        (

                            // Входные параметры (град,км)
                            xtmp1_ed,   // широта
                            ytmp1_ed,  // долгота
                            0,     // высота

                            // DATUM,m
                            dXdat_ed,
                            dYdat_ed,
                            dZdat_ed,

                            ref dLat_ed        // приращение по долготе, угл.сек

                        );

                    // ................................................................. dLat

                    // Lat,Long .............................................................
                    // Преобразования широты и долготы при пересчете WGS84->SK42

                    objClassMap1_ed.f_WGS84_SK42_Lat_Long
                           (

                               // Входные параметры (град,км)
                               xtmp1_ed,   // широта
                               ytmp1_ed,  // долгота
                               0,     // высота
                               dLat_ed,       // приращение по долготе, угл.сек
                               dLong_ed,      // приращение по долготе, угл.сек

                               // Выходные параметры (grad)
                               ref LatKrG_ed,   // широта
                               ref LongKrG_ed   // долгота

                           );

                    // ............................................................ Lat,Long


                    // *********************************** WGS84(эллипсоид)->элл.Красовского

                    // .......................................................................
                    // Эллипсоид Красовского, радианы

                    LatKrR_ed=(LatKrG_ed*Math.PI)/180;
                    LongKrR_ed = (LongKrG_ed * Math.PI) / 180;
                    // .......................................................................
                    // Эллипсоид Красовского, grad,min,sec
                    // dd.ddddd -> DD MM SS

                    // Широта
                    objClassMap3_ed.f_Grad_GMS
                      (
                        // Входные параметры (grad)
                        LatKrG_ed,

                        // Выходные параметры 
                        ref Lat_Grad_ed,
                        ref Lat_Min_ed,
                        ref Lat_Sec_ed

                      );

                    // Долгота
                    objClassMap3_ed.f_Grad_GMS
                      (
                        // Входные параметры (grad)
                        LongKrG_ed,

                        // Выходные параметры 
                        ref Long_Grad_ed,
                        ref Long_Min_ed,
                        ref Long_Sec_ed

                      );

                    // .......................................................................


                    // SK42(элл.)->Крюгер ****************************************************
                    // Преобразование геодезических координат (широта, долгота, высота) 
                    // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
                    // проекции Гаусса-Крюгера
                    // Входные параметры -> !!!grad

                    objClassMap3_ed.f_SK42_Krug
                           (
                               // Входные параметры (!!! grad)
                               // !!! эллипсоид Красовского
                               LatKrG_ed,   // широта
                               LongKrG_ed,  // долгота

                               // Выходные параметры (km)
                               ref XSP42_ed,
                               ref YSP42_ed

                           );

                    // km->m
                    XSP42_ed = XSP42_ed * 1000;
                    YSP42_ed = YSP42_ed * 1000;
                    // **************************************************** SK42(элл.)->Крюгер

                    // .......................................................................
                    // Отображение СП в выбранной СК

                    OtobrSP_ed();
                    // .......................................................................

                } // Button1: Выбор СП
*/
        // OLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLD

        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
        private void button1_Click_1(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;

            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            flCoordSP_ed = 1; // СП выбрана
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            int it = 0;
            String strLine3 = "";
            String strLine2 = "";
            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbOwnObject.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_ed = "";
                // !!! реальные координаты на местности карты в м (Plane)

                XSP_ed = GlobalVarLn.MapX1;
                YSP_ed = GlobalVarLn.MapY1;

            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_ed = Convert.ToString(cbOwnObject.Items[cbOwnObject.SelectedIndex]);
                strLine2 = GlobalVarLn.NumbSP_ed;

                for (it = 0; it < GlobalVarLn.listSP_ed.Count; it++)
                {
                    strLine3 = Convert.ToString(GlobalVarLn.listSP_ed[it].sNum); // Numb

                    // if1
                    if (String.Compare(strLine2, strLine3) == 0)
                    {
                        XSP_ed = GlobalVarLn.listSP_ed[it].X_m;
                        YSP_ed = GlobalVarLn.listSP_ed[it].Y_m;

                    } // if1

                } // For

            }
            // ----------------------------------------------------------------------
            // Ручной ввод

            if (chbXY.Checked == true)
            {
                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Недопустимые координаты центра ЗПВ");
                    return;
                }
                XSP_ed = Convert.ToDouble(tbXRect.Text);
                YSP_ed = Convert.ToDouble(tbYRect.Text);
            }
            // ----------------------------------------------------------------------

            // COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR Выбор координат

            // ......................................................................
            // Треугольник на карте

            xtmp_ed = XSP_ed;
            ytmp_ed = YSP_ed;
            GlobalVarLn.XCenter_ed = XSP_ed;
            GlobalVarLn.YCenter_ed = YSP_ed;

            // SP
            //ClassMap.f_Map_Pol_XY_stat(
            //              GlobalVarLn.XCenter_ed,  // m
            //              GlobalVarLn.YCenter_ed,
            //              1,
            //              ""
            //             );

            if (cbOwnObject.SelectedIndex == 0)
            {
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ed,  // m
                              GlobalVarLn.YCenter_ed,
                                  ""
                             );
            }
            else
            {
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ed,  // m
                              GlobalVarLn.YCenter_ed,
                              GlobalVarLn.listSP_ed[cbOwnObject.SelectedIndex-1].sNum
                             );

            }
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XSP_ed;
            ytmp_ed = YSP_ed;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_ed = xtmp1_ed;
            LongKrG_ed = ytmp1_ed;
            // .......................................................................

// CDel
/*


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_ed,
                    dYdat_ed,
                    dZdat_ed,

                    ref dLong_ed   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_ed,
                    dYdat_ed,
                    dZdat_ed,

                    ref dLat_ed        // приращение по долготе, угл.сек

                );

            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_ed,       // приращение по долготе, угл.сек
                       dLong_ed,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_ed,   // широта
                       ref LongKrG_ed   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского
*/

            // .......................................................................
            // Эллипсоид Красовского, радианы
            // Эллипсоид Красовского, радианы
            // CDel
            // !!! Здесь это WGS84

            LatKrR_ed = (LatKrG_ed * Math.PI) / 180;
            LongKrR_ed = (LongKrG_ed * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS
            // Эллипсоид Красовского, радианы
            // CDel
            // !!! Здесь это WGS84

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_ed,

                // Выходные параметры 
                ref Lat_Grad_ed,
                ref Lat_Min_ed,
                ref Lat_Sec_ed

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_ed,

                // Выходные параметры 
                ref Long_Grad_ed,
                ref Long_Min_ed,
                ref Long_Sec_ed

              );

            // .......................................................................


// CDel
/*

            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_ed,   // широта
                       LongKrG_ed,  // долгота

                       // Выходные параметры (km)
                       ref XSP42_ed,
                       ref YSP42_ed

                   );

            // km->m
            XSP42_ed = XSP42_ed * 1000;
            YSP42_ed = YSP42_ed * 1000;
            // **************************************************** SK42(элл.)->Крюгер
*/
            // .......................................................................
            // Отображение СП в выбранной СК

            OtobrSP_ed();
            // .......................................................................

        } // Button1: Выбор СП


        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW

                // ************************************************************************



                // Расчет зоны MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
                private void bAccept_Click(object sender, EventArgs e)
                {

                    if (flCoordSP_ed == 0)
                    {
                        MessageBox.Show("Не выбрана СП");
                        return;
                    }


                // Ввод параметров ********************************************************
                // !!! Координаты СП уже расчитаны и введены по кнопке СП


                 // .......................................................................
                // Из TextBox

                //if (tbHeightOwnObject.Text == "")
                //    HeightOwnObject_ed = 0;
                //else
                //    HeightOwnObject_ed = Convert.ToDouble(tbHeightOwnObject.Text);

                PowerOwn_ed = Convert.ToDouble(tbPowerOwn.Text);
                if ((PowerOwn_ed < 100) || (PowerOwn_ed > 2000))
                     {
                      MessageBox.Show("Значение мощности средства подавления вне диапазона (100 - 2000)");
                      return;
                     }

                CoeffOwn_ed = Convert.ToDouble(tbCoeffOwn.Text);
                if ((CoeffOwn_ed < 1) || (CoeffOwn_ed > 10))
                {
                    MessageBox.Show("Значение коэффициента усиления средства подавления вне диапазона (1-10)");
                    return;
                }

                // ??????????????????????
                HeightAntennOwn_ed = Convert.ToDouble(tbHAnt.Text);

                // ComboBox (Индексы)
                i_HeightOwnObject_ed=cbHeightOwnObject.SelectedIndex; 
                i_Cap1_ed = cbCap1.SelectedIndex;
                i_WidthHindrance_ed = cbWidthHindrance.SelectedIndex;
                i_Surface_ed = cbSurface.SelectedIndex;

                // ComboBox (Значения)
                Cap1_ed = Convert.ToDouble(cbCap1.Text);
                WidthHindrance_ed = Convert.ToDouble(cbWidthHindrance.Text);
                //Surface_ed = Convert.ToDouble(cbSurface.Text);
                // -----------------------------------------------------------------------
                // Для подавляемой линии

                Freq_ed = Convert.ToDouble(tbFreq.Text);
                if ((Freq_ed < 30000) || (Freq_ed > 1215000))
                {
                    MessageBox.Show("Значение несущей частоты объекта подавления вне диапазона");
                    return;
                }

                PowerOpponent_ed = Convert.ToDouble(tbPowerOpponent.Text);
                if ((PowerOpponent_ed < 1) || (PowerOpponent_ed > 1000))
                {
                    MessageBox.Show("Значение мощности объекта подавления вне диапазона (1 - 1000)");
                    return;
                }

                CoeffTransmitOpponent_ed = Convert.ToDouble(tbCoeffTransmitOpponent.Text);
                if ((CoeffTransmitOpponent_ed < 1) || (CoeffTransmitOpponent_ed > 10))
                {
                    MessageBox.Show("Значение коэффициента усиления передатчика объекта подавления вне диапазона");
                    return;
                }

                CoeffReceiverOpponent_ed = Convert.ToDouble(tbCoeffReceiverOpponent.Text);

                RangeComm_ed = Convert.ToDouble(tbRangeComm.Text);
                if ((RangeComm_ed < 100) || (RangeComm_ed > 100000))
                {
                    MessageBox.Show("Значение дальности связи объекта подавления вне диапазона");
                    return;
                }

                WidthSignal_ed = Convert.ToDouble(tbWidthSignal.Text);
                if ((WidthSignal_ed < 3) || (WidthSignal_ed > 1000))
                {
                    MessageBox.Show("Значение ширины спектра объекта подавления вне диапазона (3 - 1000)");
                    return;
                }

                HeightTransmitOpponent_ed = Convert.ToDouble(tbHeightTransmitOpponent.Text);
                if ((HeightTransmitOpponent_ed < 1) || (HeightTransmitOpponent_ed > 20))
                {
                    MessageBox.Show("Значение высоты антенны передатчика объекта подавления вне диапазона (1 - 20)");
                    return;
                }

                HeightReceiverOpponent_ed = Convert.ToDouble(tbHeightReceiverOpponent.Text);
                if ((HeightReceiverOpponent_ed < 1) || (HeightReceiverOpponent_ed > 1000))
                {
                    MessageBox.Show("Значение высоты антенны приемника объекта подавления вне диапазона (1 - 1000)");
                    return;
                }

                CoeffSupOpponent_ed = Convert.ToDouble(tbCoeffSupOpponent.Text);

                // ComboBox (Индексы)
                i_PolarOpponent_ed = cbPolarOpponent.SelectedIndex;
                i_CoeffSupOpponent_ed = cbCoeffSupOpponent.SelectedIndex;
                i_TypeCommOpponent_ed = cbTypeCommOpponent.SelectedIndex; 

                // -----------------------------------------------------------------------

        
                // ******************************************************** Ввод параметров

                // СП *********************************************************************
                // Координаты СП на местности в м

                // XSP_ed,YSP_ed;
                GlobalVarLn.tpOwnCoordRect.X = Convert.ToInt32(tbXRect.Text);
                GlobalVarLn.tpOwnCoordRect.Y = Convert.ToInt32(tbYRect.Text);				

                // ........................................................................
                // Высота из карты

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.tpOwnCoordRect.X, GlobalVarLn.tpOwnCoordRect.Y);
                OwnHeight_ed=(int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                tbOwnHeight.Text = Convert.ToString(OwnHeight_ed);
                // ********************************************************************* СП

                // H средства подавления **************************************************
                // определить значение высоты средства подавления

                         switch (i_HeightOwnObject_ed)
                         {				 
                             // рельеф местности+высота антенны
                         case 0:
                             HeightTotalOwn_ed=HeightAntennOwn_ed+OwnHeight_ed;
                             break;

                             // высота антенны
                         case 1:
                             HeightTotalOwn_ed=HeightAntennOwn_ed;
                             break;

                             // задать самостоятельно
                         case 2:
                             if (tbHeightOwnObject.Text=="")
                                 HeightTotalOwn_ed = 0;
                             else
                                 HeightTotalOwn_ed = Convert.ToDouble(tbHeightOwnObject.Text);

                             break;

                         } // Switch

                         // отобразить значение высоты
                         ichislo = (long)(HeightTotalOwn_ed);
                         tbHeightOwnObject.Text = Convert.ToString(ichislo);

                // ************************************************** H средства подавления

                // Расчет зоны ************************************************************

                dCoeffQ_ed = DefineCoeffQ(Freq_ed, i_Surface_ed);
                tbCoeffQ.Text = dCoeffQ_ed.ToString();

                dCoeffHE_ed = DefineCoeffHE(Freq_ed, dCoeffQ_ed);
                tbCoeffHE.Text = dCoeffHE_ed.ToString();

                iCorrectHeightOwn_ed = DefineCorrectHeightOwn((int)HeightAntennOwn_ed, dCoeffHE_ed);
                tbCorrectHeightOwn.Text = iCorrectHeightOwn_ed.ToString();

                iResultHeightOwn_ed = DefineResultHeightOwn(iCorrectHeightOwn_ed, (int)OwnHeight_ed);
                tbResultHeightOwn.Text = iResultHeightOwn_ed.ToString();

                iMiddleHeight_ed = DefineMiddleHeight(GlobalVarLn.tpOwnCoordRect, GlobalVarLn.axMapPointGlobalAdd, GlobalVarLn.axMapScreenGlobal);
                tbMiddleHeight.Text = iMiddleHeight_ed.ToString();

                iMinHeight_ed = DefineMinHeight(GlobalVarLn.tpOwnCoordRect, iMiddleHeight_ed);
                tbMinHeight.Text = iMinHeight_ed.ToString();

                iCorrectHeightOpponent_ed = DefineCorrectHeightOpponent((int)HeightReceiverOpponent_ed, dCoeffHE_ed);
                tbCorrectHeightOpponent.Text = iCorrectHeightOpponent_ed.ToString();

                iResultHeightOpponent_ed = DefineResultHeightOpponent(iCorrectHeightOpponent_ed, iMiddleHeight_ed);
                tbResultHeightOpponent.Text = iResultHeightOpponent_ed.ToString();

                iMaxDistance_ed = DefineMaxDistance(iResultHeightOwn_ed, iResultHeightOpponent_ed, iMinHeight_ed);
                tbMaxDistance.Text = iMaxDistance_ed.ToString();
                tbMaxDist.Text = iMaxDistance_ed.ToString();


                dGamma_ed = DefineGamma(i_PolarOpponent_ed);
                tbGamma.Text = dGamma_ed.ToString();

                GlobalVarLn.liRadiusZone_ed = DefineRadiusZone();
                tbRadiusZone.Text = GlobalVarLn.liRadiusZone_ed.ToString();

                GlobalVarLn.fl_ZonePowerAvail = 1; // Отрисовка зоны
                ClassMap.f_Map_El_XY_ed(
                                        GlobalVarLn.tpOwnCoordRect,
                                        GlobalVarLn.liRadiusZone_ed
                                        );


                // ************************************************************ Расчет зоны


                } // Принять
                // MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN  Расчет зоны

                // FUNCTIONS **************************************************************

                // ************************************************************************
                // функция чтения файла по СП 
                // ************************************************************************
                public void ReadFileSP_ed()
                {
                    // ------------------------------------------------------------------------------------
                    String strLine = "";
                    String strLine1 = "";
                    double number1 = 0;
                    int number2 = 0;
                    char symb1 = 'N';
                    char symb2 = 'X';
                    char symb3 = 'Y';
                    char symb4 = '=';
                    char symb5 = 'H';
                    char symb6 = 'R';
                    char symb7 = 'A';
                    char symb8 = 'T';
                    char symb9 = 'i';

                    int indStart = 0;
                    int indStop = 0;
                    int iLength = 0;
                    int IndZap = 0;
                    int TekPoz = 0;
                    int fi = 0;
                    int if1 = 0;

                    // Чтение файла ---------------------------------------------------------
                    String strFileName;
                    strFileName = "SP.txt";

                    StreamReader srFile;
                    try
                    {
                        srFile = new StreamReader(strFileName);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно открыть файл");
                        return;

                    }
                    // -------------------------------------------------------------------------------------
                    // Чтение
                    // NSP =...
                    // Numb =...
                    // X =...
                    // Y =...
                    // H =...
                    // Reg =...
                    // RRH =...
                    // Adr =...
                    // Type =...

                    try
                    {
                        SP objSP = new SP();

                        TekPoz = 0;
                        IndZap = 0;
                        // .......................................................
                        // NSP =...
                        // 1-я строка

                        strLine = srFile.ReadLine();

                        if (strLine == null)
                        {
                            MessageBox.Show("Нет информации");
                            srFile.Close();
                            return;
                        }

                        indStart = strLine.IndexOf(symb1, TekPoz); // N

                        if (indStart == -1)
                        {
                            MessageBox.Show("Нет информации");
                            srFile.Close();
                            return;
                        }

                        indStop = strLine.IndexOf(symb4, TekPoz);  //=

                        if ((indStop == -1) || (indStop < indStart))
                        {
                            MessageBox.Show("Нет информации");
                            srFile.Close();
                            return;
                        }

                        iLength = indStop - indStart + 1;
                        // Убираем 'NSP ='
                        strLine1 = strLine.Remove(indStart, iLength);

                        if (strLine1 == "")
                        {
                            MessageBox.Show("Нет информации");
                            srFile.Close();
                            return;
                        }

                        // Количество SP
                        number2 = Convert.ToInt32(strLine1);
                        GlobalVarLn.iSP_ed = (uint)number2;
                        // .......................................................

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (NumbSP1)
                        if ((strLine == "") || (strLine == null))
                        {
                            fi = 1;
                        }
                        // .......................................................

                        // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                        while ((strLine != "") && (strLine != null))
                        {
                            IndZap += 1;

                            // .......................................................
                            // Numb =...

                            indStart = strLine.IndexOf(symb1, TekPoz); // N
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);

                            objSP.sNum = strLine1;
                            // .......................................................
                            // X =...

                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb2, TekPoz); // X
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            number1 = Convert.ToInt32(strLine1);

                            objSP.X_m = number1;
                            // .......................................................
                            // Y =...

                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb3, TekPoz); // Y
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            number1 = Convert.ToInt32(strLine1);

                            objSP.Y_m = number1;
                            // .......................................................
                            // H =...

                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb5, TekPoz); // H
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            number1 = Convert.ToInt32(strLine1);

                            objSP.H_m = number1;
                            // .......................................................
                            // Reg =...

/*
                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb6, TekPoz); // R
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);

                            objSP.sReg = strLine1;
 */
                            // .......................................................
                            // NIP =...

                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb1, TekPoz); // N
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            number1 = Convert.ToInt32(strLine1);

                            objSP.NIP = (int)number1;
                            // .......................................................
                            // Adr =...

/*
                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb7, TekPoz); // A
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            number1 = Convert.ToInt32(strLine1);

                            objSP.Adr = (int)number1;
 */
                            // .......................................................
                            // Type =...

                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb8, TekPoz); // T
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);

                            objSP.sType = strLine1;
                            // .......................................................
                            // indzn =...

                            strLine = srFile.ReadLine();

                            indStart = strLine.IndexOf(symb9, TekPoz); // i
                            indStop = strLine.IndexOf(symb4, TekPoz);  // =
                            iLength = indStop - indStart + 1;
                            strLine1 = strLine.Remove(indStart, iLength);
                            number1 = Convert.ToInt32(strLine1);

                            objSP.indzn = (int)number1;
                            // .......................................................
                            // Занести в List

                            GlobalVarLn.listSP_ed.Add(objSP);
                            // .......................................................

                            // .......................................................
                            // Занести в таблицу

                            // !!! 0-я -> X,Y
                            //cbCenterLSR.Items[if1] = objSP.sNum;
                            cbOwnObject.Items[if1 + 1] = objSP.sNum;

                            if1 += 1;

                            // ............................................................

                            fi = 0;
                            strLine = srFile.ReadLine(); // читаем далее (Numb)
                            if ((strLine == "") || (strLine == null))
                                fi = 1;
                            // ...................................................................


                        } // WHILE
                        // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH


                    }
                    // -----------------------------------------------------------------------------------
                    catch
                    {
                        srFile.Close();
                        return;
                    }
                    // -------------------------------------------------------------------------------------
                    srFile.Close();
                    // -------------------------------------------------------------------------------------

                } // ReadfileSP_ed
                // ************************************************************************

                // ************************************************************************
                // функция выбора системы координат
                // ************************************************************************

                // OLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLD
                // CDel
                // !!! СТАРЫЙ ВАРИАНТ -> ОСТАВИТЬ
                /*

                        public void ChooseSystemCoord(int iSystemCoord)
                        {
                         gbOwnRect.Visible = false;
                         gbOwnRect42.Visible = false;
                         gbOwnRad.Visible = false;
                         gbOwnDegMin.Visible = false;
                         gbOwnDegMinSec.Visible = false;


                         switch (iSystemCoord)
                         {
                          case 0: // Метры на местности

                          gbOwnRect.Visible = true;
                          gbOwnRect.Location = new Point(8, 26);

                          if (flCoordSP_ed==1)
                          {

                           ichislo = (long)(XSP_ed);
                           tbXRect.Text = Convert.ToString(ichislo);

                           ichislo = (long)(YSP_ed);
                           tbYRect.Text = Convert.ToString(ichislo);  

                          } // IF

                          break;

                          case 1: // Метры 1942 года

                          gbOwnRect42.Visible = true;
                          gbOwnRect42.Location = new Point(8, 27);

                          if (flCoordSP_ed==1)
                          {

                           ichislo = (long)(XSP42_ed);
                           tbXRect42.Text = Convert.ToString(ichislo);

                           ichislo = (long)(YSP42_ed);
                           tbYRect42.Text = Convert.ToString(ichislo);  

                          } // IF

                          break;

                          case 2: // Радианы (Красовский)

                          gbOwnRad.Visible = true;
                          gbOwnRad.Location = new Point(8, 27);

                          if (flCoordSP_ed==1)
                          {

                           ichislo = (long)(LatKrR_ed * 1000000);
                           dchislo = ((double)ichislo) / 1000000;
                           tbBRad.Text = Convert.ToString(dchislo);   

                           ichislo = (long)(LongKrR_ed * 1000000);
                           dchislo = ((double)ichislo) / 1000000;
                           tbLRad.Text = Convert.ToString(dchislo);   

                          } // IF

                          break;

                          case 3: // Градусы (Красовский)

                          gbOwnDegMin.Visible = true;
                          gbOwnDegMin.Location = new Point(8, 27);

                          if (flCoordSP_ed==1)
                          {

                           ichislo = (long)(LatKrG_ed * 1000000);
                           dchislo = ((double)ichislo) / 1000000;
                           tbBMin1.Text = Convert.ToString(dchislo);   

                           ichislo = (long)(LongKrG_ed * 1000000);
                           dchislo = ((double)ichislo) / 1000000;
                           tbLMin1.Text = Convert.ToString(dchislo);   

                          } // IF

                          break;

                          case 4: // Градусы,мин,сек (Красовский)

                          gbOwnDegMinSec.Visible = true;
                          gbOwnDegMinSec.Location = new Point(8, 27);

                          if (flCoordSP_ed==1)
                          {

                           tbBDeg2.Text = Convert.ToString(Lat_Grad_ed);
                           tbBMin2.Text = Convert.ToString(Lat_Min_ed);
                           ichislo = (long)(Lat_Sec_ed);
                           tbBSec.Text = Convert.ToString(ichislo);

                           tbLDeg2.Text = Convert.ToString(Long_Grad_ed);
                           tbLMin2.Text = Convert.ToString(Long_Min_ed);
                           ichislo = (long)(Long_Sec_ed);
                           tbLSec.Text = Convert.ToString(ichislo);

                          } // IF


                          break;

                          default:
                          break;

                       } // SWITCH

                        } // ChooseSystemCoord
*/
                // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
                public void ChooseSystemCoord(int iSystemCoord)
                {
                    gbOwnRect.Visible = false;
                    gbOwnRect42.Visible = false;
                    gbOwnRad.Visible = false;
                    gbOwnDegMin.Visible = false;
                    gbOwnDegMinSec.Visible = false;


                    switch (iSystemCoord)
                    {
                        case 0: // Метры на местности

                            gbOwnRect.Visible = true;
                            gbOwnRect.Location = new Point(8, 26);

                            if (flCoordSP_ed == 1)
                            {

                                ichislo = (long)(XSP_ed);
                                tbXRect.Text = Convert.ToString(ichislo);

                                ichislo = (long)(YSP_ed);
                                tbYRect.Text = Convert.ToString(ichislo);

                            } // IF

                            break;
                // CDel
/*

                        case 1: // Метры 1942 года

                            gbOwnRect42.Visible = true;
                            gbOwnRect42.Location = new Point(8, 27);

                            if (flCoordSP_ed == 1)
                            {

                                ichislo = (long)(XSP42_ed);
                                tbXRect42.Text = Convert.ToString(ichislo);

                                ichislo = (long)(YSP42_ed);
                                tbYRect42.Text = Convert.ToString(ichislo);

                            } // IF

                            break;
*/
                        // CDel
                        // !!! Здесь это WGS84
                        case 1: // Радианы (Красовский)

                            gbOwnRad.Visible = true;
                            gbOwnRad.Location = new Point(8, 27);

                            if (flCoordSP_ed == 1)
                            {

                                ichislo = (long)(LatKrR_ed * 1000000);
                                dchislo = ((double)ichislo) / 1000000;
                                tbBRad.Text = Convert.ToString(dchislo);

                                ichislo = (long)(LongKrR_ed * 1000000);
                                dchislo = ((double)ichislo) / 1000000;
                                tbLRad.Text = Convert.ToString(dchislo);

                            } // IF

                            break;
                        // CDel
                        // !!! Здесь это WGS84
                        case 2: // Градусы (Красовский)

                            gbOwnDegMin.Visible = true;
                            gbOwnDegMin.Location = new Point(8, 27);

                            if (flCoordSP_ed == 1)
                            {

                                ichislo = (long)(LatKrG_ed * 1000000);
                                dchislo = ((double)ichislo) / 1000000;
                                tbBMin1.Text = Convert.ToString(dchislo);

                                ichislo = (long)(LongKrG_ed * 1000000);
                                dchislo = ((double)ichislo) / 1000000;
                                tbLMin1.Text = Convert.ToString(dchislo);

                            } // IF

                            break;
                        // CDel
                        // !!! Здесь это WGS84
                        case 3: // Градусы,мин,сек (Красовский)

                            gbOwnDegMinSec.Visible = true;
                            gbOwnDegMinSec.Location = new Point(8, 27);

                            if (flCoordSP_ed == 1)
                            {

                                tbBDeg2.Text = Convert.ToString(Lat_Grad_ed);
                                tbBMin2.Text = Convert.ToString(Lat_Min_ed);
                                ichislo = (long)(Lat_Sec_ed);
                                tbBSec.Text = Convert.ToString(ichislo);

                                tbLDeg2.Text = Convert.ToString(Long_Grad_ed);
                                tbLMin2.Text = Convert.ToString(Long_Min_ed);
                                ichislo = (long)(Long_Sec_ed);
                                tbLSec.Text = Convert.ToString(ichislo);

                            } // IF


                            break;

                        default:
                            break;

                    } // SWITCH

                } // ChooseSystemCoord


                // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
                  
                        // ************************************************************************

                        private void gbOwnRect42_Enter(object sender, EventArgs e)
                        {
                            ;
                        }

                        // ******************************************************************************************
                        // Нарисовать метку по координатам (красный треугольник)
                        //
                        // Входные параметры:
                        // X - X, m
                        // Y - Y, m
                        // ******************************************************************************************
                /*
                        public void f_Map_Pol_XY_ed(
                                                         double X,
                                                         double Y
                                                        )
                        {

                            // -------------------------------------------------------------------------------------
                            Graphics graph = axaxcMapScreen.CreateGraphics();

                            Pen penRed1 = new Pen(Color.Red, 2);
                            Brush brushRed1 = new SolidBrush(Color.Red);
                            // -------------------------------------------------------------------------------------
                            // Расстояние в м на карте -> пикселы на изображении

                            mapPlaneToPicture(GlobalVar.hmapl, ref X, ref Y);
                            // -------------------------------------------------------------------------------------

                            if (graph != null)
                            {

                                Point[] toDraw = new Point[3];
                                toDraw[0].X = (int)X - (axaxcMapScreen.MapLeft + 7);
                                toDraw[0].Y = (int)Y - (axaxcMapScreen.MapTop - 7);
                                toDraw[1].X = (int)X - axaxcMapScreen.MapLeft;
                                toDraw[1].Y = (int)Y - (axaxcMapScreen.MapTop + 7);
                                toDraw[2].X = (int)X - (axaxcMapScreen.MapLeft - 7);
                                toDraw[2].Y = (int)Y - (axaxcMapScreen.MapTop - 7);
                                graph.FillPolygon(brushRed1, toDraw);

                            }

                            // -------------------------------------------------------------------------------------

                        } // P/P f_Map_Pol_XY_ed
                */
        // *************************************************************************************

        // ******************************************************************************************
        // Зона (м на местности)
        // ******************************************************************************************

/*
        public void f_Map_El_XY_ed(
                                         Point tpCenterPoint,
                                         long iRadiusZone
                                        )
        {

            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            double XC = 0;
            double YC = 0;
            XC = tpCenterPoint.X;
            YC = tpCenterPoint.Y;

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);

            Pen pen1=new Pen(Color.HotPink, 5.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross,Color.Linen,Color.Transparent);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                dLeftX = XC - iRadiusZone/2;
                dLeftY = YC - iRadiusZone / 2;
                dRightX = XC + iRadiusZone / 2;
                dRightY = YC + iRadiusZone / 2;
                mapPlaneToPicture(GlobalVar.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVar.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVar.hmapl, ref dRightX, ref dRightY);

               graph.DrawEllipse(pen1, (int)dLeftX - axaxcMapScreen.MapLeft, (int)dLeftY - axaxcMapScreen.MapTop,
                                       (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
               graph.FillEllipse(brush1, (int)dLeftX - axaxcMapScreen.MapLeft, (int)dLeftY - axaxcMapScreen.MapTop,
                                       (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));


            }
            // -------------------------------------------------------------------------------------

        } // P/P f_Map_El_XY_ed
 */ 
        // *************************************************************************************

        // *************************************************************************************
/*
public void DrawZonePowerAvail(Point tpCenterPoint, long iRadiusZone)
{
	double dLeftX = 0;
	double dLeftY = 0;

	double dRightX = 0;
	double dRightY = 0;

	dLeftX = tpCenterPoint.X+iRadiusZone;
	dLeftY = tpCenterPoint.Y+iRadiusZone;

	dRightX = tpCenterPoint.X-iRadiusZone;
	dRightY = tpCenterPoint.Y-iRadiusZone;

	// Вызов функции
	long (WINAPI  *lpfn_mapPlaneToPicture)(HMAP hmap,double *Bx, double *Ly);
	// пересчитать координаты на местности
	// в координаты на экране
	(FARPROC&)lpfn_mapPlaneToPicture = GetProcAddress(GlobalVar::libInst, "mapPlaneToPicture");

	long int j = 0;
	j =  (*lpfn_mapPlaneToPicture)(GlobalVar::hMap,&dLeftX,&dLeftY);

	if (j > 0)
	{
		dLeftX = dLeftX - GlobalVar::iMapLeft;
		dLeftY = dLeftY - GlobalVar::iMapTop;
	}

	j = 0;
	j =  (*lpfn_mapPlaneToPicture)(GlobalVar::hMap,&dRightX,&dRightY);

	if (j > 0)
	{
		dRightX = dRightX - GlobalVar::iMapLeft;
		dRightY = dRightY - GlobalVar::iMapTop;
	}

	int iWidthRect = 0;
	iWidthRect = (int)(dLeftX - dRightX);

	int iHeightRect = 0;
	iHeightRect = (int)(dLeftY - dRightY);

	GlobalVar::grMap->DrawEllipse(GlobalVar::penZonePower,(int)dLeftX,(int)dLeftY, iHeightRect,iWidthRect);
	GlobalVar::grMap->FillEllipse(GlobalVar::hBrushZonePower,(int)dLeftX,(int)dLeftY, iHeightRect,iWidthRect);
}
*/
// *************************************************************************************



        // ************************************************************************
        // функция выбора системы координат
        // ************************************************************************

                // OLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLD
                // CDel
                // !!! СТАРЫЙ ВАРИАНТ -> ОСТАВИТЬ
        /*

        public void OtobrSP_ed()
        {

           // Метры на местности
           if (gbOwnRect.Visible == true)
             {

             ichislo = (long)(XSP_ed);
             tbXRect.Text = Convert.ToString(ichislo);

             ichislo = (long)(YSP_ed);
             tbYRect.Text = Convert.ToString(ichislo);

              } // IF


           // Метры 1942 года
           else if (gbOwnRect42.Visible == true)
              {
               ichislo = (long)(XSP42_ed);
               tbXRect42.Text = Convert.ToString(ichislo);

               ichislo = (long)(YSP42_ed);
               tbYRect42.Text = Convert.ToString(ichislo);

               } // IF


           // Радианы (Красовский)
           else if (gbOwnRad.Visible == true)
              {
                ichislo = (long)(LatKrR_ed * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbBRad.Text = Convert.ToString(dchislo);   // X, карта

                ichislo = (long)(LongKrR_ed * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbLRad.Text = Convert.ToString(dchislo);   // X, карта

              } // IF


           // Градусы (Красовский)
           else if (gbOwnDegMin.Visible == true)
              {

                ichislo = (long)(LatKrG_ed * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbBMin1.Text = Convert.ToString(dchislo);

                ichislo = (long)(LongKrG_ed * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbLMin1.Text = Convert.ToString(dchislo);

              } // IF


           // Градусы,мин,сек (Красовский)
           else if (gbOwnDegMinSec.Visible == true)
             {

              tbBDeg2.Text = Convert.ToString(Lat_Grad_ed);
              tbBMin2.Text = Convert.ToString(Lat_Min_ed);
              ichislo = (long)(Lat_Sec_ed);
              tbBSec.Text = Convert.ToString(ichislo);

              tbLDeg2.Text = Convert.ToString(Long_Grad_ed);
              tbLMin2.Text = Convert.ToString(Long_Min_ed);
              ichislo = (long)(Long_Sec_ed);
              tbLSec.Text = Convert.ToString(ichislo);

             } // IF


        } // OtobrSP_ed
*/
      // OLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLDOLD

     // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
                public void OtobrSP_ed()
                {

                    // Метры на местности
                    //if (gbOwnRect.Visible == true)
                   // {

                        ichislo = (long)(XSP_ed);
                        tbXRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(YSP_ed);
                        tbYRect.Text = Convert.ToString(ichislo);

                    //} // IF

                    // CDel
                    // Метры 1942 года
                    //else if (gbOwnRect42.Visible == true)
                   // {
                   //     ichislo = (long)(XSP42_ed);
                   //     tbXRect42.Text = Convert.ToString(ichislo);
                   // 
                   //     ichislo = (long)(YSP42_ed);
                   //     tbYRect42.Text = Convert.ToString(ichislo);

                    //} // IF


                    // Радианы (Красовский)
                    // CDel
                    // !!!Здесь это WGS84
                    //else if (gbOwnRad.Visible == true)
                    //{
                        ichislo = (long)(LatKrR_ed * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);   // X, карта

                        ichislo = (long)(LongKrR_ed * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);   // X, карта

                    //} // IF


                    // Градусы (Красовский)
                    // CDel
                     // !!!Здесь это WGS84
                    //else if (gbOwnDegMin.Visible == true)
                    //{

                        ichislo = (long)(LatKrG_ed * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrG_ed * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);

                    //} // IF


                    // Градусы,мин,сек (Красовский)
                    // CDel
                     // !!!Здесь это WGS84
                    //else if (gbOwnDegMinSec.Visible == true)
                    //{

                        tbBDeg2.Text = Convert.ToString(Lat_Grad_ed);
                        tbBMin2.Text = Convert.ToString(Lat_Min_ed);
                        ichislo = (long)(Lat_Sec_ed);
                        tbBSec.Text = Convert.ToString(ichislo);

                        tbLDeg2.Text = Convert.ToString(Long_Grad_ed);
                        tbLMin2.Text = Convert.ToString(Long_Min_ed);
                        ichislo = (long)(Long_Sec_ed);
                        tbLSec.Text = Convert.ToString(ichislo);

                    //} // IF


                } // OtobrSP_ed


     // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN

        // ************************************************************************


        // ************************************************************************

public double DefineCoeffQ(double dFreq, int iCodeSurface)
{
	double dCoeffQ = 0;
	
	double dLambda = 0;
	double dEpsilon = 0;
	double dSigma = 0;

	dLambda = LAMBDA/dFreq;
	
	switch (iCodeSurface)
	{
	case 0:
		dEpsilon = 4;
		dSigma = 0.001;
		break;

	case 1:
		dEpsilon = 10;
		dSigma = 0.01;
		break;

	case 2:
		dEpsilon = 80;
		dSigma = 0.001;
		break;

	case 3:
		dEpsilon = 80;
		dSigma = 4;
		break;

	case 4:
		dEpsilon = 7;
		dSigma = 0.001;
		break;
	}

	dCoeffQ = Math.Sqrt((dEpsilon-1)*(dEpsilon-1)+(60*dLambda*dSigma)*(60*dLambda*dSigma))/(dEpsilon*dEpsilon+(60*dLambda*dSigma)*(60*dLambda*dSigma));

	return dCoeffQ;
}
        // ************************************************************************
        // функция определения коэффициента CoeffHE

public double DefineCoeffHE(double dFreq, double dCoeffQ)
{
	double dCoeffHE = 0;

	double dLambda = 0;

	dLambda = LAMBDA/dFreq;

	dCoeffHE = dLambda*dLambda/(4*Math.PI*Math.PI*dCoeffQ);

	return dCoeffHE;

}
        // ************************************************************************
        // функция определения скорректированной высоты средства подавления для расчета

public int DefineCorrectHeightOwn(int iHeightAntennOwn,double dCoeffHE)// не уверена iHeightAntennOwn  edit11
{
	int iTotalHeightOwn = 0;

	iTotalHeightOwn = (int)(Math.Sqrt(iHeightAntennOwn*iHeightAntennOwn+dCoeffHE*dCoeffHE));

	return iTotalHeightOwn;
}
        // ************************************************************************
public int DefineResultHeightOwn(int iTotalHeightOwn, int iHeightPlaceOwn)
{
	int iResultHeightOwn = 0;

	//iResultHeightOwn = iTotalHeightOwn+iHeightPlaceOwn;

    iResultHeightOwn = (int)HeightAntennOwn_ed + iHeightPlaceOwn;

	return iResultHeightOwn;
}
        // ************************************************************************
        // Функция определения средней высота местности

public int DefineMiddleHeight(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
{
	int iRadius = 20000;
	int iStep = 100;
	int iCount = 0;
	double dMiddleHeightStep = 0;
	double dMiddleHeight = 0;

	if ((tpReferencePoint.X >0) &(tpReferencePoint.Y >0))
	{
		double dMinX = 0;
		double dMinY = 0;

		double dMaxX = 0;
		double dMaxY = 0;

		dMinX = tpReferencePoint.X-iRadius;
		dMinY = tpReferencePoint.Y-iRadius;

		dMaxX = tpReferencePoint.X+iRadius;
		dMaxY = tpReferencePoint.Y+iRadius;

		// пройти по координатам карты с шагом Shag
		for (int i=(int)dMinX; i<dMaxX; i=i+iStep)
		{
			for (int j=(int)dMinY; j<dMaxY; j=j+iStep)
			{
				double dSetX = 0;
				double dSetY = 0;

				dSetX = i;
				dSetY = j;

            //GlobalVar::axMapPointGlobal.SetPoint(dSetX,dSetY);
			//dMiddleHeightStep = 0;
		    //dMiddleHeightStep = GlobalVar::axMapScreenGlobal->PointHeight_get(GlobalVar::axMapPointGlobal);
              
                axMapPointTemp.SetPoint(dSetX, dSetY);
                dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);


				if (dMiddleHeightStep<0)
				{
					dMiddleHeight = 0;
					return (int)dMiddleHeight;
				}

				// увеличить счетчик на 1
				iCount++;

				// суммировать высоты
				dMiddleHeight = dMiddleHeight+dMiddleHeightStep;
			}
		}

		// средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
		dMiddleHeight = dMiddleHeight/(double)iCount;

		if (dMiddleHeight<0)
			dMiddleHeight = 0;		
	}

	return (int)dMiddleHeight;
}
        // ************************************************************************
public int DefineMinHeight(Point tpReferencePoint, int iMiddleHeight)
{
	int iMinHeight = 0;
	
	if ((tpReferencePoint.X >0) &(tpReferencePoint.Y >0))
	{
		double dSetX = 0;
		double dSetY = 0;

		int iHeightRefPoint = 0;


		dSetX = tpReferencePoint.X;
		dSetY = tpReferencePoint.Y;

		//GlobalVar::axMapPointGlobal->SetPoint(dSetX,dSetY);	

        //GlobalVar.axMapPointGlobalAdd.PlaceInp = TxPPLACE.PP_PLANE;
        //GlobalVar.axMapPointGlobalAdd.PlaceOut = TxPPLACE.PP_PLANE;

        GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);

		//iHeightRefPoint = (int)GlobalVar::axMapScreenGlobal->PointHeight_get(GlobalVar::axMapPointGlobal);
        iHeightRefPoint = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);


		iMinHeight = Math.Min(iHeightRefPoint,iMiddleHeight);
	}

	return iMinHeight;
}
        // ************************************************************************
public int DefineCorrectHeightOpponent(int iHeightOpponent,double dCoeffHE) //  не знаю iHeightOpponent  edit 9
{
	int iCorrectHeightOpponent = 0;

	iCorrectHeightOpponent = (int)(Math.Sqrt(iHeightOpponent*iHeightOpponent+dCoeffHE*dCoeffHE));

	return iCorrectHeightOpponent;
}
        // ************************************************************************
public int DefineResultHeightOpponent(int iCorrectHeightOpponent, int iMiddleHeight)
{
	int iiResultHeightOpponent = 0;

	////iResultHeightOpponent = iCorrectHeightOpponent+iMiddleHeight;
	//iiResultHeightOpponent = dHeightReceiverOpponent+iMiddleHeight;

    iiResultHeightOpponent = (int)HeightReceiverOpponent_ed + iMiddleHeight;


	return iiResultHeightOpponent;
}
        // ************************************************************************
public long DefineMaxDistance(int iResultHeightOwn, int iResultHeightOpponent, int iMinHeight)
{
	int iMaxDistance = 0;

	double dRes1 = 0;
	double dRes2 = 0;

	dRes1 = iResultHeightOwn-iMinHeight;
	dRes2 = iResultHeightOpponent-iMinHeight;

	iMaxDistance = (int)(Math.Sqrt(dRes1)+Math.Sqrt(dRes2));

	iMaxDistance = (int)(4.12*iMaxDistance*1000);
	
	return iMaxDistance;
}
        // ************************************************************************
public double DefineGamma(int iCodePolarOpponent)
{
	double dGamma = 0;

	switch (iCodePolarOpponent)
	{
	case 0:
		dGamma = 1;
		break;

	case 1:
		dGamma = 0;
		break;

	case 2:
		dGamma = 0.5;
		break;

	case 3:
		dGamma = 0.5;
		break;

	}
	return dGamma;
}
        // ************************************************************************

public long DefineRadiusZone()
{
	double dResult = 0;	
	int iStepDist = 0;
	iStepDist =	50;
	double dLambda = 0;
	dLambda = LAMBDA/Freq_ed;
	
	double dCorrectHeightOppTransmit = 0;
	dCorrectHeightOppTransmit = Math.Sqrt(HeightTransmitOpponent_ed*HeightTransmitOpponent_ed+dCoeffHE_ed*dCoeffHE_ed);

	double dAddWeakHindrance = 0;
	dAddWeakHindrance = 4*Math.Sin((2*Math.PI*dCorrectHeightOppTransmit*iCorrectHeightOpponent_ed)/
                       (dLambda*RangeComm_ed))*Math.Sin((2*Math.PI*dCorrectHeightOppTransmit*iCorrectHeightOpponent_ed)/
                       (dLambda*RangeComm_ed));

	double dAddWeakHindranceTwo = 0;

	double dCoeffK = 0;

	double dCoeffKConst = 0;

	double dVAr = 0;	
	double dVAr1 = 0;	

	if (WidthSignal_ed>=WidthHindrance_ed)
		dVAr1 = 1;
	else
		dVAr1 =  Convert.ToDouble(WidthSignal_ed)/ Convert.ToDouble(WidthHindrance_ed);	

	dCoeffKConst = Convert.ToDouble((PowerOwn_ed*CoeffOwn_ed*dGamma_ed*dVAr1))/
		          (Convert.ToDouble(PowerOpponent_ed*CoeffTransmitOpponent_ed));

	long iMinDistance = 0;
	long i = (long)iMaxDistance_ed;
	while( i>iMinDistance)
	{
		dAddWeakHindranceTwo = 0;
		dAddWeakHindranceTwo = 4*Math.Sin((2*Math.PI*iCorrectHeightOwn_ed*iCorrectHeightOpponent_ed)/
                              (dLambda*i))*Math.Sin((2*Math.PI*iCorrectHeightOwn_ed*iCorrectHeightOpponent_ed)/(dLambda*i));

		dVAr = dAddWeakHindranceTwo/dAddWeakHindrance;

		double dCoeffK1 = 0;

		dCoeffK1 = (Convert.ToDouble(RangeComm_ed*RangeComm_ed))/(Convert.ToDouble(i*i));
		dCoeffK = dCoeffK1*dCoeffKConst*dVAr;

		//dCoeffK = (iPowerOwn*iCoeffOwn*iRangeComm*iRangeComm*iWidthSignal*1000*dGamma*dVAr)/
			//(iPowerOpponent*iCoeffTransmitOpponent*i*i*dWidthHindrance);

		if (dCoeffK>CoeffSupOpponent_ed)
		{
			dResult = 0;
			dResult = i;
			i = iMinDistance;
		}
		i -=iStepDist;
	}                            
    return (long)dResult;
}

private void ZonePowerAvail_FormClosing(object sender, FormClosingEventArgs e)
{
    e.Cancel = true;
    Hide();

}

        // ************************************************************************




        // *************************************************************** FUNCTIONS



    } // Class
} // namespace
