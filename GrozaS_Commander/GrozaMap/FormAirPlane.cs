﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

namespace GrozaMap
{
    public partial class FormAirPlane : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public double dchislo;
        public long ichislo;


        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormAirPlane(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();
            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************

        private void FormAirPlane_Load(object sender, EventArgs e)
        {

            // ----------------------------------------------------------------------
            // TextBox

            tbLat.Text = "";
            tbLong.Text = "";
            tbAngle.Text = "0";

            // ----------------------------------------------------------------------

        } // Загрузка формы
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button "bShow": Нарисовать самолет
        // ************************************************************************
        private void bShow_Click(object sender, EventArgs e)
        {
            double xair = 0;
            double yair = 0;

            // -------------------------------------------------------------------
            GlobalVarLn.Angle_air = Convert.ToDouble(tbAngle.Text);
            GlobalVarLn.Num_air = Convert.ToInt32(tbNum.Text);
            // -------------------------------------------------------------------
            // m на местности

            xair = GlobalVarLn.MapX1;
            yair = GlobalVarLn.MapY1;
            // -------------------------------------------------------------------
            GlobalVarLn.fl_AirPlane = 1;
            // -------------------------------------------------------------------

            // mapPlaneToGeo ******************************************************

            // rad
            mapPlaneToGeo(GlobalVarLn.hmapl, ref xair, ref yair);

            // rad->grad
            GlobalVarLn.Lat_air = (xair * 180) / Math.PI;
            GlobalVarLn.Long_air = (yair * 180) / Math.PI;

            ichislo = (long)(GlobalVarLn.Lat_air * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            tbLat.Text = Convert.ToString(dchislo);   // Широта, град

            ichislo = (long)(GlobalVarLn.Long_air * 10000000);
            dchislo = ((double)ichislo) / 10000000;
            tbLong.Text = Convert.ToString(dchislo);   // Долгота, град

            // ***************************************************** mapPlaneToGeo

            // -------------------------------------------------------------------
            // Добавить самолет в структуры

            ClassMap.f_AddAirPlane(
                                     GlobalVarLn.Lat_air,
                                     GlobalVarLn.Long_air,
                                     GlobalVarLn.Num_air
                                   );

            // -------------------------------------------------------------------
            // Перерисовка

            ClassMap.f_ReDrawAirPlane();
            // -------------------------------------------------------------------


        } // Button "bShow"
        // ************************************************************************

        private void tbLat_TextChanged(object sender, EventArgs e)
        {

        }

        private void bClean_Click(object sender, EventArgs e)
        {

        }

        private void FormAirPlane_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        } 
        // ************************************************************************



    } // Class
} // Namespace
