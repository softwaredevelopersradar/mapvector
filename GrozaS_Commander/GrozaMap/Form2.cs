﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

namespace GrozaMap
{
    public partial class Form2 : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        //private Point tpOwnCoordRect42;
        //private Point tpOwnCoordRect;
        //private Point tpPoint1Rect;
        //private Point tpPoint2Rect;


        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        private double LAMBDA;

        // .....................................................................
        // Координаты СП,ys

        //private uint flCoordSP_comm; // =1-> Выбрали СП
        //private uint flCoordYS1_comm; // =1-> Выбрали YS1
        //private uint flCoordYS2_comm; // =1-> Выбрали YS2

        // Координаты СП на местности в м
        private double XSP_comm;
        private double YSP_comm;
        private double XYS1_comm;
        private double YYS1_comm;
        private double XYS2_comm;
        private double YYS2_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS1_comm;
        private double LongKrG_YS1_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS1_comm;
        private double LongKrR_YS1_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS1_comm;
        private int Lat_Min_YS1_comm;
        private double Lat_Sec_YS1_comm;
        private int Long_Grad_YS1_comm;
        private int Long_Min_YS1_comm;
        private double Long_Sec_YS1_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS142_comm;
        private double YYS142_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_YS2_comm;
        private double LongKrG_YS2_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_YS2_comm;
        private double LongKrR_YS2_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_YS2_comm;
        private int Lat_Min_YS2_comm;
        private double Lat_Sec_YS2_comm;
        private int Long_Grad_YS2_comm;
        private int Long_Min_YS2_comm;
        private double Long_Sec_YS2_comm;
        // Гаусс-крюгер(СК42) м
        private double XYS242_comm;
        private double YYS242_comm;

        // ......................................................................
        // Основные параметры

        private double OwnHeight_comm;
        private double Point1Height_comm;
        private double Point2Height_comm;
        //private double HeightOwnObject_comm;
        private double PowerOwn_comm;
        private double CoeffOwn_comm;
        //private double RadiusZone_comm;
        //private double MaxDist_comm;

        private int i_HeightOwnObject_comm;
        private int i_Cap1_comm;
        private int i_WidthHindrance_comm;
        private int i_Surface_comm;
        private double Cap1_comm;
        private double WidthHindrance_comm;
        //private double Surface_comm;

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // Для подавляемой линии
        private double Freq_comm;
        private double PowerOpponent_comm;
        private double CoeffTransmitOpponent_comm;
        private double CoeffReceiverOpponent_comm;
        private double RangeComm_comm;
        private double WidthSignal_comm;
        private double HeightTransmitOpponent_comm;
        private double HeightReceiverOpponent_comm;
        private double CoeffSupOpponent_comm;
        private int i_PolarOpponent_comm;
        private int i_CoeffSupOpponent_comm;
        private int i_TypeCommOpponent_comm;

        // ......................................................................
        // Зона

        private double dCoeffQ_comm;
        private double dCoeffHE_comm;
        private int iCorrectHeightOwn_comm;
        private int iResultHeightOwn_comm;
        private int iMiddleHeight_comm;
        private int iMinHeight_comm;
        private int iCorrectHeightOpponent_comm;
        private int iResultHeightOpponent_comm;
        private long iMaxDistance_comm;
        private double dGamma_comm;
        //private long liRadiusZone_comm;

        // ......................................................................
        private int iDistJammerComm1; // расстояние от УС1 до средства подаления
        private int iDistJammerComm2; // расстояние от УС2 до средства подаления
        private int iDistBetweenComm;
        private bool blResultSupress;


        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public Form2(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            dchislo = 0;
            ichislo = 0;

            LAMBDA = 300000;

            //flCoordSP_comm = 0; // =1-> Выбрали СП
            //flCoordYS1_comm = 0; // =1-> Выбрали YS1
            //flCoordYS2_comm = 0; // =1-> Выбрали YS2

            // .....................................................................
            // Координаты СП

            // Координаты СП на местности в м
            XSP_comm = 0;
            YSP_comm = 0;
            XYS1_comm = 0;
            YYS1_comm = 0;
            XYS2_comm = 0;
            YYS2_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS1_comm = 0;
            LongKrG_YS1_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS1_comm = 0;
            LongKrR_YS1_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS1_comm = 0;
            Lat_Min_YS1_comm = 0;
            Lat_Sec_YS1_comm = 0;
            Long_Grad_YS1_comm = 0;
            Long_Min_YS1_comm = 0;
            Long_Sec_YS1_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS142_comm = 0;
            YYS142_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_YS2_comm = 0;
            LongKrG_YS2_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_YS2_comm = 0;
            LongKrR_YS2_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_YS2_comm = 0;
            Lat_Min_YS2_comm = 0;
            Lat_Sec_YS2_comm = 0;
            Long_Grad_YS2_comm = 0;
            Long_Min_YS2_comm = 0;
            Long_Sec_YS2_comm = 0;
            // Гаусс-крюгер(СК42) м
            XYS242_comm = 0;
            YYS242_comm = 0;

            // ......................................................................
            // Основные параметры

            OwnHeight_comm = 0;
            Point1Height_comm=0;
            Point2Height_comm=0;
            //HeightOwnObject_comm = 0;
            PowerOwn_comm = 0;
            CoeffOwn_comm = 0;
            //RadiusZone_comm = 0;
            //MaxDist_comm = 0;

            i_HeightOwnObject_comm = 0;
            i_Cap1_comm = 0;
            i_WidthHindrance_comm = 0;
            i_Surface_comm = 0;
            Cap1_comm = 0;
            WidthHindrance_comm = 0;
            //Surface_comm = 0;

            // Высота средства подавления
            // ??????????????????????
            HeightAntennOwn_comm = 0;
            HeightTotalOwn_comm = 0;

            // Для подавляемой линии
            Freq_comm = 0;
            PowerOpponent_comm = 0;
            CoeffTransmitOpponent_comm = 0;
            CoeffReceiverOpponent_comm = 0;
            RangeComm_comm = 0;
            WidthSignal_comm = 0;
            HeightTransmitOpponent_comm = 0;
            HeightReceiverOpponent_comm = 0;
            CoeffSupOpponent_comm = 0;
            i_PolarOpponent_comm = 0;
            i_CoeffSupOpponent_comm = 0;
            i_TypeCommOpponent_comm = 0;

            // ......................................................................
            // Зона

            dCoeffQ_comm = 0;
            dCoeffHE_comm = 0;
            iCorrectHeightOwn_comm = 0;
            iResultHeightOwn_comm = 0;
            iMiddleHeight_comm = 0;
            iMinHeight_comm = 0;
            iCorrectHeightOpponent_comm = 0;
            iResultHeightOpponent_comm = 0;
            iMaxDistance_comm = 0;
            dGamma_comm = 0;
            //liRadiusZone_comm = 0;

            // ......................................................................
	        iDistJammerComm1=0; // расстояние от УС1 до средства подаления
	        iDistJammerComm2=0; // расстояние от УС2 до средства подаления
            iDistBetweenComm=0;
            blResultSupress = false;

        } // Конструктор
        // ***********************************************************  Конструктор


// Обработчик кнопки "Принять"
        private void bAccept_Click(object sender, EventArgs e)
        {
            ;
        }

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void Form2_Load(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            gbOwnRect.Visible = true;
            gbOwnRect.Location = new Point(8, 26);

            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;

            cbChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            gbPt1Rect.Visible = true;
            gbPt1Rect.Location = new Point(6, 11);
            gbPt2Rect.Visible = true;
            gbPt2Rect.Location = new Point(6, 11);

            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;

            gbPt2Rect42.Visible = false;
            gbPt2Rad.Visible = false;
            gbPt2DegMin.Visible = false;
            gbPt2DegMinSec.Visible = false;

            cbCommChooseSC.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Средство РП

            cbHeightOwnObject.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Пропускная способность

            cbCap1.SelectedIndex = 0;
            // ----------------------------------------------------------------------
            // Ширина спектра помехи

             cbWidthHindrance.SelectedIndex = 0;
            // ----------------------------------------------------------------------
             // Подстилающая поверхность

             cbSurface.SelectedIndex = 0;
            // ---------------------------------------------------------------------
             // Поляризация сигнала

             cbPolarOpponent.SelectedIndex = 0;
            // ----------------------------------------------------------------------
             // Коэффициент подавления

             cbCoeffSupOpponent.SelectedIndex = 0;
            // ----------------------------------------------------------------------
             // Вид связи

             cbTypeCommOpponent.SelectedIndex = 0;
            // ----------------------------------------------------------------------
             // TextBox

             tbCoeffSupOpponent.Text = "2,3";

            // ----------------------------------------------------------------------
             chbXY.Checked = false;
            // ----------------------------------------------------------------------
             chbXY1.Checked = false;
            // ----------------------------------------------------------------------
            // Переменные

             GlobalVarLn.Numb_CommPowerAvail = (int)nudCountOpponent.Value;
             GlobalVarLn.fl_CommPowerAvail = 0; // Отрисовка зоны
             GlobalVarLn.flCoordSP_comm = 0; // =1-> Выбрали СП
             GlobalVarLn.flCoordYS1_comm = 0;
             GlobalVarLn.flCoordYS2_comm = 0;
            // ----------------------------------------------------------------------

        } // LoadForm

        // ************************************************************************
        // Очистка
        // ************************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            // SP
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbXRect42.Text = "";
            tbYRect42.Text = "";
            tbBRad.Text = "";
            tbLRad.Text = "";
            tbBMin1.Text = "";
            tbLMin1.Text = "";
            tbBDeg2.Text = "";
            tbBMin2.Text = "";
            tbBSec.Text = "";
            tbLDeg2.Text = "";
            tbLMin2.Text = "";
            tbLSec.Text = "";

            // YS1
            tbPt1XRect.Text = "";
            tbPt1YRect.Text = "";
            tbPt1XRect42.Text = "";
            tbPt1YRect42.Text = "";
            tbPt1BRad.Text = "";
            tbPt1LRad.Text = "";
            tbPt1BMin1.Text = "";
            tbPt1LMin1.Text = "";
            tbPt1BDeg2.Text = "";
            tbPt1BMin2.Text = "";
            tbPt1BSec.Text = "";
            tbPt1LDeg2.Text = "";
            tbPt1LMin2.Text = "";
            tbPt1LSec.Text = "";

            // YS2
            tbPt2XRect.Text = "";
            tbPt2YRect.Text = "";
            tbPt2XRect42.Text = "";
            tbPt2YRect42.Text = "";
            tbPt2BRad.Text = "";
            tbPt2LRad.Text = "";
            tbPt2BMin1.Text = "";
            tbPt2LMin1.Text = "";
            tbPt2BDeg2.Text = "";
            tbPt2BMin2.Text = "";
            tbPt2BSec.Text = "";
            tbPt2LDeg2.Text = "";
            tbPt2LMin2.Text = "";
            tbPt2LSec.Text = "";

            tbOwnHeight.Text = "";
            tbHeightOwnObject.Text = "";

            tbPt1Height.Text = "";
            tbPt2Height.Text = "";

            tbRadiusZone.Text = "";
            tbMaxDist.Text = "";
            tbHindSignal.Text = "";
            tbResult.Text = "";

            // ...................................................................
            // переменные

            GlobalVarLn.flCoordSP_comm = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordYS1_comm = 0;
            GlobalVarLn.flCoordYS2_comm = 0;
            GlobalVarLn.fl_CommPowerAvail = 0; // Отрисовка зоны
            // ...................................................................
            chbXY.Checked = false;
            chbXY1.Checked = false;
            // ----------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // ---------------------------------------------------------------------


        }  // Clear
        // ************************************************************************


        // ************************************************************************
        // Обработчик "Изменить число абонентов УС"
        // ************************************************************************

        private void nudCountOpponent_ValueChanged(object sender, EventArgs e)
        {
            GlobalVarLn.Numb_CommPowerAvail = (int)nudCountOpponent.Value;

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК SP
        // ************************************************************************

        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoord_Comm(cbChooseSC.SelectedIndex);

        } // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbCommChooseSC": Выбор СК YS
        // ************************************************************************

        private void cbCommChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoordYS_Comm(cbCommChooseSC.SelectedIndex);

        } // Обработчик ComboBox "cbCommChooseSC": Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button1 "СП": Выбор СП
        // ************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;


            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();

            // ......................................................................
            // !!! XSP_ed,YSP_ed -> реальные координаты на местности карты в м (Plane)

            XSP_comm = GlobalVarLn.MapX1;
            YSP_comm = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY.Checked == true)
            {

                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Недопустимые координаты СП");
                    return;
                }

                XSP_comm = Convert.ToDouble(tbXRect.Text);
                YSP_comm = Convert.ToDouble(tbYRect.Text);
            }
            // ......................................................................
            // Треугольник на карте

            xtmp_ed = XSP_comm;
            ytmp_ed = YSP_comm;

            GlobalVarLn.XCenter_comm = XSP_comm;
            GlobalVarLn.YCenter_comm = YSP_comm;

            // SP
            ClassMap.f_Map_Pol_XY_stat(
                          GlobalVarLn.XCenter_comm,  // m
                          GlobalVarLn.YCenter_comm,
                          1,
                          ""
                         );
            // ......................................................................
           GlobalVarLn.fl_CommPowerAvail = 1;
           GlobalVarLn.flCoordSP_comm = 1; // СП выбрана
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XSP_comm;
            ytmp_ed = YSP_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap1_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек

                );

            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap1_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_comm,   // широта
                       ref LongKrG_comm   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );

            // .......................................................................


            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap3_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_comm,   // широта
                       LongKrG_comm,  // долгота

                       // Выходные параметры (km)
                       ref XSP42_comm,
                       ref YSP42_comm

                   );

            // km->m
            XSP42_comm = XSP42_comm * 1000;
            YSP42_comm = YSP42_comm * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отображение СП в выбранной СК

            OtobrSP_Comm();
            // .......................................................................


        } // Button1->SP
        // *************************************************************************************

        // ************************************************************************
        // Обработчик Button2 "YS1": Выбор YS1
        // ************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;


            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            ClassMap objClassMap4_ed = new ClassMap();
            ClassMap objClassMap5_ed = new ClassMap();
            ClassMap objClassMap6_ed = new ClassMap();

            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            XYS1_comm = GlobalVarLn.MapX1;
            YYS1_comm = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY1.Checked == true)
            {

                if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
                {
                    MessageBox.Show("Недопустимые координаты УС1");
                    return;
                }

                XYS1_comm = Convert.ToDouble(tbPt1XRect.Text);
                YYS1_comm = Convert.ToDouble(tbPt1YRect.Text);

            }
            // ......................................................................
            // Треугольник(сиий) на карте

            xtmp_ed = XYS1_comm;
            ytmp_ed = YYS1_comm;

            GlobalVarLn.XPoint1_comm = XYS1_comm;
            GlobalVarLn.YPoint1_comm = YYS1_comm;

            // YS1
            ClassMap.f_Map_Pol_XY_stat(
                          GlobalVarLn.XPoint1_comm,  // m
                          GlobalVarLn.YPoint1_comm,
                          2,
                          "УС1"
                         );
            // ......................................................................
            GlobalVarLn.flCoordYS1_comm = 1; // YS1 выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XYS1_comm;
            ytmp_ed = YYS1_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap4_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap4_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек

                );

            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap4_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_YS1_comm,   // широта
                       ref LongKrG_YS1_comm   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            LatKrR_YS1_comm = (LatKrG_YS1_comm * Math.PI) / 180;
            LongKrR_YS1_comm = (LongKrG_YS1_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_YS1_comm,

                // Выходные параметры 
                ref Lat_Grad_YS1_comm,
                ref Lat_Min_YS1_comm,
                ref Lat_Sec_YS1_comm

              );

            // Долгота
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_YS1_comm,

                // Выходные параметры 
                ref Long_Grad_YS1_comm,
                ref Long_Min_YS1_comm,
                ref Long_Sec_YS1_comm

              );

            // .......................................................................


            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap6_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_YS1_comm,   // широта
                       LongKrG_YS1_comm,  // долгота

                       // Выходные параметры (km)
                       ref XYS142_comm,
                       ref YYS142_comm

                   );

            // km->m
            XYS142_comm = XYS142_comm * 1000;
            YYS142_comm = YYS142_comm * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отображение СП в выбранной СК

            OtobrYS1_Comm();
            // .......................................................................

        } // Button2: YS1
        // ************************************************************************

        // ************************************************************************
        // Обработчик Button3 "YS2": Выбор YS2
        // ************************************************************************

        private void button3_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;


            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            ClassMap objClassMap7_ed = new ClassMap();
            ClassMap objClassMap8_ed = new ClassMap();
            ClassMap objClassMap9_ed = new ClassMap();
            // ......................................................................
            if(GlobalVarLn.Numb_CommPowerAvail==1)
            {
                MessageBox.Show("Число абонентов равно 1");
                return;
            }
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            XYS2_comm = GlobalVarLn.MapX1;
            YYS2_comm = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY1.Checked == true)
            {

                if ((tbPt2XRect.Text == "") || (tbPt2YRect.Text == ""))
                {
                    MessageBox.Show("Недопустимые координаты УС2");
                    return;
                }

                XYS2_comm = Convert.ToDouble(tbPt2XRect.Text);
                YYS2_comm = Convert.ToDouble(tbPt2YRect.Text);

            }
            // ......................................................................
            // Треугольник(сиий) на карте

            xtmp_ed = XYS2_comm;
            ytmp_ed = YYS2_comm;

            GlobalVarLn.XPoint2_comm = XYS2_comm;
            GlobalVarLn.YPoint2_comm = YYS2_comm;

            // YS2
            ClassMap.f_Map_Pol_XY_stat(
                          GlobalVarLn.XPoint2_comm,  // m
                          GlobalVarLn.YPoint2_comm,
                          2,
                          "УС2"
                         );
            // ......................................................................
            GlobalVarLn.flCoordYS2_comm = 1; // YS2 выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XYS2_comm;
            ytmp_ed = YYS2_comm;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................


            // WGS84(эллипсоид)->элл.Красовского *************************************
            // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)
            // WGS84(широта,долгота)из Панорамы пересчитываем в эллипсод Красовского моей функцией
            // Входные параметры -> град,km
            // Перевод в рад - внутри функции

            // dLong ..................................................................
            // Расчет приращения по долготе при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap7_ed.f_dLong
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLong_comm   // приращение по долготе, угл.сек

                );

            // ................................................................ dLong

            // dLat .................................................................
            // Расчет приращения по широте при преобразованиях координат WGS84<->SK42
            // (преобразования Молоденского), угл.сек

            objClassMap7_ed.f_dLat
                (

                    // Входные параметры (град,км)
                    xtmp1_ed,   // широта
                    ytmp1_ed,  // долгота
                    0,     // высота

                    // DATUM,m
                    dXdat_comm,
                    dYdat_comm,
                    dZdat_comm,

                    ref dLat_comm        // приращение по долготе, угл.сек

                );

            // ................................................................. dLat

            // Lat,Long .............................................................
            // Преобразования широты и долготы при пересчете WGS84->SK42

            objClassMap7_ed.f_WGS84_SK42_Lat_Long
                   (

                       // Входные параметры (град,км)
                       xtmp1_ed,   // широта
                       ytmp1_ed,  // долгота
                       0,     // высота
                       dLat_comm,       // приращение по долготе, угл.сек
                       dLong_comm,      // приращение по долготе, угл.сек

                       // Выходные параметры (grad)
                       ref LatKrG_YS2_comm,   // широта
                       ref LongKrG_YS2_comm   // долгота

                   );

            // ............................................................ Lat,Long


            // *********************************** WGS84(эллипсоид)->элл.Красовского

            // .......................................................................
            // Эллипсоид Красовского, радианы

            LatKrR_YS2_comm = (LatKrG_YS2_comm * Math.PI) / 180;
            LongKrR_YS2_comm = (LongKrG_YS2_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS

            // Широта
            objClassMap8_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_YS2_comm,

                // Выходные параметры 
                ref Lat_Grad_YS2_comm,
                ref Lat_Min_YS2_comm,
                ref Lat_Sec_YS2_comm

              );

            // Долгота
            objClassMap8_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_YS2_comm,

                // Выходные параметры 
                ref Long_Grad_YS2_comm,
                ref Long_Min_YS2_comm,
                ref Long_Sec_YS2_comm

              );

            // .......................................................................


            // SK42(элл.)->Крюгер ****************************************************
            // Преобразование геодезических координат (широта, долгота, высота) 
            // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
            // проекции Гаусса-Крюгера
            // Входные параметры -> !!!grad

            objClassMap9_ed.f_SK42_Krug
                   (
                // Входные параметры (!!! grad)
                // !!! эллипсоид Красовского
                       LatKrG_YS2_comm,   // широта
                       LongKrG_YS2_comm,  // долгота

                       // Выходные параметры (km)
                       ref XYS242_comm,
                       ref YYS242_comm

                   );

            // km->m
            XYS242_comm = XYS242_comm * 1000;
            YYS242_comm = YYS242_comm * 1000;
            // **************************************************** SK42(элл.)->Крюгер

            // .......................................................................
            // Отображение YS2 в выбранной СК

            OtobrYS2_Comm();
            // .......................................................................

        } // Button3: YS2
        // ************************************************************************

        // Расчет зоны MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN 

        private void bAccept_Click_1(object sender, EventArgs e)
        {

            if (GlobalVarLn.flCoordSP_comm == 0)
            {
                MessageBox.Show("Не выбрана СП");
                return;
            }

            if ((nudCountOpponent.Value == 1) && (GlobalVarLn.flCoordYS1_comm == 0))
            {
                MessageBox.Show("Не выбран УС1");
                return;
            }

            if ((nudCountOpponent.Value == 2) && (GlobalVarLn.flCoordYS2_comm == 0))
            {
                MessageBox.Show("Не выбран УС2");
                return;
            }


            // Ввод параметров ********************************************************
            // !!! Координаты СП уже расчитаны и введены по кнопке СП

            // Высота антенны
            // ??????????????????????
            HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);

            // Из TextBox
            // HSP
            //if (tbOwnHeight.Text == "")
            //    OwnHeight_comm = 0;
            //else
            //    OwnHeight_comm = Convert.ToDouble(tbOwnHeight.Text);

            // Мощность передатчика
            PowerOwn_comm = Convert.ToDouble(tbPowerOwn.Text);
            if ((PowerOwn_comm < 100) || (PowerOwn_comm > 2000))
            {
                MessageBox.Show("Значение мощности средства подавления вне диапазона (100 - 2000)");
                return;
            }

            // Коэффициент усиления
            CoeffOwn_comm = Convert.ToDouble(tbCoeffOwn.Text);
            if ((CoeffOwn_comm < 1) || (CoeffOwn_comm > 25))
            {
                MessageBox.Show("Значение коэффициента усиления средства подавления вне диапазона (1-25)");
                return;
            }

            // ComboBox (Индексы)
            i_HeightOwnObject_comm = cbHeightOwnObject.SelectedIndex; // Средство РП
            i_Cap1_comm = cbCap1.SelectedIndex; // Пропускная способность
            i_WidthHindrance_comm = cbWidthHindrance.SelectedIndex; // Ширина спектра помехи
            i_Surface_comm = cbSurface.SelectedIndex; // Подстилающая поверхность

            // ComboBox (Значения)
            Cap1_comm = Convert.ToDouble(cbCap1.Text); // Пропускная способность
            WidthHindrance_comm = Convert.ToDouble(cbWidthHindrance.Text); // Ширина спектра помехи
            // -----------------------------------------------------------------------
            // Для линии связи

            // Несущая частота
            Freq_comm = Convert.ToDouble(tbFreq.Text);
            if ((Freq_comm < 30000) || (Freq_comm > 1215000))
            {
                MessageBox.Show("Значение несущей частоты объекта подавления вне диапазона");
                return;
            }

            // Мощность передатчика
            PowerOpponent_comm = Convert.ToDouble(tbPowerOpponent.Text);
            if ((PowerOpponent_comm < 5) || (PowerOpponent_comm > 250))
            {
                MessageBox.Show("Значение мощности объекта подавления вне диапазона (1 - 250)");
                return;
            }

            // коэффициент усиления передатчика		 
            CoeffTransmitOpponent_comm = Convert.ToDouble(tbCoeffTransmitOpponent.Text);
            if ((CoeffTransmitOpponent_comm < 1) || (CoeffTransmitOpponent_comm > 10))
            {
                MessageBox.Show("Значение коэффициента усиления передатчика объекта подавления вне диапазона");
                return;
            }

            // коэффициент усиления приемника
            CoeffReceiverOpponent_comm = Convert.ToDouble(tbCoeffReceiverOpponent.Text);

            // Дальность связи
            RangeComm_comm = Convert.ToDouble(tbRangeComm.Text);
            if ((RangeComm_comm < 100) || (RangeComm_comm > 100000))
            {
                MessageBox.Show("Значение дальности связи объекта подавления вне диапазона");
                return;
            }

            // Ширина спектра
            WidthSignal_comm = Convert.ToDouble(tbWidthSignal.Text);
            if ((WidthSignal_comm < 3) || (WidthSignal_comm > 5000))
            {
                MessageBox.Show("Значение ширины спектра объекта подавления вне диапазона (3 - 5000)");
                return;
            }

            // антенна передатчика
            HeightTransmitOpponent_comm = Convert.ToDouble(tbHeightTransmitOpponent.Text);
            if ((HeightTransmitOpponent_comm < 1) || (HeightTransmitOpponent_comm > 20))
            {
                MessageBox.Show("Значение высоты антенны передатчика объекта подавления вне диапазона (1 - 20)");
                return;
            }

            // антенна приемника
            HeightReceiverOpponent_comm = Convert.ToDouble(tbHeightReceiverOpponent.Text);
            if ((HeightReceiverOpponent_comm < 1) || (HeightReceiverOpponent_comm > 1000))
            {
                MessageBox.Show("Значение высоты антенны приемника объекта подавления вне диапазона (1 - 1000)");
                return;
            }

            // Коэффициент подавления
            CoeffSupOpponent_comm = Convert.ToDouble(tbCoeffSupOpponent.Text);

            // ComboBox (Индексы)
            i_PolarOpponent_comm = cbPolarOpponent.SelectedIndex;       // Поляризация
            i_CoeffSupOpponent_comm = cbCoeffSupOpponent.SelectedIndex; // Коэффициент подавления
            i_TypeCommOpponent_comm = cbTypeCommOpponent.SelectedIndex; // Вид связи

            // -----------------------------------------------------------------------


            // ******************************************************** Ввод параметров

            // СП,YS ******************************************************************
            // Координаты СП на местности в м

            // XSP_ed,YSP_ed;
            GlobalVarLn.tpOwnCoordRect_comm.X = Convert.ToInt32(tbXRect.Text);
            GlobalVarLn.tpOwnCoordRect_comm.Y = Convert.ToInt32(tbYRect.Text);
            GlobalVarLn.tpPoint1Rect.X = Convert.ToInt32(tbPt1XRect.Text);
            GlobalVarLn.tpPoint1Rect.Y = Convert.ToInt32(tbPt1YRect.Text);
            GlobalVarLn.tpPoint2Rect.X = Convert.ToInt32(tbPt2XRect.Text);
            GlobalVarLn.tpPoint2Rect.Y = Convert.ToInt32(tbPt2YRect.Text);

            if ((tbXRect.Text == "") || (tbYRect.Text == "") || (tbPt1XRect.Text == "") ||
                (tbPt1YRect.Text == "") || (tbPt2XRect.Text == "") || (tbPt2YRect.Text == ""))
            {
                MessageBox.Show("Недопустимое значение координат");
                return;
            }
            // ........................................................................
            // Высота из карты

            // SP
            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.tpOwnCoordRect_comm.X, GlobalVarLn.tpOwnCoordRect_comm.Y);
            OwnHeight_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            // YS1
            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.tpPoint1Rect.X, GlobalVarLn.tpPoint1Rect.Y);
            Point1Height_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbPt1Height.Text = Convert.ToString(Point1Height_comm);

            // YS2
            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.tpPoint2Rect.X, GlobalVarLn.tpPoint2Rect.Y);
            Point2Height_comm = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            tbPt2Height.Text = Convert.ToString(Point2Height_comm);

            // ****************************************************************** СП,YS

            // H средства подавления **************************************************
            // определить значение высоты средства подавления

            switch (i_HeightOwnObject_comm)
            {
                // рельеф местности+высота антенны
                case 0:
                    HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
                    break;

                // высота антенны
                case 1:
                    HeightTotalOwn_comm = HeightAntennOwn_comm;
                    break;

                // задать самостоятельно
                case 2:
                    if (tbHeightOwnObject.Text == "")
                        HeightTotalOwn_comm = 0;
                    else
                        HeightTotalOwn_comm = Convert.ToDouble(tbHeightOwnObject.Text);

                    break;

            } // Switch

            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            // ************************************************** H средства подавления

            // Расчет зоны ************************************************************

            dCoeffQ_comm = DefineCoeffQ_Comm(Freq_comm, i_Surface_comm);
            tbCoeffQ.Text = dCoeffQ_comm.ToString();

            dCoeffHE_comm = DefineCoeffHE_Comm(Freq_comm, dCoeffQ_comm);
            tbCoeffHE.Text = dCoeffHE_comm.ToString();

            iCorrectHeightOwn_comm = DefineCorrectHeightOwn_Comm((int)HeightAntennOwn_comm, dCoeffHE_comm);
            tbCorrectHeightOwn.Text = iCorrectHeightOwn_comm.ToString();

            iResultHeightOwn_comm = DefineResultHeightOwn_Comm(iCorrectHeightOwn_comm, (int)OwnHeight_comm);
            tbResultHeightOwn.Text = iResultHeightOwn_comm.ToString();

            iMiddleHeight_comm = DefineMiddleHeight_Comm(GlobalVarLn.tpOwnCoordRect_comm, GlobalVarLn.axMapPointGlobalAdd, GlobalVarLn.axMapScreenGlobal);
            tbMiddleHeight.Text = iMiddleHeight_comm.ToString();

            iMinHeight_comm = DefineMinHeight_Comm(GlobalVarLn.tpOwnCoordRect_comm, iMiddleHeight_comm);
            tbMinHeight.Text = iMinHeight_comm.ToString();

            iCorrectHeightOpponent_comm = DefineCorrectHeightOpponent_Comm((int)HeightReceiverOpponent_comm, dCoeffHE_comm);
            tbCorrectHeightOpponent.Text = iCorrectHeightOpponent_comm.ToString();

            iResultHeightOpponent_comm = DefineResultHeightOpponent_Comm(iCorrectHeightOpponent_comm, iMiddleHeight_comm);
            tbResultHeightOpponent.Text = iResultHeightOpponent_comm.ToString();

            iMaxDistance_comm = DefineMaxDistance_Comm(iResultHeightOwn_comm, iResultHeightOpponent_comm, iMinHeight_comm);
            tbMaxDistance.Text = iMaxDistance_comm.ToString();
            tbMaxDist.Text = iMaxDistance_comm.ToString();

            // Растояние SP-YS1
            iDistJammerComm1 = DefineDistance_Comm(GlobalVarLn.tpOwnCoordRect_comm, GlobalVarLn.tpPoint1Rect);
            // Растояние SP-YS2
            iDistJammerComm2 = DefineDistance_Comm(GlobalVarLn.tpOwnCoordRect_comm, GlobalVarLn.tpPoint2Rect);
            // Растояние YS1-YS2
            iDistBetweenComm = DefineDistance_Comm(GlobalVarLn.tpPoint1Rect, GlobalVarLn.tpPoint2Rect);

            if (nudCountOpponent.Value == 1)
            {
                iDistBetweenComm = (int)RangeComm_comm;
            }

            if (iMaxDistance_comm < iDistJammerComm1)
            {
                tbResult.Text = "УС не подавляется";
                //return;
            }

            dGamma_comm = DefineGamma_Comm(i_PolarOpponent_comm);
            tbGamma.Text = dGamma_comm.ToString();

            //liRadiusZone_comm = DefineRadiusZone_Comm();
            //tbRadiusZone.Text = liRadiusZone_comm.ToString();

            tbRadiusZone.Text = iDistJammerComm1.ToString();

            //f_Map_El_XY_Comm(
            //               GlobalVar.tpOwnCoordRect_comm,
            //               liRadiusZone_comm
            //             );

            blResultSupress = false;
            blResultSupress = DefineCommSupress_Comm();

            if (blResultSupress == true)
                tbResult.Text = "УС подавляется";
            else
                tbResult.Text = "УС не подавляется";


            // ************************************************************ Расчет зоны


        } // Принять

        // MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN MAIN  Расчет зоны


        // FUNCTIONS ***************************************************************

        // ************************************************************************
        // функция выбора системы координат SP
        // ************************************************************************

        private void ChooseSystemCoord_Comm(int iSystemCoord)
        {
            gbOwnRect.Visible = false;
            gbOwnRect42.Visible = false;
            gbOwnRad.Visible = false;
            gbOwnDegMin.Visible = false;
            gbOwnDegMinSec.Visible = false;


            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    gbOwnRect.Visible = true;
                    gbOwnRect.Location = new Point(8, 26);

                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {

                        ichislo = (long)(XSP_comm);
                        tbXRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(YSP_comm);
                        tbYRect.Text = Convert.ToString(ichislo);

                    } // IF

                    break;

                case 1: // Метры 1942 года

                    gbOwnRect42.Visible = true;
                    gbOwnRect42.Location = new Point(8, 27);

                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {

                        ichislo = (long)(XSP42_comm);
                        tbXRect42.Text = Convert.ToString(ichislo);

                        ichislo = (long)(YSP42_comm);
                        tbYRect42.Text = Convert.ToString(ichislo);

                    } // IF

                    break;

                case 2: // Радианы (Красовский)

                    gbOwnRad.Visible = true;
                    gbOwnRad.Location = new Point(8, 27);

                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {

                        ichislo = (long)(LatKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                case 3: // Градусы (Красовский)

                    gbOwnDegMin.Visible = true;
                    gbOwnDegMin.Location = new Point(8, 27);

                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {

                        ichislo = (long)(LatKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    gbOwnDegMinSec.Visible = true;
                    gbOwnDegMinSec.Location = new Point(8, 27);

                    if (GlobalVarLn.flCoordSP_comm == 1)
                    {

                        tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
                        tbBMin2.Text = Convert.ToString(Lat_Min_comm);
                        ichislo = (long)(Lat_Sec_comm);
                        tbBSec.Text = Convert.ToString(ichislo);

                        tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
                        tbLMin2.Text = Convert.ToString(Long_Min_comm);
                        ichislo = (long)(Long_Sec_comm);
                        tbLSec.Text = Convert.ToString(ichislo);

                    } // IF


                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoord_Comm
        // ************************************************************************

        // ************************************************************************
        // функция выбора системы координат по УС
        // ************************************************************************

        private void ChooseSystemCoordYS_Comm(int iSystemCoord)
        {
            gbPt1Rect.Visible = false;
            gbPt1Rect42.Visible = false;
            gbPt1Rad.Visible = false;
            gbPt1DegMin.Visible = false;
            gbPt1DegMinSec.Visible = false;
            gbPt2Rect.Visible = false;
            gbPt2Rect42.Visible = false;
            gbPt2Rad.Visible = false;
            gbPt2DegMin.Visible = false;
            gbPt2DegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    // YS1
                    gbPt1Rect.Visible = true;
                    gbPt1Rect.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        ichislo = (long)(XYS1_comm);
                        tbPt1XRect.Text = Convert.ToString(ichislo);
                        ichislo = (long)(YYS1_comm);
                        tbPt1YRect.Text = Convert.ToString(ichislo);
                    }

                    // YS2
                    gbPt2Rect.Visible = true;
                    gbPt2Rect.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        ichislo = (long)(XYS2_comm);
                        tbPt2XRect.Text = Convert.ToString(ichislo);
                        ichislo = (long)(YYS2_comm);
                        tbPt2YRect.Text = Convert.ToString(ichislo);
                    }

                    break;

                case 1: // Метры 1942 года

                    // YS1
                    gbPt1Rect42.Visible = true;
                    gbPt1Rect42.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        ichislo = (long)(XYS142_comm);
                        tbPt1XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(YYS142_comm);
                        tbPt1YRect42.Text = Convert.ToString(ichislo);
                    }

                    // YS2
                    gbPt2Rect42.Visible = true;
                    gbPt2Rect42.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        ichislo = (long)(XYS242_comm);
                        tbPt2XRect42.Text = Convert.ToString(ichislo);
                        ichislo = (long)(YYS242_comm);
                        tbPt2YRect42.Text = Convert.ToString(ichislo);
                    }

                    break;

                case 2: // Радианы (Красовский)

                    // YS1
                    gbPt1Rad.Visible = true;
                    gbPt1Rad.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        ichislo = (long)(LatKrR_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(LongKrR_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LRad.Text = Convert.ToString(dchislo);
                    }

                    // YS2
                    gbPt2Rad.Visible = true;
                    gbPt2Rad.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        ichislo = (long)(LatKrR_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2BRad.Text = Convert.ToString(dchislo);
                        ichislo = (long)(LongKrR_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2LRad.Text = Convert.ToString(dchislo);
                    }

                    break;

                case 3: // Градусы (Красовский)

                    // YS1
                    gbPt1DegMin.Visible = true;
                    gbPt1DegMin.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        ichislo = (long)(LatKrG_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(LongKrG_YS1_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt1LMin1.Text = Convert.ToString(dchislo);
                    }

                    // YS2
                    gbPt2DegMin.Visible = true;
                    gbPt2DegMin.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        ichislo = (long)(LatKrG_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2BMin1.Text = Convert.ToString(dchislo);
                        ichislo = (long)(LongKrG_YS2_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbPt2LMin1.Text = Convert.ToString(dchislo);
                    }

                    break;

                case 4: // Градусы,мин,сек (Красовский)

                    // YS1
                    gbPt1DegMinSec.Visible = true;
                    gbPt1DegMinSec.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS1_comm == 1)
                    {
                        tbPt1BDeg2.Text = Convert.ToString(Lat_Grad_YS1_comm);
                        tbPt1BMin2.Text = Convert.ToString(Lat_Min_YS1_comm);
                        ichislo = (long)(Lat_Sec_YS1_comm);
                        tbPt1BSec.Text = Convert.ToString(ichislo);
                        tbPt1LDeg2.Text = Convert.ToString(Long_Grad_YS1_comm);
                        tbPt1LMin2.Text = Convert.ToString(Long_Min_YS1_comm);
                        ichislo = (long)(Long_Sec_YS1_comm);
                        tbPt1LSec.Text = Convert.ToString(ichislo);
                    }

                    // YS2
                    gbPt2DegMinSec.Visible = true;
                    gbPt2DegMinSec.Location = new Point(6, 11);

                    if (GlobalVarLn.flCoordYS2_comm == 1)
                    {
                        tbPt2BDeg2.Text = Convert.ToString(Lat_Grad_YS2_comm);
                        tbPt2BMin2.Text = Convert.ToString(Lat_Min_YS2_comm);
                        ichislo = (long)(Lat_Sec_YS2_comm);
                        tbPt2BSec.Text = Convert.ToString(ichislo);
                        tbPt2LDeg2.Text = Convert.ToString(Long_Grad_YS2_comm);
                        tbPt2LMin2.Text = Convert.ToString(Long_Min_YS2_comm);
                        ichislo = (long)(Long_Sec_YS2_comm);
                        tbPt2LSec.Text = Convert.ToString(ichislo);
                    }

                    break;

                default:
                    break;

            } // SWITCH

        } // ChooseSystemCoord1_Comm
        // ************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный треугольник)
        //
        // Входные параметры:
        // X - X, m
        // Y - Y, m
        // ******************************************************************************************
        private void f_Map_Pol_XY_Comm(
                                         double X,
                                         double Y
                                        )
        {

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                Point[] toDraw = new Point[3];
                toDraw[0].X = (int)X - (axaxcMapScreen.MapLeft + 7);
                toDraw[0].Y = (int)Y - (axaxcMapScreen.MapTop - 7);
                toDraw[1].X = (int)X - axaxcMapScreen.MapLeft;
                toDraw[1].Y = (int)Y - (axaxcMapScreen.MapTop + 7);
                toDraw[2].X = (int)X - (axaxcMapScreen.MapLeft - 7);
                toDraw[2].Y = (int)Y - (axaxcMapScreen.MapTop - 7);
                graph.FillPolygon(brushRed1, toDraw);

            }

            // -------------------------------------------------------------------------------------

        } // P/P f_Map_Pol_XY_ed
        // ******************************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный треугольник)
        //
        // Входные параметры:
        // X - X, m
        // Y - Y, m
        // R - m
        // ******************************************************************************************
        private void f_Map_El_XY_Comm(
                                    Point tpCenterPoint,
                                    long iRadiusZone
                                  )
        {

            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            double XC = 0;
            double YC = 0;
            XC = tpCenterPoint.X;
            YC = tpCenterPoint.Y;

            // -------------------------------------------------------------------------------------
            Graphics graph = axaxcMapScreen.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------------------------

            //dLeftX = tpCenterPoint.X + iRadiusZone;
            //dLeftY = tpCenterPoint.Y + iRadiusZone;
            //dRightX = tpCenterPoint.X - iRadiusZone;
            //dRightY = tpCenterPoint.Y - iRadiusZone;
            // -------------------------------------------------------------------------------------
            /*
                 long j = 0;
                 j=mapPlaneToPicture(ClassMap.hmapl, ref dLeftX, ref dLeftY);

                if (j > 0)
                {
                    dLeftX = dLeftX - axaxcMapScreen.MapLeft;
                    dLeftY = dLeftY - axaxcMapScreen.MapTop;
                }

                 j = 0;
                 j=mapPlaneToPicture(ClassMap.hmapl, ref dRightX, ref dRightY);

                if (j > 0)
                {
                    dRightX = dRightX - axaxcMapScreen.MapLeft;
                    dRightY = dRightY - axaxcMapScreen.MapTop;
                }

            */
            // -------------------------------------------------------------------------------------
            //int iWidthRect = 0;
            //iWidthRect = (int)(dLeftX - dRightX);

            //int iHeightRect = 0;
            //iHeightRect = (int)(dLeftY - dRightY);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {


                dLeftX = XC - iRadiusZone / 2;
                dLeftY = YC - iRadiusZone / 2;
                dRightX = XC + iRadiusZone / 2;
                dRightY = YC + iRadiusZone / 2;
                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);


                /*
                                mapPlaneToPicture(ClassMap.hmapl, ref XC, ref YC);
                                dLeftX = XC - 15;
                                dLeftY = YC - 15;
                                graph.FillEllipse(brushRed1, (int)dLeftX - axaxcMapScreen.MapLeft, (int)dLeftY - axaxcMapScreen.MapTop, 30, 30);
                */


                graph.FillEllipse(brushRed1, (int)dLeftX - axaxcMapScreen.MapLeft, (int)dLeftY - axaxcMapScreen.MapTop, (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
                // graph.FillEllipse(penRed1, (int)dLeftX - axaxcMapScreen.MapLeft, (int)dLeftY - axaxcMapScreen.MapTop, (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));



            }

            // -------------------------------------------------------------------------------------

        } // P/P f_Map_El_XY_ed
        // *************************************************************************************


        // ************************************************************************
        // функция отображения координат СП
        // ************************************************************************

        private void OtobrSP_Comm()
        {

            // Метры на местности
            //if (gbOwnRect.Visible == true)
            {

                ichislo = (long)(XSP_comm);
                tbXRect.Text = Convert.ToString(ichislo);

                ichislo = (long)(YSP_comm);
                tbYRect.Text = Convert.ToString(ichislo);

            } // IF


            // Метры 1942 года
            //else if (gbOwnRect42.Visible == true)
            {
                ichislo = (long)(XSP42_comm);
                tbXRect42.Text = Convert.ToString(ichislo);

                ichislo = (long)(YSP42_comm);
                tbYRect42.Text = Convert.ToString(ichislo);

            } // IF


            // Радианы (Красовский)
            //else if (gbOwnRad.Visible == true)
            {
                ichislo = (long)(LatKrR_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbBRad.Text = Convert.ToString(dchislo);   // X, карта

                ichislo = (long)(LongKrR_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbLRad.Text = Convert.ToString(dchislo);   // X, карта

            } // IF


            // Градусы (Красовский)
            //else if (gbOwnDegMin.Visible == true)
            {

                ichislo = (long)(LatKrG_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbBMin1.Text = Convert.ToString(dchislo);

                ichislo = (long)(LongKrG_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbLMin1.Text = Convert.ToString(dchislo);

            } // IF


            // Градусы,мин,сек (Красовский)
            //else if (gbOwnDegMinSec.Visible == true)
            {

                tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
                tbBMin2.Text = Convert.ToString(Lat_Min_comm);
                ichislo = (long)(Lat_Sec_comm);
                tbBSec.Text = Convert.ToString(ichislo);

                tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
                tbLMin2.Text = Convert.ToString(Long_Min_comm);
                ichislo = (long)(Long_Sec_comm);
                tbLSec.Text = Convert.ToString(ichislo);

            } // IF


        } // OtobrSP_ed
        // ************************************************************************

        // ************************************************************************
        // функция отображения координат YS1
        // ************************************************************************

        private void OtobrYS1_Comm()
        {

            // Метры на местности
            //if (gbPt1Rect.Visible == true)
            {

                ichislo = (long)(XYS1_comm);
                tbPt1XRect.Text = Convert.ToString(ichislo);

                ichislo = (long)(YYS1_comm);
                tbPt1YRect.Text = Convert.ToString(ichislo);

            } // IF


            // Метры 1942 года
            //else if (gbPt1Rect42.Visible == true)
            {
                ichislo = (long)(XYS142_comm);
                tbPt1XRect42.Text = Convert.ToString(ichislo);

                ichislo = (long)(YYS142_comm);
                tbPt1YRect42.Text = Convert.ToString(ichislo);

            } // IF


            // Радианы (Красовский)
            //else if (gbPt1Rad.Visible == true)
            {
                ichislo = (long)(LatKrR_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1BRad.Text = Convert.ToString(dchislo);   // X, карта

                ichislo = (long)(LongKrR_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1LRad.Text = Convert.ToString(dchislo);   // X, карта

            } // IF


            // Градусы (Красовский)
            //else if (gbPt1DegMin.Visible == true)
            {

                ichislo = (long)(LatKrG_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1BMin1.Text = Convert.ToString(dchislo);

                ichislo = (long)(LongKrG_YS1_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt1LMin1.Text = Convert.ToString(dchislo);

            } // IF


            // Градусы,мин,сек (Красовский)
            //else if (gbPt1DegMinSec.Visible == true)
            {

                tbPt1BDeg2.Text = Convert.ToString(Lat_Grad_YS1_comm);
                tbPt1BMin2.Text = Convert.ToString(Lat_Min_YS1_comm);
                ichislo = (long)(Lat_Sec_YS1_comm);
                tbPt1BSec.Text = Convert.ToString(ichislo);

                tbPt1LDeg2.Text = Convert.ToString(Long_Grad_YS1_comm);
                tbPt1LMin2.Text = Convert.ToString(Long_Min_YS1_comm);
                ichislo = (long)(Long_Sec_YS1_comm);
                tbPt1LSec.Text = Convert.ToString(ichislo);

            } // IF


        } // OtobrYS1_ed
        // ************************************************************************

        // ************************************************************************
        // функция отображения координат YS2
        // ************************************************************************

        private void OtobrYS2_Comm()
        {

            // Метры на местности
            //if (gbPt2Rect.Visible == true)
            {

                ichislo = (long)(XYS2_comm);
                tbPt2XRect.Text = Convert.ToString(ichislo);

                ichislo = (long)(YYS2_comm);
                tbPt2YRect.Text = Convert.ToString(ichislo);

            } // IF


            // Метры 1942 года
            //else if (gbPt2Rect42.Visible == true)
            {
                ichislo = (long)(XYS242_comm);
                tbPt2XRect42.Text = Convert.ToString(ichislo);

                ichislo = (long)(YYS242_comm);
                tbPt2YRect42.Text = Convert.ToString(ichislo);

            } // IF


            // Радианы (Красовский)
            //else if (gbPt2Rad.Visible == true)
            {
                ichislo = (long)(LatKrR_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2BRad.Text = Convert.ToString(dchislo);   // X, карта

                ichislo = (long)(LongKrR_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2LRad.Text = Convert.ToString(dchislo);   // X, карта

            } // IF


            // Градусы (Красовский)
            //else if (gbPt2DegMin.Visible == true)
            {

                ichislo = (long)(LatKrG_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2BMin1.Text = Convert.ToString(dchislo);

                ichislo = (long)(LongKrG_YS2_comm * 1000000);
                dchislo = ((double)ichislo) / 1000000;
                tbPt2LMin1.Text = Convert.ToString(dchislo);

            } // IF


            // Градусы,мин,сек (Красовский)
            //else if (gbPt2DegMinSec.Visible == true)
            {

                tbPt2BDeg2.Text = Convert.ToString(Lat_Grad_YS2_comm);
                tbPt2BMin2.Text = Convert.ToString(Lat_Min_YS2_comm);
                ichislo = (long)(Lat_Sec_YS2_comm);
                tbPt2BSec.Text = Convert.ToString(ichislo);

                tbPt2LDeg2.Text = Convert.ToString(Long_Grad_YS2_comm);
                tbPt2LMin2.Text = Convert.ToString(Long_Min_YS2_comm);
                ichislo = (long)(Long_Sec_YS2_comm);
                tbPt2LSec.Text = Convert.ToString(ichislo);

            } // IF


        } // OtobrYS1_ed
        // ************************************************************************


        private void gbPt1Rect42_Enter(object sender, EventArgs e)
        {
            ;
        }

        private void tbPt1BMin2_TextChanged(object sender, EventArgs e)
        {

        }

        private void gbPt2DegMin_Enter(object sender, EventArgs e)
        {

        }

        // ************************************************************************

        // ************************************************************************

        private double DefineCoeffQ_Comm(double dFreq, int iCodeSurface)
        {
            double dCoeffQ = 0;

            double dLambda = 0;
            double dEpsilon = 0;
            double dSigma = 0;

            dLambda = LAMBDA / dFreq;

            switch (iCodeSurface)
            {
                case 0:
                    dEpsilon = 4;
                    dSigma = 0.001;
                    break;

                case 1:
                    dEpsilon = 10;
                    dSigma = 0.01;
                    break;

                case 2:
                    dEpsilon = 80;
                    dSigma = 0.001;
                    break;

                case 3:
                    dEpsilon = 80;
                    dSigma = 4;
                    break;

                case 4:
                    dEpsilon = 7;
                    dSigma = 0.001;
                    break;
            }

            dCoeffQ = Math.Sqrt((dEpsilon - 1) * (dEpsilon - 1) + (60 * dLambda * dSigma) * (60 * dLambda * dSigma)) /
                                          (dEpsilon * dEpsilon + (60 * dLambda * dSigma) * (60 * dLambda * dSigma));

            return dCoeffQ;

        }
        // ************************************************************************
        // функция определения коэффициента CoeffHE

        private double DefineCoeffHE_Comm(double dFreq, double dCoeffQ)
        {
            double dCoeffHE = 0;

            double dLambda = 0;

            dLambda = LAMBDA / dFreq;

            dCoeffHE = dLambda * dLambda / (4 * Math.PI * Math.PI * dCoeffQ);

            return dCoeffHE;


        }
        // ************************************************************************
        // функция определения скорректированной высоты средства подавления для расчета

        private int DefineCorrectHeightOwn_Comm(int iHeightAntennOwn, double dCoeffHE)// не уверена iHeightAntennOwn  edit11
        {
            int iTotalHeightOwn = 0;

            iTotalHeightOwn = (int)(Math.Sqrt(iHeightAntennOwn * iHeightAntennOwn + dCoeffHE * dCoeffHE));

            return iTotalHeightOwn;
        }
        // ************************************************************************
        private int DefineResultHeightOwn_Comm(int iTotalHeightOwn, int iHeightPlaceOwn)
        {
            int iResultHeightOwn = 0;

            //iResultHeightOwn = iTotalHeightOwn+iHeightPlaceOwn;

            iResultHeightOwn = (int)HeightAntennOwn_comm + iHeightPlaceOwn;

            return iResultHeightOwn;

        }
        // ************************************************************************
        // Функция определения средней высота местности

        private int DefineMiddleHeight_Comm(Point tpReferencePoint, axMapPoint axMapPointTemp, AxaxcMapScreen AxaxcMapScreenTemp)
        {
            int iRadius = 2000;
            int iStep = 100;
            int iCount = 0;
            double dMiddleHeightStep = 0;
            double dMiddleHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                double dMinX = 0;
                double dMinY = 0;

                double dMaxX = 0;
                double dMaxY = 0;

                dMinX = tpReferencePoint.X - iRadius;
                dMinY = tpReferencePoint.Y - iRadius;

                dMaxX = tpReferencePoint.X + iRadius;
                dMaxY = tpReferencePoint.Y + iRadius;

                // пройти по координатам карты с шагом Shag
                for (int i = (int)dMinX; i < dMaxX; i = i + iStep)
                {
                    for (int j = (int)dMinY; j < dMaxY; j = j + iStep)
                    {
                        double dSetX = 0;
                        double dSetY = 0;

                        dSetX = i;
                        dSetY = j;

                        //GlobalVar::axMapPointGlobal.SetPoint(dSetX,dSetY);
                        //dMiddleHeightStep = 0;
                        //dMiddleHeightStep = GlobalVar::axMapScreenGlobal->PointHeight_get(GlobalVar::axMapPointGlobal);

                        axMapPointTemp.SetPoint(dSetX, dSetY);
                        dMiddleHeightStep = AxaxcMapScreenTemp.PointHeight_get(axMapPointTemp);


                        if (dMiddleHeightStep < 0)
                        {
                            dMiddleHeight = 0;
                            return (int)dMiddleHeight;
                        }

                        // увеличить счетчик на 1
                        iCount++;

                        // суммировать высоты
                        dMiddleHeight = dMiddleHeight + dMiddleHeightStep;
                    }
                }

                // средняя высота = сумма всех полученных высот/на кол-во пройденных точек     
                dMiddleHeight = dMiddleHeight / (double)iCount;

                if (dMiddleHeight < 0)
                    dMiddleHeight = 0;
            }

            return (int)dMiddleHeight;
        }
        // ************************************************************************
        private int DefineMinHeight_Comm(Point tpReferencePoint, int iMiddleHeight)
        {
            int iMinHeight = 0;

            if ((tpReferencePoint.X > 0) & (tpReferencePoint.Y > 0))
            {
                double dSetX = 0;
                double dSetY = 0;

                int iHeightRefPoint = 0;

                dSetX = tpReferencePoint.X;
                dSetY = tpReferencePoint.Y;

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(dSetX, dSetY);

                iHeightRefPoint = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                iMinHeight = Math.Min(iHeightRefPoint, iMiddleHeight);
            }

            return iMinHeight;
        }
        // ************************************************************************
        private int DefineCorrectHeightOpponent_Comm(int iHeightOpponent, double dCoeffHE) //  не знаю iHeightOpponent  edit 9
        {
            int iCorrectHeightOpponent = 0;

            iCorrectHeightOpponent = (int)(Math.Sqrt(iHeightOpponent * iHeightOpponent + dCoeffHE * dCoeffHE));

            return iCorrectHeightOpponent;
        }
        // ************************************************************************
        private int DefineResultHeightOpponent_Comm(int iCorrectHeightOpponent, int iMiddleHeight)
        {
            int iiResultHeightOpponent = 0;

            ////iResultHeightOpponent = iCorrectHeightOpponent+iMiddleHeight;
            //iiResultHeightOpponent = dHeightReceiverOpponent+iMiddleHeight;

            iiResultHeightOpponent = (int)HeightReceiverOpponent_comm + iMiddleHeight;


            return iiResultHeightOpponent;
        }
        // ************************************************************************
        private long DefineMaxDistance_Comm(int iResultHeightOwn, int iResultHeightOpponent, int iMinHeight)
        {
            int iMaxDistance = 0;

            double dRes1 = 0;
            double dRes2 = 0;

            dRes1 = iResultHeightOwn - iMinHeight;
            dRes2 = iResultHeightOpponent - iMinHeight;

            iMaxDistance = (int)(Math.Sqrt(dRes1) + Math.Sqrt(dRes2));

            iMaxDistance = (int)(4.12 * iMaxDistance * 1000);

            return iMaxDistance;


        }
        // ************************************************************************
// функция определения расстояния
private int DefineDistance_Comm(Point tpPoint1Coord, Point tpPoint2Coord)
{
	double dDistance = 0;

	double dDist1 = 0;
	double dDist2 = 0;
	
	dDist1 = ((double)tpPoint1Coord.X-(double)tpPoint2Coord.X)*((double)tpPoint1Coord.X-(double)tpPoint2Coord.X);
	dDist2 = ((double)tpPoint1Coord.Y-(double)tpPoint2Coord.Y)*((double)tpPoint1Coord.Y-(double)tpPoint2Coord.Y);

	dDistance = dDist1+dDist2;

	dDistance = Math.Sqrt(dDistance);

	return (int)dDistance;
}
        // ************************************************************************
private double DefineGamma_Comm(int iCodePolarOpponent)
{
    double dGamma = 0;

    switch (iCodePolarOpponent)
    {
        case 0:
            dGamma = 1;
            break;

        case 1:
            dGamma = 0;
            break;

        case 2:
            dGamma = 0.5;
            break;

        case 3:
            dGamma = 0.5;
            break;

    }
    return dGamma;
}
        // ************************************************************************

private long DefineRadiusZone_Comm()
{
    double dResult = 0;
    int iStepDist = 0;
    iStepDist = 50;
    double dLambda = 0;
    dLambda = LAMBDA / Freq_comm;

    double dCorrectHeightOppTransmit = 0;
    dCorrectHeightOppTransmit = Math.Sqrt(HeightTransmitOpponent_comm * HeightTransmitOpponent_comm + dCoeffHE_comm * dCoeffHE_comm);

    double dAddWeakHindrance = 0;
    dAddWeakHindrance = 4 * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit * iCorrectHeightOpponent_comm) /
                       (dLambda * RangeComm_comm)) * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit * iCorrectHeightOpponent_comm) /
                       (dLambda * RangeComm_comm));

    double dAddWeakHindranceTwo = 0;

    double dCoeffK = 0;

    double dCoeffKConst = 0;

    double dVAr = 0;
    double dVAr1 = 0;

    if (WidthSignal_comm >= WidthHindrance_comm)
        dVAr1 = 1;
    else
        dVAr1 = Convert.ToDouble(WidthSignal_comm) / Convert.ToDouble(WidthHindrance_comm);

    dCoeffKConst = Convert.ToDouble((PowerOwn_comm * CoeffOwn_comm * dGamma_comm * dVAr1)) /
                  (Convert.ToDouble(PowerOpponent_comm * CoeffTransmitOpponent_comm));

    long iMinDistance = 0;
    long i = (long)iMaxDistance_comm;
    while (i > iMinDistance)
    {
        dAddWeakHindranceTwo = 0;
        dAddWeakHindranceTwo = 4 * Math.Sin((2 * Math.PI * iCorrectHeightOwn_comm * iCorrectHeightOpponent_comm) /
                              (dLambda * i)) * Math.Sin((2 * Math.PI * iCorrectHeightOwn_comm * iCorrectHeightOpponent_comm) / (dLambda * i));

        dVAr = dAddWeakHindranceTwo / dAddWeakHindrance;

        double dCoeffK1 = 0;

        dCoeffK1 = (Convert.ToDouble(RangeComm_comm * RangeComm_comm)) / (Convert.ToDouble(i * i));
        dCoeffK = dCoeffK1 * dCoeffKConst * dVAr;

        //dCoeffK = (iPowerOwn*iCoeffOwn*iRangeComm*iRangeComm*iWidthSignal*1000*dGamma*dVAr)/
        //(iPowerOpponent*iCoeffTransmitOpponent*i*i*dWidthHindrance);

        if (dCoeffK > CoeffSupOpponent_comm)
        {
            dResult = 0;
            dResult = i;
            i = iMinDistance;
        }
        i -= iStepDist;
    }
    return (long)dResult;
}
        // ************************************************************************
// определение результата подавления/неподавления УС
private bool DefineCommSupress_Comm()
{

	bool blResult = false;

	double dLambda = 0;
    dLambda = LAMBDA / Freq_comm;

	////////////////////////
	// old
	//double dAddWeakHindrance = 0;
	//dAddWeakHindrance = 4*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistBetweenComm))*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistBetweenComm));

	double dAddWeakHindranceTwo = 0;
	double dCoeffK = 0;


	//dAddWeakHindranceTwo = 0;
	//dAddWeakHindranceTwo = 4*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistJammerComm1))*sin((2*M_PI*iCorrectHeightOwn*iCorrectHeightOpponent)/(dLambda*iDistJammerComm1));
	//dCoeffK = (iPowerOwn*iCoeffOwn*iDistBetweenComm*iDistBetweenComm*dAddWeakHindranceTwo*iWidthSignal*1000*dGamma)/
		//(iPowerOpponent*iCoeffTransmitOpponent*iDistJammerComm1*iDistJammerComm1*dAddWeakHindranceTwo*dWidthHindrance);


	// new
	double dCorrectHeightOppTransmit = 0;
    dCorrectHeightOppTransmit = Math.Sqrt(HeightTransmitOpponent_comm * HeightTransmitOpponent_comm + dCoeffHE_comm * dCoeffHE_comm);

	double dAddWeakHindrance = 0;
    dAddWeakHindrance = 4 * Math.Sin((2 * Math.PI * dCorrectHeightOppTransmit * Convert.ToDouble(iCorrectHeightOpponent_comm)) /
                      (dLambda*Convert.ToDouble(iDistBetweenComm)))*Math.Sin((2*Math.PI*dCorrectHeightOppTransmit*
                      Convert.ToDouble(iCorrectHeightOpponent_comm)) / (dLambda * Convert.ToDouble(iDistBetweenComm)));
	


		double dCoeffKConst = 0;

		double dVAr = 0;	
		double dVAr1 = 0;

        if (WidthSignal_comm >= WidthHindrance_comm)
			dVAr1 = 1;
		else
            dVAr1 = WidthSignal_comm / WidthHindrance_comm;

        dCoeffKConst = (PowerOwn_comm * CoeffOwn_comm * dGamma_comm * dVAr1) /
            (PowerOpponent_comm * CoeffTransmitOpponent_comm);

		dAddWeakHindranceTwo = 0;
        dAddWeakHindranceTwo = 4 * Math.Sin((2 * Math.PI * Convert.ToDouble(iCorrectHeightOwn_comm) * Convert.ToDouble(iCorrectHeightOpponent_comm)) /
            (dLambda * Convert.ToDouble(iDistJammerComm1))) * Math.Sin((2 * Math.PI * Convert.ToDouble(iCorrectHeightOwn_comm) *
            Convert.ToDouble(iCorrectHeightOpponent_comm)) / (dLambda * Convert.ToDouble(iDistJammerComm1)));

		dVAr = dAddWeakHindranceTwo/dAddWeakHindrance;

		double dCoeffK1 = 0;

        dCoeffK1 = (RangeComm_comm * RangeComm_comm) / (Convert.ToDouble(iDistJammerComm1 * iDistJammerComm1));
		dCoeffK = dCoeffK1*dCoeffKConst*dVAr;

////////////////////////

        if (dCoeffK >= CoeffSupOpponent_comm)
		  blResult = true;
		else
		  blResult = false;   

		tbHindSignal.Text = dCoeffK.ToString();

	return blResult;

}

private void tbRadiusZone_TextChanged(object sender, EventArgs e)
{

}

private void Form2_FormClosing(object sender, FormClosingEventArgs e)
{
    e.Cancel = true;
    Hide();

}

        // *************************************************************** FUNCTIONS


    } // Class
} // Namespace
