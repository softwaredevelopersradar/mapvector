﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrozaMap
{
    public partial class FormLayersSelection : Form
    {

        AxaxGisToolKit.AxaxcMapScreen axaxcMapScreen;
        bool start = false;

        public FormLayersSelection(ref AxaxGisToolKit.AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();
            axaxcMapScreen = axaxcMapScreen1;

            bool[] mas = new bool[18];
            axaxcMapScreen1.Selecting = true;
            for (int i = 0; i < 18; i++)
            {
                mas[i] = axaxcMapScreen1.ViewSelect.Layers_get(i);

                int listindex = 0;

                listindex = CalculateRightListNumber(i);

                if (listindex != -1)
                {
                    checkedListBox1.SetItemChecked(listindex,mas[i]);
                }

                checkedListBox1.SetItemChecked(7, CheckKeyobject7());
                checkedListBox1.SetItemChecked(13, CheckKeyobject13());
            }
            start = true;
        }

        
        private int CalculateRightListNumber(int index)
        {
            switch(index)
            {
                case 5:
                    return 0;
                    break;
                case 6:
                    return 1;
                    break;
                case 7:
                    return 2;
                    break;
                case 15:
                    return 3;
                    break;
                case 4:
                    return 4;
                    break;
                case 8:
                    return 5;
                    break;
                case 9:
                    return 6;
                    break;
                case 17:
                    return 8;
                    break;
                case 10:
                    return 9;
                    break;
                case 11:
                    return 10;
                    break;
                case 16:
                    return 11;
                    break;
                case 13:
                    return 12;
                    break;
                case 12:
                    return 14;
                    break;
                case 2:
                    return 15;
                    break;
                case 1:
                    return 16;
                    break;
                case 3:
                    return 17;
                    break;
                default:
                    break;
            }
            return -1;
        }


        private void SetLayers(int index, bool flag)
        {
            switch(index)
            {
                case 0:
                    axaxcMapScreen.ViewSelect.Layers_set(5, flag);
                    break;
                case 1:
                    axaxcMapScreen.ViewSelect.Layers_set(6, flag);
                    break;
                case 2:
                    axaxcMapScreen.ViewSelect.Layers_set(7, flag);
                    break;
                case 3:
                    axaxcMapScreen.ViewSelect.Layers_set(15, flag);
                    break;
                case 4:
                    axaxcMapScreen.ViewSelect.Layers_set(4, flag);
                    break;
                case 5:
                    axaxcMapScreen.ViewSelect.Layers_set(8, flag);
                    break;
                case 6:
                    axaxcMapScreen.ViewSelect.Layers_set(9, flag);
                    break;
                case 7:
                    axaxcMapScreen.ViewSelect.set_KeyObject("L0079310000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("P0079510000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("P0079713000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("P0079732000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("P0079757200", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("P0079757400", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("S0079759000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("V0079674000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("V0079751000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("V0079757120", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("V0079757300", flag);
                    break;
                case 8:
                    axaxcMapScreen.ViewSelect.Layers_set(17, flag);
                    break;
                case 9:
                    axaxcMapScreen.ViewSelect.Layers_set(10, flag);
                    break;
                case 10:
                    axaxcMapScreen.ViewSelect.Layers_set(11, flag);
                    break;
                case 11:
                    axaxcMapScreen.ViewSelect.Layers_set(16, flag);
                    break;
                case 12:
                    axaxcMapScreen.ViewSelect.Layers_set(13, flag);
                    break;
                case 13:
                    axaxcMapScreen.ViewSelect.set_KeyObject("L0032110000", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("L00321100001", flag);
                    axaxcMapScreen.ViewSelect.set_KeyObject("V0032110000", flag);
                    break;
                case 14:
                    axaxcMapScreen.ViewSelect.Layers_set(12, flag);
                    break;
                case 15:
                    axaxcMapScreen.ViewSelect.Layers_set(2, flag);
                    break;
                case 16:
                    axaxcMapScreen.ViewSelect.Layers_set(1, flag);
                    break;
                case 17:
                    axaxcMapScreen.ViewSelect.Layers_set(3, flag);
                    break;
            }
        }


        private bool CheckKeyobject7()
        {
            //Заполняющие знаки
            bool sign1 = axaxcMapScreen.ViewSelect.get_KeyObject("L0079310000");
            bool sign2 = axaxcMapScreen.ViewSelect.get_KeyObject("P0079510000");
            bool sign3 = axaxcMapScreen.ViewSelect.get_KeyObject("P0079713000");
            bool sign4= axaxcMapScreen.ViewSelect.get_KeyObject("P0079732000");
            bool sign5 = axaxcMapScreen.ViewSelect.get_KeyObject("P0079757200");
            bool sign6 = axaxcMapScreen.ViewSelect.get_KeyObject("P0079757400");
            bool sign7 = axaxcMapScreen.ViewSelect.get_KeyObject("S0079759000");
            bool sign8 = axaxcMapScreen.ViewSelect.get_KeyObject("V0079674000");
            bool sign9 = axaxcMapScreen.ViewSelect.get_KeyObject("V0079751000");
            bool sign10 = axaxcMapScreen.ViewSelect.get_KeyObject("V0079757120");
            bool sign11 = axaxcMapScreen.ViewSelect.get_KeyObject("V0079757300");

            //bool sign = (sign1 || sign2 || sign3 || sign4 || sign5 || sign6 || sign7 || sign8 || sign9 || sign10 || sign11);
            bool sign = (sign1 && sign2 && sign3 && sign4 && sign5 && sign6 && sign7 && sign8 && sign9 && sign10 && sign11);
            return sign;
        }

        private bool CheckKeyobject13()
        {
            //Плотины
            bool sign1 = axaxcMapScreen.ViewSelect.get_KeyObject("L0032110000");
            bool sign2 = axaxcMapScreen.ViewSelect.get_KeyObject("L00321100001");
            bool sign3 = axaxcMapScreen.ViewSelect.get_KeyObject("V0032110000");

            //bool sign = (sign1 || sign2 || sign3);
            bool sign = (sign1 && sign2 && sign3);
            return sign;

        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            /*
            var a = checkedListBox1.Items;
            var b = checkedListBox1.CheckedItems;

            Console.WriteLine("a="+ a.Count);
            Console.WriteLine("b=" + b.Count);
            */

            /*
            axaxcMapScreen.Selecting = true;

            bool Value = false;

            //axaxcMapScreen.ViewSelect.Layers_set(0, Value); //ваще всё, оставить
            //axaxcMapScreen.ViewSelect.Layers_set(1, Value); //зелёная трава оставить
            //axaxcMapScreen.ViewSelect.Layers_set(2, Value); //непонятно
            //axaxcMapScreen.ViewSelect.Layers_set(3, Value); //высотность рельефа оставить
            //axaxcMapScreen.ViewSelect.Layers_set(4, Value); //гидрография болота, непонятно
            //axaxcMapScreen.ViewSelect.Layers_set(5, Value); //гидрография оставить
            axaxcMapScreen.ViewSelect.Layers_set(6, Value); //маленькие палки может вода убрать
            axaxcMapScreen.ViewSelect.Layers_set(7, Value); //хрень убрать
            //axaxcMapScreen.ViewSelect.Layers_set(8, Value); //что-то нужное может рельефы оставить
            axaxcMapScreen.ViewSelect.Layers_set(9, Value); //что-то ересь и точки убрать
            axaxcMapScreen.ViewSelect.Layers_set(10, Value); //херь убрать
            //axaxcMapScreen.ViewSelect.Layers_set(11, Value); //города оставить
            //axaxcMapScreen.ViewSelect.Layers_set(12, Value); //железные дороги и точки оставить
            axaxcMapScreen.ViewSelect.Layers_set(13, Value); //много всяких точек убрать
            //axaxcMapScreen.ViewSelect.Layers_set(14, Value); //сетка оставить
            axaxcMapScreen.ViewSelect.Layers_set(15, Value); //белые дороги непонятно
            axaxcMapScreen.ViewSelect.Layers_set(16, Value); //хрень убрать
            axaxcMapScreen.ViewSelect.Layers_set(17, Value); //хрень убрать
            axaxcMapScreen.ViewSelect.Layers_set(18, Value); //непонятно что точно убрать
            axaxcMapScreen.ViewSelect.Layers_set(19, Value); //непонятно что точно убрать
             * 
             * */


            /*
            
            //Заполняющие знаки
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0079310000", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("P0079510000", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("P0079713000", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("P0079732000", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("P0079757200", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("P0079757400", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("S0079759000", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("V0079674000", false); 
            axaxcMapScreen1.ViewSelect.set_KeyObject("V0079751000", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("V0079757120", false); 
            axaxcMapScreen1.ViewSelect.set_KeyObject("V0079757300", false);
            //Плотины
            axaxcMapScreen1.ViewSelect.set_KeyObject("L0032110000", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("L00321100001", false);
            axaxcMapScreen1.ViewSelect.set_KeyObject("V0032110000", false);
             
             
             
             */



        }

      
        private void checkedListBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (start)
            {
                for (int i = 0; i < 18; i++)
                {
                    bool flag = checkedListBox1.GetItemChecked(i);
                    SetLayers(i, flag);
                }
                axaxcMapScreen.Repaint();
            }
        }








        private void FormLayersSelection_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

      

        
    }
}
