﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GrozaMap
{
    public partial class CheckPointForm : Form
    {
        private MapForm mapForm;

        public CheckPointForm(MapForm mapForm)
        {
            InitializeComponent();
            this.mapForm = mapForm;
        }

        public CheckPointForm(MapForm mapForm, AxaxGisToolKit.AxaxcMapScreen axaxcMapScreen)
        {
            InitializeComponent();
            this.mapForm = mapForm;
            this.Location = new Point(mapForm.Location.X + axaxcMapScreen.Right - this.Width + 5 , mapForm.Location.Y + axaxcMapScreen.Location.Y + 30);
        
        }

       public void FillTextBoxes(int hmap, double x, double y)
       {
           textBox1.Text = x.ToString();
           textBox2.Text = y.ToString();

           double planeToPicX = x;
           double planeToPicY = y;
           double planeToGeoX = x;
           double planeToGeoY = y;


           //mapPlaneToGeoWGS84(hmap, ref x42, ref y42);  // Перевод из прямоугольной системы в метрах
           //mapGeoWGS84ToPlane42(hmap, ref x42, ref y42);// в прямоугольную в метрах 42 года (как для бумажных карт)

           MapCore.mapPlaneToPicture(hmap, ref planeToPicX, ref planeToPicY);
           MapCore.mapPlaneToGeo(hmap, ref planeToGeoX, ref planeToGeoY);

           textBox3.Text = planeToPicX.ToString();
           textBox4.Text = planeToPicY.ToString();

           textBox5.Text = planeToGeoX.ToString();
           textBox6.Text = planeToGeoY.ToString();

           planeToGeoX = planeToGeoX * 180 / Math.PI;
           planeToGeoY = planeToGeoY * 180 / Math.PI;

           textBox7.Text = planeToGeoX.ToString();
           textBox8.Text = planeToGeoY.ToString();

       }

    }
}
