﻿using System;
using System.Drawing;
using AxaxGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

namespace GrozaMap
{
    public partial class FormTRO : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

      

        public FormTRO(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;



        } // Конструктор
        // ***********************************************************  Конструктор



        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormTRO_Load(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormTROG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFTRO = 1;

            // ....................................................................
            // TRO
            GlobalVarLn.blTRO_stat = true;
            GlobalVarLn.flEndTRO_stat = 1;
            // ....................................................................
            // SP

            // ----------------------------------------------------------------------
            GlobalVarLn.objFormSPG.gbRect.Visible = true;
            GlobalVarLn.objFormSPG.gbRect.Location = new Point(7, 30);
            GlobalVarLn.objFormSPG.gbRect42.Visible = false;
            GlobalVarLn.objFormSPG.gbRad.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMin.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMinSec.Visible = false;
            GlobalVarLn.objFormSPG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.objFormSPG.chbXY.Checked = false;
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.XCenter_SP = 0;
            GlobalVarLn.YCenter_SP = 0;
            GlobalVarLn.HCenter_SP = 0;
            GlobalVarLn.ClearListJS();
            // ....................................................................
            // LF1

            GlobalVarLn.objFormLFG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
            {
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.iLF1_stat = 0;
            GlobalVarLn.X_LF1 = 0;
            GlobalVarLn.Y_LF1 = 0;
            GlobalVarLn.H_LF1 = 0;
            GlobalVarLn.list_LF1.Clear();
            // ....................................................................
            // LF2

            GlobalVarLn.objFormLF2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
            {
                GlobalVarLn.objFormLF2G.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.iLF2_stat = 0;
            GlobalVarLn.X_LF2 = 0;
            GlobalVarLn.Y_LF2 = 0;
            GlobalVarLn.H_LF2 = 0;
            GlobalVarLn.list_LF2.Clear();
            // ....................................................................
            // ZO

            GlobalVarLn.objFormZOG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
            {
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.iZO_stat = 0;
            GlobalVarLn.X_ZO = 0;
            GlobalVarLn.Y_ZO = 0;
            GlobalVarLn.H_ZO = 0;
            GlobalVarLn.list_ZO.Clear();
            // ....................................................................
            // OB1

            GlobalVarLn.objFormOB1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
            {
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "","");
            }
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // ....................................................................
            // OB2

            GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "","");
            }
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            GlobalVarLn.X_OB2 = 0;
            GlobalVarLn.Y_OB2 = 0;
            GlobalVarLn.H_OB2 = 0;
            GlobalVarLn.list_OB2.Clear();
            GlobalVarLn.list1_OB2.Clear();
            // ....................................................................


            //ClassMap.f_RemoveFrm(7);


        } // Load_form
        // ************************************************************************

        // ************************************************************************
        // Очистка LF2
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {

            int fl = 0;


            GlobalVarLn.fFZagrTRO = 0;


            // 1509&&&
            //GlobalVarLn.blTRO_stat = true;
            //GlobalVarLn.flEndTRO_stat = 1;
            GlobalVarLn.blTRO_stat = false;
            GlobalVarLn.flEndTRO_stat = 0;

            // ....................................................................
            // SP

            GlobalVarLn.objFormSPG.tbXRect.Text = "";
            GlobalVarLn.objFormSPG.tbYRect.Text = "";
            GlobalVarLn.objFormSPG.tbXRect42.Text = "";
            GlobalVarLn.objFormSPG.tbYRect42.Text = "";
            GlobalVarLn.objFormSPG.tbBRad.Text = "";
            GlobalVarLn.objFormSPG.tbLRad.Text = "";
            GlobalVarLn.objFormSPG.tbBMin1.Text = "";
            GlobalVarLn.objFormSPG.tbLMin1.Text = "";
            GlobalVarLn.objFormSPG.tbBDeg2.Text = "";
            GlobalVarLn.objFormSPG.tbBMin2.Text = "";
            GlobalVarLn.objFormSPG.tbBSec.Text = "";
            GlobalVarLn.objFormSPG.tbLDeg2.Text = "";
            GlobalVarLn.objFormSPG.tbLMin2.Text = "";
            GlobalVarLn.objFormSPG.tbLSec.Text = "";
            GlobalVarLn.objFormSPG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSPG.tbNumSP.Text = "";
            GlobalVarLn.objFormSPG.chbXY.Checked = false;
            GlobalVarLn.objFormSPG.gbRect.Visible = true;
            GlobalVarLn.objFormSPG.gbRect.Location = new Point(7, 30);
            GlobalVarLn.objFormSPG.gbRect42.Visible = false;
            GlobalVarLn.objFormSPG.gbRad.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMin.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMinSec.Visible = false;
            GlobalVarLn.objFormSPG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.objFormSPG.chbXY.Checked = false;
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.XCenter_SP = 0;
            GlobalVarLn.YCenter_SP = 0;
            GlobalVarLn.HCenter_SP = 0;

            GlobalVarLn.ClearListJS();

            // ....................................................................

            // LF1

            while (GlobalVarLn.objFormLFG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Remove(GlobalVarLn.objFormLFG.dataGridView1.Rows[GlobalVarLn.objFormLFG.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormLFG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
            {
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.iLF1_stat = 0;
            GlobalVarLn.X_LF1 = 0;
            GlobalVarLn.Y_LF1 = 0;
            GlobalVarLn.H_LF1 = 0;
            GlobalVarLn.list_LF1.Clear();
            // ....................................................................
            // LF2

            while (GlobalVarLn.objFormLF2G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormLF2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormLF2G.dataGridView1.Rows[GlobalVarLn.objFormLF2G.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormLF2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
            {
                GlobalVarLn.objFormLF2G.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.iLF2_stat = 0;
            GlobalVarLn.X_LF2 = 0;
            GlobalVarLn.Y_LF2 = 0;
            GlobalVarLn.H_LF2 = 0;
            GlobalVarLn.list_LF2.Clear();
            // ....................................................................
            // ZO

            while (GlobalVarLn.objFormZOG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Remove(GlobalVarLn.objFormZOG.dataGridView1.Rows[GlobalVarLn.objFormZOG.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormZOG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
            {
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.iZO_stat = 0;
            GlobalVarLn.X_ZO = 0;
            GlobalVarLn.Y_ZO = 0;
            GlobalVarLn.H_ZO = 0;
            GlobalVarLn.list_ZO.Clear();
            // ....................................................................
            // OB1

            while (GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB1G.dataGridView1.Rows[GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormOB1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
            {
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "","");
            }
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // ....................................................................
            // OB2

            while (GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB2G.dataGridView1.Rows[GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "","");
            }
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            GlobalVarLn.X_OB2 = 0;
            GlobalVarLn.Y_OB2 = 0;
            GlobalVarLn.H_OB2 = 0;
            GlobalVarLn.list_OB2.Clear();
            GlobalVarLn.list1_OB2.Clear();
            // ....................................................................
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // -------------------------------------------------------------------

            // 1509&&&
            //GlobalVarLn.objFormTROG.WindowState = FormWindowState.Minimized;
            //GlobalVarLn.fFTRO = 0;

        } // Clear

        // ************************************************************************


        // ************************************************************************
        // Обработчик кнопки : read from file
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            int fl = 0;

            String strLine = "";
            String strLine1 = "";

            double number1 = 0;
            int number2 = 0;

            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';
            char symb6 = 'R';
            char symb7 = 'A';
            char symb8 = 'T';
            char symb9 = 'i';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;

            int IndZap = 0;
            int TekPoz = 0;

            //double x1 = 0;
            //double y1 = 0;
            //double x2 = 0;
            //double y2 = 0;
            //double dx = 0;
            //double dy = 0;
            //double li = 0;
            int fi = 0;


            GlobalVarLn.blTRO_stat = true;
            GlobalVarLn.flEndTRO_stat = 1;


            // SPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSP

            // Очистка ---------------------------------------------------------------------------

            GlobalVarLn.objFormSPG.tbXRect.Text = "";
            GlobalVarLn.objFormSPG.tbYRect.Text = "";
            GlobalVarLn.objFormSPG.tbXRect42.Text = "";
            GlobalVarLn.objFormSPG.tbYRect42.Text = "";
            GlobalVarLn.objFormSPG.tbBRad.Text = "";
            GlobalVarLn.objFormSPG.tbLRad.Text = "";
            GlobalVarLn.objFormSPG.tbBMin1.Text = "";
            GlobalVarLn.objFormSPG.tbLMin1.Text = "";
            GlobalVarLn.objFormSPG.tbBDeg2.Text = "";
            GlobalVarLn.objFormSPG.tbBMin2.Text = "";
            GlobalVarLn.objFormSPG.tbBSec.Text = "";
            GlobalVarLn.objFormSPG.tbLDeg2.Text = "";
            GlobalVarLn.objFormSPG.tbLMin2.Text = "";
            GlobalVarLn.objFormSPG.tbLSec.Text = "";
            GlobalVarLn.objFormSPG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSPG.chbXY.Checked = false;
            GlobalVarLn.objFormSPG.gbRect.Visible = true;
            GlobalVarLn.objFormSPG.gbRect.Location = new Point(7, 30);
            GlobalVarLn.objFormSPG.gbRect42.Visible = false;
            GlobalVarLn.objFormSPG.gbRad.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMin.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMinSec.Visible = false;
            GlobalVarLn.objFormSPG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.objFormSPG.chbXY.Checked = false;

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.XCenter_SP = 0;
            GlobalVarLn.YCenter_SP = 0;
            GlobalVarLn.HCenter_SP = 0;
            GlobalVarLn.ClearListJS();

            GlobalVarLn.LoadListJS();



            //GlobalVarLn.LoadListJS();
            // GlobalVarLn.objFormSPG.UpdateSPViews();
            //axaxcMapScreen.Repaint();
            //GlobalVarLn.flEndSP_stat = 1;
            //GlobalVarLn.objFormSPG.f_SPReDraw();
            //GlobalVarLn.fclSP = 0;




            // SPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSP

            // LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1
            // Очистка ---------------------------------------------------------------------------
            while (GlobalVarLn.objFormLFG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Remove(GlobalVarLn.objFormLFG.dataGridView1.Rows[GlobalVarLn.objFormLFG.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormLFG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
            {
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Add("", "", "", "");
            }
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.iLF1_stat = 0;
            GlobalVarLn.X_LF1 = 0;
            GlobalVarLn.Y_LF1 = 0;
            GlobalVarLn.H_LF1 = 0;
            GlobalVarLn.list_LF1.Clear();


            // --------------------------------------------------------------------------- Очистка

            // Чтение файла ---------------------------------------------------------
            var strFileName = "LF1.txt";
            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                //MessageBox.Show("Can’t open file (front line of own troops)");
                //return;
                goto LF1;
            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // N =...
            // X =...
            // Y =...
            // H =...

            try
            {
                if (srFile != null)
                {
                    LF objLF = new LF();

                    TekPoz = 0;
                    IndZap = 0;
                    // .......................................................
                    // N =...
                    // 1-я строка

                    strLine = srFile.ReadLine();

                    if (strLine == null)
                    {
                        //MessageBox.Show("No information (front line of own troops)");
                        //return;
                        srFile.Close();
                        goto LF1;
                    }

                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart == -1)
                    {
                        MessageBox.Show("No information (front line of own troops)");
                        return;
                    }

                    indStop = strLine.IndexOf(symb4, TekPoz);  //=

                    if ((indStop == -1) || (indStop < indStart))
                    {
                        MessageBox.Show("No information (front line of own troops)");
                        return;
                    }

                    iLength = indStop - indStart + 1;
                    // Убираем 'NSP ='
                    strLine1 = strLine.Remove(indStart, iLength);

                    if (strLine1 == "")
                    {
                        MessageBox.Show("No information (front line of own troops)");
                        return;
                    }

                    // Количество 
                    number2 = Convert.ToInt32(strLine1);
                    GlobalVarLn.iLF1_stat = (uint)number2;
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (X1)
                    if ((strLine == "") || (strLine == null))
                    {
                        fi = 1;
                    }
                    // .......................................................

                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                    while ((strLine != "") && (strLine != null))
                    {
                        IndZap += 1;

                        // .......................................................
                        // X =...

                        indStart = strLine.IndexOf(symb2, TekPoz); // X
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);
                        // number1 = Convert.ToDouble(strLine1);
                        //number2 = (int)number1;

                        objLF.X_m = number1;
                        // .......................................................
                        // Y =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb3, TekPoz); // Y
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.Y_m = number1;
                        // .......................................................
                        // H =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb5, TekPoz); // H
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.H_m = number1;
                        // .......................................................
                        // .......................................................
                        // Занести в List

                        GlobalVarLn.list_LF1.Add(objLF);

                        // .......................................................
                        // Занести в таблицу

                        //GlobalVarLn.objFormLFG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = objLF.X_m;  // X
                        //GlobalVarLn.objFormLFG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = objLF.Y_m;  // Y
                        //GlobalVarLn.objFormLFG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H

                        var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

                        GlobalVarLn.objFormLFG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = p.X.ToString("F3"); // X
                        GlobalVarLn.objFormLFG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = p.Y.ToString("F3"); // Y
                        GlobalVarLn.objFormLFG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m; // H


                        // ............................................................

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Xi)
                        if ((strLine == "") || (strLine == null))
                            fi = 1;
                        // ...................................................................

                    } // WHILE
                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                }
            }
            // -----------------------------------------------------------------------------------
            catch
            {

            }
            // -------------------------------------------------------------------------------------
            if (srFile != null)
                srFile.Close();
        // -------------------------------------------------------------------------------------
        // LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1LF1

            // LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2

            // Очистка ---------------------------------------------------------------------------
        LF1: GlobalVarLn.objFormLF2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
            {
                GlobalVarLn.objFormLF2G.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.iLF2_stat = 0;
            GlobalVarLn.X_LF2 = 0;
            GlobalVarLn.Y_LF2 = 0;
            GlobalVarLn.H_LF2 = 0;
            GlobalVarLn.list_LF2.Clear();
            // --------------------------------------------------------------------------- Очистка

            // Чтение файла ---------------------------------------------------------
            strFileName = "LF2.txt";

            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                //    MessageBox.Show("Can’t open file (front line of the enemy)");
                //    return;
                goto ZO;
            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // N =...
            // X =...
            // Y =...
            // H =...

            try
            {
                if (srFile != null)
                {
                    LF objLF = new LF();

                    TekPoz = 0;
                    IndZap = 0;
                    // .......................................................
                    // N =...
                    // 1-я строка

                    strLine = srFile.ReadLine();

                    if (strLine == null)
                        if (strLine == null)
                        {
                            //MessageBox.Show("No information (front line of own troops)");
                            //return;
                            srFile.Close();
                            goto ZO;
                        }

                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart == -1)
                    {
                        MessageBox.Show("No information (front line of the enemy)");
                        return;
                    }

                    indStop = strLine.IndexOf(symb4, TekPoz);  //=

                    if ((indStop == -1) || (indStop < indStart))
                    {
                        MessageBox.Show("No information (front line of the enemy)");
                        return;
                    }

                    iLength = indStop - indStart + 1;
                    // Убираем 'N ='
                    strLine1 = strLine.Remove(indStart, iLength);

                    if (strLine1 == "")
                    {
                        MessageBox.Show("No information (front line of the enemy)");
                        return;
                    }

                    // Количество 
                    number2 = Convert.ToInt32(strLine1);
                    GlobalVarLn.iLF2_stat = (uint)number2;
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (X1)
                    if ((strLine == "") || (strLine == null))
                    {
                        fi = 1;
                    }
                    // .......................................................

                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                    while ((strLine != "") && (strLine != null))
                    {
                        IndZap += 1;

                        // .......................................................
                        // X =...

                        indStart = strLine.IndexOf(symb2, TekPoz); // X
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.X_m = number1;
                        // .......................................................
                        // Y =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb3, TekPoz); // Y
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.Y_m = number1;
                        // .......................................................
                        // H =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb5, TekPoz); // H
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.H_m = number1;
                        // .......................................................
                        // Занести в List

                        GlobalVarLn.list_LF2.Add(objLF);
                        // .......................................................
                        // Занести в таблицу

                        //GlobalVarLn.objFormLF2G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = objLF.X_m;  // X
                        //GlobalVarLn.objFormLF2G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = objLF.Y_m;  // Y
                        //GlobalVarLn.objFormLF2G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H

                        var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

                        GlobalVarLn.objFormLF2G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = p.X.ToString("F3"); // X
                        GlobalVarLn.objFormLF2G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = p.Y.ToString("F3"); // Y
                        GlobalVarLn.objFormLF2G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m; // H

                        // ............................................................

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Xi)
                        if ((strLine == "") || (strLine == null))
                            fi = 1;
                        // ...................................................................

                    } // WHILE
                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                }
            }
            // -----------------------------------------------------------------------------------
            catch
            {

            }
            // -------------------------------------------------------------------------------------
            if (srFile != null)
                srFile.Close();
        // -------------------------------------------------------------------------------------
        // LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2LF2

            // ZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZO

            // Очистка ---------------------------------------------------------------------------

ZO: while (GlobalVarLn.objFormZOG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Remove(GlobalVarLn.objFormZOG.dataGridView1.Rows[GlobalVarLn.objFormZOG.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormZOG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
            {
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.iZO_stat = 0;
            GlobalVarLn.X_ZO = 0;
            GlobalVarLn.Y_ZO = 0;
            GlobalVarLn.H_ZO = 0;
            GlobalVarLn.list_ZO.Clear();
            // --------------------------------------------------------------------------- Очистка

            // Чтение файла ---------------------------------------------------------
            strFileName = "ZO.txt";

            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                //    MessageBox.Show("Can’t open file (zone of responsibility)");
                //    return;
                goto OB1;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // N =...
            // X =...
            // Y =...
            // H =...

            try
            {
                if (srFile != null)
                {
                    LF objLF = new LF();

                    TekPoz = 0;
                    IndZap = 0;
                    // .......................................................
                    // N =...
                    // 1-я строка

                    strLine = srFile.ReadLine();

                    if (strLine == null)
                        if (strLine == null)
                        {
                            srFile.Close();
                            goto OB1;
                        }

                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart == -1)
                    {
                        MessageBox.Show("No information (zone of responsibility)");
                        return;
                    }

                    indStop = strLine.IndexOf(symb4, TekPoz);  //=

                    if ((indStop == -1) || (indStop < indStart))
                    {
                        MessageBox.Show("No information (zone of responsibility)");
                        return;
                    }

                    iLength = indStop - indStart + 1;
                    // Убираем 'N ='
                    strLine1 = strLine.Remove(indStart, iLength);

                    if (strLine1 == "")
                    {
                        MessageBox.Show("No information (zone of responsibility)");
                        return;
                    }

                    // Количество 
                    number2 = Convert.ToInt32(strLine1);
                    GlobalVarLn.iZO_stat = (uint)number2;
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (X1)
                    if ((strLine == "") || (strLine == null))
                    {
                        fi = 1;
                    }
                    // .......................................................

                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                    while ((strLine != "") && (strLine != null))
                    {
                        IndZap += 1;

                        // .......................................................
                        // X =...

                        indStart = strLine.IndexOf(symb2, TekPoz); // X
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.X_m = number1;
                        // .......................................................
                        // Y =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb3, TekPoz); // Y
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.Y_m = number1;
                        // .......................................................
                        // H =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb5, TekPoz); // H
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.H_m = number1;
                        // .......................................................
                        // Занести в List

                        GlobalVarLn.list_ZO.Add(objLF);
                        // .......................................................
                        // Занести в таблицу

                        GlobalVarLn.objFormZOG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = objLF.X_m;  // X
                        GlobalVarLn.objFormZOG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = objLF.Y_m;  // Y
                        GlobalVarLn.objFormZOG.dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H
                        // ............................................................

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Xi)
                        if ((strLine == "") || (strLine == null))
                            fi = 1;
                        // ...................................................................

                    } // WHILE
                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                }
            }
            // -----------------------------------------------------------------------------------
            catch
            {

            }
            // -------------------------------------------------------------------------------------
            if (srFile != null)
                srFile.Close();
        // -------------------------------------------------------------------------------------
        // ZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZOZO

            // OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1

            // Очистка ---------------------------------------------------------------------------

OB1:



            while (GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB1G.dataGridView1.Rows[GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormOB1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
            {
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "", "");
            }

            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // --------------------------------------------------------------------------- Очистка

            // Чтение файла ---------------------------------------------------------
            strFileName = "OB1.txt";

            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                //    MessageBox.Show("Can’t open file (objects of own troops)");
                //    return;
                goto OB2;
            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // N =...
            // X =...
            // Y =...
            // H =...

            try
            {
                if (srFile != null)
                {
                    LF1 objLF = new LF1();

                    TekPoz = 0;
                    IndZap = 0;
                    // .......................................................
                    // N =...
                    // 1-я строка

                    strLine = srFile.ReadLine();

                    if (strLine == null)
                        if (strLine == null)
                        {
                            srFile.Close();
                            goto OB2;
                        }

                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart == -1)
                    {
                        MessageBox.Show("No information (objects of own troops)");
                        return;
                    }

                    indStop = strLine.IndexOf(symb4, TekPoz);  //=

                    if ((indStop == -1) || (indStop < indStart))
                    {
                        MessageBox.Show("No information (objects of own troops)");
                        return;
                    }

                    iLength = indStop - indStart + 1;
                    // Убираем 'N ='
                    strLine1 = strLine.Remove(indStart, iLength);

                    if (strLine1 == "")
                    {
                        MessageBox.Show("No information (objects of own troops)");
                        return;
                    }

                    // Количество 
                    number2 = Convert.ToInt32(strLine1);
                    GlobalVarLn.iOB1_stat = (uint)number2;
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (X1)
                    if ((strLine == "") || (strLine == null))
                    {
                        fi = 1;
                    }
                    // .......................................................

                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                    while ((strLine != "") && (strLine != null))
                    {
                        IndZap += 1;

                        // .......................................................
                        // X =...

                        indStart = strLine.IndexOf(symb2, TekPoz); // X
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.X_m = number1;
                        // .......................................................
                        // Y =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb3, TekPoz); // Y
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.Y_m = number1;
                        // .......................................................
                        // H =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb5, TekPoz); // H
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.H_m = number1;
                        // .......................................................
                        // Type =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb8, TekPoz); // T
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);

                        objLF.sType = strLine1;
                        // .......................................................
                        // indzn =...

                        strLine = srFile.ReadLine();

                        indStart = strLine.IndexOf(symb9, TekPoz); // i
                        indStop = strLine.IndexOf(symb4, TekPoz);  // =
                        iLength = indStop - indStart + 1;
                        strLine1 = strLine.Remove(indStart, iLength);
                        number1 = Convert.ToInt32(strLine1);

                        objLF.indzn = (int)number1;
                        // .......................................................
                        // Занести в List

                        GlobalVarLn.list1_OB1.Add(objLF);
                        // .......................................................
                        // Занести в таблицу

                        //GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = objLF.X_m;  // X
                        //GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = objLF.Y_m;  // Y
                        //GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H
                        //GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[3].Value = objLF.sType;  // Type




                        // Занести в таблицу
                        var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

                        GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = p.X.ToString("F3");  // X
                        GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = p.Y.ToString("F3");  // Y
                        GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H
                        GlobalVarLn.objFormOB1G.dataGridView1.Rows[(int)(IndZap - 1)].Cells[3].Value = objLF.sType;  // Type

                        // ............................................................

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Xi)
                        if ((strLine == "") || (strLine == null))
                            fi = 1;
                        // ...................................................................

                    } // WHILE
                    // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                }
            }
            // -----------------------------------------------------------------------------------
            catch
            {

            }
            // -------------------------------------------------------------------------------------
            if (srFile != null)
                srFile.Close();
        // -------------------------------------------------------------------------------------

            // OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1OB1

            // OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2

            // Очистка ---------------------------------------------------------------------------
        OB2: while (GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB2G.dataGridView1.Rows[GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count - 1]);
            GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "", "");
            }
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            GlobalVarLn.X_OB2 = 0;
            GlobalVarLn.Y_OB2 = 0;
            GlobalVarLn.H_OB2 = 0;
            GlobalVarLn.list_OB2.Clear();
            GlobalVarLn.list1_OB2.Clear();
            // --------------------------------------------------------------------------- Очистка

            GlobalVarLn.LoadListOB2();


            // NNNNN
            //UpdateDataGridView();

            // -------------------------------------------------------------------------------------

            // OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2OB2

            // DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAW
            // Убрать с карты
            GlobalVarLn.axMapScreenGlobal.Repaint();

            GlobalVarLn.objFormSPG.f_SPReDraw();
            GlobalVarLn.objFormLFG.f_LF1ReDraw();
            GlobalVarLn.objFormLF2G.f_LF2ReDraw();
            GlobalVarLn.objFormZOG.f_ZOReDraw();
            GlobalVarLn.objFormOB1G.f_OB1ReDraw();
            GlobalVarLn.objFormOB2G.f_OB2ReDraw();

            // DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAW



        } // Read from files
        // ************************************************************************

        // ФУНКЦИИ ********************************************************************************



        private void UpdateDataGridView()
        {
/*
            // updating rows count
            if (dataGridView1.Rows.Count < GlobalVarLn.list1_OB2.Count)
            {
                dataGridView1.Rows.Add(GlobalVarLn.list1_OB2.Count - dataGridView1.Rows.Count);
            }
            while (dataGridView1.Rows.Count > GlobalVarLn.list1_OB2.Count)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count - 1);
            }

*/
            GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add(GlobalVarLn.list1_OB2.Count);

            for (var i = 0; i < GlobalVarLn.list1_OB2.Count; ++i)
            {
                var ob = GlobalVarLn.list1_OB2[i];
                var row = GlobalVarLn.objFormOB2G.dataGridView1.Rows[i];
                var p = axaxcMapScreen.MapPlaneToRealGeo(ob.X_m, ob.Y_m);

                row.Cells[0].Value = p.X.ToString("F3");
                row.Cells[1].Value = p.Y.ToString("F3");
                row.Cells[2].Value = ob.H_m;
                row.Cells[3].Value = ob.sType;
                row.Cells[4].Value = ob.FrequencyMhz.ToString("F3");
            }
        }

        // ******************************************************************************** ФУНКЦИИ


        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormTRO_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            GlobalVarLn.fl_Open_objFormTRO = 0;

            // 1509
           //GlobalVarLn.fFTRO = 0;

            // приостановить обработку, если идет
            GlobalVarLn.blTRO_stat = false;
            GlobalVarLn.blSP_stat = false;
            GlobalVarLn.blLF1_stat = false;
            GlobalVarLn.blLF2_stat = false;
            GlobalVarLn.blZO_stat = false;
            GlobalVarLn.blOB1_stat = false;
            GlobalVarLn.blOB2_stat = false;


        } // Closing

        // ****************************************************************************************

        // *************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormTRO_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormTROG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFTRO = 1;

            GlobalVarLn.fl_Open_objFormTRO = 1;

                GlobalVarLn.blTRO_stat = true;
                GlobalVarLn.flEndTRO_stat = 1;
                GlobalVarLn.blSP_stat = true;
                GlobalVarLn.flEndSP_stat = 1;
                GlobalVarLn.blLF1_stat = true;
                GlobalVarLn.flEndLF1_stat = 1;
                GlobalVarLn.blLF2_stat = true;
                GlobalVarLn.flEndLF2_stat = 1;
                GlobalVarLn.blZO_stat = true;
                GlobalVarLn.flEndZO_stat = 1;
                GlobalVarLn.blOB1_stat = true;
                GlobalVarLn.flEndOB1_stat = 1;
                GlobalVarLn.blOB2_stat = true;
                GlobalVarLn.flEndOB2_stat = 1;

                //ClassMap.f_RemoveFrm(7);

        } // Activated

        // *************************************************************************************


    } // Class
} // NameSpace
