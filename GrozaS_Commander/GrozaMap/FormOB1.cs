﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AxaxGisToolKit;
using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

namespace GrozaMap
{
    public partial class FormOB1 : Form
    {
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);

        private AxaxcMapScreen axaxcMapScreen;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        /*
                private double dchislo;
                private long ichislo;
                private double LAMBDA;

                // .....................................................................
                // Координаты центра ЗПВ

                // Координаты центра ЗПВ на местности в м
                private double XSP_comm;
                private double YSP_comm;

                // DATUM
                private double dXdat_comm;
                private double dYdat_comm;
                private double dZdat_comm;

                private double dLat_comm;
                private double dLong_comm;

                // Эллипсоид Красовского, град
                private double LatKrG_comm;
                private double LongKrG_comm;
                // Эллипсоид Красовского, rad
                private double LatKrR_comm;
                private double LongKrR_comm;
                // Эллипсоид Красовского, град,мин,сек
                private int Lat_Grad_comm;
                private int Lat_Min_comm;
                private double Lat_Sec_comm;
                private int Long_Grad_comm;
                private int Long_Min_comm;
                private double Long_Sec_comm;
                // Гаусс-крюгер(СК42) м
                private double XSP42_comm;
                private double YSP42_comm;

                // Эллипсоид Красовского, град
                private double LatKrG_YS1_comm;
                private double LongKrG_YS1_comm;
                // Эллипсоид Красовского, rad
                private double LatKrR_YS1_comm;
                private double LongKrR_YS1_comm;
                // Эллипсоид Красовского, град,мин,сек
                private int Lat_Grad_YS1_comm;
                private int Lat_Min_YS1_comm;
                private double Lat_Sec_YS1_comm;
                private int Long_Grad_YS1_comm;
                private int Long_Min_YS1_comm;
                private double Long_Sec_YS1_comm;
                // Гаусс-крюгер(СК42) м
                private double XYS142_comm;
                private double YYS142_comm;

                // Эллипсоид Красовского, град
                private double LatKrG_YS2_comm;
                private double LongKrG_YS2_comm;
                // Эллипсоид Красовского, rad
                private double LatKrR_YS2_comm;
                private double LongKrR_YS2_comm;
                // Эллипсоид Красовского, град,мин,сек
                private int Lat_Grad_YS2_comm;
                private int Lat_Min_YS2_comm;
                private double Lat_Sec_YS2_comm;
                private int Long_Grad_YS2_comm;
                private int Long_Min_YS2_comm;
                private double Long_Sec_YS2_comm;
                // Гаусс-крюгер(СК42) м
                private double XYS242_comm;
                private double YYS242_comm;

                // ......................................................................
                // Основные параметры

                private double OwnHeight_comm;
                private double Point1Height_comm;
                private double Point2Height_comm;
                private double HeightOwnObject_comm;
                private double PowerOwn_comm;
                private double CoeffOwn_comm;
                private double RadiusZone_comm;
                private double MaxDist_comm;

                private int i_HeightOwnObject_comm;
                private int i_Cap1_comm;
                private int i_WidthHindrance_comm;
                private int i_Surface_comm;
                private double Cap1_comm;
                private double WidthHindrance_comm;
                private double Surface_comm;

                // Высота средства подавления
                private double HeightAntennOwn_comm;
                private double HeightTotalOwn_comm;

                // объект подавления
                private int i_HeightOpponent_comm;
                private double HeightOpponent_comm;
                // Высота антенны противника
                private int iOpponAnten_comm;

                // Для подавляемой линии
                private double Freq_comm;
                private double PowerOpponent_comm;
                private double CoeffTransmitOpponent_comm;
                private double CoeffReceiverOpponent_comm;
                private double RangeComm_comm;
                private double WidthSignal_comm;
                private double HeightTransmitOpponent_comm;
                private double HeightReceiverOpponent_comm;
                private double CoeffSupOpponent_comm;
                private int i_PolarOpponent_comm;
                private int i_CoeffSupOpponent_comm;
                private int i_TypeCommOpponent_comm;

                // ......................................................................
                // Зона


                private double dCoeffQ_comm;
                private double dCoeffHE_comm;
                private int iCorrectHeightOwn_comm;
                private int iResultHeightOwn_comm;
                private int iMiddleHeight_comm;
                private int iMinHeight_comm;
                private int iCorrectHeightOpponent_comm;
                private int iResultHeightOpponent_comm;
                private long iMaxDistance_comm;
                private double dGamma_comm;
                private long liRadiusZone_comm;

                // ДПВ
                private int iDSR;

                //public static AirPlane[] mass_stAirPlane = new AirPlane[1000];
                private Point[] tpPointDSR;
                private Point[] tpPointPictDSR;

                // ......................................................................
        */
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormOB1(ref AxaxcMapScreen axaxcMapScreen1)
        {
            InitializeComponent();

            axaxcMapScreen = axaxcMapScreen1;

            /*
                        dchislo = 0;
                        ichislo = 0;
                        LAMBDA = 300000;

                        // .....................................................................
                        // Координаты центра ЗПВ

                        // Координаты центра ЗПВ на местности в м
                        XSP_comm = 0;
                        YSP_comm = 0;

                        // DATUM
                        // ГОСТ 51794_2008
                        dXdat_comm = 25;
                        dYdat_comm = -141;
                        dZdat_comm = -80;

                        dLat_comm = 0;
                        dLong_comm = 0;

                        // Эллипсоид Красовского, град
                        LatKrG_comm = 0;
                        LongKrG_comm = 0;
                        // Эллипсоид Красовского, rad
                        LatKrR_comm = 0;
                        LongKrR_comm = 0;
                        // Эллипсоид Красовского, град,мин,сек
                        Lat_Grad_comm = 0;
                        Lat_Min_comm = 0;
                        Lat_Sec_comm = 0;
                        Long_Grad_comm = 0;
                        Long_Min_comm = 0;
                        Long_Sec_comm = 0;
                        // Гаусс-крюгер(СК42) м
                        XSP42_comm = 0;
                        YSP42_comm = 0;

                        // Эллипсоид Красовского, град
                        LatKrG_YS1_comm = 0;
                        LongKrG_YS1_comm = 0;
                        // Эллипсоид Красовского, rad
                        LatKrR_YS1_comm = 0;
                        LongKrR_YS1_comm = 0;
                        // Эллипсоид Красовского, град,мин,сек
                        Lat_Grad_YS1_comm = 0;
                        Lat_Min_YS1_comm = 0;
                        Lat_Sec_YS1_comm = 0;
                        Long_Grad_YS1_comm = 0;
                        Long_Min_YS1_comm = 0;
                        Long_Sec_YS1_comm = 0;
                        // Гаусс-крюгер(СК42) м
                        XYS142_comm = 0;
                        YYS142_comm = 0;

                        // Эллипсоид Красовского, град
                        LatKrG_YS2_comm = 0;
                        LongKrG_YS2_comm = 0;
                        // Эллипсоид Красовского, rad
                        LatKrR_YS2_comm = 0;
                        LongKrR_YS2_comm = 0;
                        // Эллипсоид Красовского, град,мин,сек
                        Lat_Grad_YS2_comm = 0;
                        Lat_Min_YS2_comm = 0;
                        Lat_Sec_YS2_comm = 0;
                        Long_Grad_YS2_comm = 0;
                        Long_Min_YS2_comm = 0;
                        Long_Sec_YS2_comm = 0;
                        // Гаусс-крюгер(СК42) м
                        XYS242_comm = 0;
                        YYS242_comm = 0;

                        // ......................................................................
                        // Основные параметры

                        OwnHeight_comm = 0;
                        Point1Height_comm = 0;
                        Point2Height_comm = 0;
                        HeightOwnObject_comm = 0;
                        PowerOwn_comm = 0;
                        CoeffOwn_comm = 0;
                        RadiusZone_comm = 0;
                        MaxDist_comm = 0;

                        i_HeightOwnObject_comm = 0;
                        i_Cap1_comm = 0;
                        i_WidthHindrance_comm = 0;
                        i_Surface_comm = 0;
                        Cap1_comm = 0;
                        WidthHindrance_comm = 0;
                        Surface_comm = 0;

                        // Высота средства подавления
                        // ??????????????????????
                        HeightAntennOwn_comm = 0; // антенна
                        HeightTotalOwn_comm = 0;

                        // объект подавления
                        i_HeightOpponent_comm = 0;
                        HeightOpponent_comm = 0;
                        // Высота антенны противника
                        iOpponAnten_comm = 0;

                        // Для подавляемой линии
                        Freq_comm = 0;
                        PowerOpponent_comm = 0;
                        CoeffTransmitOpponent_comm = 0;
                        CoeffReceiverOpponent_comm = 0;
                        RangeComm_comm = 0;
                        WidthSignal_comm = 0;
                        HeightTransmitOpponent_comm = 0;
                        HeightReceiverOpponent_comm = 0;
                        CoeffSupOpponent_comm = 0;
                        i_PolarOpponent_comm = 0;
                        i_CoeffSupOpponent_comm = 0;
                        i_TypeCommOpponent_comm = 0;

                        // ......................................................................
                        // Зона

                        dCoeffQ_comm = 0;
                        dCoeffHE_comm = 0;
                        iCorrectHeightOwn_comm = 0;
                        iResultHeightOwn_comm = 0;
                        iMiddleHeight_comm = 0;
                        iMinHeight_comm = 0;
                        iCorrectHeightOpponent_comm = 0;
                        iResultHeightOpponent_comm = 0;
                        iMaxDistance_comm = 0;
                        dGamma_comm = 0;
                        liRadiusZone_comm = 0;

                        // ДПВ
                        iDSR = 0;

                        // ......................................................................
            */

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormOB1_Load(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormOB1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB1 = 1;


            // 1509&
            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // .....................................................................................
                // Очистка dataGridView

                //dataGridView1.ClearSelection();
                //for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
                //{
                //    dataGridView1.Rows.Add("", "", "", "");
                //}
                // -------------------------------------------------------------------
                // Очистка dataGridView1+ Установка 100 строк
                // 09_10_2018

                // Очистка dataGridView1
                while (dataGridView1.Rows.Count != 0)
                    dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

                for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
                {
                    dataGridView1.Rows.Add("", "", "", "");
                }
                // -------------------------------------------------------------------
                // .....................................................................................
                // Флаги

                GlobalVarLn.blOB1_stat = true;
                GlobalVarLn.flEndOB1_stat = 1;
                // .....................................................................................
                GlobalVarLn.iOB1_stat = 0;
                GlobalVarLn.X_OB1 = 0;
                GlobalVarLn.Y_OB1 = 0;
                GlobalVarLn.H_OB1 = 0;
                GlobalVarLn.list_OB1.Clear();
                GlobalVarLn.list1_OB1.Clear();
                // .....................................................................................
                GlobalVarLn.iZOB1 = 0;
                pbOB1.Image = imageList1.Images[0];

            }


                // 0809_3
                //ClassMap.f_RemoveFrm(2);



/*
            imageList1.Images.Clear();
            String nn;
            String nn1;

            try
            {
                String s;
                s = Application.StartupPath + "\\Images\\OwnObject\\OwnObject\\";
                DirectoryInfo di = new DirectoryInfo(@s);

                foreach (var fi in di.GetFiles("*.png"))
                {

                    nn = fi.Name;
                    nn1 = fi.DirectoryName +"\\" +fi.Name;
                    imageList1.Images.Add(Image.FromFile(nn1));

                }
            }
            catch 
            {
                MessageBox.Show("xxx");
            }
*/


        } // Load_form
        // ************************************************************************

        // ************************************************************************
        // Очистка OB1
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            // -------------------------------------------------------------------
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // -------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк
            // 09_10_2018

            // Очистка dataGridView1
            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------


            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // -------------------------------------------------------------------

        } // Clear

        // ************************************************************************

        // ************************************************************************
        // Обработчик кнопки : сохранить
        // ************************************************************************

        // 09_10_2018
        private void bAccept_Click(object sender, EventArgs e)
        {


/*
            // OLD

            int i_tmp = 0;
            // -----------------------------------------------------------------------------------------
            String strFileName;
            strFileName = "OB1.txt";
            StreamWriter srFile;

            //StreamWriter srFile = new StreamWriter(strFileName);
            try
            {
                srFile = new StreamWriter(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t save file");
                return;
            }


            LF1 objLF = new LF1();

            // -----------------------------------------------------------------------------------------
            srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.iOB1_stat));

            for (i_tmp = 0; i_tmp < GlobalVarLn.iOB1_stat; i_tmp++)
            {

               //  Переписать содержимое таблицы в List
                //objLF.X_m = GlobalVarLn.list1_OB1[i_tmp].X_m;
                if ((dataGridView1.Rows[i_tmp].Cells[0].Value == "") ||
                    (dataGridView1.Rows[i_tmp].Cells[0].Value == null))
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }
                try
                {
                    // 01_10_2018
                    //objLF.X_m = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[0].Value);
                    objLF.X_m = GlobalVarLn.list1_OB1[i_tmp].X_m;

                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }


                //objLF.Y_m = GlobalVarLn.list1_OB1[i_tmp].Y_m;
                if ((dataGridView1.Rows[i_tmp].Cells[1].Value == "") ||
                    (dataGridView1.Rows[i_tmp].Cells[1].Value == null))
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }
                try
                {
                    // 01102018
                    //objLF.Y_m = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[1].Value);
                    objLF.Y_m = GlobalVarLn.list1_OB1[i_tmp].Y_m;

                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }


                //objLF.H_m = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[2].Value);
                if ((dataGridView1.Rows[i_tmp].Cells[2].Value == "") ||
                    (dataGridView1.Rows[i_tmp].Cells[2].Value == null))
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }
                try
                {
                    objLF.H_m = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[2].Value);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }


                objLF.sType = Convert.ToString(dataGridView1.Rows[i_tmp].Cells[3].Value);
                objLF.indzn = GlobalVarLn.list1_OB1[i_tmp].indzn;

                GlobalVarLn.list1_OB1.RemoveAt(i_tmp);
                GlobalVarLn.list1_OB1.Insert(i_tmp,objLF);


                srFile.WriteLine("X =" + Convert.ToString((int) GlobalVarLn.list1_OB1[i_tmp].X_m));
                srFile.WriteLine("Y =" + Convert.ToString((int) GlobalVarLn.list1_OB1[i_tmp].Y_m));
                srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].H_m));
                srFile.WriteLine("Type =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].sType));
                srFile.WriteLine("indzn =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].indzn));

            }
            // -------------------------------------------------------------------------------------

            srFile.Close();
            // ------------------------------------------------------------------------------------
            axaxcMapScreen.Repaint();
            f_OB1ReDraw();

            // ------------------------------------------------------------------------------------
*/

            // 09_10_2018
            // ---------------------------------------------------------------------
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.list1_OB1.Clear();
            // -----------------------------------------------------------------------------------------
            int i_tmp = 0;
            // -----------------------------------------------------------------------------------------
            String strFileName;
            strFileName = "OB1.txt";
            StreamWriter srFile;
            // -----------------------------------------------------------------------------------------
            try
            {
                srFile = new StreamWriter(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t save file");
                return;
            }

            LF1 objLF = new LF1();
            // -----------------------------------------------------------------------------------------
            int ir = 0;
            int irf = 0;
            double lt = 0;
            double lng = 0;
            double freq = 0;
            String s = "";
            // -----------------------------------------------------------------------------------------

            // FOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            for (ir = 0; ir < dataGridView1.Rows.Count; ir++)
            {
                // IF**
                if (
                    ((dataGridView1.Rows[ir].Cells[0].Value != null) && (dataGridView1.Rows[ir].Cells[0].Value != "")) &&
                    ((dataGridView1.Rows[ir].Cells[1].Value != null) && (dataGridView1.Rows[ir].Cells[1].Value != ""))
                   )
                {
                    // -----------------------------------------------------------------
                    //  Latitude из таблицы (WGS84)

                    // IF1
                    if ((dataGridView1.Rows[ir].Cells[0].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[0].Value != null))
                    {
                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[0].Value);

                        try
                        {
                            lt = Convert.ToDouble(s);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                lt = Convert.ToDouble(s);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        } // catch

                    } // IF1
                    // -----------------------------------------------------------------
                    //  Longitude из таблицы (WGS84)

                    // IF2
                    if ((dataGridView1.Rows[ir].Cells[1].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[1].Value != null))
                    {
                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[1].Value);

                        try
                        {
                            lng = Convert.ToDouble(s);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                lng = Convert.ToDouble(s);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        } // catch

                    } // IF2
                    // -----------------------------------------------------------------
                    // Имя

                    s = Convert.ToString(dataGridView1.Rows[ir].Cells[3].Value);

                    // -----------------------------------------------------------------
                    // Получить координаты на карте

                    // grad->rad
                    lt = (lt * Math.PI) / 180;
                    lng = (lng * Math.PI) / 180;

                    // Подаем rad, получаем там же расстояние на карте в м
                    mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

                    GlobalVarLn.X_OB1 = lt;
                    GlobalVarLn.Y_OB1 = lng;
                    // -----------------------------------------------------------------
                    // Заполнение структуры (X,Y,Name,Znak)

                    objLF.X_m = GlobalVarLn.X_OB1;
                    objLF.Y_m = GlobalVarLn.Y_OB1;
                    objLF.sType = s;

                    objLF.indzn = GlobalVarLn.iZOB1;
                    // -----------------------------------------------------------------
                    // H

                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.X_OB1, GlobalVarLn.Y_OB1);
                    GlobalVarLn.H_OB1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    objLF.H_m = GlobalVarLn.H_OB1;
                    dataGridView1.Rows[ir].Cells[2].Value = GlobalVarLn.H_OB1;
                    // -----------------------------------------------------------------
                    // Добавить строку

                    GlobalVarLn.list1_OB1.Add(objLF);
                    // -----------------------------------------------------------------

                    irf += 1;

                } // IF**

            } // FOR
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR

            // --------------------------------------------------------------------
            // Write in file

            srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.list1_OB1.Count));

            for (i_tmp = 0; i_tmp < GlobalVarLn.list1_OB1.Count; i_tmp++)
            {

                srFile.WriteLine("X =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].X_m));
                srFile.WriteLine("Y =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].Y_m));
                srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].H_m));
                srFile.WriteLine("Type =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].sType));
                srFile.WriteLine("indzn =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].indzn));

            }
            // -------------------------------------------------------------------------------------

            srFile.Close();
            // ------------------------------------------------------------------------------------
            axaxcMapScreen.Repaint();
            f_OB1ReDraw();

            // ------------------------------------------------------------------------------------

        } // Save to file
        // ************************************************************************

        // ************************************************************************
        // Обработчик кнопки : read from file
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";

            double number1 = 0;
            int number2 = 0;

            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';
            //char symb6 = 'R';
            //char symb7 = 'A';
            char symb8 = 'T';
            char symb9 = 'i';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;

            int IndZap = 0;
            int TekPoz = 0;

            //double x1 = 0;
            //double y1 = 0;
            //double x2 = 0;
            //double y2 = 0;
            //double dx = 0;
            //double dy = 0;
            //double li = 0;
            int fi = 0;

            // Очистка ---------------------------------------------------------------------------

            // ----------------------------------------------------------------------
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            // -------------------------------------------------------------------
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // -------------------------------------------------------------------
            // Очистка dataGridView1

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "","");
            }
            // -------------------------------------------------------------------
            // Убрать с карты

            GlobalVarLn.axMapScreenGlobal.Repaint();
            // --------------------------------------------------------------------------- Очистка

            // Чтение файла ---------------------------------------------------------
            String strFileName;
            strFileName = "OB1.txt";

            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t open file");
                return;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // N =...
            // X =...
            // Y =...
            // H =...

            try
            {
                LF1 objLF = new LF1();

                TekPoz = 0;
                IndZap = 0;
                // .......................................................
                // N =...
                // 1-я строка

                strLine = srFile.ReadLine();

                if (strLine == null)
                {
                    MessageBox.Show("No information");
                    return;
                }

                indStart = strLine.IndexOf(symb1, TekPoz); // N

                if (indStart == -1)
                {
                    MessageBox.Show("No information");
                    return;
                }

                indStop = strLine.IndexOf(symb4, TekPoz);  //=

                if ((indStop == -1) || (indStop < indStart))
                {
                    MessageBox.Show("No information");
                    return;
                }

                iLength = indStop - indStart + 1;
                // Убираем 'N ='
                strLine1 = strLine.Remove(indStart, iLength);

                if (strLine1 == "")
                {
                    MessageBox.Show("No information");
                    return;
                }

                // Количество 
                number2 = Convert.ToInt32(strLine1);
                GlobalVarLn.iOB1_stat = (uint)number2;
                // .......................................................

                fi = 0;
                strLine = srFile.ReadLine(); // читаем далее (X1)
                if ((strLine == "") || (strLine == null))
                {
                    fi = 1;
                }
                // .......................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "") && (strLine != null))
                {
                    IndZap += 1;

                    // .......................................................
                    // X =...

                    indStart = strLine.IndexOf(symb2, TekPoz); // X
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.X_m = number1;
                    // .......................................................
                    // Y =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb3, TekPoz); // Y
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.Y_m = number1;
                    // .......................................................
                    // H =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb5, TekPoz); // H
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.H_m = number1;
                    // .......................................................
                    // Type =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb8, TekPoz); // T
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);

                    objLF.sType = strLine1;
                    // .......................................................
                    // indzn =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb9, TekPoz); // i
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.indzn = (int)number1;
                    // .......................................................
                    // Занести в List

                    GlobalVarLn.list1_OB1.Add(objLF);
                    // .......................................................
                    // Занести в таблицу
                    var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = p.X.ToString("F3");  // X
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = p.Y.ToString("F3");  // Y
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[3].Value = objLF.sType;  // Type
                    // ............................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (Xi)
                    if ((strLine == "") || (strLine == null))
                        fi = 1;
                    // ...................................................................

                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH


            }
            // -----------------------------------------------------------------------------------
            catch
            {

            }
            // -------------------------------------------------------------------------------------
            srFile.Close();
            // -------------------------------------------------------------------------------------

            f_OB1ReDraw();

        } // Read from file
        // ************************************************************************

        // ************************************************************************
        // Удалить объект
        // ************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
            // -----------------------------------------------------------------------------------------
            String strLine2 = "";
            String strLine3 = "";

            int it = 0;
            int index = 0;
            // -----------------------------------------------------------------------------------------
/*
            if (tbNumSP.Text == "")
            {
                MessageBox.Show("Нет информации");
                return;
            }

            strLine2 = tbNumSP.Text;
 */ 
            // -----------------------------------------------------------------------------------------
            // Если открыта таблица

            if (GlobalVarLn.iOB1_stat != 0)
            {

//             for (it = 0; it < GlobalVarLn.iOB1_stat; it++)
//            {
 //                   if (GlobalVarLn.list1_OB1.Count != GlobalVarLn.iOB1_stat)
 //                   {
 //                       MessageBox.Show("Ошибка ввода");
 //                       return;
 //                   }

             index=dataGridView1.CurrentRow.Index;
             if (index >= GlobalVarLn.list1_OB1.Count)
             {
                 return;
             }

             //strLine3 = Convert.ToString(dataGridView1.Rows[it].Cells[3].Value); // Numb
             //strLine3 = Convert.ToString(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].Value); // Numb

             // if1
             //if (String.Compare(strLine2, strLine3) == 0)
             //{
                 // Убрать с таблицы
                 dataGridView1.Rows.Remove(dataGridView1.Rows[index]);
                 // Del from list
                 GlobalVarLn.list1_OB1.Remove(GlobalVarLn.list1_OB1[index]);
                 GlobalVarLn.iOB1_stat -= 1;
                 // Убрать с карты
                 GlobalVarLn.axMapScreenGlobal.Repaint();

             //} // if1

           //} // For

            } // IF(GlobalVarLn.iSP_stat != 0)
            // -------------------------------------------------------------------------------------

            f_OB1ReDraw();

        } // Удалить
        // ************************************************************************


        // ФУНКЦИИ ********************************************************************************

        // ****************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке OB1
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ****************************************************************************************
        public void f_OB1(
                          double X,
                          double Y
                         )
        {

            // ......................................................................
            //double xtmp_ed, ytmp_ed;
            //double xtmp1_ed, ytmp1_ed;
            //xtmp_ed = 0;
            //ytmp_ed = 0;
            //xtmp1_ed = 0;
            //ytmp1_ed = 0;

            LF1 objLF = new LF1();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.X_OB1 = GlobalVarLn.MapX1;
            GlobalVarLn.Y_OB1 = GlobalVarLn.MapY1;
            objLF.X_m = GlobalVarLn.X_OB1;
            objLF.Y_m = GlobalVarLn.Y_OB1;
            objLF.indzn = GlobalVarLn.iZOB1;
            // ......................................................................
            /*

                        // SP
                        ClassMap.f_DrawSPXY(
                                      GlobalVarLn.XCenter_SP,  // m на местности
                                      GlobalVarLn.YCenter_SP,
                                      ""
                                     );
            */
            // ......................................................................
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.MapX1, GlobalVarLn.MapY1);
            GlobalVarLn.H_OB1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            objLF.H_m = GlobalVarLn.H_OB1;
            // ......................................................................
            GlobalVarLn.iOB1_stat += 1;
            // ......................................................................
            // Добавить строку

            var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[0].Value = p.X.ToString("F3"); // X
            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[1].Value = p.Y.ToString("F3"); // Y
            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[2].Value = (int)GlobalVarLn.H_OB1; // H

            // ......................................................................
            // Добавить в List

            GlobalVarLn.list1_OB1.Add(objLF);
            // ---------------------------------------------------------------------------------
            // Перерисовать

            GlobalVarLn.axMapScreenGlobal.Repaint();

            f_OB1ReDraw();
            // ---------------------------------------------------------------------------------


        } // P/P f_OB1
        // *************************************************************************************

        // *************************************************************************************
        // Перерисовка OB1
        // *************************************************************************************
        public void f_OB1ReDraw()
        {
            ClassMap.f_OB1_stat();

        } // P/P f_OB1ReDraw
        // *************************************************************************************

        // ******************************************************************************** ФУНКЦИИ


        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormOB1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blOB1_stat = false;

            //GlobalVarLn.fFOB1 = 0;
            GlobalVarLn.fl_Open_objFormOB1 = 0;


        } // Closing

        // ****************************************************************************************

        // *************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormOB1_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormOB1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB1 = 1;

            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;

           // 0809_3
           //ClassMap.f_RemoveFrm(2);
            GlobalVarLn.fl_Open_objFormOB1 = 1;


        } // Activated

        // ****************************************************************************************
        // Значок
        // ****************************************************************************************
        private void buttonZOB1_Click(object sender, EventArgs e)
        {
            GlobalVarLn.iZOB1 += 1;
            if (GlobalVarLn.iZOB1 == imageList1.Images.Count)
                GlobalVarLn.iZOB1 = 0;
            pbOB1.Image = imageList1.Images[GlobalVarLn.iZOB1];

        }

        private void tbNumSP_TextChanged(object sender, EventArgs e)
        {

        } // Значок

        private void button2_Click(object sender, EventArgs e)
        {
            ;

        } 
        // ****************************************************************************************


    } // Class
} // Namespace
